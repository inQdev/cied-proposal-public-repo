/* Main javascript file for CIED Proposal System *
 * @version 0.1
 * Author - Agileana
 */

(function($) {

	$(document).scroll(function() {
	    if ($(this).scrollTop() > $('.sticky-top-placeholder').offset().top - 6) {
	        $('.sticky-top-placeholder .well').addClass('sticky-top');
	    } 
	    else {
	        $('.sticky-top-placeholder .well').removeClass('sticky-top');
	    }
	});

})(jQuery); // Fully reference jQuery after this point.
