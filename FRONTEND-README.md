## Theme development

We are using [gulp](http://gulpjs.com/) as the build system and [Bower](http://bower.io/) to manage front-end packages.

### Install gulp and Bower

we need [node.js](http://nodejs.org/download/) in order to use [gulp](http://gulpjs.com) and [Bower](http://bower.io/). It's recommended to update to the latest version of npm: `npm install -g npm@latest`.

From the command line:

1. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
2. Navigate to the /web directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process.

### Available gulp commands

* `gulp` — Compile and optimize the files in the assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp styles` — Compile and optimize only the styles.
* `gulp --production` — Compile assets for production (no source maps).