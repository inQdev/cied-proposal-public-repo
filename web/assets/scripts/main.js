/**
 * Main javascript file for CIED Proposal System
 *
 * @version 0.1
 * @author Felipe Salazar <pipe.salazar@inqbation.com>
 * @author Juan Obando    <juan.obando@inqbation.com>
 */

;(function ($) {
    $('.no-js').remove();

    var setHeight = function() {
        sidebarHeight = $('.sidebar').innerHeight();
        bodyHeight = $('body').innerHeight();
        if (sidebarHeight>bodyHeight) {
            $('main').css('min-height', sidebarHeight);
        } else {
             $('main').css('min-height', 'calc(100% - 236px)');
        }
    };
    setHeight();

    $(window).resize(function() {
        setHeight();
    });

    var shareModalEl = $('#shareModal'),
        peopleListInput = $("#people-list"),
        shareModalErrorMsg = shareModalEl.find("#share-modal-error-message"),
        shareModalSaveBtn  = $('#form_shareProject'),
        whoHasAccessList = $("#who-has-access-list");

        datepickers = {};

        $('.datepicker input').each(function(index) {
            datepickers['picker'+index] = new Pikaday({
                field: this,
                position: 'bottom left',
                format: 'MM/DD/YYYY',
            });
        });

    if (peopleListInput.length > 0) {
        //Disable save button when the modal is called.
        shareModalEl.on('show.bs.modal', function () {
            shareModalSaveBtn.attr("disabled", true);
        });
        shareModalEl.on('shown.bs.modal', function () {
            peopleListInput
                .focus()
                .typeahead({
                    source: embassyRELOUsers,
                    afterSelect: function (user) {
                        // check if user already added as project author
                        if (!$('[data-author-id="' + user.id + '"]').length) {
                            whoHasAccessList.append(
                                $('<li>')
                                    .attr({
                                        'class': 'list-unstyled__author-item',
                                        'data-author-id': user.id,
                                        'data-author-new': true
                                    })
                                    .append($('<div>')
                                        .attr('class', 'row')
                                        .append(
                                            $('<div>')
                                                .attr('class', 'col-md-8')
                                                .append(
                                                    $('<div>')
                                                        .attr('class', 'row list-unstyled__author-item__details')
                                                        .append(
                                                            $('<div>').attr('class', 'col-md-12').text(user.fullname),
                                                            $('<div>').attr('class', 'col-md-12')
                                                                .append($('<small>').text(user.email))
                                                        )
                                                ),
                                            $('<div>')
                                                .attr('class', 'col-md-4 text-center')
                                                .append(
                                                    $('<a>')
                                                        .attr({'class': 'un-share-project'})
                                                        .attr({'href': '#'})
                                                        .text('Un-share')
                                                )
                                        )
                                    )
                            );

                            shareModalEl.find('form').append(
                                $('<input>').attr({'type': 'hidden', 'name': 'form[shareWith][]'}).val(user.id)
                            );
                        }

                        peopleListInput.val('');
                        shareModalErrorMsg.hide();
                        shareModalSaveBtn.attr("disabled", false);
                    }
                })
                .change(function() {
                    var current = $(this).typeahead("getActive");
                    if (current) {
                        if (current.email.toLowerCase() !== $(this).val().toLowerCase()) {
                            shareModalErrorMsg.css('display', 'block');
                        } else {
                            shareModalErrorMsg.hide();
                        }
                    } else {
                        shareModalErrorMsg.css('display', 'block');
                    }
                });
        }).on('hidden.bs.modal', function () {
            // Remove from authors list all users added but that weren't saved.
            whoHasAccessList.find('[data-author-new]').remove();
            shareModalEl.find('form').find('[name^="form[shareWith], [name^="form[unShareFrom]"]').remove();

            peopleListInput.val('');
        }).on('click', '.un-share-project', function () {
            var unShareLinkEl = $(this),
                authorRowEl = unShareLinkEl.closest('.list-unstyled__author-item'),
                userId = authorRowEl.data('author-id'),
                newAuthor = authorRowEl.data('author-new');

            if (authorRowEl.hasClass('list-unstyled__author-item--deleted')) {
                unShareLinkEl.text('Un-share');

                authorRowEl
                    .removeClass('list-unstyled__author-item--deleted')
                    .find('.list-unstyled__author-item__details')
                    .removeClass('list-unstyled__author-item__details--deleted');

                $('[name^="form[unShareFrom]"][value="' + userId + '"]').remove();

                if (newAuthor) {
                    shareModalEl.find('form').append(
                        $('<input>').attr({'type': 'hidden', 'name': 'form[shareWith][]'}).val(userId)
                    );
                }
            }
            else {
                unShareLinkEl.text('Share');

                authorRowEl
                    .addClass('list-unstyled__author-item--deleted')
                    .find('.list-unstyled__author-item__details')
                    .addClass('list-unstyled__author-item__details--deleted');

                $('[name^="form[shareWith]"][value="' + userId + '"]').remove();

                if (typeof newAuthor === 'undefined') {
                    shareModalEl.find('form').append(
                        $('<input>').attr({'type': 'hidden', 'name': 'form[unShareFrom][]'}).val(userId)
                    );
                }
            }
        });
    }
    // Prevent form submission when hitting Enter/Return inside wizards, but keep texarea's behaviour
    $(document).on("keypress", ":input:not(textarea)", function(event) {
        return event.keyCode !== 13;
    });

    // Filter
    (function() {
        var $filter = $('form.filter').add('form.filter-year');

        $filter.on('change', 'input, select', function() {
            $(this).closest('form').submit();
        });
    })();
})(jQuery); // Fully reference jQuery after this point.
