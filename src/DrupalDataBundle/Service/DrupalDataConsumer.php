<?php

namespace DrupalDataBundle\Service;

use ChrisHemmings\OAuth2\Client\Provider\Drupal;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage as TokenStorage;

/**
 * Default controller.
 *
 * @package DrupalDataBundle\Service
 *
 */
class DrupalDataConsumer
{

	protected $api_url;
	protected $provider;
	protected $tokenStorage;

	public function __construct(TokenStorage $tokenStorage, $parameters)
	{
		$this->provider = new Drupal([
			'clientId' => $parameters['client_id'],
			'clientSecret' => $parameters['client_secret'],
			'redirectUri' => $parameters['redirect_url'],
			'baseUrl' => $parameters['base_url'],
			'scope' => $parameters['scope'],
		]);

		$this->api_url = $parameters['api_url'];

		$this->tokenStorage = $tokenStorage;
	}

	/**
	 * @param string $method
	 * @param string $request
	 * @return mixed
	 */
	public function sendRequest($method = 'GET', $request = '/')
	{
		$start = substr($request, 0, 4);
		if ($start != 'http') {
			if (substr($request, 0, 1) != '/') {
				$request = '/' . $request;
			}
			$request_uri = $this->api_url . $request;
		} else {
			$request_uri = $request;
		}

		$token = $this->tokenStorage->getToken();
		$response = null;

		if (!$token->isExpired()) {

			$restRequest = $this->provider->getAuthenticatedRequest($method,
				$request_uri,
				$token->getAccessToken()
			);

//			dump($request_uri); exit;
			$response = $this->provider->getResponse($restRequest);
		} else {
			// Felipe's note: This is not reached since the RoleSwitcherListeners assures the token is valid
		}

		return $response;
	}

}
