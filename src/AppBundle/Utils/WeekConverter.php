<?php

namespace AppBundle\Utils;

/**
 * WeekConverter util.
 *
 * @package AppBundle\Utils
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class WeekConverter
{
    /**
     * Return an array of weeks in a given date range.
     *
     * @param \DateTime $start The date range start date.
     * @param \DateTime $end   The date range end date.
     *
     * @return array Weeks in the given date range.
     */
    public function getWeeksRanges(\DateTime $start, \DateTime $end)
    {
        $weeks = [];

        $w = $end->format('W');
        $y = $end->format('Y');

        // This the actual end date: the last day of the Week (sunday) of the given
        // end date.
        $d = new \DateTime(date('Y-m-d', strtotime("{$y}-W{$w}-7")));

        /** @var \DateTime $date */
        for ($date = clone $start; $date <= $d; $date->add(new \DateInterval('P7D'))) {
            $week = $date->format('W');
            $year = $date->format('Y');

            //Returns the date of monday in week
            $weekStartDate = new \DateTime(
                date('Y-m-d', strtotime("{$year}-W{$week}-1"))
            );
            //Returns the date of sunday in week
            $weekEndDate = new \DateTime(
                date('Y-m-d', strtotime("{$year}-W{$week}-7"))
            );

            $weeks[] = ['start' => $weekStartDate, 'end' => $weekEndDate];
        }

        return $weeks;
    }
}
