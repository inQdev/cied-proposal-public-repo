<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ReloLocationRepository repository.
 *
 * @package AppBundle\Repository
 */
class ReloLocationRepository extends EntityRepository
{
    /**
     * Find all RELO Locations, sorted by their name.
     *
     * @return array
     */
    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }
}
