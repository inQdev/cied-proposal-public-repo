<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectOutcome;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\Region;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * FellowProjectRepository entity repository.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FellowProjectRepository extends EntityRepository
{
    /**
     * Return all Fellow Proposals created by user with Embassy role.
     *
     * @param User $user The User.
     *
     * @return array
     */
    public function findAllByEmbassyUser(User $user)
    {
        $qb = $this->findAllForDashboard();

        return $qb
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->eq('gi.createdBy', ':user'),
                    $qb->expr()->orX(
                        $qb->expr()->eq('gi.submissionStatus', ':submissionStatus'),
                        $qb->expr()->in('gi.projectReviewStatus', ':reviewStatuses')
                    )
                )
            )
            ->setParameters([
                ':user' => $user,
                ':submissionStatus' => ProjectGeneralInfo::PROJECT_NOT_SUBMITTED,
                ':reviewStatuses' => [
                    ProjectReviewStatus::UNDER_POST_RE_REVIEW,
                    ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING,
                ]
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @return array
     */
    public function findAllByRELOUser(User $user)
    {
        $qb = $this->findAllForDashboard();
        $qb2 = $this->findAllForDashboard();

        $fellowProjectsOwnedByUser = $qb
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->eq('gi.createdBy', ':user'),
                    $qb->expr()->eq('gi.submissionStatus', ':submissionStatus')
                )
            )
            ->setParameters([
                ':user' => $user,
                ':submissionStatus' => ProjectGeneralInfo::PROJECT_NOT_SUBMITTED
            ])
            ->getQuery()
            ->getResult();

        $fellowProjectsByRELOLocation = $qb2
            ->andWhere(
                $qb2->expr()->andX(
                    $qb2->expr()->in('rl', ':reloLocations'),
                    $qb2->expr()->eq('gi.projectReviewStatus', ':reviewStatus')
                )
            )
            ->setParameters([
                ':reloLocations' => $user->getReloLocations(),
                ':reviewStatus' => ProjectReviewStatus::UNDER_RELO_REVIEW
            ])
            ->getQuery()
            ->getResult();

        $fellowProjects = array_merge(
            $fellowProjectsOwnedByUser,
            $fellowProjectsByRELOLocation
        );

        return $fellowProjects;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findAllByRPOUser(User $user)
    {
        $qb = $this->findAllForDashboard();
        $qb2 = $this->findAllForDashboard();

        $fellowProjectsOwnedByUser = $qb
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->eq('gi.createdBy', ':user'),
                    $qb->expr()->eq('gi.submissionStatus', ':submissionStatus')
                )
            )
            ->setParameters([
                ':user' => $user,
                ':submissionStatus' => ProjectGeneralInfo::PROJECT_NOT_SUBMITTED
            ])
            ->getQuery()
            ->getResult();

        $fellowProjectsByRegion = $qb2
            ->andWhere(
                $qb2->expr()->andX(
                    $qb2->expr()->in('r', ':regions'),
                    $qb2->expr()->eq('gi.projectReviewStatus', ':reviewStatus')
                )
            )
            ->setParameters([
                ':regions' => $user->getRegions(),
                ':reviewStatus' => ProjectReviewStatus::UNDER_RPO_REVIEW
            ])
            ->getQuery()
            ->getResult();

        $fellowProjects = array_merge(
            $fellowProjectsOwnedByUser,
            $fellowProjectsByRegion
        );

        return $fellowProjects;
    }

	/**
	 * Finds all proposals that are "Under ECA review"
	 * @return array
	 */
	public function findAllUnderECAReview()
	{
		$qb = $this->findAllForDashboard();

		return $qb
			->where($qb->expr()->in('gi.projectReviewStatus', ':reviewStatuses'))
			->setParameters([
				':reviewStatuses' => [ProjectReviewStatus::UNDER_ECA_REVIEW,]
			])
			->getQuery()
			->getResult();
	}

	/**
     * Return the number of Fellow projects associated to a given RELO Location
     * in a given Fellow cycle.
     *
     * @param ReloLocation $reloLocation The RELO Location where the project
     *                                   will take place.
     * @param CycleFellow  $cycleFellow  The Fellow cycle.
     *
     * @return int Number of Fellow projects
     */
    public function countAllByRELOLocationInCycle(ReloLocation $reloLocation, CycleFellow $cycleFellow)
    {
        $reloLocations = new ArrayCollection([$reloLocation]);

        $qb = $this->findAllByRELOLocationsInCycle($reloLocations, $cycleFellow, false);

        if (null === $qb) {
            return 0;
        }

        $x = $qb->getQuery()->getSQL();

        return ((int) $qb->select($qb->expr()->count('fp'))->getQuery()->getSingleScalarResult());
    }

    /**
     * Return the number of Fellow projects associated to a given Regio in a
     * given Fellow cycle.
     *
     * @param Region      $region      The region where the project will take
     *                                 place.
     * @param CycleFellow $cycleFellow The Fellow cycle.
     *
     * @return int Number of Fellow projects.
     */
    public function countAllByRegionInCycle(Region $region, CycleFellow $cycleFellow)
    {
        $regions = new ArrayCollection([$region]);

        $qb = $this->findAllByRegionsInCycle($regions, $cycleFellow, false);

        if (null === $qb) {
            return 0;
        }

        return ((int) $qb->select($qb->expr()->count('fp'))->getQuery()->getSingleScalarResult());
    }

    /**
     * @param CycleFellow $cycleFellow The Fellow cycle.
     * @param bool        $results     Whether if results or QueryBuilder
     *                                 should be returned.
     *
     * @return array|QueryBuilder Return Fellow Proposals approved in given
     *                            Fellow cycle if $results is true. Otherwise,
     *                            return the QueryBuilder.
     */
    public function findAllApprovedInCycle(CycleFellow $cycleFellow, $results = true)
    {
        $outcomes = $this->getEntityManager()->getRepository(ProjectOutcome::class)->findBy(
            ['id' => [
                ProjectOutcome::SELECTED_FOR_ECA_FUNDING,
                ProjectOutcome::ALTERNATE_FUNDING_PROVIDED,
            ]]
        );

        $qb = $this->findAllByOutcomesInCycle($outcomes, $cycleFellow);

        return $results
            ? $qb->getQuery()->getResult()
            : $qb;
    }

    /**
     * Find all Fellow projects associated to a given Outcome and Fellow cycle.
     *
     * @param array       $outcomes    The outcomes.
     * @param CycleFellow $cycleFellow The Fellow Cycle
     *
     * @return QueryBuilder
     */
    private function findAllByOutcomesInCycle(array $outcomes, CycleFellow $cycleFellow)
    {
        $qb = $this->createQueryBuilder('fp');

        return $qb
            ->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
            // Join with countries to sort the result by the country name
            ->innerJoin('pgi.countries', 'c', Join::WITH)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->in('pgi.projectOutcome', ':outcomes'),
                    $qb->expr()->eq('pgi.cycle', ':cycle')
                )
            )
            ->setParameters(
                [
                    'outcomes' => $outcomes,
                    'cycle' => $cycleFellow,
                ]
            )
            ->orderBy('c.name', 'ASC');
    }

    /**
     * Query to retrieve all Proposals in a RELO Location in a given Fellow Cycle.
     *
     * @param Collection  $reloLocations The RELO Locations.
     * @param CycleFellow $cycleFellow   The Fellow Cycle.
     *
     * @return QueryBuilder|null
     */
    public function findAllByRELOLocationsInCycle(
        Collection $reloLocations,
        CycleFellow $cycleFellow,
        $results = true
    ) {
        if ($reloLocations->isEmpty()) {
            return null;
        }

        $qb = $this->findAllInCycle($cycleFellow);

        if (null === $qb) {
            return null;
        }

        $qb = $qb
            ->join('pgi.reloLocations', 'relos', Join::WITH)
            ->andWhere($qb->expr()->in('relos', ':reloLocations'))
            ->setParameter(':reloLocations', $reloLocations)
            ->orderBy('pgi.reloRanking', 'ASC');

        return $results
            ? $qb->getQuery()->getResult()
            : $qb;
    }

	/**
	 * Query to retrieve all RELO Locations Vs. Regions in a given Fellow Cycle.
	 *
	 * @param Collection  $reloLocations The RELO Locations.
	 * @param CycleFellow $cycleFellow   The Fellow Cycle.
	 * @param boolean
	 *
	 * @return QueryBuilder|null
	 */
	public function findRegionsInProjectsByRELOLocationsInCycle(
		Collection $reloLocations,
		CycleFellow $cycleFellow,
		$results = true
	) {
		if ($reloLocations->isEmpty()) {
			return null;
		}

		/** @var QueryBuilder $qb */
		$qb = $this->createQueryBuilder('fp');

		$qb
			->select('relos.id AS reloid', 'relos.name AS reloname')
			->addSelect(['region.id', 'region.name'])
			->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
			->join('pgi.reloLocations', 'relos', Join::WITH)
			->join('pgi.region', 'region', Join::WITH)
			->where($qb->expr()->eq('pgi.cycle', ':fellowCycle'))
			->andWhere($qb->expr()->in('relos', ':reloLocations'))
			->setParameter(':fellowCycle', $cycleFellow)
			->setParameter(':reloLocations', $reloLocations)
			->groupBy('reloid')
			->addGroupBy('region.id')
		;

		return $results
			? $qb->getQuery()->getResult()
			: $qb;
	}


    /**
     * Query to retrieve all Proposals in a RELO Location in a given Fellow Cycle.
     *
     * @param Collection  $regions     The regions.
     * @param CycleFellow $cycleFellow The Fellow Cycle.
     * @param bool        $results     Specify if results should be returned.
     *
     * @return array|QueryBuilder|null
     */
    public function findAllByRegionsInCycle(Collection $regions, CycleFellow $cycleFellow, $results = true)
    {
        if ($regions->isEmpty()) {
            return null;
        }

        $qb = $this->findAllInCycle($cycleFellow, 'rpoRanking');

        if (null === $qb) {
            return null;
        }

        $qb = $qb
          ->andWhere($qb->expr()->in('pgi.region', ':regions'))
          ->setParameter(':regions', $regions)
          ->orderBy('pgi.rpoRanking', 'ASC');

        return $results
          ? $qb->getQuery()->getResult()
          : $qb;
    }

    /**
     *
     */

    /**
     * Returns Fellow Proposals under consideration for being selected in a given region.
     *
     * @param Region $region The region.
     *
     * @return array Fellow proposals under consideration for being selected in the region.
     */
    public function findAllUnderConsiderationByRegion(Region $region)
    {
        $qb = $this->createQueryBuilder('fp');

        return $qb
            ->innerJoin('fp.projectGeneralInfo', 'gi', Join::WITH)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('gi.underConsideration', ':underConsideration'),
                    $qb->expr()->eq('gi.region', ':region')
                )
            )
            ->setParameters(
                [
                    ':underConsideration' => true,
                    ':region'             => $region,
                ]
            )
            ->orderBy('gi.referenceNumber')
            ->getQuery()
            ->getResult();
    }

    /**
     * Return all Fellow proposals created on a given cycle.
     *
     * @param CycleFellow $cycleFellow The Fellow cycle.
     *
     * @return QueryBuilder
     */
    private function findAllInCycle(CycleFellow $cycleFellow, $sortField='reloRanking', $sortOrder='ASC')
    {
        $qb = $this->createQueryBuilder('fp');

        return $qb
            ->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
            ->where($qb->expr()->eq('pgi.cycle', ':fellowCycle'))
	        ->orderBy($sortField, $sortOrder)
            ->setParameter(':fellowCycle', $cycleFellow);
    }

    /**
     * @param CycleFellow $cycleFellow The Fellow cycle.
     *
     * @return QueryBuilder
     */
    private function findAllSubmittedInCycle(CycleFellow $cycleFellow)
    {
        $qb = $this->createQueryBuilder('fp');

        return $qb
          ->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
          ->where(
            $qb->expr()->andX(
              $qb->expr()->eq('pgi.submissionStatus', ':submissionStatus'),
              $qb->expr()->eq('pgi.cycle', ':fellowCycle')
            )
          )
          ->setParameters(
            [
              ':submissionStatus' => ProjectGeneralInfo::PROJECT_SUBMITTED,
              ':fellowCycle'      => $cycleFellow,
            ]
          );
    }

    /**
     * @return QueryBuilder
     */
    private function findAllForDashboard()
    {
        $fields = [
            'fp.id',
            'gi.referenceNumber as reference_number',
            'r.acronym as region_acronym',
            'c.name as country',
            'rl.name as relo_location',
            'h.name as host_instituion',
            'gi.submissionStatus as submission_status',
            'rs.name as review_status'
        ];

        $qb = $this->createQueryBuilder('fp');

        return $qb
            ->select($fields)
            ->innerJoin('fp.projectGeneralInfo', 'gi', Join::WITH)
            ->leftJoin('gi.region', 'r', Join::WITH)
            ->leftJoin('gi.countries', 'c', Join::WITH)
            ->leftJoin('gi.reloLocations', 'rl', Join::WITH)
            ->leftJoin('fp.locationHostInfo', 'lh', Join::WITH)
            ->leftJoin('lh.host', 'h', Join::WITH)
            ->leftJoin('gi.projectReviewStatus', 'rs', Join::WITH)
            ->orderBy('fp.createdAt', 'DESC');
    }
}
