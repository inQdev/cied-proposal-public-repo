<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Specialist\CycleSpecialist;
use Doctrine\ORM\EntityRepository;

/**
 * CycleSpecialistRepository repository.
 *
 * @package AppBundle\Repository
 * @author  Luke Torres <lucas.torres@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CycleSpecialistRepository extends EntityRepository
{
    /**
     * Get current cycle based based on current date.
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentCycle()
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('c');

        $cycleSpecialist = $qb
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->lte('c.openPeriodStartDate', ':now'),
                    $qb->expr()->gte('c.openPeriodEndDate', ':now')
                )
            )
            ->setParameter('now', $now->format('Y-m-d'))
            ->getQuery()
            ->getSingleResult();

        return $cycleSpecialist;
    }

    /**
     * Returns the last known Specialist cycle.
     *
     * Several Specialist cycles may be sync'd already with the Proposal System.
     * However, some might not have set the Open Period range.  Ordering them
     * by that field, will give the last one with that range set.
     *
     * @return CycleSpecialist|null The last known Fellow cycle.  Null otherwise.
     */
    public function findLast()
    {
        $cycleSpecialist = $this->findBy([], ['openPeriodStartDate' => 'DESC'], 1);

        return array_pop($cycleSpecialist);
    }
}
