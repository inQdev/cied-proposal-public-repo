<?php

namespace AppBundle\Repository\Specialist;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

/**
 * Class PartneringOrganizationRepository
 * @package AppBundle\Repository\Specialist
 */
class PartneringOrganizationRepository extends EntityRepository
{
    public function findAllInCountries(Collection $countries)
    {
        $qb = $this->createQueryBuilder('po');

        return $qb
            ->where($qb->expr()->in('po.country', ':countries'))
            ->setParameter(':countries', $countries)
            ->getQuery()
            ->getResult();
    }
}
