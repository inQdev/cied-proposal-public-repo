<?php

namespace AppBundle\Repository\Specialist\Budget;

use AppBundle\Entity\Specialist\CycleSpecialist;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * PhaseRepository entity repository.
 *
 * @package AppBundle\Repository\Specialist\Budget
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PhaseRepository extends EntityRepository
{
    /**
     * @param Collection      $regions         The regions.
     * @param CycleSpecialist $cycleSpecialist The Specialist cycle.
     *
     * @return float Total ECA contribution in a given regions and cycle.
     */
    public function getTotalECAContributionInRegionsInCycle(Collection $regions, CycleSpecialist $cycleSpecialist)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getEntityManager()->getRepository(SpecialistProjectPhase::class)->findAllApprovedInCycle(
            $cycleSpecialist,
            false
        );

        if (null === $qb) {
            return 0.0;
        }

        return (float)$qb
            ->select('SUM(t.ecaTotalContribution)')
            ->innerJoin('spp.budget', 'b', Join::WITH)
            ->innerJoin('b.budgetTotals', 't', Join::WITH)
            ->andWhere($qb->expr()->in('pgi.region', ':regions'))
            ->setParameter(':regions', $regions)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param CycleSpecialist $cycleSpecialist The Fellow cycle.
     *
     * @return float Total ECA contribution in a given cycle.
     */
    public function getTotalECAContributionInCycle(CycleSpecialist $cycleSpecialist)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getEntityManager()->getRepository(SpecialistProjectPhase::class)->findAllApprovedInCycle(
            $cycleSpecialist,
            false
        );

        if (null === $qb) {
            return 0.0;
        }

        return (float)$qb
            ->select('SUM(t.ecaTotalContribution)')
            ->innerJoin('spp.budget', 'b', Join::WITH)
            ->innerJoin('b.budgetTotals', 't', Join::WITH)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
