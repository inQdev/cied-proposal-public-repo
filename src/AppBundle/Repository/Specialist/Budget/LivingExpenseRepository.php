<?php

namespace AppBundle\Repository\Specialist\Budget;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use Doctrine\ORM\Query\Expr\Join;

/**
 * LivingExpenseRepository
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 */
class LivingExpenseRepository extends \Doctrine\ORM\EntityRepository
{
	/**
	 * @param SpecialistProjectPhaseBudget $budget
	 * @param array $cityIds
	 * @return array
	 */
	public function findNotUsedCities($budget, $cityIds) {

		$qb = $this->createQueryBuilder('le');

		$qb
			->join('le.inCountryAssignment', 'ica', Join::WITH)
			->where($qb->expr()->eq('le.phaseBudget', ':budget'))
			->andWhere($qb->expr()->notIn('ica.id', ':cities'))
			->setParameter(':budget', $budget)
			->setParameter(':cities', $cityIds);

		$output = $qb
			->getQuery()
			->getResult();

		return $output;

	}

}
