<?php

namespace AppBundle\Repository\Specialist;

use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectOutcome;
use AppBundle\Entity\ProjectReviewStatus as ReviewStatus;
use AppBundle\Entity\Specialist\CycleSpecialist;
use AppBundle\Entity\User;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Validator\Constraints\Collection;
use Doctrine\ORM\EntityRepository;

/**
 * PhaseRepository entity repository.
 *
 * @package AppBundle\Repository\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PhaseRepository extends EntityRepository
{
    /**
     * Get all Phases created by and shared with a given user.
     *
     * @param User $user The user.
     *
     * @return array Phases created by and shared with user.
     */
    public function findAllByUser(User $user)
    {
        $ownedBy = $this->findAllOwnedByUser($user);
        $sharedWith = $this->findAllSharedWithUser($user);

        return ['owned' => $ownedBy, 'shared' => $sharedWith];

    }

    /**
     * Get all Phases created by a given user.
     *
     * @param User $user The user.
     *
     * @return array Phases created by with user.
     */
    public function findAllOwnedByUser(User $user)
    {
        $qb = $this->createQueryBuilder('spp');

        return $qb
            ->innerJoin('spp.projectGeneralInfo', 'gi', Join::WITH)
            ->andWhere($qb->expr()->eq('gi.createdBy', ':user'))
            ->orderBy('gi.referenceNumber', 'ASC')
            ->setParameter(':user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * Get all Phases shared with a given user.
     *
     * @param User $user The user.
     *
     * @return array Phases shared with user.
     */
    public function findAllSharedWithUser(User $user)
    {
        $qb = $this->createQueryBuilder('spp');

        return $qb
            ->innerJoin('spp.projectGeneralInfo', 'gi', Join::WITH)
            ->leftJoin('gi.authors', 'a', Join::WITH)
            ->andWhere($qb->expr()->eq('a', ':user'))
            ->orderBy('gi.referenceNumber', 'ASC')
            ->setParameter(':user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
	 * @return Collection
	 */
	public function getInCountryAssignmentOptions() {

		$query = $this->getEntityManager()->createQuery(
			'SELECT ICA.id, CONCAT(ICA.city,"(", C.name, ")") AS name
				FROM AppBundle:InCountryAssignment ICA
				JOIN AppBundle:Country C
				ON ICA.country_id = C.id
				WHERE ICA.specialist_project_phase_id = :phase_id'
		)->setParameters([
			'phase_id' => $this->getId(),
		]);

		$items = $query->getResult();

		return $items;
	}

    /**
     * Find all approved Specialist projects associated to a given Specialist
     * cycle.
     *
     * @param CycleSpecialist $cycleSpecialist The Fellow Cycle
     * @param bool            $results         Whether results or QueryBuilder should
     *                                         be returned.
     *
     * @return array|QueryBuilder Return Fellow Proposals approved in given Fellow
     * cycle if $results is true. Otherwise, return the QueryBuilder.
     */
    public function findAllApprovedInCycle(CycleSpecialist $cycleSpecialist, $results = true)
    {
        /** @var ProjectOutcome $outcome */
        $outcome = $this->getEntityManager()->getRepository(ProjectOutcome::class)->find(
            ProjectOutcome::SELECTED_FOR_ECA_FUNDING
        );

        $qb = $this->findAllByOutcomeInCycle($outcome, $cycleSpecialist);

        return $results
            ? $qb->getQuery()->getResult()
            : $qb;
    }

    /**
     * Return all Specialist proposals phases that were created and
     * shared with the user with Embassy role and that have the statuses
     * "Not Submitted", "Under Post re-review" and "Request Alternate
     * Funding".
     *
     * @param User $user The Embassy user.
     *
     * @return array Specialist proposals phases created by user.
     */
    public function findAllByEmbassyUser(User $user)
    {
        $qb = $this->findAllForDashboard();

        $result = $qb
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->eq('gi.createdBy', ':user'),
                        $qb->expr()->eq('a', ':user')
                    ),
                    $qb->expr()->orX(
                        $qb->expr()->eq('gi.submissionStatus', ':submissionStatus'),
                        $qb->expr()->in('gi.projectReviewStatus', ':reviewStatuses')
                    )
                )
            )
            ->setParameters(
                [
                    ':user' => $user,
                    ':submissionStatus' => ProjectGeneralInfo::PROJECT_NOT_SUBMITTED,
                    ':reviewStatuses' => [
                        ReviewStatus::UNDER_POST_RE_REVIEW,
                        ReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING,
                    ],
                ]
            )
            ->getQuery()
            ->getResult();

        return $this->formatDashboardResult($result);
    }

    /**
     * Return all Specialist proposals phases that were created and
     * shared with the user with RELO role, and that belong to the
     * RELO Locations associated to the user.
     *
     * @param User $user The RELO user.
     *
     * @return array Specialist proposals phases created by user.
     */
    public function findAllByRELOUser(User $user)
    {
        $qb = $this->findAllForDashboard();
        $qb2 = $this->findAllForDashboard();

        $phasesOwnedByUser = $qb
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->eq('gi.createdBy', ':user'),
                        $qb->expr()->eq('a', ':user')
                    ),
                    $qb->expr()->eq('gi.submissionStatus', ':submissionStatus')
                )
            )
            ->setParameters(
                [
                    ':user' => $user,
                    ':submissionStatus' => ProjectGeneralInfo::PROJECT_NOT_SUBMITTED,
                ]
            )
            ->getQuery()
            ->getResult();

        $phasesByRELOLocation = $qb2
            ->andWhere(
                $qb2->expr()->andX(
                    $qb2->expr()->in('rl', ':reloLocations'),
                    $qb2->expr()->eq('gi.projectReviewStatus', ':reviewStatus')
                )
            )
            ->setParameters(
                [
                    ':reloLocations' => $user->getReloLocations(),
                    ':reviewStatus' => ReviewStatus::UNDER_RELO_REVIEW,
                ]
            )
            ->getQuery()
            ->getResult();

        $result = array_merge($phasesOwnedByUser, $phasesByRELOLocation);

        return $this->formatDashboardResult($result);
    }

    /**
     * Return all Specialist proposals phases that were created and
     * shared with the user with RPO role, and that belong to the
     * regions associated to the user.
     *
     * @param User $user The RPO user.
     *
     * @return array Specialist proposals phases created by user.
     */
    public function findAllByRPOUser(User $user)
    {
        $qb = $this->findAllForDashboard();
        $qb2 = $this->findAllForDashboard();

        $phasesOwnedByUser = $qb
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->eq('gi.createdBy', ':user'),
                        $qb->expr()->eq('a', ':user')
                    ),
                    $qb->expr()->eq('gi.submissionStatus', ':submissionStatus')
                )
            )
            ->setParameters(
                [
                    ':user' => $user,
                    ':submissionStatus' => ProjectGeneralInfo::PROJECT_NOT_SUBMITTED,
                ]
            )
            ->getQuery()
            ->getResult();

        $phasesByRegion = $qb2
            ->andWhere(
                $qb2->expr()->andX(
                    $qb2->expr()->in('r', ':regions'),
                    $qb2->expr()->eq('gi.projectReviewStatus', ':reviewStatus')
                )
            )
            ->setParameters(
                [
                    ':regions' => $user->getRegions(),
                    ':reviewStatus' => ReviewStatus::UNDER_RPO_REVIEW,
                ]
            )
            ->getQuery()
            ->getResult();

        $result = array_merge($phasesOwnedByUser, $phasesByRegion);

        return $this->formatDashboardResult($result);
    }

    /**
     * Finds all proposals that are "Under ECA review"
     *
     * @return array
     */
    public function findAllUnderECAReview()
    {
        $qb = $this->findAllForDashboard();

        $result = $qb
            ->where($qb->expr()->in('gi.projectReviewStatus', ':reviewStatuses'))
            ->setParameters(
                [':reviewStatuses' => [ReviewStatus::UNDER_ECA_REVIEW]]
            )
            ->getQuery()
            ->getResult();

        return $this->formatDashboardResult($result);
    }

    /**
     * Find all Specialist projects phases associated to a given Review Status
     * and Specialist cycle.
     *
     * @param ReviewStatus    $reviewStatus    The review status.
     * @param CycleSpecialist $cycleSpecialist The Specialist cycle.
     *
     * @return QueryBuilder
     */
    private function findAllByReviewStatusInCycle(ReviewStatus $reviewStatus, CycleSpecialist $cycleSpecialist)
    {
        $qb = $this->createQueryBuilder('spp');

        return $qb
            ->innerJoin('spp.projectGeneralInfo', 'pgi', Join::WITH)
            // Join with countries to sort the result by the country name
            ->innerJoin('pgi.countries', 'c', Join::WITH)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('pgi.projectReviewStatus', ':reviewStatus'),
                    $qb->expr()->eq('pgi.cycle', ':cycle')
                )
            )
            ->setParameters(
                [
                    'reviewStatus' => $reviewStatus,
                    'cycle' => $cycleSpecialist,
                ]
            )
            ->orderBy('c.name', 'ASC');
    }

    /**
     * Find all Specialist projects associated to a given Outcome and
     * Specialist cycle.
     *
     * @param ProjectOutcome  $outcome         The project outcome.
     * @param CycleSpecialist $cycleSpecialist The Specialist cycle.
     *
     * @return QueryBuilder
     */
    private function findAllByOutcomeInCycle(ProjectOutcome $outcome, CycleSpecialist $cycleSpecialist)
    {
        $qb = $this->createQueryBuilder('spp');

        return $qb
            ->innerJoin('spp.projectGeneralInfo', 'pgi', Join::WITH)
            // Join with countries to sort the result by the country name
            ->innerJoin('pgi.countries', 'c', Join::WITH)
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('pgi.projectOutcome', ':outcome'),
                    $qb->expr()->eq('pgi.cycle', ':cycle')
                )
            )
            ->setParameters(
                [
                    'outcome' => $outcome,
                    'cycle' => $cycleSpecialist,
                ]
            )
            ->orderBy('c.name', 'ASC');
    }

    /**
     * QueryFields with fields needed for the different roles dashboards.
     *
     * @return QueryBuilder
     */
    private function findAllForDashboard()
    {
        $fields = [
            'spp.id',
            'gi.referenceNumber as reference_number',
            'spp.title',
            'spp.type',
            'r.acronym as region_acronym',
            'c.name as country',
            'rl.name as relo_location',
            'gi.submissionStatus as submission_status',
            'rs.name as review_status'
        ];

        $qb = $this->createQueryBuilder('spp');

        return $qb
            ->select($fields)
            ->innerJoin('spp.projectGeneralInfo', 'gi', Join::WITH)
            ->leftJoin('gi.authors', 'a', Join::WITH)
            ->leftJoin('gi.region', 'r', Join::WITH)
            ->leftJoin('gi.countries', 'c', Join::WITH)
            ->leftJoin('gi.reloLocations', 'rl', Join::WITH)
            ->leftJoin('gi.projectReviewStatus', 'rs', Join::WITH)
            ->orderBy('spp.createdAt', 'DESC');
    }

    /**
     * @param $result
     *
     * @return array
     */
    private function formatDashboardResult($result)
    {
        $phases = [];

        foreach ($result as $phase) {
            $phaseId = $phase['id'];

            if (array_key_exists($phaseId, $phases)) {
                if (!in_array($phase['country'], $phases[$phaseId]['countries'])) {
                    $phases[$phaseId]['countries'][] = $phase['country'];
                }

                if (!in_array($phase['relo_location'], $phases[$phaseId]['relo_locations'])) {
                    $phases[$phaseId]['relo_locations'][] = $phase['relo_location'];
                }
            } else {
                $phases[$phaseId] = $phase;

                unset($phases[$phaseId]['country'], $phases[$phaseId]['relo_location']);

                $phases[$phaseId]['countries'][] = $phase['country'];
                $phases[$phaseId]['relo_locations'][] = $phase['relo_location'];
            }
        }

        return array_values($phases);
    }
}
