<?php

namespace AppBundle\Repository\Specialist;

use AppBundle\Entity\GlobalValueSpecialist;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;

/**
 * GlobalPAARepository repository.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class GlobalValueSpecialistRepository extends EntityRepository
{
	/**
	 * @param string $pattern - optional - allows to find values matching name
	 * @return mixed
	 */
	public function findVirtualFlexible($pattern=null) {

		$parameters = [];

		$qb = $this->createQueryBuilder('gv');
		$qb
			->where('gv.periodicity > 0')
			->andWhere('gv.specialistType = :type');

		$parameters[':type'] = SpecialistProjectPhase::VIRTUAL_TYPE;

		if ($pattern != null) {
			$qb->andWhere("gv.name LIKE :pattern");

			$parameters[':pattern'] = $pattern;
		}

		$qb->setParameters($parameters);

		return $qb->getQuery()
			->getResult();

	}

}
