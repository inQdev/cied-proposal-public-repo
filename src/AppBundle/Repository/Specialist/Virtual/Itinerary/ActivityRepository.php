<?php

namespace AppBundle\Repository\Specialist\Virtual\Itinerary;

use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Virtual ActivityRepository repository
 *
 * @package AppBundle\Repository
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class ActivityRepository extends EntityRepository
{
	/**
	 * Gets virtual activities associated to a project phase
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @return array
	 */
	public function findInPhase(SpecialistProjectPhase $projectPhase) {
		$qb = $this->createQueryBuilder('via');

		$query = $qb
			->join('via.weeks', 'viaw', Join::WITH)
			->join('viaw.week', 'viw', Join::WITH)
			->where(
				$qb->expr()->eq('viw.projectPhase', ':phase')
			)
			->setParameter(':phase', $projectPhase)
			->getQuery();

		return $query->getResult();

	}

	/**
	 * Gets the Activities in a ProjectPhase and calculates the total duration
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @return int
	 */
	public function getPhaseDuration(SpecialistProjectPhase $projectPhase) {
		$output = 0;

		if ($projectPhase->getId() != null) {
			$activities = $this->findInPhase($projectPhase);
			if ($activities !== null) {
				foreach ($activities as $activity) {
					$output += $activity->getTotalDuration();
				}
			}
		}

		return $output;
	}

}
