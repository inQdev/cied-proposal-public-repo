<?php

namespace AppBundle\Repository\Specialist\Itinerary;

use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * RestRepository repository.
 *
 * @package AppBundle\Repository\Specialist\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RestRepository extends EntityRepository
{
    /**
     * Finds all Rest days associated to Specialist Project phase.
     *
     * @param SpecialistProjectPhase $projectPhase The Specialist project phase
     *                                             entity.
     *
     * @return array The entities or NULL if non found.
     */
    public function findAllInItinerary(SpecialistProjectPhase $projectPhase)
    {
        $qb = $this->createQueryBuilder('r');

        return $qb
          ->join('r.inCountryAssignment', 'ica', Join::WITH)
          ->join('ica.country', 'c', Join::WITH)
          ->where(
            $qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase')
          )
          ->setParameter(':projectPhase', $projectPhase)
          ->orderBy('r.date', 'ASC')
          ->addOrderBy('c.name', 'ASC')
          ->addOrderBy('ica.city', 'ASC')
          ->getQuery()
          ->getResult();
    }

	/**
	 * Finds the rest records in a date range, considering if those in start or end date should be retrieved
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 * @param bool $atEnd
	 * @return mixed
	 */
	public function findAllByDates(SpecialistProjectPhase $projectPhase, $startDate, $endDate=null, $atEnd=false)
	{
		$sdfn = 'gte';
		$edfn = 'lt';

		$qb = $this->createQueryBuilder('r');

		$qb
			->join('r.inCountryAssignment', 'ica', Join::WITH)
			->where($qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase'))
			->setParameter(':projectPhase', $projectPhase);

		if (!$endDate) {
			$qb
				->andWhere($qb->expr()->eq('r.date', ':startDate'));
		} else {
			if ($atEnd) {
				$sdfn = 'gt';
				$edfn = 'lte';
			}

			$qb
				->andWhere($qb->expr()->$sdfn('r.date', ':startDate'))
				->andWhere($qb->expr()->$edfn('r.date', ':endDate'))
				->setParameter(':endDate', $endDate);
		}

		$qb
			->setParameter(':startDate', $startDate);

		$output = $qb
			->getQuery()
			->getResult();

		return $output;
	}
}
