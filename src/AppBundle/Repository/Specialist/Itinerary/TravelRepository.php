<?php

namespace AppBundle\Repository\Specialist\Itinerary;

use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * TravelRepository repository.
 *
 * @package AppBundle\Repository\Specialist\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class TravelRepository extends EntityRepository
{
    /**
     * Finds all Travels associated to Specialist Project phase.
     *
     * @param SpecialistProjectPhase $projectPhase The Specialist project phase
     *                                             entity.
     *
     * @return array The entities or NULL if non found.
     */
    public function findAllInItinerary(SpecialistProjectPhase $projectPhase)
    {
        $qb = $this->createQueryBuilder('it');

        return $qb
          ->join('it.origin', 'ica', Join::WITH)
          ->join('ica.country', 'c', Join::WITH)
          ->where(
            $qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase')
          )
          ->setParameter(':projectPhase', $projectPhase)
          ->orderBy('it.date', 'ASC')
          ->addOrderBy('c.name', 'ASC')
          ->addOrderBy('ica.city', 'ASC')
          ->getQuery()
          ->getResult();
    }

	/**
	 * Finds the travels in a date range, considering if those in start or end date should be retrieved
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 * @param bool $atEnd
	 * @return mixed
	 */
	public function findAllByDates(SpecialistProjectPhase $projectPhase, $startDate, $endDate=null, $atEnd=false)
	{
		$sdfn = 'gte';
		$edfn = 'lt';

		$qb = $this->createQueryBuilder('it');

		$qb
			->join('it.origin', 'ica', Join::WITH)
			->join('ica.country', 'c', Join::WITH)
			->where($qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase'))
			->setParameter(':projectPhase', $projectPhase);

		if (!$endDate) {
			$qb
				->andWhere($qb->expr()->eq('it.date', ':startDate'));
		} else {
			if ($atEnd) {
				$sdfn = 'gt';
				$edfn = 'lte';
			}

			$qb
				->andWhere($qb->expr()->$sdfn('it.date', ':startDate'))
				->andWhere($qb->expr()->$edfn('it.date', ':endDate'))
				->setParameter(':endDate', $endDate);
		}

		$qb
			->setParameter(':startDate', $startDate);

		$output = $qb
			->getQuery()
			->getResult();

		return $output;
	}
}
