<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Country;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;

/**
 * InCountryAssignmentRepository repository
 *
 * @package AppBundle\Repository
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class InCountryAssignmentRepository extends EntityRepository
{
	/**
	 * Finds In Country Assignments associated to a specific country in the current phase
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @param Country $country
	 *
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function findInCountryAssignmentsByCountry(SpecialistProjectPhase $projectPhase, Country $country)
	{
		$qb = $this->createQueryBuilder('ica');

		$inCountryAssignments = $qb
			->where($qb->expr()->eq('ica.country', ':country'))
			->andWhere($qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase'))
			->setParameter(':country', $country)
			->setParameter(':projectPhase', $projectPhase)
			->orderBy('ica.createdAt', 'ASC')
			->getQuery()
			->getResult();

		return $inCountryAssignments;
	}
}
