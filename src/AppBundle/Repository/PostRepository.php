<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PostRepository repository.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PostRepository extends EntityRepository
{
    /**
     * Find all Posts, sorted by their name.
     *
     * @return array Posts sorted by name.
     */
    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }
}
