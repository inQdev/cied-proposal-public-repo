<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Fellow\CycleFellow;
use Doctrine\ORM\EntityRepository;

/**
 * CycleFellowRepository entity.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CycleFellowRepository extends EntityRepository
{
    /**
     * Returns current Fellow cycle.
     *
     * @return CycleFellow
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentCycle()
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('c');

        $cycleFellow = $qb
          ->where(
            $qb->expr()->andX(
              $qb->expr()->lte('c.openPeriodStartDate', ':now'),
              $qb->expr()->gte('c.openPeriodEndDate', ':now')
            )
          )
          ->setParameter('now', $now->format('Y-m-d'))
          ->getQuery()
          ->getSingleResult();

        return $cycleFellow;
    }

    /**
     * Returns the last known Fellow cycle.
     *
     * Several Fellow cycles may be sync'd already with the Proposal System.
     * However, some might not have set the Open Period range.  Ordering them
     * by that field, will give the last one with that range set.
     *
     * @return CycleFellow|null The last known Fellow cycle.  Null otherwise.
     */
    public function findLast()
    {
        $cycleFellow = $this->findBy(
          [],
          ['openPeriodStartDate' => 'DESC'],
          1
        );

        return array_pop($cycleFellow);
    }
}
