<?php

namespace AppBundle\Repository;

use AppBundle\Entity\GlobalPAA;
use Doctrine\ORM\EntityRepository;

/**
 * GlobalPAARepository repository.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class GlobalPAARepository extends EntityRepository
{
    /**
     * Return the GlobalPAA based on the length of the assignment.
     *
     * @param int $numberOfDays Number of days of the assignment.
     *
     * @return GlobalPAA The GlobalPAA entity.
     */
    public function findByLengthOfAssignment($numberOfDays)
    {
        $qb = $this->createQueryBuilder('gpaa');
	    $qb->setParameter(':n', $numberOfDays);

        $result = $qb
            ->where($qb->expr()->between(':n', 'gpaa.rangeStart', 'gpaa.rangeEnd'))
            ->getQuery()
	        ->getOneOrNullResult();

        if (!$result) {
	        $result = $qb
		        ->where($qb->expr()->gte(':n', 'gpaa.rangeStart'))
		        ->andWhere($qb->expr()->isNull('gpaa.rangeEnd'))
//		        ->setParameter(':n', $numberOfDays)
		        ->getQuery()
		        ->getOneOrNullResult();
        }

        return $result;
    }
}
