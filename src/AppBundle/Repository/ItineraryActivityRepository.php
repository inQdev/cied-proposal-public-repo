<?php

namespace AppBundle\Repository;

use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * ItineraryActivityRepository repository
 *
 * @package AppBundle\Repository
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class ItineraryActivityRepository extends EntityRepository
{
	/**
	 * Finds In Country Assignments associated to a specific country in the current phase
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @param string
	 * @param string
	 *
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function findAllInItinerary(SpecialistProjectPhase $projectPhase, $orderBy='activityDate', $order="ASC")
	{
		$qb = $this->createQueryBuilder('ia');

		$itineraryActivities = $qb
			->join('ia.inCountryAssignment', 'ica', Join::WITH)
			->join('ica.country', 'c', Join::WITH)
			->where($qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase'))
			->setParameter(':projectPhase', $projectPhase)
			->orderBy('ia.'.$orderBy, $order)
			->addOrderBy('c.name', 'ASC')
			->addOrderBy('ica.city', 'ASC')
			->getQuery()
			->getResult();

		return $itineraryActivities;
	}


	/**
	 * Finds the activities in a date range, considering if those in start or end date should be retrieved
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 * @param bool $atEnd
	 * @return mixed
	 */
	public function findAllByDates(SpecialistProjectPhase $projectPhase, $startDate, $endDate=null, $atEnd=false)
	{
		$sdfn = 'gte';
		$edfn = 'lt';

		$qb = $this->createQueryBuilder('ia');
//			->select('ia.id');

		$qb
			->join('ia.inCountryAssignment', 'ica', Join::WITH)
			->where($qb->expr()->eq('ica.specialistProjectPhase', ':projectPhase'))
			->setParameter(':projectPhase', $projectPhase);

		if (!$endDate) {
			$qb
				->andWhere($qb->expr()->eq('ia.activityDate', ':startDate'));
		} else {
			if ($atEnd) {
				$sdfn = 'gt';
				$edfn = 'lte';
			}

			$qb
				->andWhere($qb->expr()->$sdfn('ia.activityDate', ':startDate'))
				->andWhere($qb->expr()->$edfn('ia.activityDate', ':endDate'))
				->setParameter(':endDate', $endDate);
		}

		$qb
			->setParameter(':startDate', $startDate);

		$output = $qb
			->getQuery()
			->getResult();

		return $output;
	}

}
