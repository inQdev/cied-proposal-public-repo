<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * HostRepository repository.
 *
 * @package AppBundle\Repository
 */
class HostRepository extends EntityRepository
{
    /**
     * Finds Host, sorted by their name and counting the projects associated
     *
     * @return array
     */
    public function findAllWithProjects()
    {
    	$fields = [
    		'h.id', 'h.name', 'h.createdAt',
		    'c.name AS cname',
		    'count(lhi.id) AS projects'
	    ];

    	$qb = $this->createQueryBuilder('h');

    	$qb->select($fields)
		    ->leftJoin('h.locationHostInfo', 'lhi', Join::WITH)
		    ->join('h.country', 'c', Join::WITH)
		    ->groupBy('h.id')
		    ->orderBy('h.name', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
