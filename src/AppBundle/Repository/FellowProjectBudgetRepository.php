<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\FellowProject;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * FellowProjectBudgetRepository entity repository.
 *
 * @package AppBundle\Repository
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FellowProjectBudgetRepository extends EntityRepository
{
    /**
     * @param Collection  $regions
     * @param CycleFellow $cycleFellow
     *
     * @return float Total ECA contribution in a given regions and cycle.
     */
    public function getTotalECAContributionInRegionsInCycle(Collection $regions, CycleFellow $cycleFellow)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getEntityManager()->getRepository(FellowProject::class)->findAllApprovedInCycle($cycleFellow, false);

        if (null === $qb) {
            return 0.0;
        }

        return (float) $qb
//          ->select('SUM(t.ecaTotalContribution)')
          ->select('SUM(t.eca_total_contribution)')
          ->innerJoin('fp.fellowProjectBudget', 'b', Join::WITH)
          ->innerJoin('b.revisions', 'r', Join::WITH)
          ->innerJoin('r.fellowProjectBudgetTotals', 't', Join::WITH)
          ->andWhere($qb->expr()->in('pgi.region', ':regions'))
          ->setParameter(':regions', $regions)
          ->getQuery()
          ->getSingleScalarResult();
    }
    /**
     * @param CycleFellow $cycleFellow The Fellow cycle.
     *
     * @return float Total ECA contribution in a given cycle.
     */
    public function getTotalECAContributionInCycle(CycleFellow $cycleFellow)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getEntityManager()->getRepository(FellowProject::class)->findAllApprovedInCycle($cycleFellow, false);

        if (null === $qb) {
            return 0.0;
        }

        return (float) $qb
          ->select('SUM(t.eca_total_contribution)')
          ->innerJoin('fp.fellowProjectBudget', 'b', Join::WITH)
          ->innerJoin('b.revisions', 'r', Join::WITH)
          ->innerJoin('r.fellowProjectBudgetTotals', 't', Join::WITH)
          ->getQuery()
          ->getSingleScalarResult();
    }
}
