<?php
/**
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

namespace AppBundle\Helper;


use AppBundle\Entity\ItineraryIndex;
use AppBundle\Entity\Specialist\Itinerary\Rest;
use AppBundle\Entity\Specialist\Itinerary\Travel;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\Common\BaseContributionItem;
use AppBundle\Entity\SpecialistProjectPhaseBudget;

class ControllerHelper
{
	protected $em;

	public function __construct($em)
	{
		$this->em = $em;
	}


	/**
	 * TODO - Felipe: Remove this methods, seems not needed anymore since ItineraryIndexRepository is holding the functionality
	 * Helper function to define what city is taken for calculations, extracted from SpecialistBudgetController
	 * @param SpecialistProjectPhase $specialistProjectPhase
	 * @return array
	 */
	public function livingExpensesForCalculations($specialistProjectPhase)
	{
		$em =  $this->em;

		$livingExpenses = [
			'rows' => [],
			'stats' => [],
		];

		if ($specialistProjectPhase->getStartDate() && $specialistProjectPhase->getEndDate()) {
			// Gets the dates range for phase and gets the activities
			$dates = $this->dateRange($specialistProjectPhase->getStartDate(), $specialistProjectPhase->getEndDate());

//			$itineraryIndexes = $specialistProjectPhase->getItineraryIndexes();
			$activities = $em->getRepository('AppBundle:ItineraryActivity')->findAllInItinerary($specialistProjectPhase);
			$travels = $em->getRepository('AppBundle\Entity\Specialist\Itinerary\Travel')->findAllInItinerary($specialistProjectPhase);
			$restDates = $em->getRepository('AppBundle\Entity\Specialist\Itinerary\Rest')->findAllInItinerary($specialistProjectPhase);

			$livingExpenses['stats']['total'] = 0;

			/** @var \DateTime $date */
			foreach ($dates as $date) {
				$idx = $date->getTimestamp();
				$livingExpenses['rows'][$idx] = null;

				$switch = 0;
				/** @var Rest $rest */
				while (!$switch && $rest = current($restDates)) {
					if ($rest->getDate() == $date) {
						$livingExpenses['rows'][$idx]['city'] = $rest->getInCountryAssignment();

						$count_idx = $rest->getInCountryAssignment()->getId();
						$livingExpenses['stats']['count'][$count_idx] = (!empty($livingExpenses['stats']['count'][$count_idx]) ? $livingExpenses['stats']['count'][$count_idx] + 1 : 1);
						array_shift($restDates);
					} else {
						$switch = 1;
					}
				}

				$switch = 0;
				/** @var Travel $travel */
				while (!$switch && $travel = current($travels)) {
					if ($travel->getDate() == $date) {
						// Just takes the first travel found to define the city to be use for costs
						if (!$livingExpenses['rows'][$idx]) {
							$livingExpenses['rows'][$idx]['city'] = $travel->getDestination();
							$count_idx = $travel->getDestination()->getId();
							$livingExpenses['stats']['count'][$count_idx] = (!empty($livingExpenses['stats']['count'][$count_idx]) ? $livingExpenses['stats']['count'][$count_idx] + 1 : 1);
						}
						array_shift($travels);
					} else {
						$switch = 1;
					}
				}

				$switch = 0;
				/** @var ItineraryActivity $activity */
				while (!$switch && $activity = current($activities)) {
					if ($activity->getActivityDate() == $date) {
						// discards any city if one has been previously defined
						if (!$livingExpenses['rows'][$idx]) {
							$livingExpenses['rows'][$idx]['city'] = $activity->getInCountryAssignment();

							$count_idx = $activity->getInCountryAssignment()->getId();
							$livingExpenses['stats']['count'][$count_idx] = (!empty($livingExpenses['stats']['count'][$count_idx]) ? $livingExpenses['stats']['count'][$count_idx] + 1 : 1);
						}
						array_shift($activities);
					} else {
						$switch = 1;
					}
				}

				if ($livingExpenses['rows'][$idx]['city']) {
					$costItems = $livingExpenses['rows'][$idx]['city']->getLivingExpenseEstimates()->first()->getLivingExpenseCostItems();

					$livingExpenses['rows'][$idx]['total'] = 0;
					foreach ($costItems as $item) {
						$livingExpenses['rows'][$idx][$item->getCostItem()->getDescription()] = $item->getCostPerDay();
						$livingExpenses['rows'][$idx]['total'] += $item->getCostPerDay();
					}

					$livingExpenses['stats']['total'] += $livingExpenses['rows'][$idx]['total'];
				}

			}
		}

		return $livingExpenses;
	}

	/**
	 * Helper function to create a data range array
	 *
	 * @param \DateTime $first
	 * @param \DateTime $last
	 * @param boolean $includeLast
	 * @param boolean $excludeFirst
	 * @return array
	 */
	public function dateRange($first, $last, $includeLast = true, $excludeFirst = false ) {
		if ($includeLast) {
			$last = clone $last;
			$last->modify('+1 day');
		}

		if ($excludeFirst) {
			$first = clone $first;
			$first->modify('+1 day');
		}

		$periodInterval = new \DateInterval("P1D");
		$dates = new \DatePeriod($first, $periodInterval, $last);

		return $dates;
	}

	/**
	 * @param mixed $contributions
	 * @return mixed
	 */
	public function getErrorsInRows($contributions) {

		$errors = [
			'rows' => [],
			'costs' => 0,
			'totals' => 0,
		];

		foreach ($contributions as $item) {
			$result = $item->checkTotals();
			if ($result === BaseContributionItem::ROW_ERROR_NO_COST) {
				$errors['rows'][] = $item->getId();
				$errors['costs']++;
			} else if ($result === BaseContributionItem::ROW_ERROR_NEGATIVE_TOTAL) {
				$errors['rows'][] = $item->getId();
				$errors['totals']++;
			}

		}

		return $errors;
	}

	/**
	 * @param SpecialistProjectPhase $projectPhase
	 * @return mixed
	 */
	function formatDailyCosts($projectPhase)
	{
		$em = $this->em;
		$budget = $projectPhase->getBudget();

		$dailyGlobals = $em->getRepository('AppBundle:GlobalValueSpecialist')->findBy([
			'periodicity' => 1,
		]);

		// days will be 0 or higher
		$days = $projectPhase->getInCountryDuration();

		$dailyCosts = [];

		if ($budget->getBudgetTotals() && $budget->getBudgetTotals()->count()) {
			/** @var BudgetTotal $budgetTotals */
			$budgetTotals = $budget->getBudgetTotals()->first();

			/** @var GlobalValue $dailyGlobal */
			foreach ($dailyGlobals as $dailyGlobal) {
				switch ($dailyGlobal->getRules()) {
					case 'pre-work':
						if ($projectPhase->getType() == SpecialistProjectPhase::IN_COUNTRY_TYPE) {

							if ($projectPhase->getPrePostWork() != null) {
								$pwDays = $projectPhase->getPrePostWork()->getAdditionalPreWorkDays();
							}
							// to fix some cases where previous condition is met but variable is set as null
							if (!$pwDays) {
								$pwDays = 0;
							}

//							if ($budgetTotals->getPrePostWork() > 0) {
								$dailyCosts[$dailyGlobal->getName()] = [
									$pwDays,
									$dailyGlobal->getDefaultValue(),
									$dailyGlobal->getDefaultValue() * $projectPhase->getPrePostWork()->getAdditionalPreWorkDays(),
								];
//							}
						}
						break;

					case 'post-work':
						if ($projectPhase->getType() == SpecialistProjectPhase::IN_COUNTRY_TYPE) {

							if ($projectPhase->getPrePostWork() != null) {
								$pwDays = $projectPhase->getPrePostWork()->getAdditionalPostWorkDays();
							}
							// to fix some cases where previous condition is met but variable is set as null
							if (!$pwDays) {
								$pwDays = 0;
							}

//							if ($budgetTotals->getPrePostWork() && $projectPhase->getPrePostWork()->getAdditionalPostWorkDays()) {
								$dailyCosts[$dailyGlobal->getName()] = [
									$pwDays,
									$dailyGlobal->getDefaultValue(),
									$dailyGlobal->getDefaultValue() * $projectPhase->getPrePostWork()->getAdditionalPostWorkDays(),
								];
//							}
						}
						break;

					default:
						$dailyCosts[$dailyGlobal->getName()] = [
							$days,
							$dailyGlobal->getDefaultValue(),
							$budgetTotals->getDailyGlobal()
						];
						break;
				}
			}
		}

		return $dailyCosts;
	}


	public function formatVirtualFlexibleCosts(SpecialistProjectPhase $phase) {
		$em = $this->em;

		$output = [
			'hours' => $em->getRepository('AppBundle:Specialist\Virtual\Itinerary\Activity')
			->getPhaseDuration($phase),
			'item' => null,
			'total' => 0,
		];
		$globals = $em->getRepository('AppBundle:GlobalValueSpecialist')->findVirtualFlexible('Stipend%');

		if(count($globals)) {
			$output['item'] = $globals[0];
			$multiplier = $output['hours'] / $globals[0]->getPeriodicity();
			$output['total'] = ceil($globals[0]->getDefaultValue() * $multiplier); // roundup to avoid decimals
		}

		return $output;

	}
}