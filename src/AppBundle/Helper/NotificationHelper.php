<?php
/**
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

namespace AppBundle\Helper;

use AppBundle\Entity\FellowProject;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\Region;
//use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\User;
//use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage as TokenStorage;

class NotificationHelper
{
	protected $em;
	protected $mailer;
	protected $templating;
	protected $session;
	protected $sendEmails;
	protected $ecaEmailAddress;
	protected $bccEmailAddress;

	private $recipients = [];
	private $subject = '';
	private $emailBody;
	private $cc = null;
	private $bcc = null;

	public function __construct($em, $mailer, $templating, $session, $eca_email_address, $bcc_email_address, $send_emails)
	{
		$this->em = $em;
		$this->mailer = $mailer;
		$this->templating = $templating;
		$this->session = $session;
		$this->ecaEmailAddress = $eca_email_address;
		$this->bccEmailAddress = $bcc_email_address;
		$this->sendEmails = $send_emails;
//		$this-> = ;
	}

	public function setSendEmails($sendEmails) {
		$this->sendEmails = $sendEmails;
	}


	/**
	 * Prepares and send notifications originated from ranking pages (RPO/RELO)
	 * @param User $user
	 * @param string $action
	 * @param optional CycleFellow $cycleFellow
	 *
	 * @return null|array
	 */
	public function sendFellowRankingNotifications(User $user, $action, CycleFellow $cycleFellow=null)
	{
//		$em = $this->em;
		$this->subject = 'EL Fellow proposal - ';
		$this->recipients = [];
		$parameters = [
			'user' => $user,
			'sendMessage' => $this->sendEmails
		];

		switch ($action) {

			case 'notify-eca':
				$parameters['region'] = $user->getRegions()->first();
				$this->subject .= 'Proposals under '. $parameters['region']->getName() .' were ranked by an RPO';
				$template = 'notify_eca';
				$parameters['recipients'] = $this->recipients = [$this->ecaEmailAddress];
				break;

			case 'notify-rpo':

				$reloRegions = $this->em->getRepository(FellowProject::class)->findRegionsInProjectsByRELOLocationsInCycle(
					$user->getReloLocations(),
					$cycleFellow
				);
				$template = 'notify_rpo';
				$output = [];

				foreach ($reloRegions as $item) {
					/** @var Region $region */
					$region = $this->em->getRepository(Region::class)->find($item['id']);
					$rpos = $region->getRpos();

					$this->recipients = [];
					foreach ($rpos as $rpo) {
						$this->recipients[] = $rpo->getEmail();
						$output[] = $rpo->getEmail();
					}

					$this->subject .= 'Proposals under '. $item['reloname'] .' were ranked by a RELO';
					$parameters['recipients'] = $this->recipients;
					$parameters['reloLocation'] = $item['reloname'];

					$this->emailBody = $this->templating->render('email/fellowranking/'. $template .'.html.twig', $parameters);
					$this->sendNotification();
				}

				return $output; // exits the function since delivery takes place in loop
		}

		// Renders the content of the email notification to be sent.
		$this->emailBody = $this->templating->render('email/fellowranking/'. $template .'.html.twig', $parameters);
		$this->sendNotification();

		return true;

	}


	/**
	 * @param User $user
	 * @param Region $region
	 */
	public function shareCartNotification(User $user, Region $region=null)
	{
//		$template = 'notify_eca';
		$this->subject = 'EL Fellow proposal – '. $region->getName() .' Shopping cart is ready for review';
		$this->recipients = [$this->ecaEmailAddress];
		$parameters = [
			'user' => $user,
			'sendMessage' => $this->sendEmails,
			'region' => $region,
			'recipients' => $this->recipients
		];

		$this->emailBody = $this->templating->render('email/shoppingcart/notify_eca.html.twig', $parameters);
		$this->sendNotification();

	}

		/**
	 * Sends the actual notification, emailBody, recipients and subject are defined from other methods in the class, but
	 * they can be overridden when passed as parameters to the function
	 *
	 * @param optional null $emailBody
	 * @param optional array $recipients
	 * @param optional string $subject
	 * @param optional array $cc
	 * @param optional array $bcc
	 */
	public function sendNotification($emailBody=null, $recipients=[], $subject='', $cc=[], $bcc=[]) {

		// if parameters are passed (allowing override)
		if ($emailBody) {
			$this->emailBody = $emailBody;
		}
		if (count($recipients)) {
			$this->recipients = $recipients;
		}
		if ($subject) {
			$this->subject = $subject;
		}
		if (count($cc)) {
			$this->cc = $cc;
		}
		if (count($bcc)) {
			$this->bcc = $bcc;
		}

		// First, check if emails are allowed to sent in this instance.  Then,
		// check if there recipients for this email.
		if ($this->sendEmails && count($this->recipients)) {
			if ($this->sendEmails) {
//				foreach ($this->recipients as $recipient) {
					/** @var \Swift_Message $message */
					$message = \Swift_Message::newInstance()
						->setSubject($this->subject)
						->setFrom('fellow@elprograms.org')
						->setTo($this->recipients)
						->setBody($this->emailBody, 'text/html');

				if ($this->cc) {
					$message->setCc($cc);
				}

				if ($this->bcc) {
					$message->setBcc($bcc);
				}

				if ($this->bccEmailAddress) {
					$message->addBcc($this->bccEmailAddress);
				}

				$this->mailer->send($message);
//				}
			}
		} else {
			$this->session->getFlashBag()->add('debug-email', $this->emailBody);
		}
	}

}