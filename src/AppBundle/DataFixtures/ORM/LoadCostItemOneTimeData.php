<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CostItemOneTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadCostItem
 *
 * @package AppBundle\DataFixtures\ORM
 */
class costItemOneTimeData extends AbstractFixture implements OrderedFixtureInterface {

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager) {
		$costItems = [
			'Household items',
			'Internet Start Up Fees',
			'Mobile Phone Start Up Fees',
			'In-country Visa/Residency Permit Fees',
		];

		foreach ($costItems as $itemDescription) {
			$costItem = new CostItemOneTime();

			$costItem->setDescription($itemDescription);

			$manager->persist($costItem);
			$manager->flush();
		}
	}

	/**
	 * Get the order of this fixture
	 *
	 * @return integer
	 */
	public function getOrder() {
		return 10;
	}

}
