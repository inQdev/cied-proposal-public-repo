<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\GlobalValueSpecialist;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class globalValueSpecialistData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class globalValueSpecialistData extends AbstractFixture implements OrderedFixtureInterface {

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager) {
		$globalValues = [
			['Stipend', 200],
			['Planning Days allowance', 200],
			['Program Activities Allowance (PAA)', 200],
			['Post-Arrival Orientation Allowance', 200],
			['Travel booked by Georgetown', 200],
			['Pre-departure expenses', 200],
			['Rest stop expenses', 200],
			['Other Costs', 200],
		];

		foreach ($globalValues as $item) {
			$gValue = new GlobalValueSpecialist();

			$gValue->setName($item[0]);
			$gValue->setDefaultValue($item[1]);

			$manager->persist($gValue);
			$manager->flush();
		}
	}

	/**
	 * Get the order of this fixture
	 *
	 * @return integer
	 */
	public function getOrder() {
		return 10;
	}

}
