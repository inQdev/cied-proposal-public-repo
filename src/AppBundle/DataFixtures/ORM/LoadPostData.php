<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Post;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadPostData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadPostData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $countries = [
            'Andorra' => [
                'Barcelona'
            ],
            'United Arab Emirates' => [
                'Abu Dhabi',
                'Dubai'
            ],
            'Afghanistan' => [
                'Kabul'
            ],
            'Antigua and Barbuda' => [
                'Bridgetown'
            ],
            'Albania' => [
                'Tirana'
            ],
            'Armenia' => [
                'Yerevan'
            ],
            'Angola' => [
                'Luanda'
            ],
            'Argentina' => [
                'Buenos Aires'
            ],
            'Austria' => [
                'Vienna'
            ],
            'Australia' => [
                'Canberra',
                'Melbourne',
                'Perth',
                'Sydney'
            ],
            'Aruba' => [
                'Willemstad'
            ],
            'Azerbaijan' => [
                'Baku'
            ],
            'Bosnia and Herzegovina' => [
                'Sarajevo'
            ],
            'Barbados' => [
                'Bridgetown'
            ],
            'Bangladesh' => [
                'Dhaka'
            ],
            'Belgium' => [
                'Brussels'
            ],
            'Burkina Faso' => [
                'Ougadougou'
            ],
            'Bulgaria' => [
                'Sofia'
            ],
            'Bahrain' => [
                'Manama'
            ],
            'Burundi' => [
                'Bujumbura'
            ],
            'Benin' => [
                'Cotonou'
            ],
            'Bermuda' => [
                'Hamilton'
            ],
            'Brunei' => [
                'Bandar Seri Begawan'
            ],
            'Bolivia' => [
                'La Paz'
            ],
            'Brazil' => [
                'Brasilia',
                'Recife',
                'Rio de Janeiro',
                'Sao Paolo'
            ],
            'Bahamas, The' => [
                'Nassau'
            ],
            'Bhutan' => [
                'Thimphu'
            ],
            'Botswana' => [
                'Gaborone'
            ],
            'Belarus' => [
                'Minsk'
            ],
            'Belize' => [
                'Belize City'
            ],
            'Canada' => [
                'Calgary',
                'Halifax',
                'Montreal',
                'Ottawa',
                'Quebec City',
                'Toronto',
                'Vancouver',
                'Winnipeg'
            ],
            'Congo, Democratic Republic of' => [
                'Kinshasa'
            ],
            'Central African Republic' => [
                'Bangui'
            ],
            'Congo, Republic of the' => [
                'Brazzaville'
            ],
            'Switzerland' => [
                'Bern',
            'Geneva'
            ],
            'Cote d\'Ivoire' => [
                'Abidjan'
            ],
            'Chile' => [
                'Santiago'
            ],
            'Cameroon' => [
                'Yaounde'
            ],
            'China, People’s Republic of' => [
                'Beijing',
                'Chengdu',
                'Guangzhou',
                'Hong Kong',
                'Shanghai',
                'Shenyang'
            ],
            'Colombia' => [
                'Bogota'
            ],
            'Costa Rica' => [
                'San Jose'
            ],
            'Cuba' => [
                'Havana'
            ],
            'Cape Verde' => [
                'Praia'
            ],
            'Curacao' => [
                'Willemstad'
            ],
            'Cyprus' => [
                'Nicosia'
            ],
            'Czech Republic' => [
                'Prague'
            ],
            'Germany' => [
                'Berlin',
                'Dusseldorf',
                'Frankfurt',
                'Hamburg',
                'Leipzig',
                'Munich'
            ],
            'Djibouti' => [
                'Djibouti'
            ],
            'Denmark' => [
                'Copenhagen'
            ],
            'Dominica' => [
                'Bridgetown'
            ],
            'Dominican Republic' => [
                'Santo Domingo'
            ],
            'Algeria' => [
                'Algiers'
            ],
            'Ecuador' => [
                'Guyaquil',
            'Quito'
            ],
            'Estonia' => [
                'Tallinn'
            ],
            'Egypt' => [
                'Cairo'
            ],
            'Eritrea' => [
                'Asmara'
            ],
            'Spain' => [
                'Barcelona',
            'Madrid'
            ],
            'Ethiopia' => [
                'Addis Ababa'
            ],
            'Finland' => [
                'Helsinki'
            ],
            'Fiji' => [
                'Suva'
            ],
            'Micronesia' => [
                'Kolonia'
            ],
            'France' => [
                'Bordeaux',
                'Lille',
                'Lyon',
                'Marseille',
                'Paris',
                'Rennes',
                'Strasbourg',
                'Toulouse'
            ],
            'Gabon' => [
                'Libreville'
            ],
            'United Kingdom' => [
                'Belfast',
                'Edinburgh',
                'London'
            ],
            'Grenada' => [
                'Saint George’s'
            ],
            'Georgia' => [
                'Tbilisi'
            ],
            'Ghana' => [
                'Accra'
            ],
            'Gambia, The' => [
                'Banjul'
            ],
            'Guinea' => [
                'Conakry'
            ],
            'Equatorial Guinea' => [
                'Malabo'
            ],
            'Greece' => [
                'Athens',
            'Thessaloniki'
            ],
            'Guatemala' => [
                'Guatemala'
            ],
            'Guinea-Bissau' => [
                'Bissau'
            ],
            'Guyana' => [
                'Georgetown'
            ],
            'Hong Kong' => [
                'Hong Kong'
            ],
            'Honduras' => [
                'Tegucigalpa'
            ],
            'Croatia' => [
                'Zagreb'
            ],
            'Haiti' => [
                'Port Au Prince'
            ],
            'Hungary' => [
                'Budapest'
            ],
            'Indonesia' => [
                'Jakarta',
            'Surabaya'
            ],
            'Ireland' => [
                'Dublin'
            ],
            'Israel' => [
                'Tel Aviv'
            ],
            'India' => [
                'Chennai',
                'Kolkota',
                'Mumbai',
                'New Delhi'
            ],
            'Iraq' => [
                'Baghdad'
            ],
            'Iran' => [
                'Manama'
            ],
            'Iceland' => [
                'Reykjavik'
            ],
            'Italy' => [
                'Florence',
                'Milan',
                'Naples',
                'Rome'
            ],
            'Jamaica' => [
                'Kingston'
            ],
            'Jordan' => [
                'Amman'
            ],
            'Japan' => [
                'Fukuoka',
                'Nagoya',
                'Naha, Okinawa',
                'Osaka\Kobe',
                'Sapporo',
                'Tokyo'
            ],
            'Kenya' => [
                'Nairobi'
            ],
            'Kyrgyzstan' => [
                'Bishkek'
            ],
            'Cambodia' => [
                'Phnom Penh'
            ],
            'Kiribati' => [
                'Suva'
            ],
            'Comoros' => [
                'Moroni'
            ],
            'Saint Kitts and Nevis' => [
                'Bridgetown'
            ],
            'North Korea' => [
                'Seoul'
            ],
            'South Korea' => [
                'Seoul'
            ],
            'Kuwait' => [
                'Kuwait'
            ],
            'Cayman Islands' => [
                'George Town'
            ],
            'Kazakhstan' => [
                'Astana'
            ],
            'Laos' => [
                'Vientiane'
            ],
            'Lebanon' => [
                'Beirut'
            ],
            'Saint Lucia' => [
                'Bridgetown'
            ],
            'Liechtenstein' => [
                'Bern'
            ],
            'Sri Lanka' => [
                'Colombo'
            ],
            'Liberia' => [
                'Monrovia'
            ],
            'Lesotho' => [
                'Maseru'
            ],
            'Lithuania' => [
                'Vilnius'
            ],
            'Luxembourg' => [
                'Luxembourg'
            ],
            'Latvia' => [
                'Riga'
            ],
            'Libya' => [
                'Tripoli'
            ],
            'Morocco' => [
                'Casablanca',
            'Rabat'
            ],
            'Monaco' => [
                'Marseille'
            ],
            'Moldova' => [
                'Chisinau'
            ],
            'Montenegro' => [
                'Podgorica'
            ],
            'St. Maarten' => [
                'Willemstad'
            ],
            'Madagascar' => [
                'Antananarivo'
            ],
            'Marshall Islands' => [
                'Majuro'
            ],
            'Macedonia' => [
                'Skopje'
            ],
            'Mali' => [
                'Bamako'
            ],
            'Burma' => [
                'Rangoon'
            ],
            'Mongolia' => [
                'Ulaanbaatar'
            ],
            'Mauritania' => [
                'Nouakchott'
            ],
            'Malta' => [
                'Valletta'
            ],
            'Mauritius' => [
                'Port Louis'
            ],
            'Maldives' => [
                'Male'
            ],
            'Malawi' => [
                'Lilongwe'
            ],
            'Mexico' => [
                'Ciudad Juarez',
                'Guadalajara',
                'Hermosillo',
                'Matamoros',
                'Merida',
                'Mexico City',
                'Monterey',
                'Nogales',
                'Nuevo Laredo',
                'Puerto Vallarta',
                'Tijuana'
            ],
            'Malaysia' => [
                'Kuala Lumpur'
            ],
            'Mozambique' => [
                'Maputo'
            ],
            'Namibia' => [
                'Windhoek'
            ],
            'Niger' => [
                'Niamey'
            ],
            'Nigeria' => [
                'Abuja',
            'Lagos'
            ],
            'Nicaragua' => [
                'Managua'
            ],
            'Netherlands' => [
                'Amsterdam',
            'The Hague'
            ],
            'Norway' => [
                'Oslo'
            ],
            'Nepal' => [
                'Katmandu'
            ],
            'Nauru' => [
                'Suva'
            ],
            'New Zealand' => [
                'Auckland',
            'Wellington'
            ],
            'Oman' => [
                'Muscat'
            ],
            'Panama' => [
                'Panama City'
            ],
            'Peru' => [
                'Lima'
            ],
            'Papua New Guinea' => [
                'Port Moresby'
            ],
            'Philippines' => [
                'Manila'
            ],
            'Pakistan' => [
                'Islamabad',
                'Karachi',
                'Lahore',
                'Peshawar'
            ],
            'Poland' => [
                'Krakow',
            'Warsaw'
            ],
            'Portugal' => [
                'Lisbon',
            'Ponta Delgada'
            ],
            'Palau' => [
                'Koror'
            ],
            'Paraguay' => [
                'Asuncion'
            ],
            'Qatar' => [
                'Doha'
            ],
            'Romania' => [
                'Bucharest'
            ],
            'Serbia' => [
                'Belgrade'
            ],
            'Russia' => [
                'Moscow',
                'St. Petersburg',
                'Vladivostok',
                'Yekaterinburg'
            ],
            'Rwanda' => [
                'Kigali'
            ],
            'Saudi Arabia' => [
                'Dhahran',
                'Jeddah',
                'Riyadh'
            ],
            'Solomon Islands' => [
                'Port Moresby'
            ],
            'Seychelles' => [
                'Victoria'
            ],
            'Sudan' => [
                'Khartoum'
            ],
            'Sweden' => [
                'Stockholm'
            ],
            'Singapore' => [
                'Singapore'
            ],
            'Slovenia' => [
                'Ljubljana'
            ],
            'Slovakia' => [
                'Bratislava'
            ],
            'Sierra Leone' => [
                'Freetown'
            ],
            'San Marino' => [
                'Florence'
            ],
            'Senegal' => [
                'Dakar'
            ],
            'Somalia' => [
                'Mogadishu'
            ],
            'Suriname' => [
                'Paramaribo'
            ],
            'South Sudan' => [
                'Juba'
            ],
            'Sao Tome and Principe' => [
                'Sao Tome'
            ],
            'El Salvador' => [
                'San Salvador'
            ],
            'Syria' => [
                'Damascus'
            ],
            'Swaziland' => [
                'Mbabane'
            ],
            'Chad' => [
                'Ndjamena'
            ],
            'Togo' => [
                'Lome'
            ],
            'Thailand' => [
                'Bangkok',
            'Chiang Mai'
            ],
            'Tajikistan' => [
                'Dushanbe'
            ],
            'East Timor' => [
                'Dili'
            ],
            'Turkmenistan' => [
                'Ashgabat'
            ],
            'Tunisia' => [
                'Tunis'
            ],
            'Tonga' => [
                'Port Moresby'
            ],
            'Turkey' => [
                'Adana',
                'Ankara',
                'Istanbul'
            ],
            'Trinidad and Tobago' => [
                'Port of Spain'
            ],
            'Tuvalu' => [
                'Port Moresby'
            ],
            'Taiwan' => [
                'Taipei'
            ],
            'Tanzania' => [
                'Dar-Es-Salaam'
            ],
            'Ukraine' => [
                'Kyiv'
            ],
            'Uganda' => [
                'Kampala'
            ],
            'United States' => [
                'Washington, DC'
            ],
            'Uruguay' => [
                'Montevideo'
            ],
            'Uzbekistan' => [
                'Tashkent'
            ],
            'Vatican City (Holy See)' => [
                'Vatican City'
            ],
            'Saint Vincent and the Grenadines' => [
                'Bridgetown'
            ],
            'Venezuela' => [
                'Caracas'
            ],
            'Vietnam' => [
                'Hanoi',
            'Ho Chi Minh City'
            ],
            'Vanuatu' => [
                'Port Moresby'
            ],
            'West Bank' => [
                'Jerusalem'
            ],
            'Samoa' => [
                'Apia'
            ],
            'Kosovo' => [
                'Pristina'
            ],
            'Yemen' => [
                'Sanaa'
            ],
            'South Africa' => [
                'Cape Town',
                'Durban',
                'Johannesburg',
                'Pretoria'
            ],
            'Zambia' => [
                'Lusaka'
            ],
            'Zimbabwe' => [
                'Harare'
            ]
        ];

        foreach ($countries as $countryName => $countryPosts) {
            foreach ($countryPosts as $postName) {
                $post = new Post();

                $post->setName($postName);
                $post->setCountry($this->getReference($countryName));

                $manager->persist($post);
            }
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}