<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Region;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadRegionData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadRegionData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $regions = [
          'Africa (AF)',
          'East Asia and Pacific (EAP)',
          'Europe and Eurasia (EUR)',
          'Near East and North Africa (NEA)',
          'South and Central Asia (SCA)',
          'Western Hemisphere (WHA)',
        ];

        foreach ($regions as $regionName) {
            $region = new Region();

            $region->setName($regionName);

            $manager->persist($region);
            $manager->flush();

            $this->addReference($regionName, $region);
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}