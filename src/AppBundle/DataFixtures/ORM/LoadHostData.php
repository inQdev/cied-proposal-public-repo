<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Host;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadHostData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadHostData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $countries = [
            'United States' => [
              'Host United States 1',
              'Host United States 2',
              'Host United States 3',
            ],
            'Colombia' => [
              'Host Colombia 1',
              'Host Colombia 2',
              'Host Colombia 3',
            ]
        ];

        foreach ($countries as $countryName => $countryHosts) {
            foreach ($countryHosts as $hostName) {
                $host = new Host();

                $host->setName($hostName);
                $host->setCountry($this->getReference($countryName));

                $manager->persist($host);
            }
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}