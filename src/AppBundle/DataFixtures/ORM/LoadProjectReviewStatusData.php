<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ProjectReviewStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadProjectReviewStatusData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadProjectReviewStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $projectReviewStatuses = [
          ProjectReviewStatus::UNDER_RELO_REVIEW    => 'Under RELO review',
          ProjectReviewStatus::UNDER_POST_RE_REVIEW => 'Under Post re-review',
          ProjectReviewStatus::UNDER_ECA_REVIEW     => 'Under ECA review',
          ProjectReviewStatus::UNDER_RPO_REVIEW     => 'Under RPO review',
          ProjectReviewStatus::REVIEW_COMPLETE      => 'Review Complete',
        ];

        foreach ($projectReviewStatuses as $projectReviewStatusId => $projectReviewStatusName) {
            /** @var \AppBundle\Entity\ProjectReviewStatus $projectReviewStatus */
            $projectReviewStatus = new ProjectReviewStatus();
            $projectReviewStatus->setId($projectReviewStatusId);
            $projectReviewStatus->setName($projectReviewStatusName);

            $manager->persist($projectReviewStatus);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 11;
    }
}