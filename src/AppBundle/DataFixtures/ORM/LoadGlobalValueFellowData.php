<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\GlobalValueFellow;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class globalValueFellowData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class globalValueFellowData extends AbstractFixture implements OrderedFixtureInterface {

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager) {
		$globalValues = [
			['Stipend', 200],
			['Dependent allowance', 200],
			['Shipping allowance', 200],
			['Program Activities Allowance (PAA)', 200],
			['Pre-departure allowance', 200],
			['Post-Arrival Orientation Allowance', 200],
			['D.C. Pre-Departure Orientation Allowance', 200],
			['D.C. Pre-Departure Orientation Hotel and Meals', 200],
			['D.C. Pre-departure Orientation Round-trip Ticket', 200],
			['Round-trip Travel to/from Host City', 200],
			['Mid-year cost', 200],
			['Other Costs', 200],
		];

		foreach ($globalValues as $item) {
			$gValue = new GlobalValueFellow();

			$gValue->setName($item[0]);
			$gValue->setDefaultValue($item[1]);

			$manager->persist($gValue);
			$manager->flush();
		}
	}

	/**
	 * Get the order of this fixture
	 *
	 * @return integer
	 */
	public function getOrder() {
		return 10;
	}

}
