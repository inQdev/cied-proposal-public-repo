<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Country;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadCountryData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadCountryData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $regions = [
          'Africa (AF)'          => [ 
            'countries' => [
              "AO" => "Angola",
              "BF" => "Burkina Faso",
              "BI" => "Burundi",
              "BJ" => "Benin",
              "BW" => "Botswana",
              "CD" => "Congo, Democratic Republic of",
              "CF" => "Central African Republic",
              "CG" => "Congo, Republic of the",
              "CI" => "Cote d'Ivoire",
              "CM" => "Cameroon",
              "CV" => "Cape Verde",
              "DJ" => "Djibouti",
              "ER" => "Eritrea",
              "ET" => "Ethiopia",
              "GA" => "Gabon",
              "GH" => "Ghana",
              "GM" => "Gambia, The",
              "GN" => "Guinea",
              "GQ" => "Equatorial Guinea",
              "GW" => "Guinea-Bissau",
              "KE" => "Kenya",
              "KM" => "Comoros",
              "LR" => "Liberia",
              "LS" => "Lesotho",
              "MG" => "Madagascar",
              "ML" => "Mali",
              "MR" => "Mauritania",
              "MT" => "Malta",
              "MU" => "Mauritius",
              "MW" => "Malawi",
              "MZ" => "Mozambique",
              "NA" => "Namibia",
              "NE" => "Niger",
              "NG" => "Nigeria",
              "RW" => "Rwanda",
              "SC" => "Seychelles",
              "SL" => "Sierra Leone",
              "SN" => "Senegal",
              "SO" => "Somalia",
              "SS" => "South Sudan",
              "ST" => "Sao Tome and Principe",
              "SZ" => "Swaziland",
              "TD" => "Chad",
              "TG" => "Togo",
              "TZ" => "Tanzania",
              "UG" => "Uganda",
              "ZA" => "South Africa",
              "ZM" => "Zambia",
              "ZW" => "Zimbabwe"
            ]
          ],
          'East Asia and Pacific (EAP)'          => [ 
            'countries' => [
              "ID" => "Indonesia",
              "AU" => "Australia",
              "BN" => "Brunei",
              "CN" => "China, People’s Republic of",
              "FJ" => "Fiji",
              "FM" => "Micronesia",
              "HK" => "Hong Kong",
              "JP" => "Japan",
              "KH" => "Cambodia",
              "KI" => "Kiribati",
              "KP" => "North Korea",
              "KR" => "South Korea",
              "LA" => "Laos",
              "MH" => "Marshall Islands",
              "MM" => "Burma",
              "MN" => "Mongolia",
              "MY" => "Malaysia",
              "NR" => "Nauru",
              "NZ" => "New Zealand",
              "PG" => "Papua New Guinea",
              "PH" => "Philippines",
              "PW" => "Palau",
              "SB" => "Solomon Islands",
              "SG" => "Singapore",
              "TH" => "Thailand",
              "TL" => "East Timor",
              "TO" => "Tonga",
              "TV" => "Tuvalu",
              "TW" => "Taiwan",
              "VN" => "Vietnam",
              "VU" => "Vanuatu"
            ]
          ],
          'Europe and Eurasia (EUR)'          => [ 
            'countries' => [
              "AD" => "Andorra",
              "AL" => "Albania",
              "AM" => "Armenia",
              "AT" => "Austria",
              "AZ" => "Azerbaijan",
              "BA" => "Bosnia and Herzegovina",
              "BE" => "Belgium",
              "BG" => "Bulgaria",
              "BY" => "Belarus",
              "CH" => "Switzerland",
              "CY" => "Cyprus",
              "CZ" => "Czech Republic",
              "DE" => "Germany",
              "DK" => "Denmark",
              "EE" => "Estonia",
              "ES" => "Spain",
              "FI" => "Finland",
              "FR" => "France",
              "GB" => "United Kingdom",
              "GE" => "Georgia",
              "GR" => "Greece",
              "HR" => "Croatia",
              "HU" => "Hungary",
              "IE" => "Ireland",
              "IS" => "Iceland",
              "IT" => "Italy",
              "LI" => "Liechtenstein",
              "LT" => "Lithuania",
              "LU" => "Luxembourg",
              "LV" => "Latvia",
              "MC" => "Monaco",
              "MD" => "Moldova",
              "ME" => "Montenegro",
              "MK" => "Macedonia",
              "NL" => "Netherlands",
              "NO" => "Norway",
              "PL" => "Poland",
              "PT" => "Portugal",
              "RO" => "Romania",
              "RS" => "Serbia",
              "RU" => "Russia",
              "SE" => "Sweden",
              "SI" => "Slovenia",
              "SK" => "Slovakia",
              "SM" => "San Marino",
              "TR" => "Turkey",
              "UA" => "Ukraine",
              "VA" => "Vatican City (Holy See)",
              "XK" => "Kosovo",
            ]
          ],
          'Near East and North Africa (NEA)'          => [ 
            'countries' => [
              "AE" => "United Arab Emirates",
              "BH" => "Bahrain",
              "DZ" => "Algeria",
              "EG" => "Egypt",
              "IL" => "Israel",
              "IQ" => "Iraq",
              "IR" => "Iran",
              "JO" => "Jordan",
              "KW" => "Kuwait",
              "LB" => "Lebanon",
              "LY" => "Libya",
              "MA" => "Morocco",
              "OM" => "Oman",
              "QA" => "Qatar",
              "SA" => "Saudi Arabia",
              "SD" => "Sudan",
              "SY" => "Syria",
              "TN" => "Tunisia",
              "WB" => "West Bank",
              "YE" => "Yemen"
            ]
          ],
          'South and Central Asia (SCA)'          => [ 
            'countries' => [
              "AF" => "Afghanistan",
              "BD" => "Bangladesh",
              "BT" => "Bhutan",
              "IN" => "India",
              "KG" => "Kyrgyzstan",
              "KZ" => "Kazakhstan",
              "LK" => "Sri Lanka",
              "MV" => "Maldives",
              "NP" => "Nepal",
              "PK" => "Pakistan",
              "TJ" => "Tajikistan",
              "TM" => "Turkmenistan",
              "UZ" => "Uzbekistan"
            ]
          ],
          'Western Hemisphere (WHA)'          => [ 
            'countries' => [
              "AG" => "Antigua and Barbuda",
              "AR" => "Argentina",
              "AW" => "Aruba",
              "BB" => "Barbados",
              "BM" => "Bermuda",
              "BO" => "Bolivia",
              "BR" => "Brazil",
              "BS" => "Bahamas, The",
              "BZ" => "Belize",
              "CA" => "Canada",
              "CL" => "Chile",
              "CO" => "Colombia",
              "CR" => "Costa Rica",
              "CU" => "Cuba",
              "CW" => "Curacao",
              "DM" => "Dominica",
              "DO" => "Dominican Republic",
              "EC" => "Ecuador",
              "GD" => "Grenada",
              "GT" => "Guatemala",
              "GY" => "Guyana",
              "HN" => "Honduras",
              "HT" => "Haiti",
              "JM" => "Jamaica",
              "KN" => "Saint Kitts and Nevis",
              "KY" => "Cayman Islands",
              "LC" => "Saint Lucia",
              "MF" => "St. Maarten",
              "MX" => "Mexico",
              "NI" => "Nicaragua",
              "PA" => "Panama",
              "PE" => "Peru",
              "PY" => "Paraguay",
              "SR" => "Suriname",
              "SV" => "El Salvador",
              "TT" => "Trinidad and Tobago",
              "US" => "United States",
              "UY" => "Uruguay",
              "VC" => "Saint Vincent and the Grenadines",
              "VE" => "Venezuela",
              "WS" => "Samoa"
            ]
          ],
        ];

        foreach ($regions as $regionName => $regionCountries) {
            foreach ($regionCountries['countries'] as $countryIso2Code => $countryName) {
                $country = new Country();

                $country->setIso2Code($countryIso2Code);
                $country->setName($countryName);
                $country->setRegion($this->getReference($regionName));

                $manager->persist($country);

                $manager->flush();

                $this->addReference($countryName, $country);
            }
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}