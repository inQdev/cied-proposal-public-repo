<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ProjectOutcome;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadProjectOutcomeData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadProjectOutcomeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $projectOutcomes = [
          ProjectOutcome::SELECTED_FOR_ECA_FUNDING => 'Rejected by RELO',
          ProjectOutcome::NOT_SELECTED_FOR_ECA_FUNDING  => 'Rejected by ECA',
          ProjectOutcome::APPROVED_BY_ECA  => 'Approved by ECA',
        ];

        foreach ($projectOutcomes as $projectOutcomeId => $projectOutcomeName) {
            /** @var \AppBundle\Entity\ProjectOutcome $projectOutcome */
            $projectOutcome = new ProjectOutcome();
            $projectOutcome->setId($projectOutcomeId);
            $projectOutcome->setName($projectOutcomeName);

            $manager->persist($projectOutcome);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 12;
    }
}