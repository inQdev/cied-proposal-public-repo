<?php
/**
 * Listener to switch role in Symfony when changes on Drupal side
 * @author felipe.ceballos@inqbation.com
 * @date 4/19/2016
 */

namespace AppBundle\EventListener;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Controller\FellowProjectReviewController;
use AppBundle\Controller\SpecialistProjectReviewController;
use DrupalDataBundle\Service\DrupalDataConsumer;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RoleSwitcherListener
{
	private $token;
	protected $drupalDataConsumer;

	public function __construct(TokenStorage $token, DrupalDataConsumer $drupalDataConsumer) {
		$this->token = $token;
		$this->drupalDataConsumer = $drupalDataConsumer;
	}

	public function onKernelController(FilterControllerEvent $event) {
		$controller = $event->getController();

		if (!is_array($controller)) {
			return;
		}

		if ($controller[0] instanceof RoleSwitcherInterface) {

			// bypassing some actions in some controllers/actions
			if ($controller[0] instanceof FellowProjectReviewController && $controller[1] == 'reviewAction'
				|| $controller[0] instanceof SpecialistProjectReviewController && $controller[1] == 'reviewAction') {
				return;
			}

			/** @var Request $request */
			$request = Request::createFromGlobals();

			// Reads the role set on the Drupal side using the cookie
			$response = $this->drupalDataConsumer->sendRequest(
				'GET',
				'/role-test/get'
			);
			$current_role = '';
//			dump($response); exit;

			if (is_array($response) && $response[0]) {
				$current_role = $response[0];
			}

//			dump($current_role); exit;
			if (!$current_role || $request->query->get('current_role')) {
				$current_role = $request->query->get('current_role')?$request->query->get('current_role'):'Embassy';
			}

			if ($current_role) {
				$current_role = 'ROLE_' . strtoupper(str_replace(' ', '_', $current_role));
				$token = $this->token->getToken();
				$new_roles = array($current_role, 'ROLE_USER');

				// checks token expiration to make sure user always has a valid session
				if (!$token->isExpired()) {

					// checks if the role changed
					if (count($token->getRoles()) > 2 || $current_role != $token->getRoles()[0]->getRole()) {

						// We have to create a token from scratch to be able to rebuild the roles attribute
						$new_token = new OAuthToken($token->getRawToken(), $new_roles);
						$new_token->setResourceOwnerName($token->getResourceOwnerName());
						$new_token->setUser($token->getUser());
						$new_token->setCreatedAt($token->getCreatedAt());
						$new_token->setTokenSecret($token->getTokenSecret());
						$new_token->setAuthenticated(true);

						// Finally, replaces the existing token
						$this->token->setToken($new_token);
					}
				} else {
					// nullifies the token and redirects to the current URL, this way the token is refreshed automagically
					$this->token->setToken(null);
					$redirectUrl = $request->server->get('REQUEST_URI');
					$event->setController(function() use ($redirectUrl) {
						return new RedirectResponse($redirectUrl);
					});

					// alternatively, get a new access_token using the refresh_token
					// Felipe's NOTE: to get a refresh_token when authorizing, I added offline_access to the scope list (config.yml)
					// and disabled the implicit_flow in the OAuth2 Server (Drupal side),
					// more info: https://knpuniversity.com/screencast/oauth/refresh-token
					// TODO: Optionally explore that option
				}
			}

			return;
		}

	}

} // end class