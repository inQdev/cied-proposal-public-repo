<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\LivingExpense;
use AppBundle\Entity\Specialist\Budget\BudgetTotal;
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense;
use AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem;
use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowance;
use AppBundle\Entity\Specialist\Budget\Contribution\Transportation;
use AppBundle\Entity\ContributionOneTime;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * SpecialistProjectPhaseBudgetListener listener.
 *
 * @package AppBundle\EventListener
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class SpecialistProjectPhaseBudgetListener
{

	/**
	 * Creates Budget and initialize dependent entities/records:
	 * Contribution for Living Expense, Transportation, PAA, One Time if In-Country of Mixed
	 * PAA if Virtual
	 * Pre and Post Work if In-Country
	 * BudgetTotal
	 *
	 * NOTE: Remember that this event is called only for new "records"
	 * @param LifecycleEventArgs $args
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		/** @var SpecialistProjectPhaseBudget $phaseBudget */
		$phaseBudget = $args->getEntity();

		// only act on some "SpecialistProjectPhaseBudget" entity
		if (!$phaseBudget instanceof SpecialistProjectPhaseBudget) {
			return;
		}

		$em = $args->getEntityManager();

		// Builds up the structure for associated entities for the budget
		if ($phaseBudget->getPhase()->getType() == SpecialistProjectPhase::IN_COUNTRY_TYPE
			|| $phaseBudget->getPhase()->getType() == SpecialistProjectPhase::MIXED_TYPE) {

			// Creates base for Living Expenses (per country)
//			$inCountryAssignments = $phaseBudget->getPhase()->getInCountryAssignments();
//			if ($inCountryAssignments->count()) {
//				foreach ($inCountryAssignments as $inCountryAssignment) {
//					$livingExpense = new LivingExpense($phaseBudget, $inCountryAssignment);
//					$em->persist($livingExpense);
//				}
//			}

			$contributionLivingExpense = new ContributionLivingExpense($phaseBudget);
			$em->persist($contributionLivingExpense);

			// creates base for GroundTransport and for Transportation between cities
			$costItems = $em->getRepository('AppBundle:CostItemGroundTransport')->findAll();
			$contributionGroundTransport = new GroundTransportCostItem($phaseBudget);
			$contributionGroundTransport->setCostItem(current($costItems));
			$em->persist($contributionGroundTransport);

			$contributionTransportation = new Transportation($phaseBudget);
			$em->persist($contributionTransportation);

			// Creates one-time contribution for the phase.
			$contributionOneTime = new ContributionOneTime($phaseBudget);
			$em->persist($contributionOneTime);

			// Creates PAA for the phase.
			$paa = new ProgramActivitiesAllowance($phaseBudget);
			$em->persist($paa);

			// Clones fixed Global values, and fixed PAA
			$em->getRepository('AppBundle:Specialist\Budget\GlobalValue')
				->cloneGlobalValuesToBudget($phaseBudget);

			$phaseBudget->addContributionLivingExpense($contributionLivingExpense);
			$phaseBudget->setContributionGroundTransport($contributionGroundTransport);
			$phaseBudget->setContributionTransportation($contributionTransportation);
			$phaseBudget->setContributionOneTime($contributionOneTime);
			$phaseBudget->setProgramActivitiesAllowance($paa);
		} else {  // Virtual project
			// Creates PAA for the phase.
			$paa = new ProgramActivitiesAllowance($phaseBudget);
			$em->persist($paa);

			// Clones fixed Global values, and fixed PAA
			$em->getRepository('AppBundle:Specialist\Budget\GlobalValue')
				->cloneGlobalValuesToBudget($phaseBudget);

			$phaseBudget->setProgramActivitiesAllowance($paa);
		}

		// creates record in totals table
		$budgetTotal = new BudgetTotal($phaseBudget);
		$budgetTotal->setRevisionId(1);
		$phaseBudget->addBudgetTotal($budgetTotal);

		// Pre- Post- Work applies only for InCountry projects
		if ($phaseBudget->getPhase()->getType() == SpecialistProjectPhase::IN_COUNTRY_TYPE) {
			$em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->initPrePostWork($budgetTotal);
		}

//		$em->flush();
	}

	// TODO: Felipe - Logic in case the phaseBudget changes (preUpdate method)
}
