<?php

namespace AppBundle\EventListener;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AddHostFieldSubscriber
 *
 * @package AppBundle\EventListener
 */
class AddHostFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SET_DATA => 'preSetData'];
    }

    /**
     * @param \Symfony\Component\Form\FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /* @var \AppBundle\Entity\LocationHostInfo $locationHostInfo */
        $locationHostInfo = $event->getData();

        /* @var \AppBundle\Entity\Country $country */
        $country = $locationHostInfo->getCountry();

        $hosts = (null !== $country)
          ? $country->getHosts()
          : [];

        $form = $event->getForm();

        if (null !== $locationHostInfo) {
            $form->add(
              'host',
              EntityType::class,
              [
                'class'       => 'AppBundle:Host',
                'choices'     => $hosts,
                'label'       => 'label.primary_host_assignment',
                'required'    => false,
                'placeholder' => 'Select a Host Institution'
              ]
            );
        }
    }
}