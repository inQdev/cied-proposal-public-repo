<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\FellowProject;
use AppBundle\Entity\Sequence;
use AppBundle\Entity\WizardStepStatus;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * FellowProjectListener listener.
 *
 * @package AppBundle\EventListener
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class FellowProjectListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        /** @var FellowProject $fellowProject */
        $fellowProject = $args->getEntity();

        // only act on some "FellowProject" entity
        if (!$fellowProject instanceof FellowProject) {
            return;
        }

        $em = $args->getEntityManager();

        $this->setDependentsSectionStatus($fellowProject, $em);
    }

    /**
     * @param FellowProject                              $fellowProject
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    private function setDependentsSectionStatus(FellowProject $fellowProject, $em)
    {
        $newStepStatus = WizardStepStatus::NOT_STARTED;

        if ((null !== $fellowProject->getBringDependents())
          && (null !== $fellowProject->getBringChildren())
          && (null !== $fellowProject->getBringUnmarriedPartner())
          && (null !== $fellowProject->getBringSameSexSpouse())
          && (null !== $fellowProject->getBringSameSexPartner())
          && (null !== $fellowProject->getHousingDependents())
        ) {
            $newStepStatus = WizardStepStatus::COMPLETED;
        }
        elseif ((null !== $fellowProject->getBringDependents())
          || (null !== $fellowProject->getBringChildren())
          || (null !== $fellowProject->getBringUnmarriedPartner())
          || (null !== $fellowProject->getBringSameSexSpouse())
          || (null !== $fellowProject->getBringSameSexPartner())
          || (null !== $fellowProject->getDependentRestrictionsDesc())
          || (null !== $fellowProject->getHousingDependents())
          || (null !== $fellowProject->getFellowFundsHousing())
          || (null !== $fellowProject->getHousingDependentInfo())
        ) {
            $newStepStatus = WizardStepStatus::IN_PROGRESS;
        }

        /** @var WizardStepStatus $dependentsSectionStatus */
        $dependentsSectionStatus = $em->getRepository('AppBundle:WizardStepStatus')
          ->findStepStatusByProject(
            $fellowProject,
            WizardStepStatus::HOUSING_INFO_STEP
          );

        if ($newStepStatus !== $dependentsSectionStatus->getStatus()) {
            $dependentsSectionStatus->setStatus($newStepStatus);

            $em->persist($dependentsSectionStatus);
            $em->flush();
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var FellowProject $fellowProject */
        $fellowProject = $args->getEntity();

        // only act on some "FellowProject" entity
        if (!$fellowProject instanceof FellowProject) {
            return;
        }

        $em = $args->getEntityManager();

        /** @var Sequence $sequence */
        $sequence = $em->getRepository('AppBundle:Sequence')->findOneByName('FellowProject');

        // sets the reference number
        $referencePrefix = $fellowProject->getProjectGeneralInfo()->getCycle()->getStateDepartmentName();
        $fellowProject->getProjectGeneralInfo()->setReferenceNumber($referencePrefix .'-'. $sequence->getNextValue());

        // updates the Sequence
        $sequence->setNextValue($sequence->getNextValue()+1);
        $em->persist($sequence);
        $em->flush();
    }
}
