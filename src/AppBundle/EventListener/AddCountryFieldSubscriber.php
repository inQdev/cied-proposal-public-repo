<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * AddCountryFieldSubscriber event listener.
 *
 * @package AppBundle\EventListener
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class AddCountryFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SET_DATA => 'preSetData'];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /* @var \AppBundle\Entity\ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $event->getData();

        /* @var \AppBundle\Entity\Region $region */
        $region = $projectGeneralInfo->getRegion();

        $form = $event->getForm();

        if (null !== $region) {
            $form->add(
              'countries',
              EntityType::class,
              [
                'class'         => 'AppBundle:Country',
                'query_builder' => function (EntityRepository $er) use ($region) {
                    $qb = $er->createQueryBuilder('c');

                    return $qb
                      ->innerJoin('c.reloLocations', 'rl')
                      ->where($qb->expr()->eq('c.region', ':region'))
                      ->setParameter('region', $region)
                      ->orderBy('c.name', 'ASC');
                },
                'label'         => 'label.country',
                'placeholder'   => 'Select a country',
                'required'      => false,
                'data'          => $projectGeneralInfo->getCountries()->first(),
                'mapped'        => false,
              ]
            );
        }
    }
}
