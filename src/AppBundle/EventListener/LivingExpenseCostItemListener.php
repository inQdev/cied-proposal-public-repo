<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense;
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpenseCostItem;
use AppBundle\Entity\LivingExpenseCostItem;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;

/**
 * LivingExpenseCostItem listener.
 *
 * @package AppBundle\EventListener
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class LivingExpenseCostItemListener
{

	private $contributionCostItems = [];

	/**
	 * NOTE: Remember that this event is called only for new "records"
	 * @param LifecycleEventArgs $args
	 */
	public function prePersist(LifecycleEventArgs $args)
	{
		/** @var LivingExpenseCostItem $expenseCostItem */
		$expenseCostItem = $args->getEntity();

		// only act on some "LivingExpenseCostItem" entity
		if (!$expenseCostItem instanceof LivingExpenseCostItem) {
			return;
		}

		$em = $args->getEntityManager();

		// Gets the ContributionLivingExpense to associate new Contribution item
		// TODO: Felipe - Modify the logic when versions are managed
		/** @var ContributionLivingExpense $contributionLivingExpense */
		$contributionLivingExpense = $expenseCostItem->getLivingExpense()->getPhaseBudget()->getContributionLivingExpenses()->first();

		// Builds up the structure for associated entities for the budget
		$contributionItem = new ContributionLivingExpenseCostItem($contributionLivingExpense, $expenseCostItem);
		$contributionLivingExpense->addContributionLivingExpenseCostItem($contributionItem);

		$em->persist($contributionItem);
		$em->persist($contributionLivingExpense);

	}


	/**
	 * NOTE: When listener has been defined for the class, LifecycleCallbacks don't work on entity
	 * @param LifecycleEventArgs $args
	 */
	public function preUpdate(LifecycleEventArgs $args) {

//		$em = $args->getEntityManager();
		/** @var LivingExpenseCostItem $expenseCostItem */
		$expenseCostItem = $args->getEntity();

		// only act on some "LivingExpenseCostItem" entity
		if (!($expenseCostItem instanceof LivingExpenseCostItem)) {
			return;
		}

//		$expenseCostItem->calculateTotals();
 	}

	/**
	 * @param OnFlushEventArgs $eventArgs
	 */
	public function onFlush(OnFlushEventArgs $eventArgs) {
		$em = $eventArgs->getEntityManager();
		$uow = $em->getUnitOfWork();

		foreach($uow->getScheduledEntityUpdates() as $entity) {
			if ($entity instanceof LivingExpenseCostItem) {

				$entity->calculateTotals(); // required, otherwise the data taken could be outdated
				$contributionItem = $entity->getContributionLivingExpenseCostItem();

				if ($contributionItem) {
					$contributionItem->setTotalCost($entity->getCostTotal());
					$em->persist($contributionItem);
//					$classMetadata = $em->getClassMetadata('AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpenseCostItem');
//					$uow->recomputeSingleEntityChangeSet($classMetadata, $contributionItem);
				}
			}
		}
		$uow->computeChangeSets();
	}

}
