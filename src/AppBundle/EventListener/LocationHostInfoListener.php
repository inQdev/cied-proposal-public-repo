<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\FellowProject;
use AppBundle\Entity\LocationHostInfo;
use AppBundle\Entity\WizardStepStatus;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * LocationHostInfoListener listener.
 *
 * @package AppBundle\EventListener
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class LocationHostInfoListener
{
    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $locationHostInfo = $args->getEntity();

        // only act on some "LocationHostInfo" entity
        if (!$locationHostInfo instanceof LocationHostInfo) {
            return;
        }

        $em = $args->getEntityManager();

        $this->setLocationHostInfoStepStatus($locationHostInfo, $em);
    }

    /**
     * @param LocationHostInfo            $locationHostInfo
     * @param \Doctrine\ORM\EntityManager $em
     */
    private function setLocationHostInfoStepStatus(LocationHostInfo $locationHostInfo, $em)
    {
        /** @var FellowProject $fellowProject */
        $fellowProject = $locationHostInfo->getFellowProject();

        $newStepStatus = WizardStepStatus::NOT_STARTED;

        if ((null !== $locationHostInfo->getHostCity())
          && (null !== $locationHostInfo->getHostCityDescription())
          && (null !== $locationHostInfo->getHostAcademicYear())
          && (null !== $locationHostInfo->getNewHost())
          && (null !== $locationHostInfo->getFellowAtHost())
          && (null !== $locationHostInfo->getHostDesc())
        ) {
            $newStepStatus = WizardStepStatus::COMPLETED;
        }
        elseif ((null !== $locationHostInfo->getHostCity())
          || (null !== $locationHostInfo->getHostCityDescription())
          || (null !== $locationHostInfo->getHost())
          || (null !== $locationHostInfo->getHostAcademicYear())
          || (null !== $locationHostInfo->getHostAcademicYearStartDate())
          || (null !== $locationHostInfo->getHostAcademicYearEndDate())
          || (null !== $locationHostInfo->getNewHost())
          || (null !== $locationHostInfo->getFellowAtHost())
          || (null !== $locationHostInfo->getCurrentFellowName())
          || (null !== $locationHostInfo->getHostDesc())
          || (null !== $locationHostInfo->getAdditionalInfoDesc())
        ) {
            $newStepStatus = WizardStepStatus::IN_PROGRESS;
        }

        /** @var WizardStepStatus $locationHostInfoSectionStatus */
        $locationHostInfoSectionStatus = $em->getRepository('AppBundle:WizardStepStatus')
          ->findStepStatusByProject(
            $fellowProject,
            WizardStepStatus::LOCATION_HOST_INFO_STEP
          );

        if ($newStepStatus !== $locationHostInfoSectionStatus->getStatus()) {
            $locationHostInfoSectionStatus->setStatus($newStepStatus);

            $em->persist($locationHostInfoSectionStatus);
            $em->flush();
        }

    }
}
