<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * GlobalValueType type
 *
 * @package AppBundle\Form\GlobalValue
 */
class GlobalValueType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'name',
            TextType::class,
            [
              'disabled' => true,
            ]
          )
          ->add('defaultValue');

        $builder->addEventListener(
          FormEvents::PRE_SUBMIT,
          function (FormEvent $event) {
              $data = $event->getData();

              $data['defaultValue'] = str_replace(
                ['$', ','],
                '',
                $data['defaultValue']
              );

              $event->setData($data);
          }
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          [
            'data_class' => 'AppBundle\Entity\GlobalValue'
          ]
        );
    }
}
