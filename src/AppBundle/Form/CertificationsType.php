<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class CertificationsType
 *
 * @package AppBundle\Form
 */
class CertificationsType extends BaseWizardStepType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'rsoCertification',
            ChoiceType::class,
            [
              'label' => 'label.rso_certification',
              'choices' => [
                'Post certifies that RSO has no objection' => true
              ],
              'placeholder' => 'Select',
              'expanded' => false,
              'multiple' => false,
              'required' => false
            ]
          )
          ->add(
            'housingCertification',
            ChoiceType::class,
            [
              'label' => 'label.housing_certification',
              'choices' => [
                'Post agrees to ensure suitable housing is provided' => true
              ],
              'placeholder' => 'Select',
              'expanded' => false,
              'multiple' => false,
              'required' => false
            ]
          )
          ->add(
            'visaCertification',
            ChoiceType::class,
            [
              'label' => 'label.visa_certification',
              'choices' => [
                'Post agrees to provide visa information' => true
              ],
              'placeholder' => 'Select',
              'expanded' => false,
              'multiple' => false,
              'required' => false
            ]
          )
          ->add(
            'medicalCertification',
            ChoiceType::class,
            [
              'label' => 'label.medical_certification',
              'choices' => [
                'Post agrees to provide a list of medical facilities' => true
              ],
              'placeholder' => 'Select',
              'expanded' => false,
              'multiple' => false,
              'required' => false
            ]
          )
          ->add(
            'postArrivalCertification',
            ChoiceType::class,
            [
              'label' => 'label.post_arrival_certification',
              'choices' => [
                'Post agrees to arrange an in-country orientation' => true
              ],
              'placeholder' => 'Select',
              'expanded' => false,
              'multiple' => false,
              'required' => false
            ]
          )
          ->add(
            'costSharingCertification',
            ChoiceType::class,
            [
              'label' => 'label.cost_sharing_certification',
              'choices' => [
                'Post agrees to assume unfulfilled cost-sharing commitments' => true
              ],
              'placeholder' => 'Select',
              'expanded' => false,
              'multiple' => false,
              'required' => false
            ]
          );
    }
}