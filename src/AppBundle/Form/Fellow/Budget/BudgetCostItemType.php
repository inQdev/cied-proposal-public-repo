<?php

namespace AppBundle\Form\Fellow\Budget;

use AppBundle\Entity\CostItemCustom;
use AppBundle\Entity\CostItemCustomInKind;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem;
use AppBundle\Form\CostItemCustomInKindType;
use AppBundle\Form\CostItemCustomType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BudgetCostItemType extends AbstractType {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder
			->add('host_contribution_in_kind');

		// sets listener to add some fields dynamically depending on the type of funding organization
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
			/** @var FellowProjectBudgetCostItem $costItem */
			$costItem = $event->getData();
			$form = $event->getForm();

			if ($costItem) {

				if ($costItem->getCostItem() instanceof CostItemCustomInKind) {
					$form->
					add('cost_item', CostItemCustomInKindType::class, [
						'label' => false,
						'label_format' => ' ',
					]);
				} else {
					$form->add('full_cost')
						->add('host_contribution_monetary');

					if ($costItem->getBudgetRevision()->getBudget()->getFundedBy() == 'ECA') {
						$form->add('post_contribution')
						->add('post_host_total_contribution', null, [
							'disabled' => true,
						])
						->add('eca_total_contribution', null, [
							'disabled' => true,
						]);
					} else {
						$form->add('post_total_contribution', null, [
							'disabled' => true,
						]);
					}

					// checks if an additional field must be shown to save the custom cost items
					if ($costItem->getCostItem() instanceof CostItemCustom) {
						$form->
							add('cost_item', CostItemCustomType::class, [
								'label' => false,
								'label_format' => ' ',
						])
						->remove('host_contribution_in_kind');
					}
				}

			}

		});

		// Adds listener to convert currency formatted values to basic numbers
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
			$costItem = $event->getData();
			$form = $event->getForm();

			if (isset($costItem['full_cost'])) {
				$costItem['full_cost'] = str_replace(array('$', ','), '', $costItem['full_cost']);
				$costItem['host_contribution_monetary'] = str_replace(array('$', ','), '', $costItem['host_contribution_monetary']);
			}

			if (isset($costItem['host_contribution_in_kind'])) {
				$costItem['host_contribution_in_kind'] = str_replace(array('$', ','), '', $costItem['host_contribution_in_kind']);
			}

			if (isset($costItem['post_contribution'])) {
				$costItem['post_contribution'] = str_replace(array('$', ','), '', $costItem['post_contribution']);
			}

			$event->setData($costItem);
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => FellowProjectBudgetCostItem::class
		));
	}

}
