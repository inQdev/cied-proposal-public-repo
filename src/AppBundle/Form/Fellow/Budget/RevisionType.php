<?php

namespace AppBundle\Form\Fellow\Budget;

use AppBundle\Entity\Fellow\Budget\Revision;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RevisionType extends AbstractType
{

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('budgetCostItems', CollectionType::class, [
				'entry_type' => BudgetCostItemType::class,
				'label_format' => ' ',
				'by_reference' => false,
				'required' => false,
				'error_bubbling' => true,
				'allow_add' => true,
			]);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => Revision::class
		));
	}


}
