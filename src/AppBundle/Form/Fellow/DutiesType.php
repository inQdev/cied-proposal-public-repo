<?php

namespace AppBundle\Form\Fellow;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DutiesType
 *
 * @package AppBundle\Form\Fellow
 */
class DutiesType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'dutyPrimaryActivities',
            CollectionType::class,
            ['entry_type' => DutyActivityType::class]
          )
          ->add(
            'dutySecondaryActivities',
            CollectionType::class,
            ['entry_type' => DutyActivitySecondaryType::class]
          )
          ->add(
            'addPrimary',
            SubmitType::class,
            ['label' => 'Add a Primary Activity']
          )
          ->add(
            'addSecondary',
            SubmitType::class,
            ['label' => 'Add a Secondary Activity']
          )
          ->add(
            'activityPrimaryIdToRemove',
            HiddenType::class,
            ['mapped' => false]
          )
          ->add(
            'activitySecondaryIdToRemove',
            HiddenType::class,
            ['mapped' => false]
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\FellowProject']
        );
    }
}
