<?php

namespace AppBundle\Form\Fellow\Financials;

use AppBundle\Entity\Fellow\Budget\BudgetOneTimeTotals;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class OneTimeTotalsType extends AbstractType {

	private $fields_opts = [
		'disabled' => true,
	];

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder
			->add('full_cost', null, $this->fields_opts)
			->add('host_contribution_monetary', null, $this->fields_opts)
			->add('post_contribution_gu')
			->add('post_contribution_non_gu', null, $this->fields_opts)
			->add('host_contribution_gu')
			->add('host_contribution_non_gu', null, $this->fields_opts)

			->add('post_contribution', null, $this->fields_opts)
			->add('eca_total_contribution', null, $this->fields_opts);
		;

		// Adds listener to convert currency formatted values to basic numbers
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
			$costItem = $event->getData();
			$form = $event->getForm();

			$costItem['post_contribution_gu'] = str_replace(array('$', ','), '', $costItem['post_contribution_gu']);
			$costItem['host_contribution_gu'] = str_replace(array('$', ','), '', $costItem['host_contribution_gu']);

			$event->setData($costItem);
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => BudgetOneTimeTotals::class,
		));
	}

}
