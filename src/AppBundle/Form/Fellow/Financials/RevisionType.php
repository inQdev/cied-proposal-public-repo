<?php

namespace AppBundle\Form\Fellow\Financials;

use AppBundle\Entity\Fellow\Budget\Revision;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RevisionType extends AbstractType
{

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('budgetCostItems', CollectionType::class, [
				'entry_type' => CostItemType::class,
				'label_format' => ' ',
				'required' => false
			])
			->add('budgetOneTimeTotals', OneTimeTotalsType::class)
			->add('projectGlobalValues', CollectionType::class, [
				'entry_type' => GlobalValuesType::class,
			])
			->add('changeDesc', CustomTextareaType::class, [
				'label' => 'Description/Justification',
				'required' => true,
			])
			->add('save', SubmitType::class, ['label' => 'Save'])
			->add('changed', HiddenType::class, [
				'mapped' => false,
				'data' => 0,
			])
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => Revision::class,
		));
	}

}
