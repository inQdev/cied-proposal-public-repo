<?php

namespace AppBundle\Form\Fellow\Financials;

use AppBundle\Entity\Fellow\Budget\ProjectGlobalValue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class GlobalValuesType extends AbstractType {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder
			->add('full_cost')
			->add('host_contribution_monetary')
			->add('post_contribution_gu')
			->add('post_contribution_non_gu', null, [
				'disabled' => true,
			])
			->add('host_contribution_gu')
			->add('host_contribution_non_gu', null, [
				'disabled' => true,
			])
			->add('post_contribution')
			->add('eca_total_contribution', null, [
				'disabled' => true,
			])
		;

		// Adds listener to convert currency formatted values to basic numbers
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
			$costItem = $event->getData();
			$form = $event->getForm();

			$costItem['full_cost'] = str_replace(array('$', ','), '', $costItem['full_cost']);
			$costItem['host_contribution_monetary'] = str_replace(array('$', ','), '', $costItem['host_contribution_monetary']);
			$costItem['post_contribution'] = str_replace(array('$', ','), '', $costItem['post_contribution']);
			$costItem['post_contribution_gu'] = str_replace(array('$', ','), '', $costItem['post_contribution_gu']);
			$costItem['host_contribution_gu'] = str_replace(array('$', ','), '', $costItem['host_contribution_gu']);

			$event->setData($costItem);
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => ProjectGlobalValue::class,
		));
	}

}
