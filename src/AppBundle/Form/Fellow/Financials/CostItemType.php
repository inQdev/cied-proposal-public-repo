<?php

namespace AppBundle\Form\Fellow\Financials;

use AppBundle\Entity\CostItemCustom;
use AppBundle\Entity\CostItemCustomInKind;
use AppBundle\Entity\CostItemOneTime;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CostItemType extends AbstractType {

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {

		$builder
			->add('host_contribution_in_kind')
			->add('post_contribution_non_gu', null, [
				'disabled' => true,
			])
			->add('host_contribution_non_gu', null, [
				'disabled' => true,
			])
		;

		// sets listener to add some fields dynamically depending on the type of funding organization
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
			$costItem = $event->getData();
			$form = $event->getForm();

			if ($costItem) {

				$gu_properties = [];
				if ($costItem->getCostItem() instanceof CostItemOneTime
					|| $costItem->getCostItem() instanceof CostItemCustom) {
					$gu_properties = [
						'disabled' => true,
					];
				}

				if (!($costItem->getCostItem() instanceof CostItemCustomInKind)) {
					$form->add('full_cost')
						->add('host_contribution_monetary')
						->add('post_contribution_gu', null, $gu_properties)
						->add('host_contribution_gu', null, $gu_properties)
					;

					$form
						->add('post_contribution')
						->add('eca_total_contribution', null, [
							'disabled' => true,
						]);

					// checks if an additional field must be shown to save the custom cost items
					if ($costItem->getCostItem() instanceof CostItemCustom) {
						$form
							->remove('host_contribution_in_kind');
					}
				}

			}

		});

		// Adds listener to convert currency formatted values to basic numbers
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
			$costItem = $event->getData();
			$form = $event->getForm();

			if (isset($costItem['full_cost'])) {
				$costItem['full_cost'] = str_replace(array('$', ','), '', $costItem['full_cost']);
				$costItem['host_contribution_monetary'] = str_replace(array('$', ','), '', $costItem['host_contribution_monetary']);
			}

			if (isset($costItem['host_contribution_in_kind'])) {
				$costItem['host_contribution_in_kind'] = str_replace(array('$', ','), '', $costItem['host_contribution_in_kind']);
			}

			if (isset($costItem['post_contribution'])) {
				$costItem['post_contribution'] = str_replace(array('$', ','), '', $costItem['post_contribution']);
			}

			if (isset($costItem['post_contribution_gu'])) {
				$costItem['post_contribution_gu'] = str_replace(array('$', ','), '', $costItem['post_contribution_gu']);
			}

			if (isset($costItem['host_contribution_gu'])) {
				$costItem['host_contribution_gu'] = str_replace(array('$', ','), '', $costItem['host_contribution_gu']);
			}

			$event->setData($costItem);
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
			'data_class' => FellowProjectBudgetCostItem::class,
		));
	}

}
