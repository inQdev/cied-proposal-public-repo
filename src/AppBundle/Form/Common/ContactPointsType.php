<?php

namespace AppBundle\Form\Common;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * ContactPointsType form.
 *
 * @package AppBundle\Form\Common
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContactPointsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'contactPoints',
            CollectionType::class,
            [
              'entry_type' => ContactPointType::class,
              'allow_add'  => true,
            ]
          )
          ->add(
            'additional',
            SubmitType::class,
            ['label' => 'Add Point of Contact']
          );
    }
}
