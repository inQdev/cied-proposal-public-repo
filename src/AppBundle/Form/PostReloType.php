<?php

namespace AppBundle\Form;

use AppBundle\EventListener\AddPostReloFieldsSubscriber;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * PostReloType form.
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PostReloType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'fellowProject',
            ProjectHouseInfoStartDateAndCommentType::class
          );

        $builder->addEventSubscriber(new AddPostReloFieldsSubscriber());
    }
}
