<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\Country;
use AppBundle\Entity\Post;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * PostsType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PostsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'posts',
            CollectionType::class,
            [
              'label'    => 'label.post',
              'required' => false,
            ]
          );

        $builder->addEventListener(
          FormEvents::POST_SET_DATA,
          function (FormEvent $event) {
              /* @var ProjectGeneralInfo $projectGeneralInfo */
              $projectGeneralInfo = $event->getData();

              // Current Countries associated to Project General Info
              $countries = $projectGeneralInfo->getCountries();

              // Current Posts associated to Project General Info
              $posts = $projectGeneralInfo->getPosts();

              $form = $event->getForm();

              // Loop over the countries associated to the Project General Info
              for ($i = 0, $n = $countries->count(); $i < $n; $i += 1) {
                  /** @var Country $country */
                  $country = $countries->get($i);

                  $postData = '';

                  /** @var Post $post */
                  foreach ($posts as $post) {
                      if ($country->getId() === $post->getCountry()->getId()) {
                          /** @var Post $countryPost */
                          foreach ($country->getPosts() as $countryPost) {
                              // This validation will check if current country
                              // has a Post that is associted to the Project
                              // General Info
                              if ($post->getId() === $countryPost->getId()) {
                                  $postData = $post;
                                  break 1;
                              }
                          }
                      }
                  }

                  $form->get('posts')->add(
                    $i,
                    EntityType::class,
                    [
                      'class'       => 'AppBundle:Post',
                      'choices'     => $country->getPosts(),
                      'label'       => $country->getName(),
                      'placeholder' => 'Select a post',
                      'required'    => false,
                      'mapped'      => false,
                      'data'        => $postData,
                    ]
                  );
              }
          }
        );
    }
}
