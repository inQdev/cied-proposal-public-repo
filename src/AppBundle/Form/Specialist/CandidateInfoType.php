<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CandidateInfoType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CandidateInfoType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('areasOfExpertise', null, [
				'expanded' => true,
				'multiple' => true,
			])
			->add('furtherDetailsDesc', CustomTextareaType::class)
//			->add('degreeRequirements', YesNoType::class)
			->add('degreeRequirementsDesc', CustomTextareaType::class)
			->add('proposedCandidate')
			->add('proposedCandidateContacted', YesNoType::class)
			->add('proposedCandidateEmail');

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

			/** @var SpecialistProjectPhase $data */
			$data = $event->getData();
			$form = $event->getForm();

//			$readonly = [];
//			if ($data->getCandidateSame() == true) {
//				$readonly = ['disabled' => true];
//			}

//			$areaExpertiseOpt = array_merge([
//				'expanded' => true,
//				'multiple' => true,
//			], $readonly);

			if ($data->getSourcePhase() != null) {
				$form
					->add('candidateSame', YesNoType::class)
					->add('candidateSameContacted', YesNoType::class);
			}
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\SpecialistProjectPhase']
		);
	}
}
