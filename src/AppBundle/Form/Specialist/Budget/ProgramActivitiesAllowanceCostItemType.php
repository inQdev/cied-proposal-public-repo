<?php

namespace AppBundle\Form\Specialist\Budget;

use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowanceCostItem;
use AppBundle\Form\Specialist\CostItemProgramActivityAllowanceDescriptionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ProgramActivitiesAllowanceCostItemType form..
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ProgramActivitiesAllowanceCostItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'costItem',
            CostItemProgramActivityAllowanceDescriptionType::class
          )
          ->add('totalCost')
          ->add('hostContributionMonetary')
          ->add('hostContributionInKind')
          ->add('postHostTotalContribution', null, ['disabled' => true])
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
                $paaCostItem = $event->getData();

                $form = $event->getForm();

                if (null !== $paaCostItem) {
                    $fundedBy = $paaCostItem->getProgramActivitiesAllowance()->getPhaseBudget()->getFundedBy();

                    if ('ECA' === $fundedBy) {
                        $form
                          ->add('postContribution')
                          ->add(
                            'ecaTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    } elseif ('Post' === $fundedBy) {
                        $form
                          ->add(
                            'postTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    }
                }
            }
          )
          ->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $paaCostItem = $event->getData();

                $paaCostItem['totalCost'] = str_replace(
                  ['$', ','],
                  '',
                  $paaCostItem['totalCost']
                );
                $paaCostItem['hostContributionMonetary'] = str_replace(
                  ['$', ','],
                  '',
                  $paaCostItem['hostContributionMonetary']
                );
                $paaCostItem['hostContributionInKind'] = str_replace(
                  ['$', ','],
                  '',
                  $paaCostItem['hostContributionInKind']
                );

                if (array_key_exists('postContribution', $paaCostItem)) {
                    $paaCostItem['postContribution'] = str_replace(
                      ['$', ','],
                      '',
                      $paaCostItem['postContribution']
                    );
                }

                $event->setData($paaCostItem);
            }
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => ProgramActivitiesAllowanceCostItem::class]
        );
    }
}
