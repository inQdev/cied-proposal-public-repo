<?php

namespace AppBundle\Form\Specialist\Budget;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * GroundTransportExpenseType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class GroundTransportExpenseType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('totalCost')
			->addEventListener(
				FormEvents::PRE_SUBMIT,
				function (FormEvent $event) {
					$groundTransportExpense = $event->getData();

					$groundTransportExpense['totalCost'] = str_replace(
						['$', ','],
						'',
						$groundTransportExpense['totalCost']
					);

					$event->setData($groundTransportExpense);
				}
			);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem']
		);
	}
}
