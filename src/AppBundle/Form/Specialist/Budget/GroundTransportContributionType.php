<?php

namespace AppBundle\Form\Specialist\Budget;

use AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GroundTransportContributionType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class GroundTransportContributionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('totalCost', null, ['disabled' => true])
          ->add('hostContributionMonetary')
          ->add('hostContributionInKind')
          ->add('postHostTotalContribution', null, ['disabled' => true])
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var GroundTransportCostItem $contributionCostItem */
	            $contributionCostItem = $event->getData();

                $form = $event->getForm();

                if (null !== $contributionCostItem) {
                    $fundedBy = $contributionCostItem->getPhaseBudget()->getFundedBy();

                    if ('ECA' === $fundedBy) {
                        $form
                          ->add('postContribution')
                          ->add(
                            'ecaTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    } elseif ('Post' === $fundedBy) {
                        $form
                          ->add(
                            'postTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    }
                }
            }
          )
          ->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $contributionCostItem = $event->getData();

                $contributionCostItem['hostContributionMonetary'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionCostItem['hostContributionMonetary']
                );
                $contributionCostItem['hostContributionInKind'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionCostItem['hostContributionInKind']
                );

                if (array_key_exists('postContribution', $contributionCostItem)) {
                    $contributionCostItem['postContribution'] = str_replace(
                      ['$', ','],
                      '',
                      $contributionCostItem['postContribution']
                    );
                }

                $event->setData($contributionCostItem);
            }
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem']
        );
    }
}
