<?php

namespace AppBundle\Form\Specialist\Budget;

use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpenseCostItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContributionLivingExpenseCostItemType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContributionLivingExpenseCostItemType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('totalCost', null, ['disabled' => true])
			->add('hostContributionMonetary')
			->add('hostContributionInKind')
			->add('postHostTotalContribution', null, ['disabled' => true])
			->addEventListener(
				FormEvents::PRE_SET_DATA,
				function (FormEvent $event) {
					/** @var ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem */
					$contributionLivingExpenseCostItem = $event->getData();

					if (null !== $contributionLivingExpenseCostItem) {
						$form = $event->getForm();

						$fundedBy = $contributionLivingExpenseCostItem->getContributionLivingExpense()->getPhaseBudget()->getFundedBy();

						if ('ECA' === $fundedBy) {
							$form
								->add('postContribution')
								->add(
									'ecaTotalContribution',
									null,
									['disabled' => true]
								);
						} elseif ('Post' === $fundedBy) {
							$form
								->add(
									'postTotalContribution',
									null,
									['disabled' => true]
								);
						}
					}
				}
			)
			->addEventListener(
				FormEvents::PRE_SUBMIT,
				function (FormEvent $event) {
					$contributionLivingExpenseCostItem = $event->getData();

					$contributionLivingExpenseCostItem['hostContributionMonetary'] = str_replace(
						['$', ','],
						'',
						$contributionLivingExpenseCostItem['hostContributionMonetary']
					);
					$contributionLivingExpenseCostItem['hostContributionInKind'] = str_replace(
						['$', ','],
						'',
						$contributionLivingExpenseCostItem['hostContributionInKind']
					);

					if (array_key_exists('postContribution', $contributionLivingExpenseCostItem)) {
						$contributionLivingExpenseCostItem['postContribution'] = str_replace(
							['$', ','],
							'',
							$contributionLivingExpenseCostItem['postContribution']
						);
					}

					$event->setData($contributionLivingExpenseCostItem);
				}
			);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpenseCostItem']
		);
	}
}
