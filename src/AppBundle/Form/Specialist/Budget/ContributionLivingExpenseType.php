<?php

namespace AppBundle\Form\Specialist\Budget;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContributionLivingExpenseType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContributionLivingExpenseType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add(
				'contributionLivingExpenseCostItems',
				CollectionType::class,
				[
					'entry_type' => ContributionLivingExpenseCostItemType::class
				]
			);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense']
		);
	}
}
