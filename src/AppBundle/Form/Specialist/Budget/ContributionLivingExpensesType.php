<?php

namespace AppBundle\Form\Specialist\Budget;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContributionLivingExpensesType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class ContributionLivingExpensesType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('contributionLivingExpenses', CollectionType::class, [
				'entry_type' => ContributionLivingExpenseType::class,
			])
			->add('contributionGroundTransport', GroundTransportContributionType::class)
			->add('calculate', SubmitType::class, [
				'label' => 'Calculate',
			]);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\SpecialistProjectPhaseBudget']
		);
	}
}
