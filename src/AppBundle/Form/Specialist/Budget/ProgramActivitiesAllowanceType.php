<?php

namespace AppBundle\Form\Specialist\Budget;

use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowance;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ProgramActivitiesAllowanceType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ProgramActivitiesAllowanceType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'programActivitiesAllowanceCostItems',
                CollectionType::class,
                ['entry_type' => ProgramActivitiesAllowanceCostItemType::class]
            )
            ->add(
                'calculate',
                SubmitType::class,
                [
                    'label' => 'Calculate',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            ['data_class' => ProgramActivitiesAllowance::class]
        );
    }
}
