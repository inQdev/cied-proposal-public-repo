<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * TitleRegionType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class TitleRegionType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('specialistProjectPhase', TitleType::class)
			->add('region', null, [
				'label' => 'label.region',
				'placeholder' => 'Select a region',
				'required' => false,
			]);
	}
}
