<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\ItineraryIndex;
use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Specialist\Itinerary\ItineraryIndexType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//use Symfony\Component\Validator\Constraints\Collection;
use AppBundle\Form\Specialist\Budget\GroundTransportExpenseType;

/**
 * LivingExpenseType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class LivingExpensesType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('livingExpenseExceptions', CustomTextareaType::class, [
				'property_path' => 'budget.livingExpenseExceptions',
				'label' => 'Are there any days when the Specialist will not need the Lodging allowance, for example because of overnight travel on a plane or train?  Please specify below, identifying day, city and reason.  In these instances, the Lodging amount will be adjusted accordingly when the Agreement is drawn up.',
				'required' => false,
			])
			->add('itineraryIndexes', CollectionType::class, [
				'entry_type' => ItineraryIndexType::class,
			])
			->add('contributionGroundTransport', GroundTransportExpenseType::class, [
				'property_path' => 'budget.contributionGroundTransport',
			])
			->add('calculate', SubmitType::class, [
				'label' => 'Calculate',
			]);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\SpecialistProjectPhase']
		);
	}
}
