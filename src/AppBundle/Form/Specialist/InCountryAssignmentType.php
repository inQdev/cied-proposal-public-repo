<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InCountryAssignmentType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('city', HiddenType::class)
//            ->add('country', HiddenType::class)
			->add('deliverablesDesc', CustomTextareaType::class, [
				'label' => 'Description of Deliverables:',
			])
			->add('hostDesc', CustomTextareaType::class, [
				'label' => 'Description of Host:',
			])
//            ->add('specialistProjectPhase')
//			->add('host', null, [
//				'label' => 'Select the Host Institution:',
//			])
			->add('newHost', TextType::class, [
				'label' => 'Or enter a Host Institution:',
				'mapped' => false,
				'required' => false,
			])
			->add('assignmentActivities', CollectionType::class, [
				'entry_type' => InCountryAssignmentActivityType::class,
				'label_format' => ' ',
				'by_reference' => false,
				'required' => false,
				'error_bubbling' => true,
				'allow_add' => true,
			])
			->add('deleteId', HiddenType::class, [
				'mapped' => false,
				'data' => '',
			])
			->add('add', SubmitType::class, [
				'label' => 'Add Activity',
			])
			->add('here', SubmitType::class, [
				'label' => 'Save',
			])
			->add('save', SubmitType::class, [
				'label' => 'Save and return to cities',
			])
		;

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

			$inCountryAssignment = $event->getData();

			$country = $inCountryAssignment->getCountry();

			$hosts = (null !== $country)
				? $country->getHosts()
				: [];

			$form = $event->getForm();

			if (null !== $inCountryAssignment) {
				$form->add(
					'host',
					EntityType::class,
					[
						'class' => 'AppBundle:Host',
						'choices' => $hosts,
						'label' => 'label.primary_host_assignment',
						'required' => false,
						'placeholder' => 'Select a Host Institution'
					]
				);
			}
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\InCountryAssignment'
		));
	}
}
