<?php

namespace AppBundle\Form\Specialist\Virtual\Itinerary;

use AppBundle\Entity\AreaOfExpertise;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\AnticipatedAudience;
use AppBundle\Entity\Specialist\Itinerary\ActivityType as ItineraryActivityType;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Specialist\Itinerary\Activity\AnticipatedAudienceType;
use AppBundle\Form\Type\CustomTextareaType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ActivityType form type.
 *
 * @package AppBundle\Form\Specialist\Virtual\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ActivityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var SpecialistProjectPhase $projectPhase */
        $projectPhase = $options['project_phase'];
        $audience = $options['audience'];

        $builder
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    /** @var Activity $activity */
                    $activity = $event->getData();
                    $form = $event->getForm();

                    $mapped = ($activity instanceof Activity);

                    $form
                        ->add(
                            'type',
                            EntityType::class,
                            [
                                'class' => ItineraryActivityType::class,
                                'placeholder' => 'Select Activity Type',
                                'required' => false,
                                'mapped' => $mapped,
                            ]
                        )
                        ->add(
                            'typeOther',
                            null,
                            ['required' => false, 'mapped' => $mapped]
                        )
                        ->add(
                            'typeDesc',
                            CustomTextareaType::class,
                            ['mapped' => $mapped]
                        )
                        ->add(
                            'onlineAvailabilityDesc',
                            CustomTextareaType::class,
                            ['mapped' => $mapped]
                        )
                        ->add(
                            'topics',
                            EntityType::class,
                            [
                                'class' => AreaOfExpertise::class,
                                'expanded' => true,
                                'multiple' => true,
                                'required' => false,
                                'mapped' => $mapped,
                            ]
                        )
                        ->add(
                            'topicOther',
                            null,
                            ['required' => false, 'mapped' => $mapped,]
                        )
                        ->add(
                            'anticipatedAudience',
                            CollectionType::class,
                            [
                                'entry_type' => AnticipatedAudienceType::class,
                                'required' => false,
                                'mapped' => false,
                            ]
                        )
                        ->add(
                            'audienceOther',
                            null,
                            ['required' => false, 'mapped' => false]
                        )
                        ->add(
                            'weeks',
                            CollectionType::class,
                            [
                                'entry_type' => HoursPerWeekType::class,
                                'required' => false,
                                'mapped' => false,
                            ]
                        )
                        ->add(
                            'add',
                            SubmitType::class,
                            ['label' => 'Save and add to Itinerary']
                        );
                }
            )
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                function (FormEvent $event) use ($projectPhase, $audience) {
                    /** @var Activity $activity */
                    $activity = $event->getData();
                    $form = $event->getForm();

                    if ($activity instanceof Activity) {
                        /** @var ArrayCollection $anticipatedAudience */
                        $anticipatedAudienceData = $activity->getAnticipatedAudience();
                        /** @var ArrayCollection $weeksData */
                        $activityWeeksData = $activity->getWeeks();

                        /**
                         * @var int      $index
                         * @var Audience $a
                         */
                        foreach ($audience as $index => $a) {
                            $data = null;

                            /** @var AnticipatedAudience $anticipatedAudience */
                            foreach ($anticipatedAudienceData as $anticipatedAudience) {
                                if ($a == $anticipatedAudience->getAudience()) {
                                    if ('Other' === $anticipatedAudience->getAudience()->getName()) {
                                        $form->get('audienceOther')->setData(
                                            $anticipatedAudience->getAudienceOther()
                                        );
                                    }

                                    $data = $anticipatedAudience;
                                    break;
                                }
                            }

                            $form->get('anticipatedAudience')->add(
                                $index,
                                AnticipatedAudienceType::class,
                                [
                                    'audience' => $a,
                                    'anticipated_audience_data' => $data,
                                ]
                            );
                        }

                        /**
                         * @var int  $index
                         * @var Week $week
                         */
                        foreach ($projectPhase->getVirtualItineraryWeeks() as $index => $w) {
                            $data = null;

                            /** @var $activityWeek ActivityWeek */
                            foreach ($activityWeeksData as $activityWeek) {
                                if ($w == $activityWeek->getWeek()) {
                                    $data = $activityWeek;
                                    break;
                                }
                            }

                            $form->get('weeks')->add(
                                $index,
                                HoursPerWeekType::class,
                                [
                                    'week' => $w,
                                    'activity_week_data' => $data
                                ]
                            );
                        }
                    } else {
                        /**
                         * @var int      $index
                         * @var Audience $a
                         */
                        foreach ($audience as $index => $a) {
                            $form->get('anticipatedAudience')->add(
                                $index,
                                AnticipatedAudienceType::class,
                                ['audience' => $a]
                            );
                        }

                        /**
                         * @var int  $index
                         * @var Week $week
                         */
                        foreach ($projectPhase->getVirtualItineraryWeeks() as $index => $week) {
                            $form->get('weeks')->add(
                                $index,
                                HoursPerWeekType::class,
                                ['week' => $week]
                            );
                        }
                    }
                }
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Activity::class,
                'project_phase' => null,
                'audience' => null,
            ]
        );
    }
}
