<?php

namespace AppBundle\Form\Specialist\Virtual\Itinerary;

use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * HoursPerWeekType form type.
 *
 * @package AppBundle\Form\Specialist\Virtual\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class HoursPerWeekType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Week $week */
        $week = $options['week'];
        /** @var ActivityWeek $activityWeekData */
        $activityWeekData = $options['activity_week_data'];
        $weekData = null;
        $duration = null;

        if (null !== $activityWeekData) {
            $weekData = new ArrayCollection([$activityWeekData->getWeek()]);
            $duration = $activityWeekData->getDuration();
        }

        $builder
            ->add(
                'week',
                EntityType::class,
                [
                    'class' => Week::class,
                    'choices' => [$week],
                    'data' => $weekData,
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false,
                ]
            )
            ->add(
                'hours',
                NumberType::class,
                [
                    'data' => $duration,
                    'required' => false,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'week' => null,
                'activity_week_data' => null,
            ]
        );
    }
}
