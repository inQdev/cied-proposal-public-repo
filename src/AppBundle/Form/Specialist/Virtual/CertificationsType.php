<?php

namespace AppBundle\Form\Specialist\Virtual;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * CertificationsType form.
 *
 * @package AppBundle\Form\Common
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CertificationsType extends BaseWizardStepType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'logisticsCertification',
                ChoiceType::class,
                [
                    'label' => 'label.rso_certification',
                    'choices' => [
                        'Post certifies that they will be responsible for program logistics.' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'monitoringCertification',
                ChoiceType::class,
                [
                    'choices' => [
                        'Post certifies that they are responsible for monitoring the Specialist’s work and schedule.' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            );
    }
}
