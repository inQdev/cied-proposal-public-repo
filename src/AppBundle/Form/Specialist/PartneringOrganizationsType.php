<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\Country;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * PartneringOrganizationsType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PartneringOrganizationsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'partneringOrganizations',
                CollectionType::class,
                [
                    'entry_type' => CountryPartneringOrganizationsType::class,
                    'required'   => false,
                    'mapped'     => false,
                ]
            )
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                function (FormEvent $event) {
                    /** @var SpecialistProjectPhase $projectPhase */
                    $projectPhase = $event->getData();

                    if (null !== $projectPhase) {
                        /** @var ProjectGeneralInfo $projectGeneralInfo */
                        $projectGeneralInfo = $projectPhase->getProjectGeneralInfo();

                        if (!$projectGeneralInfo->getCountries()->isEmpty()) {
                            /** @var Country $country */
                            foreach ($projectGeneralInfo->getCountries() as $country) {
                                $event
                                    ->getForm()
                                    ->get('partneringOrganizations')
                                    ->add(
                                        $country->getId(),
                                        CountryPartneringOrganizationsType::class,
                                        [
                                            'country'                  => $country,
                                            'specialist_project_phase' => $projectPhase,
                                            'label'                    => $country->getName(),
                                        ]
                                    );
                            }
                        }
                    }
                }
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SpecialistProjectPhase::class]);
    }
}
