<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\Specialist\Budget\BudgetBaseType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
//use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FundedByType type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class FundedByType extends BudgetBaseType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add('fundedBy',
            ChoiceType::class,
            [
              'choices'     => [
                'ECA'  => 'ECA',
                'Post' => 'Post',
              ],
              'expanded'    => true,
              'choice_attr' => function () {
                  return ['class' => 'yes_no_choice'];
              },
            ]
          )
          ->add('postFundingSource', CustomTextareaType::class);
    }

}
