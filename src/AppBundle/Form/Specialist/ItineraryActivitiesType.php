<?php

/**
 * NOTE: this is more like a dummy class to have the buttons in the Itinerary Activities page
 */

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\YesNoType;
use AppBundle\Form\Type\CustomTextareaType;

class ItineraryActivitiesType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);
		$builder
          ->add('restDayWeek', YesNoType::class)
          ->add(
            'restDayWeekDesc',
            CustomTextareaType::class,
            [
              'label' => 'If no, please explain.',
            ]
          )
          ->add('hoursRequiredDay', YesNoType::class)
          ->add(
            'hoursRequiredDayDesc',
            CustomTextareaType::class,
            [
              'label' => 'If no, please explain.',
            ]
          );
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
        $resolver->setDefaults(['data_class' => SpecialistProjectPhase::class]);
	}
}
