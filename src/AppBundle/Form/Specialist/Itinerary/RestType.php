<?php

namespace AppBundle\Form\Specialist\Itinerary;

use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Type\DateRangeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * RestType form type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RestType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'dateRanges',
            CollectionType::class,
            [
              'entry_type' => DateRangeType::class,
              'allow_add'  => true,
              'mapped'     => false,
              'required'   => false,
            ]
          )
          ->add(
            'addDate',
            SubmitType::class,
            ['label' => 'Add date or date range']
          )
          ->add(
            'add',
            SubmitType::class,
            ['label' => 'Save and add to Itinerary']
          )
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var SpecialistProjectPhase $projectPhase */
                $projectPhase = $event->getData();

                if (null !== $projectPhase) {
                    $form = $event->getForm();
                    $choices = [];

                    /** @var InCountryAssignment $assignment */
                    foreach ($projectPhase->getInCountryAssignments() as $assignment) {
                        /** @var Country $country */
                        $country = $assignment->getCountry();

                        $choices[$country->getName()][$assignment->getCity()] = $assignment->getId();
                    }

                    $form->add(
                      'inCountryAssignment',
                      ChoiceType::class,
                      [
                        'choices'     => $choices,
                        'placeholder' => 'Select a city',
                        'mapped'      => false,
                        'required'    => false,
                      ]
                    );
                }
            }
          )
          ->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {
                $event->getForm()->get('dateRanges')->add(
                  0,
                  DateRangeType::class
                );
            }
          );
    }
}
