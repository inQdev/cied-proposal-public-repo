<?php

namespace AppBundle\Form\Specialist\Itinerary\Activity;

use AppBundle\Entity\Specialist\Itinerary\Activity\AnticipatedAudience;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AnticipatedAudienceType
 *
 * @package AppBundle\Form\Specialist\Itinerary\Activity
 */
class AnticipatedAudienceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Audience $audience */
        $audience = $options['audience'];
        /** @var AnticipatedAudience $anticipatedAudienceData */
        $anticipatedAudienceData = $options['anticipated_audience_data'];
        $audienceData = null;
        $estimatedParticipants = null;

        if (null !== $anticipatedAudienceData) {
            $audienceData = new ArrayCollection([$anticipatedAudienceData->getAudience()]);
            $estimatedParticipants = $anticipatedAudienceData->getEstimatedParticipants();
        }

        $builder
          ->add(
            'audience',
            EntityType::class,
            [
              'class'    => Audience::class,
              'choices'  => [$audience],
              'expanded' => true,
              'multiple' => true,
              'required' => false,
              'data'     => $audienceData,
            ]
          )
          ->add(
            'estimatedParticipants',
            NumberType::class,
            ['required' => false, 'data' => $estimatedParticipants]
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['audience' => null, 'anticipated_audience_data' => null]
        );
    }
}
