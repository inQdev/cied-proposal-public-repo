<?php

namespace AppBundle\Form\Specialist\Itinerary;

use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\Specialist\Itinerary\Travel;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Type\CustomDateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * TravelEditType form type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class TravelEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('date', CustomDateType::class, ['required' => false])
          ->add('mode', null, ['required' => false])
          ->add('travelTime', null, ['required' => false])
          ->add(
            'bookingResponsible',
            ChoiceType::class,
            [
              'choices'     => Travel::getBookingResponsibleChoices(),
              'required'    => false,
              'placeholder' => 'Select',
            ]
          )
          ->add(
            'add',
            SubmitType::class,
            ['label' => 'Save and add to Itinerary']
          )
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var Travel $travel */
                $travel = $event->getData();

                if (null !== $travel) {
                    /** @var SpecialistProjectPhase $projectPhase */
                    $projectPhase = $travel->getOrigin()->getSpecialistProjectPhase();
                    $form = $event->getForm();
                    $choices = [];

                    /** @var InCountryAssignment $assignment */
                    foreach ($projectPhase->getInCountryAssignments() as $assignment) {
                        /** @var Country $country */
                        $country = $assignment->getCountry();

                        $choices[$country->getName()][$assignment->getCity()] = $assignment->getId();
                    }

                    $form
                      ->add(
                        'origin',
                        ChoiceType::class,
                        [
                          'choices'     => $choices,
                          'placeholder' => 'Select a city',
                          'required'    => false,
                          'mapped'      => false,
                          'data'        => $travel->getOrigin()->getId(),
                        ]
                      )
                      ->add(
                        'destination',
                        ChoiceType::class,
                        [
                          'choices'     => $choices,
                          'placeholder' => 'Select a city',
                          'required'    => false,
                          'mapped'      => false,
                          'data'        => $travel->getDestination()->getId(),
                        ]
                      );
                }
            }
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Travel::class]);
    }
}
