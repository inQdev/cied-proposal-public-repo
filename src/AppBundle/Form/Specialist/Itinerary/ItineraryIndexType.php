<?php

namespace AppBundle\Form\Specialist\Itinerary;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ItineraryIndexType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('date')
//			->add('setWay', HiddenType::class)
			->addEventListener(
				FormEvents::PRE_SET_DATA,
				function (FormEvent $event) {
					$data = $event->getData();
					/** @var SpecialistProjectPhase $projectPhase */
					$projectPhase = $data->getPhase();

					$form = $event->getForm();
					$choices = $projectPhase->getInCountryAssignments();

					$form->add(
						'inCountryAssignment',
						EntityType::class,
						[
							'class' => 'AppBundle:InCountryAssignment',
							'choices' => $choices,
							'placeholder' => 'Select a city',
							'required' => false,
							'choice_label' => function ($assignment) {
								return $assignment->getCity();
							},
							'group_by' => function ($val, $key, $index) {
								return $val->getCountry()->getName();
							},
						]
					);

				})

//			->addEventListener(
//				FormEvents::PRE_SUBMIT,
//				function (FormEvent $event) {
//					$data = $event->getData();
//					$form = $event->getForm();
//
//					if ($form->has('inCountryAssignment')) {
//						$oldCity = $form->get('inCountryAssignment')->getViewData()->getInCountryAssignment()->getId();
//						if ($oldCity != $data['inCountryAssignment']) {
//							dump($oldCity);
//							dump($data['inCountryAssignment']);
//							exit;
////			        	$form->get('setWay')->setData();
//						}
//					}
//	)
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\ItineraryIndex'
		));
	}
}
