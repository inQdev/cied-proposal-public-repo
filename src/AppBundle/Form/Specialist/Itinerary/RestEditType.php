<?php

namespace AppBundle\Form\Specialist\Itinerary;

use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\Specialist\Itinerary\Rest;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Type\CustomDateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * RestEditType form type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RestEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('date', CustomDateType::class, ['required' => false,])
          ->add(
            'add',
            SubmitType::class,
            ['label' => 'Save and add to Itinerary']
          )
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var Rest $rest */
                $rest = $event->getData();

                if (null !== $rest) {
                    /** @var SpecialistProjectPhase $projectPhase */
                    $projectPhase = $rest->getInCountryAssignment()->getSpecialistProjectPhase();
                    $form = $event->getForm();
                    $choices = [];

                    /** @var InCountryAssignment $assignment */
                    foreach ($projectPhase->getInCountryAssignments() as $assignment) {
                        /** @var Country $country */
                        $country = $assignment->getCountry();

                        $choices[$country->getName()][$assignment->getCity()] = $assignment->getId();
                    }

                    $form->add(
                      'inCountryAssignment',
                      ChoiceType::class,
                      [
                        'choices'     => $choices,
                        'placeholder' => 'Select a city',
                        'mapped'      => false,
                        'required'    => false,
                        'data'        => $rest->getInCountryAssignment()->getId(),
                      ]
                    );
                }
            }
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Rest::class]);
    }
}
