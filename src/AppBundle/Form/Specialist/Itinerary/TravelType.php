<?php

namespace AppBundle\Form\Specialist\Itinerary;

use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\Specialist\Itinerary\Travel;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Type\CustomDateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * TravelType form type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class TravelType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'date',
            CustomDateType::class,
            ['mapped' => false, 'required' => false]
          )
          ->add('mode', null, ['mapped' => false, 'required' => false])
          ->add('travelTime', null, ['mapped' => false, 'required' => false])
          ->add(
            'bookingResponsible',
            ChoiceType::class,
            [
              'choices'     => Travel::getBookingResponsibleChoices(),
              'mapped'      => false,
              'required'    => false,
              'placeholder' => 'Select',
              'choice_translation_domain' => false
            ]
          )
          ->add(
            'add',
            SubmitType::class,
            ['label' => 'Save and add to Itinerary']
          )
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var SpecialistProjectPhase $projectPhase */
                $projectPhase = $event->getData();

                if (null !== $projectPhase) {
                    $form = $event->getForm();
                    $choices = [];

                    /** @var InCountryAssignment $assignment */
                    foreach ($projectPhase->getInCountryAssignments(
                    ) as $assignment) {
                        /** @var Country $country */
                        $country = $assignment->getCountry();

                        $choices[$country->getName()][$assignment->getCity(
                        )] = $assignment->getId();
                    }

                    $cityFieldOpts = [
                      'choices'     => $choices,
                      'placeholder' => 'Select a city',
                      'mapped'      => false,
                      'required'    => false,
                    ];

                    $form
                      ->add('origin', ChoiceType::class, $cityFieldOpts)
                      ->add('destination',ChoiceType::class, $cityFieldOpts);
                }
            }
          );
    }
}
