<?php

namespace AppBundle\Form\Specialist\Mixed\Itinerary;

use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * VirtualComponentType form type.
 *
 * @package AppBundle\Form\Specialist\Mixed\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class VirtualComponentType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'mixedVirtualComponent',
            ChoiceType::class,
            [
                'choices' => SpecialistProjectPhase::getMixedVirtualComponentChoices(),
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
            ]
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SpecialistProjectPhase::class,
            ]
        );
    }
}
