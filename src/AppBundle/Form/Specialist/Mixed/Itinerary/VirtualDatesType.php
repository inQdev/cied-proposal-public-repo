<?php

namespace AppBundle\Form\Specialist\Mixed\Itinerary;

use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * VirtualDatesType form type.
 *
 * @package AppBundle\Form\Specialist\Mixed\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class VirtualDatesType extends BaseWizardStepType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('virtualDatesFlexible', YesNoType::class)
            ->add(
                'virtualDateLengthDesc',
                CustomTextareaType::class,
                ['label' => 'If yes, please explain']
            )
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    /** @var SpecialistProjectPhase $projectPhase */
                    $projectPhase = $event->getData();
                    $form = $event->getForm();
                    $mixedVirtualComponent = $projectPhase->getMixedVirtualComponent();

                    if (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_BEFORE === $mixedVirtualComponent) {
                        $form
                            ->add(
                                'virtualStartDateBefore',
                                CustomDateType::class,
                                ['label' => 'Estimated Start Date']
                            )
                            ->add(
                                'virtualEndDateBefore',
                                CustomDateType::class,
                                ['label' => 'Estimated End Date']
                            );
                    } elseif (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_AFTER === $mixedVirtualComponent) {
                        $form
                            ->add(
                                'virtualStartDateAfter',
                                CustomDateType::class,
                                ['label' => 'Estimated Start Date']
                            )
                            ->add(
                                'virtualEndDateAfter',
                                CustomDateType::class,
                                ['label' => 'Estimated End Date']
                            );
                    }  elseif (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_BEFORE_AND_AFTER === $mixedVirtualComponent) {
                        $form
                            ->add(
                                'virtualStartDateBefore',
                                CustomDateType::class,
                                ['label' => 'Estimated Start Date']
                            )
                            ->add(
                                'virtualEndDateBefore',
                                CustomDateType::class,
                                ['label' => 'Estimated End Date']
                            )
                            ->add(
                                'virtualStartDateAfter',
                                CustomDateType::class,
                                ['label' => 'Estimated Start Date']
                            )
                            ->add(
                                'virtualEndDateAfter',
                                CustomDateType::class,
                                ['label' => 'Estimated End Date']
                            );
                    }
                }
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SpecialistProjectPhase::class]);
    }
}
