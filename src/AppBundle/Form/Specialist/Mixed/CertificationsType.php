<?php

namespace AppBundle\Form\Specialist\Mixed;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * CertificationsType form.
 *
 * @package AppBundle\Form\Common
 * @author  Luke Torres <lucas.torres@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CertificationsType extends BaseWizardStepType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'rsoCertification',
                ChoiceType::class,
                [
                    'label' => 'label.rso_certification',
                    'choices' => [
                        'Post certifies that RSO has no objection' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'visaCertification',
                ChoiceType::class,
                [
                    'label' => 'label.visa_certification',
                    'choices' => [
                        'Post agrees to provide visa information' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'inCountryOrientationCertification',
                ChoiceType::class,
                [
                    'label' => 'label.in_country_orientation_certification',
                    'choices' => [
                        'Post agrees to arrange an in-country orientation' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'costSharingCertification',
                ChoiceType::class,
                [
                    'label' => 'label.cost_sharing_certification',
                    'choices' => [
                        'Post agrees to assume unfulfilled cost-sharing commitments' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'logisticsCertification',
                ChoiceType::class,
                [
                    'label' => 'label.logistics_certification',
                    'choices' => [
                        'Post certifies that they will be responsible for program logistics.' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            )
            ->add(
                'monitoringCertification',
                ChoiceType::class,
                [
                    'choices' => [
                        'Post certifies that they are responsible for monitoring the Specialist’s work and schedule.' => true,
                    ],
                    'placeholder' => 'Select',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                ]
            );
    }
}
