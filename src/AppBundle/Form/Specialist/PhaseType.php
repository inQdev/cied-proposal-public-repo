<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * PhaseType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PhaseType extends BaseWizardStepType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'type',
            ChoiceType::class,
            [
                'choices' => SpecialistProjectPhase::getTypeChoices(),
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'property_path' => 'specialistProjectPhase.type',
            ]
        );

	    $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

		    /** @var ProjectGeneralInfo $data */
		    $data = $event->getData();
		    $form = $event->getForm();

		    if ($data->getSpecialistProjectPhase()->getSourcePhase() != null) {
			    $form
				    ->add('title', TextType::class, [
					    'required' => false,
				    	'property_path' => 'specialistProjectPhase.title',
				    ]);
		    }
	    });
    }
}
