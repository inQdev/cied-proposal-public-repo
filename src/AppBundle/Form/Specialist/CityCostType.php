<?php

namespace AppBundle\Form\Specialist;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CityCostType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('livingExpenseCostitems', CollectionType::class, [
				'entry_type' => CityCostItemType::class
			])
			->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
				$livingExpense = $event->getData();
				$form = $event->getForm();

				$form->add('country', HiddenType::class, [
					'mapped' => false,
					'data' => $livingExpense->getInCountryAssignment()->getCountry()->getId(),
					]);

				if ($livingExpense->getInCountryAssignment()->getCity()) {
					$form
						->add('city', TextType::class, [
							'required' => false,
							'mapped' => false,
							'data' => $livingExpense->getInCountryAssignment()->getCity(),
						])
						->add('currentCity', HiddenType::class, [
							'mapped' => false,
							'data'  => $livingExpense->getInCountryAssignment()->getCity(),
						])
						->add('inCountryAssignmentId', HiddenType::class, [
							'mapped' => false,
							'data'  => $livingExpense->getInCountryAssignment()->getId(),
						]);
				} else {
					$form->add('city', TextType::class, [
						'required' => false,
						'mapped' => false,
					]);
				}
			});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
//			'data_class' => 'AppBundle\Entity\InCountryAssignment'
			'data_class' => 'AppBundle\Entity\LivingExpense'
		));
	}
}
