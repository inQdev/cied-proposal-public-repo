<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
/**
 * CityCostsType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

class CityCostsType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('cityCosts', CollectionType::class, [
				'entry_type' => CityCostType::class,
			])
			->add('deleteId', HiddenType::class, [
				'mapped' => false,
				'data' => '',
			])
			->add('here', SubmitType::class)
			->add('addCity', SubmitType::class)
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => null,
		]);
	}
}
