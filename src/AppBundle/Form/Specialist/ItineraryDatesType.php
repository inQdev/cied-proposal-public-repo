<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;

class ItineraryDatesType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'startDate',
            CustomDateType::class,
            [
              'label' => 'Start Date',
            ]
          )
          ->add(
            'endDate',
            CustomDateType::class,
            [
              'label' => 'End Date',
            ]
          )
          ->add('datesFlexible', YesNoType::class)
          ->add(
            'dateLengthDesc',
            CustomTextareaType::class,
            [
              'label' => 'If yes, please explain',
            ]
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SpecialistProjectPhase::class]);
    }
}
