<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\Country;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\Region;
use AppBundle\Form\BaseWizardStepType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * CountriesType type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CountriesType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('addCountry', SubmitType::class, ['label' => 'Add another country'])
            ->add(
                'deleteId',
                HiddenType::class,
                [
                    'mapped' => false,
                    'data'   => null,
                ]
            )
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    /* @var ProjectGeneralInfo $projectGeneralInfo */
                    $projectGeneralInfo = $event->getData();
                    /* @var Region $region */
                    $region = $projectGeneralInfo->getRegion();

                    if (null !== $region) {
                        $form = $event->getForm();

                        $form->add(
                            'country',
                            EntityType::class,
                            [
                                'class'         => Country::class,
                                'query_builder' => function (EntityRepository $er) use ($projectGeneralInfo, $region) {
                                    $qb = $er->createQueryBuilder('c');
                                    $qb2 = $er->createQueryBuilder('c2');

                                    // This query will retrieve all countries associated
                                    // to the Project from a particular region.
                                    $qb2
                                        ->select(['c2.id'])
                                        ->innerJoin('c2.projects', 'p')
                                        ->where(
                                            $qb2->expr()->andX(
                                                $qb2->expr()->eq('c2.region', ':region'),
                                                $qb2->expr()->eq('p', ':projectGeneralInfo')
                                            )
                                        );

                                    // This query will retrieve all countries that
                                    // aren't associated to the Project from a
                                    // particular region and that are associated to a
                                    // RELO Location.
                                    return $qb
                                        ->innerJoin('c.reloLocations', 'rl')
                                        ->where(
                                            $qb->expr()->andX(
                                                $qb->expr()->eq('c.region', ':region'),
                                                $qb->expr()->notIn('c.id', $qb2->getDQL())
                                            )
                                        )
                                        ->setParameters(
                                            [
                                                'region'             => $region,
                                                'projectGeneralInfo' => $projectGeneralInfo,
                                            ]
                                        )
                                        ->orderBy('c.name', 'ASC');
                                },
                                'label'         => 'label.country',
                                'placeholder'   => 'Select a country',
                                'required'      => false,
                                'mapped'        => false,
                            ]
                        );
                    }
                }
            );
    }
}
