<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\AreaOfExpertise;
use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use AppBundle\Entity\Specialist\Itinerary\ActivityType;
use AppBundle\Entity\Specialist\PartneringOrganization;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Specialist\Itinerary\Activity\AnticipatedAudienceType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\DateRangeType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ItineraryActivityType form type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ItineraryActivityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $audience = $options['audience'];

        $builder
	        ->add('activityType', EntityType::class, [
	        	'class' => 'AppBundle\Entity\Specialist\Itinerary\ActivityType',
		        'placeholder' => 'Select Activity Type',
	        	'mapped' => false,
		        'required' => false,
		    ])
          ->add(
            'activityTypeOther',
            null,
            ['mapped' => false, 'required' => false]
          )
          ->add('activityTypeDesc', CustomTextareaType::class, ['mapped' => false])
          ->add(
            'topics',
            EntityType::class,
            [
              'class'    => AreaOfExpertise::class,
              'expanded' => true,
              'multiple' => true,
              'required' => false,
              'mapped'   => false,
            ]
          )
          ->add('topicOther', null, ['mapped' => false, 'required' => false])
          ->add(
            'anticipatedAudience',
            CollectionType::class,
            [
              'entry_type' => AnticipatedAudienceType::class,
              'mapped'     => false,
              'required'   => false,
            ]
          )
          ->add('audienceOther', null, ['mapped' => false, 'required' => false])
          ->add('duration', null, ['mapped' => false, 'required' => false])
          ->add(
            'dateRanges',
            CollectionType::class,
            [
              'entry_type' => DateRangeType::class,
              'allow_add'  => true,
              'mapped'     => false,
              'required'   => false,
            ]
          )
          ->add(
            'partneringOrganizationNA',
            CheckboxType::class,
            ['label' => 'N/A', 'required' => false, 'mapped' => false]
          )
          ->add(
            'addDate',
            SubmitType::class,
            ['label' => 'Add date or date range']
          )
          ->add(
            'add',
            SubmitType::class,
            ['label' => 'Save and add to Itinerary']
          )
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var SpecialistProjectPhase $projectPhase */
                $projectPhase = $event->getData();
                $form = $event->getForm();
                $choices = [];

                /** @var InCountryAssignment $assignment */
                foreach ($projectPhase->getInCountryAssignments() as $assignment) {
                    /** @var Country $country */
                    $country = $assignment->getCountry();

                    $choices[$country->getName()][$assignment->getCity()] = $assignment->getId();
                }

                $form
                  // TODO: We can refactor this to be EntityType instead of ChoiceType.
                  ->add(
                    'inCountryAssignment',
                    ChoiceType::class,
                    [
                      'choices'     => $choices,
                      'placeholder' => 'Select a city',
                      'mapped'      => false,
                      'required'    => false,
                    ]
                  )
                  ->add(
                    'partneringOrganizations',
                    EntityType::class,
                    [
                      'class'       => PartneringOrganization::class,
                      'query_builder' => function (EntityRepository $er) use ($projectPhase) {
                          $qb = $er->createQueryBuilder('po');

                          return $qb
                              ->innerJoin('po.projectsPhases', 'pp')
                              ->where(
                                  $qb->expr()->andX(
                                      $qb->expr()->eq('pp.id', ':projectPhase'),
                                      $qb->expr()->eq('po.notApplicable', ':notApplicable')
                                  )
                              )
                              ->setParameters([
                                  ':projectPhase' => $projectPhase,
                                  ':notApplicable' =>  false
                              ]);
                      },
                      'group_by'    => 'country.name',
                      'expanded'    => true,
                      'multiple'    => true,
                      'mapped'      => false,
                      'required'    => false,
                    ]
                  );
            }
          )
          ->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($audience) {
                $form = $event->getForm();

                $form->get('dateRanges')->add(0, DateRangeType::class);

                /**
                 * @var int $index
                 * @var Audience $a
                 */
                foreach ($audience as $index => $a) {
                    $form->get('anticipatedAudience')->add(
                      $index,
                      AnticipatedAudienceType::class,
                      ['audience' => $a]
                    );
                }
            }
          )
	        ->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
				$form = $event->getForm();
		        $data = $event->getData();

		        // Since the form is not attached to a model, we need a workaround to check the
		        // selected activityType selected
		        $em = $event->getForm()->get('activityType')->getConfig()->getOption('em');

		        /** @var ActivityType $activity */
		        $activity = $em->getRepository('AppBundle:Specialist\Itinerary\ActivityType')->find($data['activityType']);

		        if ($activity) {
			        if ($activity->getName() != 'Other') {
				        $data['activityTypeOther'] = null;
		            }
		        }

//		        dump($data); exit;
		        $event->setData($data);

	        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['audience' => null]);
    }
}
