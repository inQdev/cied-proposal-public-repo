<?php

namespace AppBundle\Form\Specialist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CityCostItemType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('costPerDay');

		// Adds listener to convert currency formatted values to basic numbers
		$builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
			$costItem = $event->getData();
			$form = $event->getForm();

			$costItem['costPerDay'] = str_replace(array('$', ','), '', $costItem['costPerDay']);

			$event->setData($costItem);
		});
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\LivingExpenseCostItem'
		));
	}
}
