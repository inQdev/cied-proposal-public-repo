<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\Country;
use AppBundle\Entity\Specialist\PartneringOrganization;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CountryPartneringOrganizationsType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CountryPartneringOrganizationsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Country $country */
        $country = $options['country'];
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $options['specialist_project_phase'];

        $builder
            ->add(
                'partneringOrganizationCustom',
                TextType::class,
                [
                    'label'    => 'Or enter a new partnering organization',
                    'required' => false,
                    'mapped'   => false,
                ]
            )
            ->add(
                'addPartneringOrganization',
                SubmitType::class,
                ['label' => 'Add another partner']
            )
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($country, $specialistProjectPhase) {
                    $form = $event->getForm();

                    $form->add(
                        'partneringOrganization',
                        EntityType::class,
                        [
                            'class'         => PartneringOrganization::class,
                            'query_builder' => function (EntityRepository $er) use ($country, $specialistProjectPhase) {
                                $qb = $er->createQueryBuilder('po');

                                $qb
                                    ->where(
                                        $qb->expr()->andX(
                                        // Get all Partnering Organizations associated to
                                        // the given country
                                            $qb->expr()->in('po.country', ':countries')
                                        )
                                    )
                                    ->orderBy('po.notApplicable', 'DESC')
                                    ->addOrderBy('po.name');

                                if (!$specialistProjectPhase->getPartneringOrganizations()->isEmpty()) {
                                    // Do not show those ones that are already
                                    // associated to the project phase.
                                    $qb
                                        ->andWhere(
                                            $qb->expr()->notIn(
                                                'po.id',
                                                ':projectPhasePartneringOrganizations'
                                            )
                                        )
                                        ->setParameters(
                                            [
                                                ':countries'                           => $country,
                                                ':projectPhasePartneringOrganizations' => $specialistProjectPhase->getPartneringOrganizations(
                                                ),
                                            ]
                                        );
                                } else {
                                    $qb->setParameter(':countries', $country);
                                }

                                return $qb;
                            },
                            'required'      => false,
                            'placeholder'   => 'Select a partnering organization',
                            'empty_data'    => null,
                        ]
                    );
                }
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            ['country' => null, 'specialist_project_phase' => null]
        );
    }
}
