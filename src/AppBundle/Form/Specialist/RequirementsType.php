<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * RequirementsType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RequirementsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add('specialistProjectPhase', VisaRequirementsType::class)
          ->add(
            'visaRequirementsDesc',
            CustomTextareaType::class,
            ['label' => 'label.visa_requirements_desc']
          )
          ->add(
            'medicalRestrictionsDesc',
            CustomTextareaType::class,
            ['label' => 'label.medical_restrictions_desc']
          )
          ->add('ageRestrictionDesc', CustomTextareaType::class)
          ->add(
            'degreeRequirements',
            YesNoType::class,
            ['label' => 'label.degree_requirements']
          )
          ->add(
            'degreeRequirementsDesc',
            CustomTextareaType::class,
            ['label' => 'label.degree_requirements_desc',]
          )
          ->add('securityInfoDesc', CustomTextareaType::class)
          ->add('additionalRequirementsDesc', CustomTextareaType::class);

	    $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

		    /** @var ProjectGeneralInfo $data */
		    $data = $event->getData();
		    $form = $event->getForm();

		    if ($data->getSpecialistProjectPhase()->getSourcePhase() != null) {
			    $form
				    ->add('candidateSame', YesNoType::class, [
				    	'property_path' => 'specialistProjectPhase.candidateSame'
				    ])
				    ->add('candidateSameNewVisa', YesNoType::class, [
					    'property_path' => 'specialistProjectPhase.candidateSameNewVisa'
				    ]);
		    }
	    });
    }
}
