<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\SpecialistProjectPhase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FilterType form type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $facetSet = $options['facet_set'];

        $orderAndNames = [
            'region_s' => 'Region',
            'relo_locations_ss' => 'RELO Location',
            'countries_ss' => 'Country',
            'type_s' => 'Type',
            'proposal_status_s' => 'Proposal Status',
            'proposal_outcome_s' => 'Proposal Outcome',
        ];

        foreach ($orderAndNames as $facetKey => $name) {
            $facet = $facetSet->getFacet($facetKey);

            if (!$facet) {
                continue;
            }

            $choices = [];

            foreach ($facet->getValues() as $key => $value) {
                if ('type_s' === $facetKey) {
                    $sanitizedKey = array_search(
                        $key,
                        SpecialistProjectPhase::getTypeChoices(),
                        true
                    );

                    $label = sprintf('%s (%s)', $sanitizedKey, $value);
                } else {
                    $label = sprintf('%s (%s)', $key, $value);
                }

                if ($value) {
                    $choices[$label] = $key;
                }
            }

            if (!empty($choices)) {
                $builder
                    ->add(
                        $facetKey,
                        ChoiceType::class,
                        [
                            'label' => $name,
                            'choices' => $choices,
                            'choice_attr' => function ($val, $key, $index) {
                                $disabled = false;

                                if (strrpos($key, '(0)')) {
                                    $disabled = true;
                                }

                                return $disabled ? ['disabled' => 'disabled'] : [];
                            },
                            'expanded' => true,
                            'multiple' => true,
                        ]
                    );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(['method' => Request::METHOD_GET])
            ->setRequired(['facet_set']);
    }
}
