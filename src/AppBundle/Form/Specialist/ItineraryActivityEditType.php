<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\AreaOfExpertise;
use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\ItineraryActivity;
use AppBundle\Entity\Specialist\Itinerary\Activity\AnticipatedAudience;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use AppBundle\Entity\Specialist\PartneringOrganization;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Specialist\Itinerary\Activity\AnticipatedAudienceType;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ItineraryActivityType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Felipe Ceballos <juan.obando@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ItineraryActivityEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $audience = $options['audience'];

        $builder
	        ->add('activityType', EntityType::class, [
		        'class' => 'AppBundle\Entity\Specialist\Itinerary\ActivityType',
		        'placeholder' => 'Select Activity Type',
		        'required' => false,
	        ])
          ->add('activityTypeOther', TextType::class, ['required' => false])
          ->add(
            'topics',
            EntityType::class,
            [
              'class'    => AreaOfExpertise::class,
              'expanded' => true,
              'multiple' => true,
              'required' => false,
            ]
          )
          ->add('activityTypeDesc', CustomTextareaType::class)
          ->add('topicOther', null, ['required' => false])
          ->add(
            'anticipatedAudience',
            CollectionType::class,
            [
              'entry_type' => AnticipatedAudienceType::class,
              'mapped'     => false,
              'required'   => false,
            ]
          )
          ->add('audienceOther', null, ['mapped' => false, 'required' => false])
          ->add('duration', null, ['required' => false])
          ->add(
            'partneringOrganizationNA',
            CheckboxType::class,
            ['label' => 'N/A', 'required' => false, 'mapped' => false]
          )
          ->add('activityDate', CustomDateType::class, ['required' => false])
          ->add(
            'add',
            SubmitType::class,
            ['label' => 'Save and update Itinerary']
          )
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var ItineraryActivity $activity */
                $activity = $event->getData();
                /** @var SpecialistProjectPhase $projectPhase */
                $projectPhase = $activity->getInCountryAssignment()->getSpecialistProjectPhase();
                $form = $event->getForm();
                $choices = [];

                /** @var InCountryAssignment $assignment */
                $assignments = $projectPhase->getInCountryAssignments();

                foreach ($assignments as $assignment) {
                    /** @var Country $country */
                    $country = $assignment->getCountry();

                    $choices[$country->getName()][$assignment->getCity()] = $assignment->getId();
                }

                $form
                  ->add(
                    'inCountryAssignment',
                    ChoiceType::class,
                    [
                      'choices'     => $choices,
                      'placeholder' => 'Select a city',
                      'required'    => false,
                      'mapped'      => false,
                      'data'        => $activity->getInCountryAssignment()->getId(),
                    ]
                  )
                  ->add(
                    'partneringOrganizations',
                    EntityType::class,
                    [
                      'class'    => PartneringOrganization::class,
                      'choices'  => $projectPhase->getPartneringOrganizations(),
                      'group_by' => 'country.name',
                      'expanded' => true,
                      'multiple' => true,
                      'required' => false,
                    ]
                  );
            }
          )
          ->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($audience) {
                /** @var ItineraryActivity $activity */
                $activity = $event->getData();
                /** @var ArrayCollection $anticipatedAudience */
                $anticipatedAudienceData = $activity->getAnticipatedAudience();
                $form = $event->getForm();

                // Partnering Organization N/A option should be checked if
                // the Activity doesn't associated any Partnering Organization.
                $form->get('partneringOrganizationNA')->setData(
                  $activity->getPartneringOrganizations()->isEmpty()
                );

                /**
                 * @var int $index
                 * @var Audience $a
                 */
                foreach ($audience as $index => $a) {
                    $data = null;

                    /** @var AnticipatedAudience $anticipatedAudience */
                    foreach ($anticipatedAudienceData as $anticipatedAudience) {
                        if ($a == $anticipatedAudience->getAudience()) {
                            if ('Other' === $anticipatedAudience->getAudience()->getName()) {
                                $form->get('audienceOther')->setData(
                                  $anticipatedAudience->getAudienceOther()
                                );
                            }

                            $data = $anticipatedAudience;
                            break;
                        }
                    }

                    $form->get('anticipatedAudience')->add(
                      $index,
                      AnticipatedAudienceType::class,
                      [
                        'audience'                  => $a,
                        'anticipated_audience_data' => $data,
                      ]
                    );
                }
            }
          )
	        ->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) {
		        /** @var ItineraryActivity $data */
		        $data = $event->getData();
                $hasTopicOther = false;

//		        dump($data); exit;
		        /** @var ActivityType $activity */
		        $activity = $data->getActivityType();

		        if ($activity) {
			        if ($activity->getName() != 'Other') {
				        $data->setActivityTypeOther(null);
			        }
		        }

		        // Check if the `Other` option has been marked from the list.
                /** @var AreaOfExpertise $topic */
                foreach ($data->getTopics() as $topic) {
                    $hasTopicOther = 'Other' === $topic->getName();

                    if ($hasTopicOther) {
                        continue;
                    }
                }

                // If it hasn't (never was checked or was unchecked by the user)
                // the `Other topic` should be set as null.
                if (!$hasTopicOther) {
                    $data->setTopicOther(null);
                }

		        $event->setData($data);

	        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => ItineraryActivity::class, 'audience' => null]
        );
    }
}
