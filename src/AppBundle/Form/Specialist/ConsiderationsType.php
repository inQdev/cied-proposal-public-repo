<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * FundedByType type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ConsiderationsType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('additionalRequirementsDesc', CustomTextareaType::class);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(
			['data_class' => 'AppBundle\Entity\ProjectGeneralInfo']
		);
	}
}
