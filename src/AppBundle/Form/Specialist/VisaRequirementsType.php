<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\SpecialistProjectPhase;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisaRequirementsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'requiredVisaType',
            ChoiceType::class,
            [
              'choices'     => SpecialistProjectPhase::getRequiredVisaTypeChoices(),
              'placeholder' => 'Select an option',
              'required'    => false,
            ]
          )
          ->add('otherVisaType')
          ->add(
            'visaAvgLength',
            null,
            [
              'label'       => 'label.visa_avg_length',
              'placeholder' => 'Select an option',
            ]
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => SpecialistProjectPhase::class]);
    }
}
