<?php

namespace AppBundle\Form;

use AppBundle\Entity\Fellow\Budget\FellowProjectBudget;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReviewStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class FellowProjectBudgetBaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('next', SubmitType::class, ['label' => 'Save and continue']);

	    // sets listener to add some fields dynamically depending on the type of funding organization
	    $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
		    /** @var FellowProjectBudget $fellowProjectBudget */
		    $fellowProjectBudget = $event->getData();
		    $form = $event->getForm();

		    // default label for Save button
		    $label = 'Save and return to Panel';

		    /** @var ProjectGeneralInfo $projectGeneralInfo */
		    $projectGeneralInfo = $fellowProjectBudget->getFellowProject()->getProjectGeneralInfo();

		    // Changes the button label depending on the status of the proposal
		    if ($projectGeneralInfo->getProjectReviewStatus()) {

			    $roles = $this->convertRolesToArray($form->getConfig()->getOption('role'));

			    if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
				    && in_array('ROLE_EMBASSY', $roles)) {
				    $label = 'Save and return to Review';
			    }
		    }
		    // Adds the button
		    $form->add('save', SubmitType::class, ['label' => $label]);

	    });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FellowProjectBudget::class,
			'role' => ['ROLE_USER'],
        ]);
    }


	/**
	 * Helper function to convert a roles object into roles array
	 * @param $rolesObj
	 * @return array
	 */
	protected function convertRolesToArray($rolesObj)
	{
		$rolesArray = array();

		foreach ($rolesObj as $role) {
			$rolesArray[] = $role->getRole();
		}

		return $rolesArray;
	}

}
