<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * RegionBaseType type.
 *
 * @package AppBundle\Form
 */
class RegionBaseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('budget', null);

        $builder->addEventListener(
          FormEvents::PRE_SUBMIT,
          function (FormEvent $event) {
            $data = $event->getData();

            $data['budget'] = str_replace(
              ['$', ','],
              '',
              $data['budget']
            );

            $event->setData($data);
          }
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\Region']);
    }
}