<?php

namespace AppBundle\Form;

use AppBundle\EventListener\AddCountryFieldSubscriber;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * CountryType form.
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CountryType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventSubscriber(new AddCountryFieldSubscriber());
    }
}
