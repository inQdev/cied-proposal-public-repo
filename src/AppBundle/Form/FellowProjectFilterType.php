<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FellowProjectFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$facetSet = $options['facet_set'];

		$orderAndNames = [
			'region_s' => 'Region',
			'relo_location_s' => 'RELO Location',
			'country_s' => 'Country',
			'cycle_season_s' => 'Cycle',
			'proposal_status_s' => 'Proposal Status',
			'proposal_outcome_s' => 'Proposal Outcome',
		];

		foreach ($orderAndNames as $facetKey => $name) {
			$facet = $facetSet->getFacet($facetKey);
			if (!$facet) {
				continue;
			}

			$choices = [];
			foreach ($facet->getValues() as $key => $value) {
				$label = sprintf('%s (%s)', $key, $value);
				if ($value) {
					$choices[$label] = $key;
				}
			}

			if (count($choices)) {
				$builder
					->add($facetKey, ChoiceType::class, [
						'label' => $name,
						'choices' => $choices,
						'choice_attr' => function ($val, $key, $index) {
							$disabled = false;
							if (strrpos($key, '(0)')) {
								$disabled = true;
							}
							return $disabled ? ['disabled' => 'disabled'] : [];
						},
						'expanded' => true,
						'multiple' => true,
					]);
			}
		}
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'method' => 'GET',
		]);

		$resolver->setRequired([
			'facet_set',
		]);
	}
}
