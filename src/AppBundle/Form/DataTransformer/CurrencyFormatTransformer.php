<?php

namespace AppBundle\Form\DataTransformer;

//use AppBundle\Entity\FellowProjectBudgetCostItem;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class CurrencyFormatTransformer implements DataTransformerInterface
{
	private $manager;

	public function __construct(ObjectManager $manager)
	{
		$this->manager = $manager;
	}

	/**
	 * Transforms a number into a currency formatted string.
	 *
	 * @param  int $number
	 * @return string
	 */
	public function transform($number = null)
	{
		if (null === $number) {
			return 0;
		}

//		return number_format($number);
		return '$'.number_format($number);
	}

	/**
	 * Transforms a currency formatted string to a number.
	 *
	 * @param  string $formattedNumber
	 * @return int|null
	 */
	public function reverseTransform($formattedNumber = null)
	{
		if (!$formattedNumber) {
			return 0;
		}

		$number = str_replace(array('$', ','), '', $formattedNumber);
		return (int)$number;
	}
}