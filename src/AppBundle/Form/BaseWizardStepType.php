<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * BaseWizardStepType type.
 *
 * @package AppBundle\Form
 */
abstract class BaseWizardStepType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('back', SubmitType::class, ['label' => 'Save and go back'])
          ->add('save', SubmitType::class, ['label' => 'Save and return to Panel'])
          ->add('next', SubmitType::class, ['label' => 'Save and continue']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ProjectGeneralInfo']
        );
    }
}
