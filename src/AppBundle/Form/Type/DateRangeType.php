<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class DateRangeType
 *
 * @package AppBundle\Form\Type
 */
class DateRangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('startDate', CustomDateType::class, ['label' => 'Start Date'])
          ->add('endDate', CustomDateType::class, ['label' => 'End Date']);
    }
}
