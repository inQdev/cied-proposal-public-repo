<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CustomTextareaType
 *
 * @package AppBundle\Form\Type
 */
class CustomTextareaType extends AbstractType
{
    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['required' => false, 'attr' => ['rows' => '4']]
        );
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return TextareaType::class;
    }
}