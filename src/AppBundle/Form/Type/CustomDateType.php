<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CustomDateType form type.
 *
 * @package AppBundle\Form\Type
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CustomDateType extends AbstractType
{
    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          [
            'widget'         => 'single_text',
            'required'       => false,
            'html5'          => false,
            'format'         => 'MM/dd/yyyy',
//            'model_timezone' => 'America/New_York',
//            'view_timezone'  => 'America/New_York',
          ]
        );
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return DateType::class;
    }
}
