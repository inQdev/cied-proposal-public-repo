<?php

namespace AppBundle\Form\Admin;

use AppBundle\Entity\Country;
use AppBundle\Entity\Host;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * HostType form type.
 *
 * @package AppBundle\Form\Admin
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class HostType extends AbstractType
{
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name');

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
			$data = $event->getData();
			$form = $event->getForm();

			if ($data->getId() == null) {
				$form->add(
					'country',
					EntityType::class,
					[
						'class' => Country::class,
						'multiple' => false,
						'expanded' => false,
						'query_builder' => function (EntityRepository $er) {
							return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
						},
					]
				);
			}
		});
	}

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(['data_class' => Host::class]);
	}
}
