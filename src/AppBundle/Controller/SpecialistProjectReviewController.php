<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\ProjectOutcome;
use AppBundle\Entity\ProjectReviewComment;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\Specialist\CycleSpecialist;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReview;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\User;
use AppBundle\Form\Specialist\SubmitSpecialistProposalType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Specialist Project Review Controller
 *
 * @package AppBundle\Controller
 */
class SpecialistProjectReviewController extends Controller implements RoleSwitcherInterface
{
	/**
	 * @Route(
	 *   "/project/specialist/{id}/submit",
	 *   requirements={"id": "\d+"},
	 *   name="specialist_project_submit"
	 * )
	 * @Method("POST")
	 *
	 * @param Request $request
	 * @param SpecialistProjectPhase $specialistProjectPhase
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function submitAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
	{

		$authenticatedUser = $this->getUser();

		$form = $this->createForm(SubmitSpecialistProposalType::class);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			/** @var ProjectGeneralInfo $projectGeneralInfo */
			$projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

			if ($this->isGranted('ROLE_RELO')) {
				$reviewStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
				$submittedRole = 'RELO';
				$notificationAction = 'Submitted to RPO review';
			} else {
				$reviewStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
				$notificationAction = 'Submitted to RELO review';
				if ($this->isGranted('ROLE_RPO')) {
					$submittedRole = 'RPO';
				} elseif ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
					$submittedRole = 'ECA';
				} elseif ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {
					$submittedRole = 'Admin';
				} else {
					$submittedRole = 'Embassy';
				}
			}

			$projectGeneralInfo->setProjectReviewStatus(
				$em->getRepository('AppBundle:ProjectReviewStatus')->find($reviewStatus)
			);
			$projectGeneralInfo->setSubmittedBy($authenticatedUser); // Saves the user that submits proposal
			$projectGeneralInfo->setSubmittedByRole($submittedRole);
			$projectGeneralInfo->setSubmissionStatus(ProjectGeneralInfo::PROJECT_SUBMITTED);

//			$em->persist($projectGeneralInfo);

			$specialistProjectReview = new ProjectReview();
			// TODO: should relation between SpecialistProject and ProjectReview be 1:1?
			$specialistProjectReview->addSpecialistProject($specialistProjectPhase);

			$em->persist($specialistProjectReview);
			$em->flush();

			// TODO: Check notifications
			$this->sendNotifications($specialistProjectPhase, $authenticatedUser, $notificationAction, $submittedRole);

			// Update the Solr index
			// TODO: should be this placed in a EventListener? Service?
			$this->get('solr.client')->updateDocument($specialistProjectPhase);

			$this->addFlash(
				'success',
				'The Proposal has been submitted successfully.'
			);
		}

		return $this->redirectToRoute('specialist_project_phase_show', ['id' => $specialistProjectPhase->getId()]);
	}

	/**
	 * NOTE 20161109: This action is called from the Review page, it was separated from the submission to be able to avoid remote
	 * check of the user role (bypassing a request against the auth provider/Drupal)
	 * The bypass instruction is in the RoleSwitcherListener
	 *
	 * @Route("/project/specialist/{id}/review", name="specialist_project_review")
	 * @Method({"POST"})
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function reviewAction(SpecialistProjectPhase $projectPhase, Request $request)
	{
		$authenticatedUser = $this->getUser();
		$reviewComment = new ProjectReviewComment();
		$reviewComment->setCommentBy($authenticatedUser);
		$reviewComment->setProjectReview($projectPhase->getSpecialistProjectReviews()->first());

		$reviewForm = $this->createReviewCommentForm($reviewComment, $projectPhase->getProjectGeneralInfo());

		// Get Budget Summary info to show in page
		$projectPhaseBudgetTotals = null;
		$monthlyCostItems = $oneTimeCostItems = [];
		$totals = 0;

//		if ($projectPhase->getBudget()) {
//			// Gets monthly and onetime cost items
//			$monthlyCostItems = $this->getDoctrine()->getRepository(
//				'AppBundle:CostItemMonthly'
//			)->findAll();
//			$oneTimeCostItems = $this->getDoctrine()->getRepository(
//				'AppBundle:CostItemOneTime'
//			)->findAll();
//
//			// calculations for budget section
//			$totals = $projectPhase->getBudget()->getSummaryTotals();
//			$projectPhaseBudgetTotals = $projectPhase->getBudget()->getBudgetTotals()->first();
//		}

		return $this->render(
			'project/specialist/review/review_form.html.twig',
			[
				'projectPhase' => $projectPhase,
				'projectPhaseReview' => $projectPhase->getSpecialistProjectReviews()->first(),
				'projectBudgetTotals' => $projectPhaseBudgetTotals,
				'reviewForm' => ($reviewForm) ? $reviewForm->createView() : null,
				'totals' => $totals,
//				'specialistProject' => $projectPhase->getSpecialistProject(),
				'phase' => $projectPhase->getSpecialistProject()->getPhases()->indexOf($projectPhase),
			]
		);
	}

	/**
	 * This function only processes submissions from Review form and redirects to the show page
	 *
	 * @Route("/project/specialist/{id}/process-review", name="specialist_project_process_review")
	 * @Method({"POST"})
	 *
	 * @param SpecialistProjectPhase $projectPhase
	 * @param $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function processReviewAction(SpecialistProjectPhase $projectPhase, Request $request)
	{
		$authenticatedUser = $this->getUser();
		$reviewComment = new ProjectReviewComment();
		$reviewComment->setCommentBy($authenticatedUser);
		$reviewComment->setProjectReview($projectPhase->getSpecialistProjectReviews()->first());
		$projectGeneralInfo = $projectPhase->getProjectGeneralInfo();

		$reviewForm = $this->createReviewCommentForm($reviewComment, $projectGeneralInfo);

		if ($reviewForm) {
			$reviewForm->handleRequest($request);

			if ($reviewForm->isValid()) {
				$em = $this->getDoctrine()->getManager();

				// Changes things depending on the user role
				$proposalStatus = $role = $commentAction = '';
				$outcome = null;
				$addComment = true;

				if ($this->isGranted('ROLE_EMBASSY')) {
					$role = 'Embassy';
				} else if ($this->isGranted('ROLE_RELO')) {
					$role = 'RELO';
				} else if ($this->isGranted('ROLE_RPO')) {
					$role = 'RPO';
				} else if ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
					$role = 'ECA';
					if ($reviewForm->has('save') && $reviewForm->get('save')->isClicked()) {
						// Outcome
						if ($reviewForm->get('outcome')->getViewData() !== '') {
							$outcome = $reviewForm->get('outcome')->getViewData();
							$commentAction = $em->getRepository('AppBundle:ProjectOutcome')->find($outcome)->getName();
						} else {
							$projectGeneralInfo->setProjectOutcome(null);
//							$commentAction = 'Outcome reset';
						}
					}

				} else if ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {

					// adds a note ONLY if the Admin fills in the comment field
					$role = 'Admin';
//					if ($reviewComment->getComment()) {
					if (!$reviewForm->get('save')->isClicked()) {
						$commentAction = 'Admin Comment'; // This may change below depending on button clicked
					} else {
						$addComment = false;
					}

					// Adjusts values on proposal
					// Outcome
					if ($reviewForm->get('outcome')->getViewData() !== '') {
						$outcome = $reviewForm->get('outcome')->getViewData();
					} else {
						$projectGeneralInfo->setProjectOutcome(null);
					}

					// Review Status
					if ($reviewForm->get('reviewStatus')->getViewData() !== '') {
						$proposalStatus = $reviewForm->get('reviewStatus')->getViewData();
					}

					// Cycle
					$projectGeneralInfo->setCycle(
						$em->getRepository('AppBundle:Specialist\CycleSpecialist')->find($reviewForm->get('cycle')->getViewData())
					);

					// Submission Status
					$projectGeneralInfo->setSubmissionStatus($reviewForm->get('submissionStatus')->getViewData());

				}

				$currentProposalStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();

				switch ($currentProposalStatus) {

					case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
						// Submission handler for setting alternate funding.
						if ($reviewForm->has('accept') && $reviewForm->get('accept')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Funding changed';
							$outcome = ProjectOutcome::ALTERNATE_FUNDING_PROVIDED;
						} // Submission handler when Embassy declines alternate funding.
						elseif ($reviewForm->has('decline') && $reviewForm->get('decline')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Alternate Funding not accepted'; // to avoid confusion with the outcome set by ECA
							$outcome = ProjectOutcome::ALTERNATE_FUNDING_UNAVAILABLE;
						}
						break;

					case ProjectReviewStatus::UNDER_POST_RE_REVIEW:

						// Submission handler for re-review proposal.
						if ($reviewForm->has('submit') && $reviewForm->get('submit')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
//							$commentAction = 'Submitted to RELO review';
							$commentAction = 'Re-submitted to RELO review';
						}
						break;

					case ProjectReviewStatus::UNDER_RELO_REVIEW:

						if ($reviewForm->get('approve')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
							$commentAction = 'Submitted to RPO review';
						} else if ($reviewForm->get('return')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_POST_RE_REVIEW;
							$commentAction = 'Returned to Post for revision';
						}
						break;

					case ProjectReviewStatus::UNDER_RPO_REVIEW:

						if ($reviewForm->get('approve')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Review Complete';
						} else if ($reviewForm->get('ask_eca')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_ECA_REVIEW;
							$commentAction = 'Submitted to ECA for review';
						} else if ($reviewForm->get('return_relo')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
							$commentAction = 'Returned to RELO for revision';
						} else if ($reviewForm->get('return_post')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_POST_RE_REVIEW;
							$commentAction = 'Returned to Post for revision';
						}
						break;

					case ProjectReviewStatus::REVIEW_COMPLETE:
					case ProjectReviewStatus::UNDER_ECA_REVIEW:

						if ($reviewForm->get('return')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
							$commentAction = 'Returned to RPO for revision';
							$projectGeneralInfo->setProjectOutcome(null);
						} else if ($reviewForm->get('alternate_funding')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING;
							$commentAction = 'Alternate funding requested';
							$projectGeneralInfo->setProjectOutcome(null);
						}
						break;
				}

				if ($proposalStatus !== '') {
					$projectGeneralInfo->setProjectReviewStatus(
						$em->getRepository('AppBundle:ProjectReviewStatus')->find($proposalStatus)
					);
				}

				if ($outcome !== null) {
					$projectGeneralInfo->setProjectOutcome(
						$em->getRepository('AppBundle:ProjectOutcome')->find($outcome)
					);
				}

				// Saves changes to the DB
//				$em->persist($projectGeneralInfo);

				// Notifications (if simple Admin comment, avoid sending email message)
				if ($commentAction && $commentAction != 'Admin Comment') {
					$this->sendNotifications($projectPhase, $authenticatedUser, $commentAction, $role);
				}

				// Sets values for comment and save
				if ($addComment) {
					$reviewComment->setCommentBy($authenticatedUser);
					$reviewComment->setActionTaken($commentAction);
					$reviewComment->setCommentByRole($role);
					$em->persist($reviewComment);
				}
				$em->flush();

				// Update the Solr index
				// TODO: should be this placed in a EventListener? Service?
				$this->get('solr.client')->updateDocument($projectPhase);

				// TODO: The JSON generation should be placed somewhere else (service manager, maybe?)
				// If outcome is `Approved by ECA`, the project should appear
				// in the list of Rosters project list (Application System);
				// the JSON with projects should be updated.
				if ((ProjectOutcome::SELECTED_FOR_ECA_FUNDING === $outcome
						|| ProjectOutcome::ALTERNATE_FUNDING_PROVIDED === $outcome)
					&& $projectGeneralInfo->getProjectReviewStatus()->getId() === ProjectReviewStatus::REVIEW_COMPLETE) {
					// The project will be added to the JSON for the cycle it
					// has associated.
					/** @var CycleSpecialist $specialistCycle */
					$specialistCycle = $projectPhase->getProjectGeneralInfo()->getCycle();

					// Projects for the cycle (2016 - 2017) were added manually.
					// This validation will avoid the Proposal System to
					// overwrite the JSON file with those projects.
//					if ((null !== $specialistCycle->getTid()) && ($specialistCycle->getTid() > 5147)) {
//						$fs = new Filesystem();
//
//						$rosterPath = $this->getParameter(
//							'application_system_roster_dir'
//						);
//
//						$projectPhases = $em->getRepository('AppBundle:SpecialistProject')->findAllApprovedInCycle($specialistCycle);
//
//						// Check if the path where the JSON should be placed exits.
//						if ($fs->exists($rosterPath)) {
//							$approvedFellowProjects = [];
//
//							/** @var FellowProject $approvedFellowProject */
//							foreach ($projectPhases as $approvedFellowProject) {
//								/** @var ProjectGeneralInfo $projectGeneralInfo */
//								$projectGeneralInfo = $approvedFellowProject->getProjectGeneralInfo();
//
//								/** @var Country $country */
//								$country = $projectGeneralInfo->getCountries()->last();
//								$countryName = $country->getName();
//
//								/** @var LocationHostInfo $locationHostInfo */
//								$locationHostInfo = $approvedFellowProject->getLocationHostInfo();
//
//								// This validation will avoid adding the
//								// Country-wide project more than once.
//								if (!array_key_exists($countryName, $approvedFellowProjects)) {
//									$approvedFellowProjects[$countryName]['name'] = $countryName;
//									$approvedFellowProjects[$countryName]['ISO_code'] = $country->getIso2Code();
//
//									// Every country should have a Country-wide
//									// project.
//									$approvedFellowProjects[$countryName]['projects'][] = [
//										'id' => 100,
//										'name' => 'Country-wide',
//									];
//								}
//
//								// Make sure the Host instance has set the `name`
//								// property.
//								$host = (null !== $locationHostInfo->getHost())
//									? $locationHostInfo->getHost()->getName()
//									: '';
//
//								$referenceNumber = (null !== $projectGeneralInfo->getReferenceNumber())
//									? $projectGeneralInfo->getReferenceNumber()
//									: '';
//
//								$relo = '';
//								$reloEmail = '';
//
//								// Check if there are RELO users for RELO
//								// Location associated to the project.
//								if (!$projectGeneralInfo->getFirstRELOLocation()->getRelos()->isEmpty()) {
//									// There could be more than one user
//									// associated to the RELO Location; the
//									// first user will be the one included in
//									// the JSON file.
//									/** @var User $reloUser */
//									$reloUser = $projectGeneralInfo->getFirstRELOLocation()->getRelos()->first();
//
//									$relo = trim($reloUser->getName());
//									$reloEmail = trim($reloUser->getEmail());
//								}
//
//								$approvedFellowProjects[$countryName]['projects'][] = [
//									'id' => $approvedFellowProject->getId(),
//									'name' => trim("$host ($referenceNumber)"),
//									'region' => $projectGeneralInfo->getRegion()->getName(),
//									'relo' => $relo,
//									'relo_email' => $reloEmail,
//								];
//							}
//
//							// The array cannot be an Associative one: Country names
//							// are not expected as keys in the JSON file.
//							$approvedFellowProjects = array_values($approvedFellowProjects);
//
//							$jsonFileName = "cied_roster_cycle_{$fellowCycle->getTid()}.json";
//
//							$jsonFilePath = $rosterPath . DIRECTORY_SEPARATOR . $jsonFileName;
//
//							// The JSON file is going to be completely replaced
//							// with the content just generated.
//							if ($fs->exists($jsonFilePath)) {
//								$fs->remove($jsonFilePath);
//							}
//
//							$fs->dumpFile(
//								$jsonFilePath,
//								json_encode(
//									['countries' => $approvedFellowProjects]
//								)
//							);
//						}
//					}
				}

			}
		}

		return $this->redirectToRoute(
			'specialist_project_phase_show', [
				'id' => $projectPhase->getId()
			]
		);
	}

	/**
	 * Creates the form (or not) depending on the Proposal Status and the user role set on the session
	 *
	 * @param ProjectReviewComment $reviewComment
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 *
	 * @return mixed (Form or null)
	 */
	private function createReviewCommentForm($reviewComment, $projectGeneralInfo)
	{
//		$token = $this->get('security.token_storage')->getToken();
		$projectReviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();

		$formBuilder = $form = null;
		$formOptions = array(
			'action' => $this->generateUrl('specialist_project_process_review',
				['id' => $projectGeneralInfo->getSpecialistProjectPhase()->getId()]
			),
		);

		$commentFieldLabel = $this->get('translator')->trans('Add comment');

		if ($this->isGranted('review', $projectGeneralInfo)) {

			// Creates empty formBuilder
			$formBuilder = $this->createFormBuilder($reviewComment, $formOptions);

			// When the user is ADMIN, creates a special form
			if ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {

				$formBuilder
					->add('cycle', EntityType::class, [
						'class' => 'AppBundle:Specialist\CycleSpecialist',
						'data' => $projectGeneralInfo->getCycle(),
						'choice_label' => function (CycleSpecialist $cycle) {
							return $cycle->getName();
						},
						'mapped' => false,
						'label' => 'Cycle',
					])
					->add('reviewStatus', EntityType::class, [
						'class' => ProjectReviewStatus::class,
						'choice_label' => function (ProjectReviewStatus $reviewStatus) {
							return $reviewStatus->getName();
						},
						'data' => $projectGeneralInfo->getProjectReviewStatus(),
						'mapped' => false,
						'label' => 'Proposal Review Status',
					])
					->add('outcome', EntityType::class, [
						'class' => ProjectOutcome::class,
						'choice_label' => function (ProjectOutcome $projectOutcome) {
							return $projectOutcome->getName();
						},
						'data' => $projectGeneralInfo->getProjectOutcome(),
						'mapped' => false,
						'required' => false,
						'label' => 'Proposal Outcome',
						'placeholder' => 'Not defined',
					])
					->add('submissionStatus', ChoiceType::class, [
						'choices' => ['Submitted' => 'submitted', 'Not Submitted' => 'not_submitted'],
						'data' => $projectGeneralInfo->getSubmissionStatus(),
						'mapped' => false,
						'label' => 'Proposal Submission Status',
					])
					->add('save', SubmitType::class, [
						'label' => 'Save',
					]);

				// Adds a special Submit comment button for the end of the form
				$formBuilder->add('adminComment', SubmitType::class, [
					'label' => 'Send Admin Comment',
					'attr' => array('class' => 'm-5 btn btn-success'),
				]);
			}

			switch ($projectReviewStatus) {
				case ProjectReviewStatus::UNDER_RELO_REVIEW:
//					$formBuilder = $this->createFormBuilder($reviewComment, $formOptions)
					$formBuilder
						->add('approve', SubmitType::class, [
							'label' => 'Submit to RPO Review',
							'attr' => array('class' => 'm-5 btn btn-success'),
						])
						->add('return', SubmitType::class, [
							'label' => 'Return to Post for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						]);
					break;

				case ProjectReviewStatus::UNDER_RPO_REVIEW:
					$formBuilder
						->add('approve', SubmitType::class, [
							'label' => 'Review Complete',
							'attr' => array('class' => 'm-5 btn btn-success'),
						])
						->add('ask_eca', SubmitType::class, [
							'label' => 'Ask ECA',
							'attr' => array('class' => 'm-5 btn btn-default'),
						])
						->add('return_relo', SubmitType::class, [
							'label' => 'Return to RELO for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('return_post', SubmitType::class, [
							'label' => 'Return to Post for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						]);
					break;

				case ProjectReviewStatus::UNDER_POST_RE_REVIEW:
					$formBuilder
						->add('submit', SubmitType::class, [
							'label' => 'Submit to RELO review',
							'attr' => array('class' => 'm-5 btn btn-success'),
						]);
					break;

				case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
					$acceptBtnDisabled = 'ECA' === $projectGeneralInfo->getSpecialistProjectPhase()->getBudget()->getFundedBy();
					$acceptBtnClasses = 'm-7 btn btn-success';

					if ($acceptBtnDisabled) {
						$acceptBtnClasses .= ' disabled';
					}

					$formBuilder
						->add('accept', SubmitType::class, [
							'label' => 'Alternate Funding Provided',
							'attr' => array('class' => $acceptBtnClasses),
							'disabled' => $acceptBtnDisabled
						])
						->add('decline', SubmitType::class, [
							'label' => 'Alternate Funding Unavailable',
							'attr' => array('class' => 'm-5 btn btn-danger '),
						]);

//					$commentFieldLabel = $this->get('translator')->trans('Specify alternate funding');

					break;

				case ProjectReviewStatus::REVIEW_COMPLETE:

					if ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
						$formBuilder
							->add('outcome', EntityType::class, [
								'class' => 'AppBundle:ProjectOutcome',
								'choice_label' => function (ProjectOutcome $projectOutcome) {
									return $projectOutcome->getName();
								},
								'data' => $projectGeneralInfo->getProjectOutcome(),
								'mapped' => false,
								'required' => false,
								'label' => 'Proposal Outcome',
								'placeholder' => 'Not defined',
							])
							->add('save', SubmitType::class, [
								'label' => 'Save',
							]);
					}

					$formBuilder
						->add('return', SubmitType::class, [
							'label' => 'Return to RPO for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('alternate_funding', SubmitType::class, [
							'label' => 'Request alternate funding',
							'attr' => array('class' => 'm-5 btn btn-default '),
						]);
					break;

				case ProjectReviewStatus::UNDER_ECA_REVIEW:
					$formBuilder
						->add('return', SubmitType::class, [
							'label' => 'Return to RPO for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('alternate_funding', SubmitType::class, [
							'label' => 'Request alternate funding',
							'attr' => array('class' => 'm-5 btn btn-default '),
						]);
					break;

			}

			$formBuilder
				->add('comment', CustomTextareaType::class, [
					'label' => $commentFieldLabel,
					'required' => false,
				]);

			$form = $formBuilder
				->getForm();
		}

		return $form;

	}

	/**
	 * @param SpecialistProjectPhase $projectPhase
	 * @param User $user
	 * @param string $action
	 * @param string $role
	 *
	 * @return null|array
	 */
	public function sendNotifications(SpecialistProjectPhase $projectPhase, User $user, $action, $role)
	{
		$projectGeneralInfo = $projectPhase->getProjectGeneralInfo();
//		$specialistProject = $projectPhase->getSpecialistProject();
//		$em = $this->getDoctrine()->getManager();
		$recipients = array();
		$template = 'proposal_review';
		$subject = 'EL Specialist proposal - ';

		// Calls the notification service
		$notification = $this->get('proposal.helper.notification_helper');
//		$notification->sendFellowRankingNotifications($user, 'notify-eca');

		switch ($action) {

			// TODO-Felipe: The template is just a clone of another one, it must be redone with client's input
			case 'Submitted to ECA for review':
				$subject .= 'submitted to ECA review';
				$template = 'eca_asked';
				$recipients[] = $this->getParameter('eca_email_address');
				break;

			case 'Submitted to RELO review':
				$subject .= 'ready for RELO review';
				$template = 'relo_review';
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo));
				break;

			case 'Submitted to RPO review':
				$subject .= 'ready for RPO review';
				$template = 'rpo_review';
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));
				break;

			case 'Review Complete':
				$subject .= 'review complete';
				$template = 'proposal_approved';

				// Changes on recipients on 7/7/2016
				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$recipients[] = $projectGeneralInfo->getSubmittedBy()->getEmail();
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo));

				if ($role != 'RPO') {
					$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));
				}
				if ($role != 'ECA') {
					$recipients[] = $this->getParameter('eca_email_address');
				}
				break;

			case 'Returned to Post for revision':
				$subject .= "returned for revision";
				$template = 'post_revision';
				$recipients = array_merge($recipients, $this->getRecipients('author', $projectGeneralInfo));

				// This will notify RELOs when a Proposal is sent back to Post Re-review.
				// (Only notifies when user is an RPO or higher).
				// TODO: Check if this validation can be integrated with Voters logic.
				if (!$this->isGranted('ROLE_RELO')) {
					$reloRecipients = $this->getRecipients('relo', $projectGeneralInfo, true);

					// Renders the content of the email notification to be sent.
					$reloPostRevisionEmailBody = $this->renderView(
						'email/specialist/review/relo_post_revision.html.twig',
						[
//							'specialistProject' => $specialistProject,
//							'phase' => $specialistProject->getPhases()->indexOf($projectPhase),
							'projectPhase' => $projectPhase,
							'user'          => $user,
							'role'          => $role,
							'recipients'    => $reloRecipients,
							'sendMessage'   => $this->getParameter('send_emails')
						]
					);

					$notification->sendNotification($reloPostRevisionEmailBody, $reloRecipients, 'EL Specialist proposal – returned for revision');
				}

				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$recipients[] = $projectGeneralInfo->getSubmittedBy()->getEmail();
				break;

			case 'Returned to RELO for revision':
				$subject .= 'returned to RELO for revision';
				$template = 'relo_revision';
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo));
			break;

			case 'Returned to RPO for revision':
				$subject .= 'returned to RPO for revision';
				$template = 'rpo_revision';
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));
			break;

			case 'Alternate funding requested':
				$subject .= 'alternate funding requested';
				$template = 'alternate_funding_request';
				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$recipients[] = $projectGeneralInfo->getSubmittedBy()->getEmail();
				$recipients = array_merge($recipients, $this->getRecipients('author', $projectGeneralInfo));

				// Notify RPO users of Region associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));

				// Notify RELO users of RELO Locations associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo, true));

				break;

			case 'Funding changed':
				$subject .= 'alternate funding provided';
				$template = 'alternate_funding_resubmit';

				$recipients[] = $this->getParameter('eca_email_address');
				$recipients[] = $this->getParameter('gu_specialist_email');

				// Notify RPO users of Region associated to Proposal.
				/** @var User $rpo */
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));

				// Notify RELO users of RELO Locations associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo, true));

				break;

			case 'Withdrawn':
				$subject .= 'proposal withdrawn';
				$template = 'proposal_withdrawn';

				$recipients[] = $this->getParameter('eca_email_address');
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo));
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));
				break;

			case 'Alternate Funding not accepted':
				$subject .= 'alternative funding unavailable';
				$template = 'alternate_funding_unavailable';

				$recipients[] = $this->getParameter('eca_email_address');

				// Notify RPO users of Region associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));

				// Notify RELO users of RELO Locations associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo, true));

				break;

			case 'Re-submitted to RELO review':
				$subject .= 'ready for RELO re-review';
				$template = 'relo_re_review';
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo));
				break;

			case 'Selected for ECA funding':
				$subject .= $action;
				$template = 'proposal_eca_funding_selected';

				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$recipients[] = $projectGeneralInfo->getSubmittedBy()->getEmail();
				$recipients = array_merge($recipients, $this->getRecipients('author', $projectGeneralInfo));
				$recipients[] = $this->getParameter('eca_email_address');
				$recipients[] = $this->getParameter('gu_specialist_email');

				// Notify RPO users of Region associated to Proposal.
				/** @var User $rpo */
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));

				// Notify RELO users of RELO Locations associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo, true));
				break;

			case 'Not Selected for ECA funding':
				$subject .= $action;
				$template = 'proposal_eca_funding_not_selected';

				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$recipients[] = $projectGeneralInfo->getSubmittedBy()->getEmail();
				$recipients = array_merge($recipients, $this->getRecipients('author', $projectGeneralInfo));

				// Notify RPO users of Region associated to Proposal.
				/** @var User $rpo */
				$recipients = array_merge($recipients, $this->getRecipients('rpo', $projectGeneralInfo));

				// Notify RELO users of RELO Locations associated to Proposal.
				$recipients = array_merge($recipients, $this->getRecipients('relo', $projectGeneralInfo, true));
				break;
		}

		// to avoid duplicate recipients
		if (count($recipients)) {
			$recipients = array_unique($recipients);

			// Render and the content of the email notification to be sent.
			$reviewEmailBody = $this->renderView(
				'email/specialist/review/' . $template . '.html.twig',
				[
//					'specialistProject' => $specialistProject,
//					'phase' => $specialistProject->getPhases()->indexOf($projectPhase),
					'projectPhase' => $projectPhase,
					'user' => $user,
					'role' => $role,
					'recipients' => $recipients,
					'sendMessage' => $this->getParameter('send_emails'),
				]
			);

			$notification->sendNotification($reviewEmailBody, $recipients, $subject);
		}
	}


	/**
	 * @param string $type
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param bool $all
	 * @return array
	 */
	private function getRecipients($type, $projectGeneralInfo, $all=false) {

		$recipients = [];

		switch ($type) {
			case 'rpo':
				$region = $projectGeneralInfo->getRegion();
				$rpos = $region->getRpos();
				foreach ($rpos as $rpo) {
					$recipients[] = $rpo->getEmail();
				}
				break;

			case 'relo':
				if ($all) {
					$reloLocations = $projectGeneralInfo->getReloLocations();
					$relos = [];
					/** @var ReloLocation $reloLocation */
					foreach ($reloLocations as $reloLocation) {
						$relos = array_merge($relos, $reloLocation->getRelos()->toArray());
					}
				} else {
					$reloLocation = $projectGeneralInfo->getFirstRELOLocation();
					$relos = $reloLocation->getRelos();
				}

				// Now, gets the users in all the relos retrieved
				foreach ($relos as $relo) {
					$reloEmail = $relo->getEmail();

					if (!in_array($reloEmail, $recipients)) {
						$recipients[] = $reloEmail;
					}
				}
				break;

			case 'author':
				$authors = $projectGeneralInfo->getAuthors();
				foreach ($authors as $author) {
					$recipients[] = $author->getEmail();
				}
				break;

		}

		return $recipients;
	}

}