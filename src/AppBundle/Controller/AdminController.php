<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\Host;
use AppBundle\Entity\Post;
use AppBundle\Entity\ProjectOutcome;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\Specialist\CycleSpecialist;
use AppBundle\Entity\User;
use AppBundle\Form\Admin\HostType;
use AppBundle\Form\Admin\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * AdminController controller.
 *
 * Collects functions only available to SuperAdmin users
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * Synchronize users from Application System.
     *
     * @Route("/sync/users", name="proposal_sync_users")
     * @Method({"GET"})
     */
    public function usersAction()
    {
        $batchSize = 25;

        $em = $this->getDoctrine()->getManager();

        $apiConsumer = $this->get('proposal.drupal_data_consumer');

        $ciedUsers = $apiConsumer->sendRequest('GET', '/users-sync/get');

        $usersCount = count($ciedUsers);

        for ($i = 0, $n = $usersCount; $i < $n; $i += 1) {
            $ciedUser = $ciedUsers[$i];

            $ciedId = $ciedUser['id'];
            $email = $ciedUser['email'];

            // Sanitize user first name
            $firstName = is_array($ciedUser['first_name'])
              ? ''
              : $ciedUser['first_name'];

            // Sanitize user last name
            $lastName = is_array($ciedUser['last_name'])
              ? ''
              : $ciedUser['last_name'];

            $name = trim("$firstName $lastName");

            // Ensure user has roles assigned
            if (null !== $ciedUser['roles']) {

                $roles = explode(',', $ciedUser['roles']);
            }
            else {
                $roles = [];
            }

            // Check if user was already synchronized in the Proposal db
            $user = $em->getRepository('AppBundle:User')->findOneBy(
              ['ciedId' => $ciedId]
            );

            // If user hasn't been synchronize, create a new record to store
            // in the db
            if (null === $user) {
                $user = new User();
                $user->setCiedId($ciedId);
                $user->setEnabled(true);
            }

            $user->setUsername($ciedUser['user_name']);
            $user->setEmail($email);
            $user->setName($name);
            $user->setRoles($roles);
            $user->setPassword('');
            $user->setCiedProfileUri($ciedUser['slug']);

            $em->persist($user);

            if (($i % $batchSize) === 0) {
                $em->flush();
                $em->clear(); // Detaches all objects from Doctrine!
            }

            unset($ciedUser, $ciedId, $email, $name, $rawRoles, $roles, $user);
        }

        $em->flush(); //Persist objects that did not make up an entire batch
        $em->clear();

        return $this->render('admin/sync/users.html.twig', ['users' => $usersCount]);
    }

    /**
     * Synchronize cycles (both Fellow and Specialist) from Application System.
     *
     * @Route("/sync/cycles", name="proposal_sync_cycles")
     * @Method({"GET"})
     */
    public function syncCyclesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $apiConsumer = $this->get('proposal.drupal_data_consumer');

        $fellowCyclesStats = [
          'added'   => 0,
          'updated' => 0,
        ];

        $specialistCyclesStats = [
          'added'   => 0,
          'updated' => 0,
        ];

        $fellowCycles = $apiConsumer->sendRequest('GET', '/cycle-fellow');
        $specialistCycles = $apiConsumer->sendRequest('GET', '/cycle-specialist');

        // Loop to iterate over retrieved Fellow cycles.
        foreach ($fellowCycles as $fellowCycle) {
            // Check current Fellow cycle is already synchronize into the
            // Proposal
            $cycle = $em->getRepository('AppBundle:Fellow\CycleFellow')->findOneBy(
              ['tid' => $fellowCycle['tid']]
            );

            if (null === $cycle) {
                $cycle = new CycleFellow();
                $cycle->setTid($fellowCycle['tid']);

                $fellowCyclesStats['added'] += 1;
            }
            else {
                $fellowCyclesStats['updated'] += 1;
            }

            $cycle->setName($fellowCycle['name']);

            if (!empty($fellowCycle['state_department_name'])) {
                $cycle->setStateDepartmentName(
                  $fellowCycle['state_department_name']
                );
            }

            $cycle->setStartDate(
              \DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $fellowCycle['date']['value']
              )
            );
            $cycle->setEndDate(
              \DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $fellowCycle['date']['value2']
              )
            );

            if (!empty($fellowCycle['open_period_range'])) {
                $cycle->setOpenPeriodStartDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['open_period_range']['value']
                  )
                );
                $cycle->setOpenPeriodEndDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['open_period_range']['value2']
                  )
                );
            }

            if (!empty($fellowCycle['display_period_range'])) {
                $cycle->setDisplayPeriodStartDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['display_period_range']['value']
                  )
                );
                $cycle->setDisplayPeriodEndDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['display_period_range']['value2']
                  )
                );
            }

            if (!empty($fellowCycle['proposal_regular_cycle_period'])) {
                $cycle->setRegularCyclePeriodStartDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['proposal_regular_cycle_period']['value']
                  )
                );
                $cycle->setRegularCyclePeriodEndDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['proposal_regular_cycle_period']['value2']
                  )
                );
            }

            if (!empty($fellowCycle['proposal_off_cycle_period'])) {
                $cycle->setOffCyclePeriodStartDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['proposal_off_cycle_period']['value']
                  )
                );
                $cycle->setOffCyclePeriodEndDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $fellowCycle['proposal_off_cycle_period']['value2']
                  )
                );
            }

            $em->persist($cycle);
        }

        $em->flush();
        $em->clear();

        unset($cycle);

        // Loop to iterate over retrieved Specialist cycles.
        foreach ($specialistCycles as $specialistCycle) {
            // Check current Specialist cycle is already synchronize into the
            // Proposal
            $cycle = $em->getRepository('AppBundle:Specialist\CycleSpecialist')->findOneBy(
              ['tid' => $specialistCycle['tid']]
            );

            if (null === $cycle) {
                $cycle = new CycleSpecialist();
                $cycle->setTid($specialistCycle['tid']);

                $specialistCyclesStats['added'] += 1;
            }
            else {
                $specialistCyclesStats['updated'] += 1;
            }

            $cycle->setName($specialistCycle['name']);

            $cycle->setStartDate(
              \DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $specialistCycle['date']['value']
              )
            );
            $cycle->setEndDate(
              \DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $specialistCycle['date']['value2']
              )
            );

            if (!empty($specialistCycle['open_period_range'])) {
                $cycle->setOpenPeriodStartDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $specialistCycle['open_period_range']['value']
                  )
                );
                $cycle->setOpenPeriodEndDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $specialistCycle['open_period_range']['value2']
                  )
                );
            }

            if (!empty($specialistCycle['display_period_range'])) {
                $cycle->setDisplayPeriodStartDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $specialistCycle['display_period_range']['value']
                  )
                );
                $cycle->setDisplayPeriodEndDate(
                  \DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $specialistCycle['display_period_range']['value2']
                  )
                );
            }

            if (!empty($specialistCycle['state_department_name'])) {
                $cycle->setStateDepartmentName(
                  $specialistCycle['state_department_name']
                );
            }

          $em->persist($cycle);
        }

        $em->flush();
        $em->clear();

        return $this->render(
          'admin/sync/cycles.html.twig',
          [
            'fellowCycles'          => $fellowCycles,
            'specialistCycles'      => $specialistCycles,
            'fellowCyclesStats'     => $fellowCyclesStats,
            'specialistCyclesStats' => $specialistCyclesStats,
          ]
        );
    }

    /**
     * Triggers the clear cache command
     *
     * @param int $result
     *
     * @Route("/clear-cache", name="admin_clear_cache")
     * @Method("GET")
     *
     * @return Response
     */
    public function clearCacheAction($result=0) {

        $dir = $this->get('kernel')->getRootDir() . '/../var/cache/';
        register_shutdown_function(function() use ($dir) {
            `rm -rf $dir/*`;
        });

        return $this->render('admin/clear_cache.html.twig');

    }

    /**
     * @Route("/review-status", name="proposal_admin_review_status_index")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reviewStatusIndexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $reviewStatuses = $em->getRepository('AppBundle:ProjectReviewStatus')->findAll();

        return $this->render(
          'admin/review_status/index.html.twig',
          ['reviewStatuses' => $reviewStatuses]
        );
    }

    /**
     * @Route(
     *     "/review-status/{id}/edit",
     *     requirements={"id": "\d+"},
     *     name="proposal_admin_review_status_edit"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request             $request
     * @param ProjectReviewStatus $reviewStatus
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function reviewStatusEditAction(
      Request $request,
      ProjectReviewStatus $reviewStatus
    ) {
        $form = $this->createForm(
          'AppBundle\Form\ReviewStatusType',
          $reviewStatus
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reviewStatus);
            $em->flush();

            $this->addFlash('success', 'Review Status has been saved succesfully.');

            return $this->redirectToRoute('proposal_admin_review_status_index');
        }

        return $this->render(
          'admin/review_status/edit.html.twig',
          ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/outcome", name="proposal_admin_outcome_index")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function outcomeIndexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $outcomes = $em->getRepository('AppBundle:ProjectOutcome')->findAll();

        return $this->render(
          'admin/outcome/index.html.twig',
          ['outcomes' => $outcomes]
        );
    }

    /**
     * @Route(
     *     "/outcome/{id}/edit",
     *     requirements={"id": "\d+"},
     *     name="proposal_admin_outcome_edit"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request        $request
     * @param ProjectOutcome $outcome
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function outcomeEditAction(
      Request $request,
      ProjectOutcome $outcome
    ) {
        $form = $this->createForm(
          'AppBundle\Form\OutcomeType',
          $outcome
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($outcome);
            $em->flush();

            $this->addFlash('success', 'Outcome has been saved succesfully.');

            return $this->redirectToRoute('proposal_admin_outcome_index');
        }

        return $this->render(
          'admin/outcome/edit.html.twig',
          ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/post", name="admin_post_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function postIndexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository(Post::class)->findAll();

        $deleteForm = $this->createPostDeleteForm($posts[0]);

        return $this->render(
            ':admin/post:index.html.twig',
            ['posts' => $posts, 'delete_form' => $deleteForm->createView()]
        );
    }

    /**
     * @Route("/post/new", name="admin_post_new")
     * @Route("/post/{id}/edit", name="admin_post_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request   $request The request.
     * @param Post|null $post    The post to edit.  Null if new.
     *
     * @return Response
     */
    public function postAction(Request $request, Post $post = null)
    {
        $newPost = false;

        if (null === $post) {
            $post = new Post();
            $newPost = true;
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render(
            ':admin/post:new.html.twig',
            ['new_post' => $newPost, 'form' => $form->createView()]
        );
    }

	/**
	 * @Route("/host-institution", name="admin_host_index")
	 * @param Request $request
	 * @return Response
	 */
	public function hostIndexAction(Request $request) {
    	$em = $this->getDoctrine()->getManager();
    	$hosts = $em->getRepository(Host::class)->findAllWithProjects();

		$deleteForm = $this->createDeleteForm('host', [1]);

		return $this->render(':admin/host:index.html.twig', [
			'hosts' => $hosts,
			'deleteForm' => $deleteForm->createView(),
		]);
	}

	/**
	 * @Route("/host-institution/new", name="admin_host_new")
	 * @Route("/host-institution/{id}/edit", name="admin_host_edit")
	 * @Method({"GET", "POST"})
	 *
	 * @param Request   $request The request.
	 * @param Host|null $host    The Host institution to edit.  Null if new.
	 *
	 * @return Response
	 */
	public function hostAction(Request $request, Host $host = null)
	{
		$newHost = false;

		if (null === $host) {
			$host = new Host();
			$newHost = true;
		}

		$form = $this->createForm(HostType::class, $host);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($host);
			$em->flush();

			return $this->redirectToRoute('admin_host_index');
		}

		return $this->render(
			':admin/host:new.html.twig',
			[
				'form' => $form->createView(),
				'new_post' => $newHost
			]
		);
	}

	/**
	 * @Route("/host-institution/{id}/delete", name="admin_host_delete")
	 * @Method("DELETE")
	 * @param Host $host
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function hostDeleteAction(Request $request, Host $host) {
		$deleteForm = $this->createDeleteForm('host', [$host->getId()]);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->remove($host);
			$em->flush();
		}

		return $this->redirectToRoute('admin_host_index');

	}

	/**
	 * General purpose function to create a form to delete an item.
	 * TODO: Add more cases according to needs
	 *
	 * @param string
	 * @param mixed
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($type, $parameters)
	{
		switch ($type) {
			// To delete Host Institutions
			case 'host':
				$action = 'admin_host_delete';
				$urlParameters = [
					'id' => $parameters[0],
				];
				break;

		}

		return $this->createFormBuilder()
			->setAction(
				$this->generateUrl($action, $urlParameters)
			)
			->setMethod('DELETE')
			->getForm();
	}

    /**
     * @Route("/post/{id}/delete", name="admin_post_delete")
     * @Method("DELETE")
     *
     * @param Request $request The request.
     * @param Post    $post    The post to delete.
     *
     * @return Response
     */
    public function postDeleteAction(Request $request, Post $post)
    {
        $form = $this->createPostDeleteForm($post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
        }

        return $this->redirectToRoute('admin_post_index');
    }

    /**
     * Creates a form to delete a Post entity.
     *
     * @param Post $post The ReloLocation entity
     *
     * @return Form The form
     */
    private function createPostDeleteForm(Post $post)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'admin_post_delete',
                    ['id' => $post->getId()]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();
    }

}
