<?php

namespace AppBundle\Controller\Specialist;

use AppBundle\Entity\ContactPointAdditional;
use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\Post;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReview;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\Specialist\CycleSpecialist;
use AppBundle\Entity\Specialist\PartneringOrganization;
use AppBundle\Entity\Specialist\Virtual\Itinerary\WeekAfter;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\User;
use AppBundle\Form\Common\ContactPointsType;
use AppBundle\Form\Specialist\CandidateInfoSourceType;
use AppBundle\Form\Specialist\CandidateInfoType;
use AppBundle\Form\Specialist\CertificationsType;
use AppBundle\Form\Specialist\CountriesType;
use AppBundle\Form\Specialist\FilterType;
use AppBundle\Form\Specialist\GoalsSustainabilityType;
use AppBundle\Form\Specialist\PartneringOrganizationsType;
use AppBundle\Form\Specialist\PhaseType;
use AppBundle\Form\Specialist\PostsType;
use AppBundle\Form\Specialist\RequirementsType;
use AppBundle\Form\Specialist\SubmitSpecialistProposalType;
use AppBundle\Form\Specialist\Virtual\CertificationsType as VirtualCertificationsType;
use AppBundle\Form\Specialist\Mixed\CertificationsType as MixedCertificationsType;
use AppBundle\Helper\ControllerHelper;
use Doctrine\ORM\NoResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Solarium\QueryType\Select\Result\FacetSet;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Specialist phase controller (common for all Phase types).
 *
 * @package AppBundle\Controller\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class PhaseController extends BaseController
{
    /**
     * List specialist projects phases associated to user.
     *
     * @Route("/", name="specialist_project_index")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function indexAction()
    {
        $phases = $this->getDoctrine()->getManager()
            ->getRepository(SpecialistProjectPhase::class)
            ->findAllByUser($this->getUser());

        return $this->render(
            ':project/specialist:index.html.twig',
            [
                'owned_phases' => $phases['owned'],
                'shared_phases' => $phases['shared']
            ]
        );
    }

    /**
     * List all Specialist Project phases.
     *
     * @Route("/{id}/phases", name="specialist_project_phases")
     * @Method({"GET"})
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return Response
     */
    public function phasesAction(SpecialistProjectPhase $specialistProjectPhase)
    {
        return $this->render(
            ':project/specialist:phases.html.twig',
            ['phase' => $specialistProjectPhase]
        );
    }

    /**
     * @Route(
     *     "/{id}/share",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_share"
     * )
     * @Method({"POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse
     */
    public function shareAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('AppBundle:User');

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $postData = $request->request->get('form');

        $shareWith = array_key_exists('shareWith', $postData)
            ? $postData['shareWith']
            : [];

        $unShareFrom = array_key_exists('unShareFrom', $postData)
            ? $postData['unShareFrom']
            : [];

        // Check if there are users to share project with
        if (!empty($shareWith)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($shareWith as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be persisted
                $projectGeneralInfo->addAuthor($user);
                $user->addProject($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
                'success',
                '<p>Project has been shared successfully with the following users:</p>'
                .$sharedWithList
            );
        }

        // Check if there are users to un-share project from
        if (!empty($unShareFrom)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($unShareFrom as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be removed
                $projectGeneralInfo->getAuthors()->removeElement($user);
                $user->getProjects()->removeElement($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
                'success',
                '<p>Project has been un-shared successfully from the following users:</p>'
                .$sharedWithList
            );
        }

        return $this->redirectToRoute('specialist_project_phase_admin_panel', ['id' => $specialistProjectPhase->getId()]);
    }

    /**
     * Creates a new SpecialistProject entity and save the record in the database.
     *
     * @Route("/new", name="specialist_project_phase_new")
     * @Route("/{phase_id}/clone", name="specialist_project_phase_clone")
     * @Method({"GET", "POST"})
     * @ParamConverter(
     *     "sourcePhase",
     *     class="AppBundle:SpecialistProjectPhase",
     *     options={"mapping": {"phase_id": "id"}}
     * )
     *
     * @param Request                $request     The request.
     * @param SpecialistProjectPhase $sourcePhase Phase to clone. NULL if new.
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, SpecialistProjectPhase $sourcePhase = null)
    {
    	$translator = $this->get('translator');
        $isNewProposal = null === $sourcePhase;
        $em = $this->getDoctrine()->getManager();

        try {
            $currentSpecialistCycle = $em->getRepository(CycleSpecialist::class)->getCurrentCycle();
        } catch (NoResultException $e) {
            $this->addFlash(
                'danger',
                $translator->trans("The Specialist proposal couldn't be created because there isn't a Cycle for the current period.")
            );

            return $this->redirectToRoute('specialist_project_index');
        }

        $specialistProject = $isNewProposal
            ? new SpecialistProject()
            : $sourcePhase->getSpecialistProject();

        $projectGeneralInfo = new ProjectGeneralInfo($currentSpecialistCycle);
        $projectPhase = new SpecialistProjectPhase(
            $specialistProject,
            $projectGeneralInfo
        );

        if (!$isNewProposal) {
	        $projectPhase->setSourcePhase($sourcePhase);
	        $projectPhase->setTitle($sourcePhase->getTitle()); // temporary to show the title in the form, it will change when form is submitted
        }

        // Statement needed to render a the form without a persisted entity.
        $projectGeneralInfo->setSpecialistProjectPhase($projectPhase);

        $form = $this->createForm(PhaseType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isValid()) {
        	$msg = '';
        	$redirectUrl = 'specialist_project_phase_new';
        	$parameters = [];

        	if (!$isNewProposal) {
        		if (null == $projectPhase->getTitle()) {
			        $msg .= $translator->trans('You must provide a phase title');
		        }
		        $redirectUrl = 'specialist_project_phase_clone';
        		$parameters = ['phase_id' => $sourcePhase->getId()];
	        }

            if (null === $projectPhase->getType()) {
        		$msg .= ($msg?'<br>':'');
	            $msg .= $translator->trans('You must select the type of project before moving forward');
            }

            if ($msg) { // If error
	            $this->addFlash(
		            'danger',
		            $msg
	            );

                return $this->redirectToRoute($redirectUrl, $parameters);
            } else {

                if ($this->isGranted('ROLE_RPO')) {
                    $createdRole = 'RPO';
                } elseif ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
                    $createdRole = 'ECA';
                } elseif ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {
                    $createdRole = 'Admin';
                } elseif ($this->isGranted('ROLE_RELO')) {
                    $createdRole = 'RELO';
                }else {
                    $createdRole = 'Embassy';
                }
                $projectGeneralInfo->setCreatedByRole($createdRole);

	            if ($isNewProposal) {
		            // Persisting the Specialist Project applies for new
		            // Projects/Phases only.
		            $em->persist($specialistProject);
	            } else {
		            // When $sourcePhase isn't null, it means the cloning process
		            // is the one taking place.  The $sourcePhase must be set as
		            // the Source from which the new Phase is being created of.
		            $projectPhase->setSourcePhase($sourcePhase);
	            }
	            $em->persist($projectPhase);
                $em->flush();

                $this->get('logger')->info(
                    "Specialist Proposal {$specialistProject->getId()} ({$projectGeneralInfo->getReferenceNumber()}) created."
                );

                $this->addFlash(
                    'success',
	                $translator->trans('Specialist Project created successfully.')
                );
            }

            return $this->redirectToRoute(
                'specialist_project_phase_admin_panel',
                ['id' => $projectPhase->getId()]
            );
        }

	    return $this->render(
            ':project/specialist:select_type.html.twig', [
            	'form' => $form->createView(),
		        'newProposal' => $isNewProposal,
			    'sourcePhase' => $sourcePhase,
		        'goBack' => null,
	        ]
        );
    }

    /**
     * @Route(
     *     "/{id}",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_show"
     * )
     * @Method({"GET"})
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return Response
     */
    public function showAction(SpecialistProjectPhase $specialistProjectPhase) {
        $em = $this->getDoctrine()->getManager();

        $phaseType = '';
        $requiredVisaType = '';
        $virtualMixedComponent = '';
        $projectPhasePartneringOrganizations = [];

        /** @var PartneringOrganization $partneringOrganization */
        foreach ($specialistProjectPhase->getPartneringOrganizations() as $partneringOrganization) {
            $projectPhasePartneringOrganizations[$partneringOrganization->getCountry()->getName()][] = $partneringOrganization;
        }

        if (null !== $specialistProjectPhase->getType()) {
            $phaseType = array_search(
                $specialistProjectPhase->getType(),
                SpecialistProjectPhase::getTypeChoices(),
                true
            );
        }

        if (null !== $specialistProjectPhase->getRequiredVisaType()) {
            $requiredVisaType = array_search(
                $specialistProjectPhase->getRequiredVisaType(),
                SpecialistProjectPhase::getRequiredVisaTypeChoices(),
                true
            );
        }

        if (null !== $specialistProjectPhase->getMixedVirtualComponent()) {
            $virtualMixedComponent = array_search(
                $specialistProjectPhase->getMixedVirtualComponent(),
                SpecialistProjectPhase::getMixedVirtualComponentChoices(),
                true
            );
        }

        // Initializing some variables to prevent notices and warnings in template
        $virtualActivities = null;
        $phaseBudget = $specialistProjectPhase->getBudget();
        $errors = [
            'livingExpenses' => [],
            'groundTransport' => [],
            'transportation' => [],
            'oneTime' => [],
            'paa' => [],
        ];
        $dailyCosts = [];
        $stipendCosts = [];

        // if form not submitted, checks discrepancies in data
        /** @var ControllerHelper $helper */
        $helper = $this->get('proposal.helper.controller_helper');

        if (in_array($specialistProjectPhase->getType(), [SpecialistProjectPhase::IN_COUNTRY_TYPE, SpecialistProjectPhase::MIXED_TYPE])) {

            $contributionLivingCostItems = $em->getRepository('AppBundle:CostItemLivingExpense')->findAll();
            list($itineraryActivities, $emptyRows) = $this->itineraryActivitiesFormat($specialistProjectPhase);

            $livingExpenses = $em->getRepository('AppBundle:ItineraryIndex')->getCostsStats($specialistProjectPhase);

            if ($phaseBudget) {
                $contributionLE = $phaseBudget->getContributionLivingExpenses();
                if ($contributionLE) {
                    $errors['livingExpenses'] = $helper->getErrorsInRows($contributionLE->first()->getContributionLivingExpenseCostItems());
                }
                if ($phaseBudget->getContributionGroundTransport()) {
                    $errors['groundTransport'] = $helper->getErrorsInRows(array($phaseBudget->getContributionGroundTransport()));
                }

                $contributionTransportation = $phaseBudget->getContributionTransportation();
                if ($contributionTransportation) {
                    $errors['transportation'] = $helper->getErrorsInRows($contributionTransportation->getTransportationCostItems());
                }

                $contributionOneTime = $phaseBudget->getContributionOneTime();
                if ($contributionOneTime) {
                    $errors['oneTime'] = $helper->getErrorsInRows($contributionOneTime->getContributionOneTimeCostItems());
                }

                $dailyCosts = $helper->formatDailyCosts($specialistProjectPhase);
            }

            $totals = [
                'Meals' => 0,
                'Lodging' => 0,
            ];

            foreach ($livingExpenses['rows'] as $livingExpenseItem) {
                if ($livingExpenseItem != null) {
                    $totals['Meals'] += $livingExpenseItem['Meals'];
                    $totals['Lodging'] += $livingExpenseItem['Lodging'];
                }
            }
        } else {
            $contributionLivingCostItems = [];
            $totals = [];
            $livingExpenses = [];
            $itineraryActivities = [];
            $emptyRows = [];
        }

        // This applies for any type of project: In Country, Mixed, Virtual
        if ($phaseBudget) {
            $paa = $phaseBudget->getProgramActivitiesAllowance();
            if ($paa) { // $paa was already defined before the form handling
                $errors['paa'] = $helper->getErrorsInRows($paa->getProgramActivitiesAllowanceCostItems());
            }
        }

        // For mixed and virtual projects
        if (in_array($specialistProjectPhase->getType(), [SpecialistProjectPhase::VIRTUAL_TYPE, SpecialistProjectPhase::MIXED_TYPE])) {
            // To build the summary table, we need the activities
            $virtualActivities = $em->getRepository('AppBundle:Specialist\Virtual\Itinerary\Activity')->findInPhase($specialistProjectPhase);

            if ($phaseBudget) {
                $stipendCosts = $helper->formatVirtualFlexibleCosts($specialistProjectPhase);
            }
        }

        $form = $this->createForm(SubmitSpecialistProposalType::class, null, [
                'action' => $this->generateUrl(
                    'specialist_project_submit', [
                        'id' => $specialistProjectPhase->getId(),
                    ]),
            ]);

        $weeks = $specialistProjectPhase->getVirtualItineraryWeeks()->filter(
            function ($week) {
                return !$week instanceof WeekAfter;
            }
        );

        $weeksAfter = $specialistProjectPhase->getVirtualItineraryWeeksAfter();

        return $this->render(
            ':project/specialist:show.html.twig',
            [
                'phaseType' => $phaseType,
                'requiredVisaType' => $requiredVisaType,
                'virtual_mixed_component' => $virtualMixedComponent,
                'specialistProjectPhase' => $specialistProjectPhase,
                'livingExpensesCostItems' => $contributionLivingCostItems,
                'livingExpenses' => $livingExpenses,
                'totals' => $totals,
                'projectPhasePartneringOrganizations' => $projectPhasePartneringOrganizations,
                'itineraryActivities' => $itineraryActivities,
                'emptyRows' => $emptyRows,
                'errors' => $errors,
                'dailyCosts' => $dailyCosts,
                'virtualActivities' => $virtualActivities,
                'stipendCosts' => $stipendCosts,
                'form' => $form->createView(),
                'weeks' => $weeks,
                'weeks_after' => $weeksAfter,
            ]
        );
    }

    /**
     * Export the Project form in pdf format.
     *
     * @Route(
     *     "/{id}/export-pdf",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_export_pdf"
     * )
     * @Method("GET")
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return Response
     */
    public function exportPdfAction(SpecialistProjectPhase $specialistProjectPhase)
    {
        $pageUrl = $this->generateUrl(
            'specialist_project_phase_show',
            ['id' => $specialistProjectPhase->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $pageUrl .= '?print=1';

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $PHPSESSID = $session->getId();

        $output = $this->get('knp_snappy.pdf')->getOutput($pageUrl, array(
                'cookie' => array(
                    'PHPSESSID' => $PHPSESSID,
                ),
                'zoom' => 0.8,
                'run-script' => "javascript:(\$(function(){ \$('#header, #review-navigation').hide()   ;}))",
            )
        );

        return new Response(
            $output,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="specialist-proposal-phase.pdf"'),
            )
        );
    }

    /**
     * @Route(
     *     "/{id}/admin-panel",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_admin_panel",
     * )
     * @Method({"GET"})
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return Response
     */
    public function adminPanelAction(SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $em = $this->getDoctrine()->getManager();

        $submitProjectForm = $this->createForm(
	        SubmitSpecialistProposalType::class,
            null, [
                'action' => $this->generateUrl(
                    'specialist_project_submit', [
                        'id' => $specialistProjectPhase->getId(),
                    ]),
            ]);

        $phaseType = '';

        if (null !== $specialistProjectPhase->getType()) {
            $phaseType = array_search(
                $specialistProjectPhase->getType(),
                SpecialistProjectPhase::getTypeChoices()
            );
        }

        $shareProjectForm = $this->createFormBuilder()
            ->setMethod('POST')
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_share', [
                        'id'    => $specialistProjectPhase->getId(),
	                ]
                )
            )
            ->add('shareProject', SubmitType::class, ['label' => 'Save'])
            ->getForm();

        $authors = $projectGeneralInfo->getAuthors()->toArray();

        // Include creator user in the authors list
        $authors[] = $projectGeneralInfo->getCreatedBy();

        // Get all users with RELO and EMBASSY roles, excluding those who are
        // authors of this project already.
        $embassyRELOUsers = $em->getRepository('AppBundle:User')
            ->findAllByRoles(['ROLE_EMBASSY', 'ROLE_RELO'], $authors);

        // The data should be mapped in a way the JS auto-complete plugin
        // understands it.
        $embassyRELOUsers = array_map(
            function ($user) {
                return [
                    'id'       => $user['id'],
                    'name'     => trim($user['name'].' <'.$user['email'].'>'),
                    'fullname' => $user['name'],
                    'email'    => $user['email'],
                ];
            },
            $embassyRELOUsers
        );

        return $this->render(
            'project/specialist/admin_panel.html.twig',
            [
                'phaseType'              => $phaseType,
                'specialistProjectPhase' => $specialistProjectPhase,
                'projectGeneralInfo'     => $projectGeneralInfo,
                'embassyRELOUsers'       => json_encode($embassyRELOUsers),
                'shareProjectForm'       => $shareProjectForm->createView(),
                'submitProjectForm'      => $submitProjectForm->createView(),
            ]
        );
    }

    /**
     * Browse Specialist projects.  The projects will be retrieved based on the
     * authenticated user active role.
     *
     * @Route("/browse", name="specialist_project_browse")
     * @Method({"GET"})
     *
     * @param Request $request The request.
     *
     * @return Response
     */
    public function browseAction(Request $request)
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        try {
            /** @var CycleSpecialist $currentFellowCycle */
            $currentCycle = $em
                ->getRepository(CycleSpecialist::class)
                ->getCurrentCycle();
        } catch (NoResultException $e) {
            $currentCycle = $em->getRepository(CycleSpecialist::class)->findLast();
        }

        $cycleForm = $this
            ->get('form.factory')
            ->createNamedBuilder(
                'cycle',
                EntityType::class,
                $currentCycle,
                [
                    'method' => Request::METHOD_GET,
                    'class' => CycleSpecialist::class,
                    'choice_label' => 'name',
                ]
            )
            ->getForm();
        $cycleForm->handleRequest($request);

        $specialistProjectRepo = $this->get(
            'proposal.solr_repo.specialist_project_phase'
        );
        $allowedSorts = [
            'reference_number_s',
            'title_s',
            'type_s',
            'proposal_status_s',
            'proposal_outcome_s',
            'region_s',
        ];
        $facetFields = [
            'type_s',
            'proposal_status_s',
            'proposal_outcome_s',
            'countries_ss',
        ];
        $criteria = ['cycle_id_i' => [$cycleForm->getData()->getId()]];
        $filterFormData = $request->query->get('filter', []);
        $results = null;
//        $totalECAContribution = null;

        // If user's active role is RELO, projects should be filtered by his/her
        // RELO Location associated.
        if ($this->isGranted('ROLE_RELO')) {
            $terms = $authenticatedUser
                ->getReloLocations()
                ->map(
                    function ($reloLocation) {
                        return (string)$reloLocation;
                    }
                )
                ->toArray();

            if (!empty($terms)) {
                $criteria['relo_locations_ss'] = $terms;
            } else {
                $results = [
                    'facet_set' => new FacetSet([]),
                    'specialist_projects_phases' => [],
                ];
            }
        } elseif ($this->isGranted('ROLE_RPO')) {
            $terms = $authenticatedUser
                ->getRegions()
                ->map(
                    function ($region) {
                        return $region->getAcronym();
                    }
                )
                ->toArray();

            if (!empty($terms)) {
                $criteria['region_s'] = $terms;
            } else {
                $results = [
                    'facet_set' => new FacetSet([]),
                    'fellow_projects' => [],
                ];
            }

            $facetFields[] = 'relo_locations_ss';

//            $totalECAContribution = $em
//                ->getRepository(SpecialistProjectPhaseBudget::class)
//                ->getTotalECAContributionInRegionsInCycle(
//                    $authenticatedUser->getRegions(),
//                    $cycleForm->getData()
//                );
        } elseif ($this->isGranted('ROLE_STATE_DEPARTMENT') || $this->isGranted('ROLE_ADMIN')) {
            $facetFields[] = 'relo_locations_ss';
            $facetFields[] = 'region_s';

//            $totalECAContribution = $em
//                ->getRepository(SpecialistProjectPhaseBudget::class)
//                ->getTotalECAContributionInCycle($cycleForm->getData());
        } else {
            $results = [
                'facet_set' => new FacetSet([]),
                'specialist_projects_phases' => [],
            ];
        }

        foreach ($facetFields as $filter) {
            if (isset($filterFormData[$filter])) {
                $criteria[$filter] = $filterFormData[$filter];
            }
        }

        if (!$results) {
            $results = $specialistProjectRepo->getFacetedResultsForSearch(
                $criteria,
                $facetFields,
                $allowedSorts,
                1,
                $request->query->get('sort'),
                $request->query->get('sortDirection')
            );
        }

        $filterForm = $this->createForm(
            FilterType::class,
            [],
            ['csrf_protection' => false, 'facet_set' => $results['facet_set']]
        );
        $filterForm->handleRequest($request);

        return $this->render(
            ':project/specialist:browse.html.twig',
            [
                'cycle_form' => $cycleForm->createView(),
                'filter_form' => $filterForm->createView(),
                'specialist_projects_phases' => $results['specialist_projects_phases'],
//                'total_eca_contribution' => $totalECAContribution
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/title-region",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_title_region"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function titleRegionAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(
            'AppBundle\Form\Specialist\TitleRegionType',
	        $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
//            $em->persist($projectGeneralInfo);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('next')->isClicked()) {
                if (!$projectGeneralInfo->getRegion()) {
                    $this->addFlash('no_region', 'Please select a Region');
                    $nextAction = 'specialist_project_title_region';
                } else {
                    $nextAction = 'specialist_project_phase_countries';
                }
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            'project/specialist/wizard/title_region.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form'                   => $form->createView(),
                'goBack'                 => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/countries",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_countries"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function countriesAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(CountriesType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        $prevAction = 'specialist_project_title_region';

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Country $country */
            $country = $form->get('country')->getData();

            if (null !== $country) {
                // Associate selected country to Project.
                $projectGeneralInfo->addCountry($country);

                // Verify if selected Country has RELO Locations associated to it.
                // If it has, get the first of the list.
                if (!$country->getReloLocations()->isEmpty()) {
                    /** @var ReloLocation $reloLocation */
                    $reloLocation = $country->getReloLocations()->first();

                    // This validation will ensure no RELO Location is associated twice with the Project.
                    if (!$projectGeneralInfo->getReloLocations()->contains($reloLocation)) {
                        $projectGeneralInfo->addReloLocation($reloLocation);
                    }
                }

                $em->persist($projectGeneralInfo);
            }

            $nextAction = 'specialist_project_phase_countries';

            // removes and assignment Id, and cascade delete activities, living expenses and contributions
            if ($form->get('deleteId')->getViewData()) {
                $countryToDelete = $em->getRepository(Country::class)->find($form->get('deleteId')->getViewData());
                $assignmentsToDelete = $em->getRepository(InCountryAssignment::class)->findInCountryAssignmentsByCountry($specialistProjectPhase, $countryToDelete);

                // Removes assignments and cascade on expenses, activities, itinerary, etc.
                foreach ($assignmentsToDelete as $assignment) {
                    $em->remove($assignment);
                }

                // Removes association with Partnering Organizations in country that is being removed
                /** @var PartneringOrganization $partner */
                foreach($specialistProjectPhase->getPartneringOrganizations() as $partner) {
                    if ($partner->getCountry() == $countryToDelete) {
                        $specialistProjectPhase->removePartneringOrganization($partner);
                    }
                }

                // Finds the post associated for the country and removes it
                $posts = $projectGeneralInfo->getPosts();
                /** @var Post $post */
                foreach ($posts as $post) {
                    if ($post->getCountry() == $countryToDelete) {
                        $projectGeneralInfo->removePost($post);
                        break;
                    }
                }

                // Remove association of the Project with those RELO Locations associated to the Country that is
                // going to be deleted.
                /** @var ReloLocation $reloLocation */
                foreach ($countryToDelete->getReloLocations() as $reloLocation) {
                    $reloLocationInUse = false;

                    // The RELO Location cannot be deleted if it is associated to another Country associated to
                    // the Project.
                    /** @var Country $country */
                    foreach ($projectGeneralInfo->getCountries() as $country) {
                        // Ignore the Country that is going to be deleted.
                        if ($countryToDelete != $country) {
                            if ($country->getReloLocations()->contains($reloLocation)) {
                                $reloLocationInUse = true;
                                break;
                            }
                        }
                    }

                    if (!$reloLocationInUse) {
                        $projectGeneralInfo->removeReloLocation($reloLocation);
                    }
                }

                // Removes association between project and country
                $projectGeneralInfo->removeCountry($countryToDelete);
            } elseif ($form->get('save')->isClicked()) {
                $nextAction = 'specialist_project_phase_admin_panel';
            } elseif ($form->get('next')->isClicked()) {
                if ((null === $country) && $projectGeneralInfo->getCountries()->isEmpty()) {
                    $this->addFlash('no_country', 'Please add at least one country');
                } else {
                    $nextAction = 'specialist_project_phase_posts';
                }
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = $prevAction;
            }

            // Persists the changes
            $em->flush();

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($specialistProjectPhase);

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        $this->validateAccess($specialistProjectPhase, 0, 'region', 'specialist_project_title_region');

        return $this->render(
            ':project/specialist/wizard:countries.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form'                   => $form->createView(),
                'goBack'                 => $prevAction,
            ]
        );
    }

    /**
     * TODO - Felipe: 12/15/2016: This method is not really used, must be checked if we are supporting Non-JS
     * @Route(
     *     "/{id}/countries/{cid}/delete",
     *     name="specialist_project_phase_countries_delete"
     * )
     * @Method("DELETE")
     *
     * @ParamConverter(
     *     "Country",
     *     class="AppBundle:Country",
     *     options={"mapping": {"cid": "id"}}
     * )
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param Country                $country
     *
     * @return RedirectResponse
     */
    public function countriesDeleteAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, Country $country)
    {
        /** @var SpecialistProject $specialistProject */
        $specialistProject = $specialistProjectPhase->getSpecialistProject();
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createDeleteCountryForm($country, $specialistProject, 0);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $inCountryAssignments = $em->getRepository('AppBundle:InCountryAssignment')->findInCountryAssignmentsByCountry($specialistProjectPhase, $country);

            // Removes assignments and cascade on expenses, activities, itinerary, etc.
            foreach($inCountryAssignments as $assignment) {
                $em->remove($assignment);
            }

            // disassociate Partnering Organizations in country that is being removed
            /** @var PartneringOrganization $partner */
            foreach($specialistProjectPhase->getPartneringOrganizations() as $partner) {
                if ($partner->getCountry() == $country) {
                    $specialistProjectPhase->removePartneringOrganization($partner);
                }
            }

            // Removes association between project and country
            $projectGeneralInfo->removeCountry($country);
            $em->flush();
        }

        return $this->redirectToRoute('specialist_project_phase_countries', ['id' => $specialistProjectPhase]);
    }

    /**
     * @Route(
     *     "/{id}/posts",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_posts"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function postsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(PostsType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        $prevAction = 'specialist_project_phase_countries';

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $projectGeneralInfo->getPosts()->clear();
            $postsIds = $request->request->get('posts')['posts'];

            if (!empty($postsIds)) {
                foreach ($postsIds as $postId) {
                    if ('' !== $postId) {
                        $post = $em->getRepository(Post::class)->find($postId);

                        if ($post) {
                            $projectGeneralInfo->addPost($post);
                        }
                    }
                }
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('next')->isClicked()) {
                if (SpecialistProjectPhase::VIRTUAL_TYPE != $specialistProjectPhase->getType()) {
                    $nextAction = 'specialist_project_phase_city_costs';
                }
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = $prevAction;
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            'project/specialist/wizard/posts.html.twig',
            [
//                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'goBack' => $prevAction,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/contact-points",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_contact_points"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function contactPointsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(ContactPointsType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $nextAction = 'specialist_project_phase_admin_panel';
            $em = $this->getDoctrine()->getManager();

            $em->persist($projectGeneralInfo);

            if ($form->get('additional')->isClicked()) {
                $nextAction = 'specialist_project_phase_contact_points';
                $contactPointAdditional = new ContactPointAdditional(
                    $projectGeneralInfo
                );

                $em->persist($contactPointAdditional);
            }

            $em->flush();

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        // Get last Contact Point entry and check if it is Additional.  If it
        // is, a delete form for Contact Points needs to be created.  This
        // delete form will handle the deletion of all Additional Contact
        // Points through JavaScript.
        $contactPoint = $projectGeneralInfo->getContactPoints()->last();

        // to have something to pass to the template
        $deleteForm = false;

        if ($contactPoint instanceof ContactPointAdditional) {
            $deleteForm = $this->createDeleteContactPointForm(
                $contactPoint,
                $specialistProjectPhase
            );
        }

        return $this->render(
            ':project/specialist/wizard:contact_points.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form'                   => $form->createView(),
                'deleteForm'             => $deleteForm ? $deleteForm->createView() : false,
                'goBack'                 => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/contact-points/{cid}/delete",
     *     name="specialist_project_phase_contact_points_delete"
     * )
     * @Method("DELETE")
     *
     * @ParamConverter(
     *     "contactPointAdditional",
     *     class="AppBundle:ContactPointAdditional",
     *     options={"mapping": {"cid": "id"}}
     * )
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param ContactPointAdditional $contactPointAdditional
     *
     * @return RedirectResponse
     */
    public function deleteContactPointAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, ContactPointAdditional $contactPointAdditional)
    {
        $form = $this->createDeleteContactPointForm(
            $contactPointAdditional,
            $specialistProjectPhase
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contactPointAdditional);
            $em->flush();
        }

        return $this->redirectToRoute(
            'specialist_project_phase_contact_points',
            ['id' => $specialistProjectPhase->getId()]
        );
    }

    /**
     * @Route(
     *     "/{id}/requirements",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_requirements"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function requirementsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);
        $this->denyAccessUnlessType($specialistProjectPhase, [
            SpecialistProjectPhase::IN_COUNTRY_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        $form = $this->createForm(RequirementsType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_admin_panel',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard:requirements.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/certifications",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_certifications"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function certificationsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());

        $formType = '';
        $view = '';

        // TODO: is this the best approach? or maybe building the form dynamically in the FormType?
        switch ($specialistProjectPhase->getType()) {
            case SpecialistProjectPhase::IN_COUNTRY_TYPE:
                $formType = CertificationsType::class;
                $view = ':project/specialist/wizard:certifications.html.twig';
                break;
            case SpecialistProjectPhase::VIRTUAL_TYPE:
                $formType = VirtualCertificationsType::class;
                $view = ':project/specialist/wizard/virtual:certifications.html.twig';
                break;
            case SpecialistProjectPhase::MIXED_TYPE:
                $formType = MixedCertificationsType::class;
                $view = ':project/specialist/wizard/mixed:certifications.html.twig';
                break;
        }

        $form = $this->createForm($formType, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_admin_panel',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            $view,
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/candidate-info",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_candidate_info"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function candidateInfoAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());

        $form = $this->createForm(CandidateInfoType::class, $specialistProjectPhase);
        $form->handleRequest($request);

        if ($form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';
//            if ($form->get('back')->isClicked()) {
//            	$nextAction = 'specialist_project_phase_candidate_info_source';
//            }

            // User shouldn't select more than five Areas of Expertise.
            if (count($form->get('areasOfExpertise')->getViewData()) <= 5) {
                return $this->redirectToRoute(
                    $nextAction,
                    ['id' => $specialistProjectPhase->getId()]
                );
            }
            else {
                $this->addFlash(
                    'danger',
                    'You must check at least one Area of Expertise and no more than five.'
                );
            }
        }

        return $this->render(
            ':project/specialist/wizard:candidate_info.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/partnering-organizations",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_partnering_organizations"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function partneringOrganizationsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());

        $projectPhasePartneringOrganizations = [];

        /** @var PartneringOrganization $partneringOrganization */
        foreach ($specialistProjectPhase->getPartneringOrganizations() as $partneringOrganization) {
            $projectPhasePartneringOrganizations[$partneringOrganization->getCountry()->getName()][] = $partneringOrganization;
        }

        $form = $this->createForm(
            PartneringOrganizationsType::class,
            $specialistProjectPhase
        );
        $form->handleRequest($request);

        $deleteForm = $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_partnering_organization_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'po_id' => 0,
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $modelData = $form->get('partneringOrganizations')->getData();
            $partneringOrganizations = $request->get('partnering_organizations');

            if (array_key_exists('partneringOrganizations', $partneringOrganizations)) {
                foreach ($request->get('partnering_organizations')['partneringOrganizations'] as $key => $rawData) {
                    $partneringOrganization = null;

                    // If a new Partnering Organization is entered, it won't matter
                    // if a Partnering Organization is selected from the drop-down
                    // list of existing ones: the new entered one will have priority
                    // over any other.
                    if ('' !== $rawData['partneringOrganizationCustom']) {
                        $country = $em->getRepository(Country::class)->find($key);

                        if (null !== $country) {
                            $partneringOrganization = new PartneringOrganization(
                                $country
                            );
                            $partneringOrganization->setName(
                                $rawData['partneringOrganizationCustom']
                            );

                            $em->persist($partneringOrganization);
                            $em->flush();
                        }
                    } elseif (('' !== $rawData['partneringOrganization'])
                        && (null !== $modelData[$key]['partneringOrganization'])
                    ) {
                        $partneringOrganization = $modelData[$key]['partneringOrganization'];

                        // If "No Partnering Organization" option is selected, all
                        // previous ones should be de-associated from project phase.
                        if ($partneringOrganization->isNotApplicable()) {
                            $specialistProjectPhase->getPartneringOrganizations()->clear();
                        }
                    }

                    // Double check a PO has been selected/entered before associated it
                    // to the Project phase.
                    if (null !== $partneringOrganization) {
                        $specialistProjectPhase->addPartneringOrganization(
                            $partneringOrganization
                        );

                        $this->addFlash(
                            'success',
                            'Partnering Organization associated to Project'
                        );
                    }
                }

                $em->persist($specialistProjectPhase);
                $em->flush();
            }

            $nextAction = 'specialist_project_phase_partnering_organizations';

            if ($form->get('save')->isClicked() || $form->get('next')->isClicked()) {
                $nextAction = 'specialist_project_phase_admin_panel';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        $this->validateAccess($specialistProjectPhase, 0, 'countries', 'specialist_project_phase_countries');

        return $this->render(
            ':project/specialist/wizard:partnering_organizations.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'projectPhasePartneringOrganizations' => $projectPhasePartneringOrganizations,
                'form' => $form->createView(),
                'deleteForm' => $deleteForm->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/partnering-organization/{po_id}/delete",
     *     requirements={"id": "\d+", "po_id": "\d+"},
     *     name="specialist_project_phase_partnering_organization_delete"
     * )
     * @ParamConverter(
     *     "partneringOrganization",
     *     class="AppBundle:Specialist\PartneringOrganization",
     *     options={"mapping": {"po_id": "id"}}
     * )
     * @Method("DELETE")
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param PartneringOrganization $partneringOrganization
     *
     * @return RedirectResponse
     */
    public function partneringOrganizationDeleteAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, PartneringOrganization $partneringOrganization)
    {
        $deleteForm = $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_partnering_organization_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'po_id' => $partneringOrganization->getId(),
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();
        $deleteForm->handleRequest($request);

        if ($deleteForm->isValid()) {
            // What is remove is the relation between the Partnering Organization
            // and the Specialist Project Phase; the Partnering Organization
            // will still be displayed as an option for the country it is
            // associated to.
            $specialistProjectPhase->removePartneringOrganization($partneringOrganization);

            foreach ($specialistProjectPhase->getInCountryAssignments() as $inCountryAssignment) {
                foreach ($inCountryAssignment->getAssignmentActivities() as $assignmentActivity) {
                    foreach ($assignmentActivity->getPartneringOrganizations() as $currentPartneringOrganization) {
                        if ($partneringOrganization == $currentPartneringOrganization) {
                            $assignmentActivity->removePartneringOrganization($partneringOrganization);
                        }
                    }
                }
            } ;

            $em = $this->getDoctrine()->getManager();
            $em->persist($specialistProjectPhase);
            $em->flush();

            $this->addFlash(
                'success',
                'The Partnering Organization has been deleted successfully.'
            );
        }

        return $this->redirectToRoute(
            'specialist_project_phase_partnering_organizations',
            ['id' => $specialistProjectPhase->getId()]
        );
    }

    /**
     * @Route(
     *     "/{id}/goals-sustainability",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_goals_sustainability"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function goalsAndSustainabilityAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());

        $form = $this->createForm(
            GoalsSustainabilityType::class,
            $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_admin_panel',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard:goals_sustainability.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * Deletes an Specialist Project entity.
     *
     * @Route("/{id}", name="specialist_project_phase_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param SpecialistProjectPhase $projectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(SpecialistProjectPhase $projectPhase, Request $request)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $projectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('delete', $projectGeneralInfo);

        $form = $this->createDeleteForm('project', [$projectPhase->getId()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Gets the project review (if exists) to remove it later
            $reviews = $projectPhase->getSpecialistProjectReviews();

            $projectReview = null;
            if ($reviews->count()) {
                /** @var ProjectReview $projectReview */
                $projectReview = $reviews->first();
            }

            // Gets the virtual activities - to remove them manually
            $activities = $em->getRepository('AppBundle:Specialist\Virtual\Itinerary\Activity')->findInPhase($projectPhase);

            /**
             * NOTES: Budget and most of its dependent records are removed in cascade once the phase is removed
             */
	        $specialistProject = $projectPhase->getSpecialistProject();
	        $specialistProject->removePhase($projectPhase);

	        // Removes projectPhase and ProjectGeneralInfo
            $em->remove($projectPhase);
            $em->remove($projectGeneralInfo);

//            $em->flush();

            // Removes the project if no more phases exist
            if ($specialistProject->getPhases()->count() == 0) {
                $em->remove($specialistProject);
            }
            // Removes those entities that can not be removed by DB cascade:
            // Project Review and comments, custom cost items
            if ($projectReview) {
                $em->remove($projectReview);
            }

            // Removes the activities associated to the phase
            foreach ($activities as $activity) {
                $em->remove($activity);
            }
            $em->flush();

        }

        return $this->redirectToRoute('specialist_project_index');
    }

    /**
     * Generates page to confirm removal of Fellow Project
     *
     * @Route("/{id}/delete", name="specialist_project_phase_confirm_delete")
     * @Method("GET")
     *
     * @param SpecialistProjectPhase $projectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirmAction(SpecialistProjectPhase $projectPhase)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $projectPhase->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('delete', $projectGeneralInfo);

        $deleteForm = $this->createDeleteForm('project', [$projectPhase->getId()]);

        return $this->render('project/specialist/confirm_delete.html.twig', array(
            'projectPhase' => $projectPhase,
            'projectGeneralInfo' => $projectGeneralInfo,
            'deleteForm' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to delete an additional contact point
     *
     * @param ContactPointAdditional $contactPoint
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return Form The form.
     */
    private function createDeleteContactPointForm(ContactPointAdditional $contactPoint, SpecialistProjectPhase $specialistProjectPhase)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_contact_points_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'cid' => $contactPoint->getId(),
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();
    }
}
