<?php

namespace AppBundle\Controller\Specialist;

use AppBundle\Entity\AreaOfExpertise;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\ItineraryActivity;
use AppBundle\Entity\ItineraryIndex;
use AppBundle\Entity\LivingExpense;
use AppBundle\Entity\LivingExpenseCostItem;
use AppBundle\Entity\PrePostWork;
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense;
use AppBundle\Entity\Specialist\Budget\Contribution\Transportation;
use AppBundle\Entity\Specialist\Budget\Contribution\TransportationCostItem;
use AppBundle\Entity\Specialist\Itinerary\Activity\AnticipatedAudience;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use AppBundle\Entity\Specialist\Itinerary\Rest;
use AppBundle\Entity\Specialist\Itinerary\Travel;
use AppBundle\Entity\Specialist\PartneringOrganization;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use AppBundle\Form\Specialist\CityCostsType;
use AppBundle\Form\Specialist\Itinerary\RestEditType;
use AppBundle\Form\Specialist\Itinerary\RestType;
use AppBundle\Form\Specialist\Itinerary\TravelEditType;
use AppBundle\Form\Specialist\Itinerary\TravelType;
use AppBundle\Form\Specialist\ItineraryActivitiesType;
use AppBundle\Form\Specialist\ItineraryActivityEditType;
use AppBundle\Form\Specialist\ItineraryActivityType;
use AppBundle\Form\Specialist\PhaseType;
use AppBundle\Form\Specialist\PrePostWorkType;
use AppBundle\Helper\ControllerHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Specialist In Country phase controller.
 *
 * @package AppBundle\Controller\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class PhaseInCountryController extends BaseController
{
    /**
     * City Costs: Allows to set the daily estimated costs for living expenses per city
     *
     * @Route(
     *     "/{id}/city-costs",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_city_costs"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase  $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function cityCostsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($specialistProjectPhase, [
            SpecialistProjectPhase::IN_COUNTRY_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        /** @var SpecialistProject $specialistProject */
        $specialistProject = $specialistProjectPhase->getSpecialistProject();

        $inCountryAssignments = $specialistProjectPhase->getInCountryAssignments();

        $prevAction = 'specialist_project_phase_posts';

        // verifies that at least one country is selected
        if (!$this->validateAccess($specialistProjectPhase, 0, 'countries', 'specialist_project_phase_countries')) {

            $contributionLivingCostItems = false;
            $form = $this->createForm(PhaseType::class, $specialistProjectPhase->getProjectGeneralInfo()); // to have form defined

        } else {

            $em = $this->getDoctrine()->getManager();

            /** @var SpecialistProjectPhaseBudget $phaseBudget */
            $phaseBudget = $specialistProjectPhase->getBudget();

            // TODO: Felipe - remove this section, it only applies for old projects in the dev/local environments, production must be OK due to the consistency of the data
//            if (!$phaseBudget->getContributionLivingExpenses()->count()) {
//                $contributionLivingExpense = new ContributionLivingExpense($phaseBudget);
//                $em->persist($contributionLivingExpense);
//                $em->flush();
//            }

            // preparing data for populate "anonymous" form, this just creates empty fields to add cities
            // Felipe: I found that you need an object implementing ArrayAccess for "createForm" or an
            // array, that later should be converted to object (when processing the request)
            $formData = [];
            $formData['cityCosts'] = new ArrayCollection();

            // Adds existing inCountryAssignments to the formData
            /** @var InCountryAssignment $inCountryAssignment */
            foreach ($inCountryAssignments as $inCountryAssignment) {
                $cityCost = $inCountryAssignment->getLivingExpenseEstimates()->first();
                $formData['cityCosts']->add($cityCost);
            }

            // Creates structure to show field in form to add more city costs, one per country
            $contributionLivingCostItems = $em->getRepository('AppBundle:CostItemLivingExpense')->findAll();
            foreach ($specialistProjectPhase->getProjectGeneralInfo()->getCountries() as $country) {
                $inCountryAssignment = new InCountryAssignment($country);
                $cityCost = new LivingExpense($phaseBudget, $inCountryAssignment);

                foreach($contributionLivingCostItems as $costItem) {
                    $livingExpenseCostItem = new LivingExpenseCostItem($cityCost, $costItem);
                    $cityCost->addLivingExpenseCostItem($livingExpenseCostItem);
                }
                $formData['cityCosts']->add($cityCost);
            }

            $form = $this->createForm(CityCostsType::class, $formData);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                // removes and assignment Id, and cascade delete activities, living expenses and contributions
                // and avoid any other action
                if ($form->get('deleteId')->getViewData()) {
                    $assignmentToDelete = $em->getRepository('AppBundle:InCountryAssignment')->find($form->get('deleteId')->getViewData());
                    $em->remove($assignmentToDelete);
                } else {

                    $formData = (object)$formData;
                    // checks already existing LivingExpenses and saves costs
                    foreach ($formData->cityCosts as $cityCost) {
                        if ($cityCost->getId()) {
                            foreach ($cityCost->getLivingExpenseCostItems() as $cost) {
                                $cost->calculateTotals();
                                $em->persist($cost);
                            }
                        }

                    }

                    // checks if new data was entered and creates records in DB
                    foreach ($form->get('cityCosts') as $newCost) {

                        if ($newCost->has('inCountryAssignmentId')) { // existing city

                            // we only care for saving the city here, if applies
                            if ($newCost->get('city')->getviewData()
                                && $newCost->get('city')->getviewData() != $newCost->get('currentCity')->getviewData()
                            ) {
                                $inCountryAssignment = $em->getRepository('AppBundle:inCountryAssignment')->find($newCost->get('inCountryAssignmentId')->getviewData());
                                $inCountryAssignment->setCity($newCost->get('city')->getviewData());
                                $em->persist($inCountryAssignment);
                                //						    dump($inCountryAssignment);
                            }

                        } else { // brand new city hence living expenses

                            $city = $newCost->get('city')->getviewData();
                            if ($city) {
                                /** @var LivingExpense $livingExpense */
                                $livingExpense = $newCost->getviewData();

                                $livingExpense->getInCountryAssignment()->setCity($city);
                                $livingExpense->getInCountryAssignment()->setSpecialistProjectPhase($specialistProjectPhase);
                                $em->persist($livingExpense);
                            }

                        }
                    }
                }

                $em->flush();

                $nextAction = 'specialist_project_phase_city_costs';
                $urlParameters = ['id' => $specialistProjectPhase->getId()];

                if ($form->get('back')->isClicked()) {
                    $nextAction = $prevAction;
                }
                if ($form->get('save')->isClicked() || $form->get('next')->isClicked()) {
                    $nextAction = 'specialist_project_phase_admin_panel';
                }

                return $this->redirectToRoute($nextAction, $urlParameters);
            }
        }

        return $this->render(
            'project/specialist/wizard/city_costs.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'inCountryAssignments' => $inCountryAssignments,
                'livingExpensesCostItems' => $contributionLivingCostItems,
                'phase' => 0,
                'form' => $form->createView(),
                'goBack' => $prevAction,
            ]
        );
    }

    /**
     * In Country Assignment Duty Delete
     *
     * @Route(
     *     "/{id}/city-costs/{icaid}/delete",
     *     requirements={"id": "\d+", "icaid": "\d+"},
     *     name="specialist_project_phase_city_costs_delete"
     * )
     * @ParamConverter(
     *     "inCountryAssignment",
     *     class="AppBundle:InCountryAssignment",
     *     options={"mapping": {"icaid": "id"}}
     * )
     * @Method("DELETE")
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param InCountryAssignment    $inCountryAssignment
     *
     * @return RedirectResponse
     */
    public function cityCostsDeleteAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, InCountryAssignment $inCountryAssignment)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());

        $form = $this->createDeleteForm(
            'city_costs',
            [
                $specialistProjectPhase->getId(),
                $inCountryAssignment->getId(),
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inCountryAssignment);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_city_costs',
                ['id' => $specialistProjectPhase->getId()]
            );
        }
    }

    /**
     * To confirm deletion of city costs associated to virtual projects (when JS is not enabled)
     *
     * @Route(
     *     "/{id}/{phase}/cities/{icaid}/delete",
     *     requirements={"id": "\d+", "phase": "\d+", "icaid": "\d+"},
     *     name="specialist_project_phase_city_costs_confirm_delete"
     * )
     * @ParamConverter("specialistProject", class="AppBundle:SpecialistProject", options={"mapping": {"id": "id"}})
     * @ParamConverter("inCountryAssignment", class="AppBundle:InCountryAssignment", options={"mapping": {"icaid":
     *                                        "id"}})
     * @Method("GET")
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param InCountryAssignment $inCountryAssignment
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cityCostsConfirmDeleteAction(SpecialistProject $specialistProject, $phase,
        InCountryAssignment $inCountryAssignment, Request $request)
    {
        $projectPhase = $specialistProject->getPhases()->get($phase);
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [
            SpecialistProjectPhase::IN_COUNTRY_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        $deleteForm = $this->createDeleteForm('city_costs', [
            $specialistProject->getId(),
            $phase,
            $inCountryAssignment->getId(),
        ]);

        return $this->render(
            'project/specialist/wizard/city_confirm_delete.html.twig',
            [
                'specialistProject' => $specialistProject,
                'phase' => $phase,
                'inCountryAssignment' => $inCountryAssignment,
                'delete_form' => $deleteForm->createView(),
            ]
        );

    }

    /**
     * @Route(
     *     "/{id}/pre-post-work",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_pre_post_work"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function prePostWorkAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($specialistProjectPhase, [SpecialistProjectPhase::IN_COUNTRY_TYPE]);

        /** @var PrePostWork $prePostWork */
        $prePostWork = $specialistProjectPhase->getPrePostWork();

        $currentPreWorkDays = $currentPostWorkDays = 0;
        if (null === $prePostWork) {
            $prePostWork = new PrePostWork($specialistProjectPhase);
        } else {
            $currentPreWorkDays = $prePostWork->getAdditionalPreWorkDays();
            $currentPostWorkDays = $prePostWork->getAdditionalPostWorkDays();
        }

        $form = $this->createForm(PrePostWorkType::class, $prePostWork);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $phaseBudget = $specialistProjectPhase->getBudget();
            if ($phaseBudget && $phaseBudget->getBudgetTotals()) {
                if ($currentPreWorkDays != $prePostWork->getAdditionalPreWorkDays()
                    || $currentPostWorkDays != $prePostWork->getAdditionalPostWorkDays()) {

                    $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updatePrePostWork($phaseBudget);
                    $phaseBudget->setSummaryTotals();
                }
            }

            $em->persist($prePostWork);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_admin_panel',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard:pre_post_work.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * Itinerary Dates
     *
     * @Route(
     *     "/{id}/itinerary/dates",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_dates"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryDatesAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($specialistProjectPhase, [
            SpecialistProjectPhase::IN_COUNTRY_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        $currentDates = [$specialistProjectPhase->getStartDate(), $specialistProjectPhase->getEndDate()];
        $currentDatesExist = $specialistProjectPhase->getStartDate() && $specialistProjectPhase->getStartDate();

        $translator = $this->get('translator');

        $form = $this->createForm(
            'AppBundle\Form\Specialist\ItineraryDatesType',
            $specialistProjectPhase
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $error = false;

            $nextAction = 'specialist_project_phase_admin_panel';
            $urlParameters = array('id' => $specialistProjectPhase->getId());

            // Doesn't allow empty dates
            if ($form->get('next')->isClicked() || $currentDatesExist) {
                if (!$specialistProjectPhase->getStartDate()) {
                    $this->addFlash('field-error-start-date', $translator->trans('Please enter the Start date for the Itinerary'));
                    $error = true;
                }
                if (!$specialistProjectPhase->getEndDate()) {
                    $this->addFlash('field-error-end-date', $translator->trans('Please enter the End date for the Itinerary'));
                    $error = true;
                }

            }

            // TODO: This can have some impact on budget, analyze and decide
            if ($error) {
                $this->addFlash('danger', $translator->trans('You must select Start and End dates to continue'));
                $nextAction = 'specialist_project_phase_itinerary_dates';

                // but if errors, checks if current dates were already set, then redirects to avoid losing them
                if ($currentDatesExist) {
                    return $this->redirectToRoute($nextAction, $urlParameters);
                }
            } else {

                /** @var ControllerHelper $helper */
                $helper = $this->get('proposal.helper.controller_helper');
                $itineraryIndexes = $specialistProjectPhase->getItineraryIndexes();

                $updatedIndexes = false; // to check if we must update the itinerary indexes and related data
//	            $updateSummaryTotals = false; // to trigger the updates of global values (if they are already defined)

                // If new start date is higher than old one, checks activities and removes them
                if ($currentDates[0]) {
                    if ($currentDates[0] < $specialistProjectPhase->getStartDate()) {
                        // Removes activities
                        $activitiesToDelete = $em->getRepository('AppBundle:ItineraryActivity')
                            ->findAllByDates($specialistProjectPhase, $currentDates[0], $specialistProjectPhase->getStartDate());

                        foreach ($activitiesToDelete as $item) {
                            $em->remove($item);
                        }

                        $travelsToDelete = $em->getRepository('AppBundle:Specialist\Itinerary\Travel')
                            ->findAllByDates($specialistProjectPhase, $currentDates[0], $specialistProjectPhase->getStartDate());

                        foreach ($travelsToDelete as $item) {
                            $em->remove($item);
                        }

                        $restdaysToDelete = $em->getRepository('AppBundle:Specialist\Itinerary\Rest')
                            ->findAllByDates($specialistProjectPhase, $currentDates[0], $specialistProjectPhase->getStartDate());

                        foreach ($restdaysToDelete as $item) {
                            $em->remove($item);
                        }

                        // Removes records from Itinerary Index
                        $indexToDelete = $em->getRepository('AppBundle:ItineraryIndex')
                            ->findAllByDates($specialistProjectPhase, $currentDates[0], $specialistProjectPhase->getStartDate());

                        foreach ($indexToDelete as $item) {
                            if ($item->getInCountryAssignment() != null) {
                                $updatedIndexes = true;
                            }
                            $em->remove($item);
                        }

//			            $updateSummaryTotals = true;
                    } // If the new start date is lower, dates/records must be added to the index
                    elseif ($currentDates[0] > $specialistProjectPhase->getStartDate() && $itineraryIndexes->count()) {
                        $dates = $helper->dateRange($specialistProjectPhase->getStartDate(), $currentDates[0], false);
                        foreach ($dates as $date) {
                            $itineraryIndex = new ItineraryIndex($specialistProjectPhase, $date);
                            $specialistProjectPhase->addItineraryIndex($itineraryIndex);
                            $em->persist($itineraryIndex);
                        }

//			            $updateSummaryTotals = true;
                    }
                }

                // If new end date is lower than old one, checks activities and removes them
                if ($currentDates[1]) {
                    if ($currentDates[1] > $specialistProjectPhase->getEndDate()) {

                        $activitiesToDelete = $em->getRepository('AppBundle:ItineraryActivity')
                            ->findAllByDates($specialistProjectPhase, $specialistProjectPhase->getEndDate(), $currentDates[1], true);

                        foreach ($activitiesToDelete as $item) {
                            $em->remove($item);
                        }

                        $travelsToDelete = $em->getRepository('AppBundle:Specialist\Itinerary\Travel')
                            ->findAllByDates($specialistProjectPhase, $specialistProjectPhase->getEndDate(), $currentDates[1], true);

                        foreach ($travelsToDelete as $item) {
                            $em->remove($item);
                        }

                        $restdaysToDelete = $em->getRepository('AppBundle:Specialist\Itinerary\Rest')
                            ->findAllByDates($specialistProjectPhase, $specialistProjectPhase->getEndDate(), $currentDates[1], true);

                        foreach ($restdaysToDelete as $item) {
                            $em->remove($item);
                        }

                        // Removes records from Itinerary Index
                        $indexToDelete = $em->getRepository('AppBundle:ItineraryIndex')
                            ->findAllByDates($specialistProjectPhase, $specialistProjectPhase->getEndDate(), $currentDates[1], true);

                        foreach ($indexToDelete as $item) {
                            if ($item->getInCountryAssignment() != null) {
                                $updatedIndexes = true;
                            }
                            $em->remove($item);
                        }

//			            $updateSummaryTotals = true;
                    } // If the new end date is higher, dates/records must be added to the index
                    elseif ($currentDates[1] < $specialistProjectPhase->getEndDate() && $itineraryIndexes->count()) {
                        $dates = $helper->dateRange($currentDates[1], $specialistProjectPhase->getEndDate(), true, true);
                        foreach ($dates as $date) {
                            $itineraryIndex = new ItineraryIndex($specialistProjectPhase, $date);
                            $specialistProjectPhase->addItineraryIndex($itineraryIndex);
                            $em->persist($itineraryIndex);
                        }

//			            $updateSummaryTotals = true;
                    }
                }

                // generates the itinerary index if it doesn't exist - would happen for old test projects
                $newDatesExist = $specialistProjectPhase->getStartDate() && $specialistProjectPhase->getEndDate();
                if ($newDatesExist) {
                    if (!$itineraryIndexes->count()) {
                        $dates = $helper->dateRange($specialistProjectPhase->getStartDate(), $specialistProjectPhase->getEndDate());

                        foreach ($dates as $date) {
                            $itineraryIndex = new ItineraryIndex($specialistProjectPhase, $date);
                            $specialistProjectPhase->addItineraryIndex($itineraryIndex);
                            $em->persist($itineraryIndex);
                        }
                    }

                    if ($currentDates[0] != $specialistProjectPhase->getStartDate()
                        || $currentDates[1] != $specialistProjectPhase->getEndDate()
                    ) {

                        if ($budget = $specialistProjectPhase->getBudget()) {

                            $totals = $budget->getBudgetTotals();
                            if ($totals && $totals->count()) {

                                if ($budget->getGlobalValues() == null || !$budget->getGlobalValues()->count()) {
                                    $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')
                                        ->cloneGlobalValuesToBudget($budget);
                                } else {
                                    // updates fixed PAA depending on the number of days
                                    $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')
                                        ->updatePAAValue($specialistProjectPhase);
                                }
                                // updates daily global values depending on the number of days
                                $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')
                                    ->updateDailyValues($budget);

                                $em->flush();
                                // recalculates totals, some additional validations are required for compatibility with old projects
                                if ($totals->first()->getFullCost() > 0) {
                                    $specialistProjectPhase->getBudget()->setSummaryTotals($em, true);
                                }
                            }
                        }
                    }
                }

//		        $em->persist($specialistProjectPhase);

                if ($updatedIndexes) {
                    // calculates the number of days in cities per day and updates records
                    $iiRepo = $em->getRepository('AppBundle:ItineraryIndex');
                    $iiRepo->updateCities($specialistProjectPhase);
                    $em->flush();

                    $stats = $iiRepo->getCitiesStats($specialistProjectPhase);
                    foreach ($stats as $item) {
                        $livingExpense = $em->getRepository('AppBundle:LivingExpense')->findOneByInCountryAssignment($item['id']);
                        /** @var LivingExpenseCostItem $costItem */
                        foreach ($livingExpense->getLivingExpenseCostItems() as $costItem) {
                            $costItem->setNumberOfDays($item['quantity']);
                            $costItem->calculateTotals(); // Note - Felipe: had to explicitly call it because preUpdate was not triggered
                        }
                    }

                }

                $em->flush();

                // redirects when no errors
                if ($form->get('next')->isClicked()) {
                    $nextAction = 'specialist_project_phase_itinerary_activities';
                }
                return $this->redirectToRoute($nextAction, $urlParameters);
            }
        }

        $this->validateAccess($specialistProjectPhase, 0, 'city-costs', 'specialist_project_phase_city_costs');
        $this->validateAccess($specialistProjectPhase, 0, 'partnering-organizations', 'specialist_project_phase_partnering_organizations');

        // Prepares alert message for dates change
        if ($specialistProjectPhase->getStartDate() && $specialistProjectPhase->getEndDate()) {
            $this->addFlash('warning', $translator->trans('Changing the dates may erase some current dates and their assigned activities'));
        }

        return $this->render(
            ':project/specialist/wizard/itinerary:dates.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'currentDates' => $currentDates,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );

    }

    /**
     * Itinerary Activities
     *
     * @Route(
     *     "/{id}/itinerary/activities",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_activities"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase  $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryActivitiesAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($specialistProjectPhase, [
            SpecialistProjectPhase::IN_COUNTRY_TYPE,
            SpecialistProjectPhase::MIXED_TYPE
        ]);

        $em = $this->getDoctrine()->getManager();
        $audience = $em->getRepository(Audience::class)->findAll();

        list($itineraryActivities, $emptyRows, $activityConflicts) = $this->itineraryActivitiesFormat($specialistProjectPhase);

        // Form to add Activities
        $addActivityForm = $this->createForm(
            ItineraryActivityType::class,
            $specialistProjectPhase,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_activities_add',
                    ['id' => $specialistProjectPhase->getId()]
                ),
                'audience' => $audience,
            ]
        );
        // Form to add Activities (nonJS support)
        $addActivityFormNonJS = clone $addActivityForm;

        // Delete form for Activities
        $deleteForm = $this->createDeleteForm(
            'ItineraryActivity',
            [$specialistProjectPhase->getId(), 0]
        );

        // Form to add Travel
        $addTravelForm = $this->createForm(
            TravelType::class,
            $specialistProjectPhase,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_travel_add',
                    ['id' => $specialistProjectPhase->getId()]
                ),
            ]
        );
        $addTravelFormNonJS = clone $addTravelForm;

        $deleteTravelForm = $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_itinerary_travel_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'tid' => 0,
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();

        // Form to add Rest day
        $addRestForm = $this->createForm(
            RestType::class,
            $specialistProjectPhase,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_rest_add',
                    ['id' => $specialistProjectPhase->getId()]
                ),
            ]
        );
        $addRestNonJSForm = clone $addRestForm;

        $deleteRestForm = $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_itinerary_rest_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'rid' => 0,
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();

        $form = $this->createForm(ItineraryActivitiesType::class, $specialistProjectPhase);
        $form->handleRequest($request);

        $prevStep = 'specialist_project_phase_itinerary_dates';

        if ($form->isValid()) {
            $em->persist($specialistProjectPhase);

            $nextAction = 'specialist_project_phase_admin_panel';
            $urlParameters = ['id' => $specialistProjectPhase->getId()];

            if ($form->get('back')->isClicked()) {
                $nextAction = $prevStep;
            }

            // calculates the number of days in cities per day and updates records
            $iiRepo = $em->getRepository('AppBundle:ItineraryIndex');
            $iiRepo->updateCities($specialistProjectPhase);
            $em->flush();

            $stats = $iiRepo->getCitiesStats($specialistProjectPhase);
            $cityIds = [];
            foreach($stats as $item) {
                $livingExpense = $em->getRepository('AppBundle:LivingExpense')->findOneByInCountryAssignment($item['id']);
                $cityIds[] = $item['id'];
//		        dump($livingExpense);
                /** @var LivingExpenseCostItem $costItem */
                foreach($livingExpense->getLivingExpenseCostItems() as $costItem) {
                    $costItem->setNumberOfDays($item['quantity']);
                    $costItem->calculateTotals(); // Note-Felipe: I had to explicitly call it because preUpdate was not triggered
                }
            }

            // We have to reset the living expenses not used (setNumOfDays = 0)
            $livingExpenses = $em->getRepository('AppBundle:LivingExpense')
                ->findNotUsedCities($specialistProjectPhase->getBudget(), $cityIds);

            if (count($livingExpenses)) {
                /** @var LivingExpenseCostItem $costItem */
                foreach ($livingExpenses as $livingExpense) {
                    foreach ($livingExpense->getLivingExpenseCostItems() as $costItem) {
                        $costItem->setNumberOfDays(0);
                        $costItem->calculateTotals();
                    }
                }
            }

            $em->flush();
            // updates summary totals but after other updates have taken place, this does not impact the global values
            $specialistProjectPhase->getBudget()->setSummaryTotals($em, true);

            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        $this->validateAccess($specialistProjectPhase, 0, 'city-costs', 'specialist_project_phase_city_costs');
        $this->validateAccess($specialistProjectPhase, 0, 'partnering-organizations', 'specialist_project_phase_partnering_organizations');

        return $this->render(
            ':project/specialist/wizard/itinerary:activities.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form' => $form->createView(),
                'formAddActivity' => $addActivityForm->createView(),
                'formAddActivityNonJS' => $addActivityFormNonJS->createView(),
                'deleteForm' => $deleteForm->createView(),
                'formAddTravel' => $addTravelForm->createView(),
                'formAddTravelNonJS' => $addTravelFormNonJS->createView(),
                'deleteTravelForm' => $deleteTravelForm->createView(),
                'addRestForm' => $addRestForm->createView(),
                'addRestNonJSForm' => $addRestNonJSForm->createView(),
                'deleteRestForm' => $deleteRestForm->createView(),
                'itineraryActivities' => $itineraryActivities,
                'emptyRows' => $emptyRows,
                'activityConflicts' => $activityConflicts,
                'goBack' => $prevStep,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/itinerary/activities/add",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_activities_add"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryActivityAddAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType(
            $specialistProjectPhase,
            [
                SpecialistProjectPhase::IN_COUNTRY_TYPE,
                SpecialistProjectPhase::MIXED_TYPE,
            ]
        );

        $em = $this->getDoctrine()->getManager();
        $audience = $em->getRepository(Audience::class)->findAll();

        $form = $this->createForm(
            ItineraryActivityType::class,
            $specialistProjectPhase,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_activities_add',
                    ['id' => $specialistProjectPhase->getId()]
                ),
                'audience' => $audience,
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $ranslator = $this->get('translator');

            if (null === $form->get('inCountryAssignment')->getViewData()) {
                $this->addFlash(
                    'danger',
                    $ranslator->trans('Please select the City where the Activity is going to take place.')
                );
            }
            else {
                /** @var ArrayCollection $topics */
                $topics = $form->get('topics')->getData();

                if ($topics->isEmpty() || ($topics->count() > 3)) {
                    $this->addFlash(
                        'danger',
                        $ranslator->trans('You must check at least one Topic and no more than three')
                    );
                }
                else {
                    $inCountryAssignment = $em->getRepository(InCountryAssignment::class)->find(
                        $form->get('inCountryAssignment')->getViewData()
                    );
                    $rests = $em->getRepository(Rest::class)->findAllInItinerary(
                        $specialistProjectPhase
                    );
                    $restDates = array_map(
                        function ($rest) {
                            /** @var Rest $rest */
                            return $rest->getDate();
                        },
                        $rests
                    );
                    /** @var ArrayCollection $anticipatedAudienceFormData */
                    $anticipatedAudienceFormData = $form->get('anticipatedAudience')->getData();

                    $baseItineraryActivity = new ItineraryActivity();
                    $baseItineraryActivity->setActivityType(
                        $form->get('activityType')->getData()
                    );
                    $baseItineraryActivity->setActivityTypeOther(
                        $form->get('activityTypeOther')->getData()
                    );
                    $baseItineraryActivity->setActivityTypeDesc(
                        $form->get('activityTypeDesc')->getData()
                    );

                    /** @var AreaOfExpertise $topic */
                    foreach ($topics as $topic) {
                        $baseItineraryActivity->addTopic($topic);

                        // Set the "topicOther" field only if the Other option
                        // from the Topics checklist is checked.
                        if (('Other' === $topic->getName())
                            && (null !== $form->get('topicOther')->getData())
                        ) {
                            $baseItineraryActivity->setTopicOther(
                                $form->get('topicOther')->getData()
                            );
                        }
                    }

                    $baseItineraryActivity->setDuration($form->get('duration')->getViewData());
                    $baseItineraryActivity->setInCountryAssignment($inCountryAssignment);

                    if (!$form->get('partneringOrganizationNA')->getData()) {
                        /** @var ArrayCollection $partneringOrganizations */
                        $partneringOrganizations = $form->get('partneringOrganizations')->getData();

                        /** @var PartneringOrganization $partneringOrganization */
                        foreach ($partneringOrganizations as $partneringOrganization) {
                            $baseItineraryActivity->addPartneringOrganization(
                                $partneringOrganization
                            );
                        }
                    }

                    // Iterate over every date range defined by the user.
                    foreach ($form->get('dateRanges')->getData() as $dateRange) {
                        // if user didn't enter the end date for a date range, only
                        // add the activity for the start date
                        if (null === $dateRange['endDate']) {
                            /** @var \DateTime $date */
                            $date = $dateRange['startDate'];

                            //If the $date is not an object, it comes from an
                            // empty field, skip the logic then
                            if (is_object($date)) {
                                if ($date >= $specialistProjectPhase->getStartDate()
                                    && $date <= $specialistProjectPhase->getEndDate()
                                ) {
                                    if (!in_array($date, $restDates)) {
                                        /** @var ItineraryActivity $itineraryActivity */
                                        $itineraryActivity = clone $baseItineraryActivity;
                                        $itineraryActivity->setActivityDate(
                                            $date
                                        );

                                        $em->persist($itineraryActivity);

                                        // TODO: check if there's a more efficient way to save AnticipatedAudience
                                        foreach ($anticipatedAudienceFormData as $anticipatedAudienceFormDataItem) {
                                            /** @var ArrayCollection $audienceFormData */
                                            $audienceFormData = $anticipatedAudienceFormDataItem['audience'];

                                            if (!$audienceFormData->isEmpty()) {
                                                /** @var Audience $audience */
                                                $audience = $audienceFormData->first();

                                                $anticipatedAudience = new AnticipatedAudience(
                                                    $itineraryActivity,
                                                    $audience
                                                );
                                                $anticipatedAudience->setEstimatedParticipants(
                                                    $anticipatedAudienceFormDataItem['estimatedParticipants']
                                                );

                                                if ('Other' === $audience->getName()) {
                                                    $anticipatedAudience->setAudienceOther(
                                                        $form->get('audienceOther')->getData()
                                                    );
                                                }

                                                $itineraryActivity->addAnticipatedAudience(
                                                    $anticipatedAudience
                                                );
                                            }
                                        }
                                    }
                                    else {
                                        $this->addFlash(
                                            'danger',
                                            'You cannot add neither Activities nor Travels on ' . $date->format('l, F d, Y') . ' because it is a Rest day.'
                                        );
                                    }
                                }
                                else {
                                    $this->addFlash(
                                        'danger',
                                        $date->format('l, F d, Y' ) . ' is not inside the Itinerary date range.'
                                    );
                                }
                            }
                        }
                        else {
                            $helper = $this->get('proposal.helper.controller_helper');
                            $dateRange = $helper->dateRange($dateRange['startDate'],  $dateRange['endDate']);

                            /** @var \DateTime $date */
                            foreach ($dateRange as $date) {
                                //If the $date is not an object, it comes from
                                // an empty field, skip the logic then
                                if (is_object($date)) {
                                    if ($date >= $specialistProjectPhase->getStartDate()
                                        && $date <= $specialistProjectPhase->getEndDate()
                                    ) {
                                        if (!in_array($date, $restDates)) {
                                            /** @var ItineraryActivity $itineraryActivity */
                                            $itineraryActivity = clone $baseItineraryActivity;
                                            $itineraryActivity->setActivityDate(
                                                $date
                                            );

                                            $em->persist($itineraryActivity);

                                            // TODO: check if there's a more efficient way to save AnticipatedAudience
                                            foreach ($anticipatedAudienceFormData as $anticipatedAudienceFormDataItem) {
                                                /** @var ArrayCollection $audienceFormData */
                                                $audienceFormData = $anticipatedAudienceFormDataItem['audience'];

                                                if (!$audienceFormData->isEmpty()) {
                                                    /** @var Audience $audience */
                                                    $audience = $audienceFormData->first();

                                                    $anticipatedAudience = new AnticipatedAudience(
                                                        $itineraryActivity,
                                                        $audience
                                                    );
                                                    $anticipatedAudience->setEstimatedParticipants(
                                                        $anticipatedAudienceFormDataItem['estimatedParticipants']
                                                    );

                                                    if ('Other' === $audience->getName()) {
                                                        $anticipatedAudience->setAudienceOther(
                                                            $form->get('audienceOther')->getData()
                                                        );
                                                    }

                                                    $itineraryActivity->addAnticipatedAudience(
                                                        $anticipatedAudience
                                                    );
                                                }
                                            }
                                        }
                                        else {
                                            $this->addFlash(
                                                'danger',
                                                'You cannot add neither Activities nor Travels on ' . $date->format('l, F d, Y') . ' because it is a Rest day.'
                                            );
                                        }
                                    }
                                    else {
                                        $this->addFlash(
                                            'danger',
                                            $date->format('l, F d, Y') . ' is not inside the Itinerary date range.'
                                        );
                                    }
                                }
                            }
                        }
                    }

                    $em->flush();
                }
            }

            return $this->redirectToRoute(
                'specialist_project_phase_itinerary_activities',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard/itinerary:add_activity.html.twig',
            ['formAddActivity' => $form->createView()]
        );
    }

    /**
     * @Route("/{id}/itinerary/activities/{iaid}/edit",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_activity_edit"
     * )
     * @ParamConverter(
     *     "itineraryActivity",
     *     class="AppBundle:ItineraryActivity",
     *     options={"mapping": {"iaid": "id"}}
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param ItineraryActivity      $itineraryActivity
     *
     * @return RedirectResponse|Response
     */
    public function itineraryActivityEditAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, ItineraryActivity $itineraryActivity)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType(
            $specialistProjectPhase,
            [
                SpecialistProjectPhase::IN_COUNTRY_TYPE,
                SpecialistProjectPhase::MIXED_TYPE,
            ]
        );

        $em = $this->getDoctrine()->getManager();
        $audience = $em->getRepository(Audience::class)->findAll();

        $form = $this->createForm(
            ItineraryActivityEditType::class,
            $itineraryActivity,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_activity_edit',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'iaid' => $itineraryActivity->getId(),
                    ]
                ),
                'audience' => $audience,
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()/* && $form->isValid()*/) {
            // Deals with the city selected
            $inCountryAssignment = $em->getRepository(InCountryAssignment::class)->find($form->get('inCountryAssignment')->getViewData());
            $itineraryActivity->setInCountryAssignment($inCountryAssignment);
            $anticipatedAudienceFormData = $form->get('anticipatedAudience')->getData();

            /** @var AnticipatedAudience $anticipatedAudience */
            foreach ($itineraryActivity->getAnticipatedAudience() as $anticipatedAudience) {
                $itineraryActivity->removeAnticipatedAudience($anticipatedAudience);
                $em->remove($anticipatedAudience);
            }

            foreach ($anticipatedAudienceFormData as $anticipatedAudienceFormDataItem) {
                /** @var ArrayCollection $audienceFormData */
                $audienceFormData = $anticipatedAudienceFormDataItem['audience'];

                if (!$audienceFormData->isEmpty()) {
                    /** @var Audience $audience */
                    $audience = $audienceFormData->first();

                    $anticipatedAudience = new AnticipatedAudience(
                        $itineraryActivity,
                        $audience
                    );
                    $anticipatedAudience->setEstimatedParticipants(
                        $anticipatedAudienceFormDataItem['estimatedParticipants']
                    );

                    if ('Other' === $audience->getName()) {
                        $anticipatedAudience->setAudienceOther(
                            $form->get('audienceOther')->getData()
                        );
                    }

                    $itineraryActivity->addAnticipatedAudience(
                        $anticipatedAudience
                    );
                }
            }

            if ($form->get('partneringOrganizationNA')->getData()) {
                $itineraryActivity->getPartneringOrganizations()->clear();
            }

            $em->persist($itineraryActivity);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_itinerary_activities',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard/itinerary:add_activity.html.twig',
            ['form' => $form->createView(), 'formNonJS' => null]
        );
    }

    /**
     * Itinerary Activity Delete
     *
     * @Route(
     *     "/{id}/itinerary/activity/{iaid}/delete",
     *     requirements={"id": "\d+", "iaid": "\d+"},
     *     name="specialist_project_phase_itinerary_activity_delete"
     * )
     * @ParamConverter(
     *     "itineraryActivity",
     *     class="AppBundle:ItineraryActivity",
     *     options={"mapping": {"iaid": "id"}}
     * )
     * @Method("DELETE")
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param ItineraryActivity      $itineraryActivity
     *
     * @return RedirectResponse
     */
    public function itineraryActivityDeleteAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, ItineraryActivity $itineraryActivity)
    {
        $form = $this->createDeleteForm(
            'ItineraryActivity',
            [
                $specialistProjectPhase->getId(),
                $itineraryActivity->getId(),
            ]
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($itineraryActivity);
            $em->flush();
        }

        return $this->redirectToRoute(
            'specialist_project_phase_itinerary_activities',
            ['id' => $specialistProjectPhase->getId()]
        );
    }

    /**
     * Generates page to confirm removal of Itinerary activity
     *
     * @Route(
     *     "/{id}/itinerary/activity/{iaid}/delete",
     *     requirements={"id": "\d+", "iaid": "\d+"},
     *     name="specialist_project_phase_itinerary_activity_confirm_delete"
     * )
     * @Method("GET")
     * @ParamConverter(
     *     "itineraryActivity",
     *     class="AppBundle:ItineraryActivity",
     *     options={"mapping": {"iaid": "id"}}
     * )
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param ItineraryActivity      $itineraryActivity
     *
     * @return Response
     */
    public function itineraryActivityConfirmDeleteAction(SpecialistProjectPhase $specialistProjectPhase, ItineraryActivity $itineraryActivity)
    {
        $deleteForm = $this->createDeleteForm(
            'ItineraryActivity',
            [
                $specialistProjectPhase->getId(),
                $itineraryActivity->getId(),
            ]
        );

        return $this->render(
            ':project/specialist/wizard/itinerary:activity_confirm_delete.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'itineraryActivity' => $itineraryActivity,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/itinerary/travel/add",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_travel_add"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryTravelAddAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType(
            $specialistProjectPhase,
            [
                SpecialistProjectPhase::IN_COUNTRY_TYPE,
                SpecialistProjectPhase::MIXED_TYPE,
            ]
        );

        $form = $this->createForm(
            TravelType::class,
            $specialistProjectPhase,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_travel_add',
                    ['id' => $specialistProjectPhase->getId()]
                ),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()/* && $form->isValid()*/) {
            $em = $this->getDoctrine()->getManager();
            /** @var \DateTime $date */
            $date = $form->get('date')->getData();

            if ($date >= $specialistProjectPhase->getStartDate()
                && $date <= $specialistProjectPhase->getEndDate()
            ) {
                if ((null != $form->get('origin')->getData())
                    && (null != $form->get('destination')->getData())
                ) {
                    $inCountryAssignmentRepo = $em->getRepository(
                        InCountryAssignment::class
                    );

                    $origin = $inCountryAssignmentRepo->find(
                        $form->get('origin')->getData()
                    );
                    $destination = $inCountryAssignmentRepo->find(
                        $form->get('destination')->getData()
                    );

                    if ((null !== $origin) && (null !== $destination)) {
                        $rests = $em->getRepository(Rest::class)->findAllInItinerary(
                            $specialistProjectPhase
                        );
                        $restDates = array_map(
                            function ($rest) {
                                return $rest->getDate();
                            },
                            $rests
                        );

                        if (!in_array($date, $restDates)) {
                            $travel = new Travel($origin, $destination);
                            $travel->setDate($date);
                            $travel->setMode($form->get('mode')->getData());
                            $travel->setTravelTime(
                                $form->get('travelTime')->getData()
                            );
                            $travel->setBookingResponsible(
                                $form->get('bookingResponsible')->getData()
                            );

                            $em->persist($travel);
                            $em->flush();

                            /** @var Transportation $transportation */
                            $transportation = $specialistProjectPhase->getBudget()->getContributionTransportation();

                            if (null === $transportation) {
                                $transportation = new Transportation(
                                    $specialistProjectPhase->getBudget()
                                );

                                $em->persist($transportation);
                                $em->flush();
                            }

                            $transportationItem = new TransportationCostItem(
                                $transportation
                            );
                            $transportationItem->setTravel($travel);

                            $em->persist($transportationItem);
                            $em->flush();

                            $this->addFlash(
                                'success',
                                'Travel was added successfully to the Itinerary.'
                            );
                        } else {
                            $this->addFlash(
                                'danger',
                                'You cannot add neither Activities nor Travels on ' . $date->format('l, F d, Y') . ' because it is a Rest day.'
                            );
                        }
                    }
                } else {
                    $this->addFlash(
                        'danger',
                        'Please select an option for both <strong>To</strong> and <strong>From</strong> fields to add a new Travel.'
                    );
                }
            }
            else {
                $this->addFlash(
                    'danger',
                    $date->format('l, F d, Y') . ' is not inside the Itinerary date range.'
                );
            }

            return $this->redirectToRoute(
                'specialist_project_phase_itinerary_activities',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard/itinerary/travel:add.html.twig',
            ['formAddActivity' => $form->createView()]
        );
    }

    /**
     * Edit a travel.
     *
     * @Route(
     *     "/{id}/itinerary/travel/{tid}/edit",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_travel_edit"
     * )
     * @ParamConverter(
     *     "travel",
     *     class="AppBundle:Specialist\Itinerary\Travel",
     *     options={"mapping": {"tid": "id"}}
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param Travel            $travel
     * @param int               $phase
     *
     * @return RedirectResponse
     */
    public function itineraryTravelEditAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, Travel $travel)
    {
        $form = $this->createForm(
            TravelEditType::class,
            $travel,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_travel_edit',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'tid' => $travel->getId(),
                    ]
                ),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()/* && $form->isValid()*/) {
            $em = $this->getDoctrine()->getManager();

            $inCountryAssignmentRepo = $em->getRepository(InCountryAssignment::class);

            $origin = $inCountryAssignmentRepo->find($form->get('origin')->getData());
            $destination = $inCountryAssignmentRepo->find($form->get('destination')->getData());

            $travel->setOrigin($origin);
            $travel->setDestination($destination);

            $em->persist($travel);
            $em->flush();

            $this->addFlash(
                'success',
                'The travel was updated successfully.'
            );

            return $this->redirectToRoute(
                'specialist_project_phase_itinerary_activities',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard/itinerary/travel:add.html.twig',
            ['form' => $form->createView(), 'formNonJS' => null]
        );
    }

    /**
     * Delete a travel from Itinerary.
     *
     * @Route(
     *     "/{id}/itinerary/travel/{tid}/delete",
     *     requirements={"id": "\d+", "tid": "\d+"},
     *     name="specialist_project_phase_itinerary_travel_delete"
     * )
     * @ParamConverter(
     *     "travel",
     *     class="AppBundle:Specialist\Itinerary\Travel",
     *     options={"mapping": {"tid": "id"}}
     * )
     * @Method("DELETE")
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param Travel                 $travel
     *
     * @return RedirectResponse
     */
    public function itineraryTravelDeleteAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, Travel $travel)
    {
        $deleteTravelForm = $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_itinerary_travel_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'tid' => $travel->getId(),
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();
        $deleteTravelForm->handleRequest($request);

        if ($deleteTravelForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($travel);
            $em->flush();

            $this->addFlash(
                'success',
                'The travel activity was delete from the Itinerary successfully.'
            );
        }

        return $this->redirectToRoute(
            'specialist_project_phase_itinerary_activities',
            ['id' => $specialistProjectPhase->getId()]
        );
    }

    /**
     * @Route(
     *     "/{id}/itinerary/rest/add",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_rest_add"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryRestAddAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType(
            $specialistProjectPhase,
            [
                SpecialistProjectPhase::IN_COUNTRY_TYPE,
                SpecialistProjectPhase::MIXED_TYPE,
            ]
        );

        $form = $this->createForm(
            RestType::class,
            $specialistProjectPhase,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_rest_add',
                    ['id' => $specialistProjectPhase->getId()]
                ),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()/* && $form->isValid()*/) {
            $em = $this->getDoctrine()->getManager();

            if (null === $form->get('inCountryAssignment')->getData()) {
                $this->addFlash(
                    'danger',
                    'Please select the City where the Rest is going to take place.'
                );
            }
            else {
                $inCountryAssignment = $em->getRepository(InCountryAssignment::class)->find(
                    $form->get('inCountryAssignment')->getData()
                );

                if (null === $inCountryAssignment) {
                    $this->addFlash(
                        'danger',
                        'Error while trying to associate the City for the Rest day.'
                    );
                } else {
                    $activities = $em->getRepository(ItineraryActivity::class)->findAllInItinerary(
                        $specialistProjectPhase
                    );
                    $activitiesDates = array_map(
                        function ($activity) {
                            return $activity->getActivityDate();
                        },
                        $activities
                    );
                    $travels = $em->getRepository(Travel::class)->findAllInItinerary(
                        $specialistProjectPhase
                    );
                    $travelsDates = array_map(
                        function ($travel) {
                            return $travel->getDate();
                        },
                        $travels
                    );
                    $activitiesTravelsDates = array_merge(
                        $activitiesDates,
                        $travelsDates
                    );

                    // Iterate over every date range defined by the user.
                    foreach ($form->get('dateRanges')->getData() as $dateRange) {
                        // if user didn't enter the end date for a date range,
                        // only add the activity for the start date.
                        if (null === $dateRange['endDate']) {
                            /** @var \DateTime $date */
                            $date = $dateRange['startDate'];

                            if ($date instanceof \DateTime) {
                                // Date must be inside the Itinerary date range.
                                if ($date >= $specialistProjectPhase->getStartDate()
                                    && $date <= $specialistProjectPhase->getEndDate()
                                ) {
                                    if (!in_array($date, $activitiesTravelsDates)) {
                                        $rest = new Rest($inCountryAssignment);
                                        $rest->setDate($dateRange['startDate']);

                                        $em->persist($rest);
                                    } else {
                                        $this->addFlash(
                                            'warning',
                                            $date->format(
                                                'l, F d, Y'
                                            ) . ' cannot be Rest day because there are Activities and/or Travels on it.'
                                        );
                                    }
                                } else {
                                    $this->addFlash(
                                        'danger',
                                        $date->format('l, F d, Y') . ' is not inside the Itinerary date range.'
                                    );
                                }
                            }
                        } else {
                            $helper = $this->get('proposal.helper.controller_helper');
                            $dateRange = $helper->dateRange($dateRange['startDate'], $dateRange['endDate']);

                            /** @var \DateTime $date */
                            foreach ($dateRange as $date) {
                                if ($date >= $specialistProjectPhase->getStartDate()
                                    && $date <= $specialistProjectPhase->getEndDate()
                                ) {
                                    if (!in_array($date, $activitiesTravelsDates)) {
                                        $rest = new Rest($inCountryAssignment);
                                        $rest->setDate($date);

                                        $em->persist($rest);
                                    }
                                    else {
                                        $this->addFlash(
                                            'warning',
                                            $date->format('l, F d, Y').' cannot be Rest day because there are Activities and/or Travels on it.'
                                        );
                                    }
                                }
                                else {
                                    $this->addFlash(
                                        'danger',
                                        $date->format('l, F d, Y').' is not inside the Itinerary date range.'
                                    );
                                }
                            }
                        }
                    }

                    $em->flush();
                }
            }

            return $this->redirectToRoute(
                'specialist_project_phase_itinerary_activities',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard/itinerary/rest:add.html.twig',
            ['formAddActivity' => $form->createView()]
        );
    }

    /**
     * Edit a rest day.
     *
     * @Route(
     *     "/{id}/itinerary/rest/{rid}/edit",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_rest_edit"
     * )
     * @ParamConverter(
     *     "rest",
     *     class="AppBundle:Specialist\Itinerary\Rest",
     *     options={"mapping": {"rid": "id"}}
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param Rest                   $rest
     *
     * @return RedirectResponse|Response
     */
    public function itineraryRestEditAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, Rest $rest)
    {
        $form = $this->createForm(
            RestEditType::class,
            $rest,
            [
                'action' => $this->generateUrl(
                    'specialist_project_phase_itinerary_rest_edit',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'rid' => $rest->getId(),
                    ]
                ),
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()/* && $form->isValid()*/) {
            $em = $this->getDoctrine()->getManager();

            $inCountryAssignment = $em->getRepository(InCountryAssignment::class)->find(
                $form->get('inCountryAssignment')->getData()
            );

            $rest->setInCountryAssignment($inCountryAssignment);

            $em->persist($rest);
            $em->flush();

            $this->addFlash(
                'success',
                'The Rest day was updated successfully.'
            );

            return $this->redirectToRoute(
                'specialist_project_phase_itinerary_activities',
                ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            ':project/specialist/wizard/itinerary/rest:add.html.twig',
            ['form' => $form->createView(), 'formNonJS' => null]
        );
    }

    /**
     * Delete a rest day from Itinerary.
     *
     * @Route(
     *     "/{id}/itinerary/rest/{rid}/delete",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_rest_delete"
     * )
     * @ParamConverter(
     *     "rest",
     *     class="AppBundle:Specialist\Itinerary\Rest",
     *     options={"mapping": {"rid": "id"}}
     * )
     * @Method("DELETE")
     *
     * @param Request                $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param Rest                   $rest
     *
     * @return RedirectResponse
     */
    public function itineraryRestDeleteAction(Request $request, SpecialistProjectPhase $specialistProjectPhase, Rest $rest)
    {
        $deleteRestForm = $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'specialist_project_phase_itinerary_rest_delete',
                    [
                        'id' => $specialistProjectPhase->getId(),
                        'rid' => $rest->getId(),
                    ]
                )
            )
            ->setMethod(Request::METHOD_DELETE)
            ->getForm();
        $deleteRestForm->handleRequest($request);

        if ($deleteRestForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rest);
            $em->flush();

            $this->addFlash(
                'success',
                'The Rest day was delete from the Itinerary successfully.'
            );
        }

        return $this->redirectToRoute(
            'specialist_project_phase_itinerary_activities',
            ['id' => $specialistProjectPhase->getId()]
        );
    }
}
