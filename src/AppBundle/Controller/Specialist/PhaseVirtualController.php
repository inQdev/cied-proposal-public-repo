<?php

namespace AppBundle\Controller\Specialist;

use AppBundle\Entity\AreaOfExpertise;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use AppBundle\Entity\Specialist\Virtual\Itinerary\WeekAfter;
//use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity as VirtualActivity;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\AnticipatedAudience as VirtualAnticipatedAudience;
use AppBundle\Form\Specialist\ConsiderationsType;
use AppBundle\Form\Specialist\ItineraryActivitiesType;
use AppBundle\Form\Specialist\ItineraryDatesType;
use AppBundle\Form\Specialist\Virtual\Itinerary\ActivityType as VirtualActivityType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Specialist Virtual phase controller.
 *
 * @package AppBundle\Controller\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class PhaseVirtualController extends BaseController
{
    /**
     * @Route(
     *     "/{id}/considerations",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_considerations"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function considerationsAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {

        $this->denyAccessUnlessGranted('edit', $specialistProjectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($specialistProjectPhase, [SpecialistProjectPhase::VIRTUAL_TYPE]);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        if ($specialistProjectPhase->getType() == 'in-country') {
            // TODO: prevent access to section
        }

        $form = $this->createForm(ConsiderationsType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_admin_panel', ['id' => $specialistProjectPhase->getId()]
            );
        }

        return $this->render(
            'project/specialist/wizard/considerations.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form'                   => $form->createView(),
                'goBack'                 => null,
            ]
        );
    }

    /**
     * Virtual Itinerary dates.
     *
     * @Route(
     *     "/{id}/virtual-itinerary/dates",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_virtual_dates"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProjectPhase $projectPhase The Specialist project
     * @param Request           $request           The request
     *
     * @return Response|RedirectResponse
     */
    public function itineraryDatesAction(SpecialistProjectPhase $projectPhase, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [
            SpecialistProjectPhase::VIRTUAL_TYPE,
        ]);

        $currentDates = [$projectPhase->getStartDate(), $projectPhase->getEndDate()];

        $form = $this->createForm(ItineraryDatesType::class, $projectPhase);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $translator = $this->get('translator');
            $error = false;

            if (!$projectPhase->getStartDate()) {
                $this->addFlash(
                    'field-error-start-date',
                    $translator->trans(
                        'Please enter the Start date for the Itinerary'
                    )
                );

                $error = true;
            }
            if (!$projectPhase->getEndDate()) {
                $this->addFlash(
                    'field-error-end-date',
                    $translator->trans(
                        'Please enter the End date for the Itinerary'
                    )
                );

                $error = true;
            }

            if ($error) {
                // If there are error messages, show them only when user clicks
                // "Save and Continue"
                if ($form->get('next')->isClicked()) {
                    $nextAction = 'specialist_project_phase_itinerary_virtual_dates';

                    $this->addFlash(
                        'danger',
                        $translator->trans(
                            'You must select Start and End dates to continue'
                        )
                    );
                } else {
                    $nextAction = 'specialist_project_phase_admin_panel';
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($projectPhase);
                $em->flush();

                return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
            } else {
                $this->getItineraryDateRanges(
                    $projectPhase,
                    $projectPhase->getStartDate(),
                    $projectPhase->getEndDate()
                );
            }

            $nextAction = $form->get('save')->isClicked()
                ? 'specialist_project_phase_admin_panel'
                : 'specialist_project_phase_itinerary_virtual_activities';

            return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
        }

        return $this->render(
            ':project/specialist/wizard/virtual/itinerary:dates.html.twig',
            [
                'specialistProjectPhase' => $projectPhase,
                'currentDates' => $currentDates,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * Virtual Itinerary activities.
     *
     * @Route(
     *     "/{id}/virtual-itinerary/activities",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_virtual_activities"
     * )
     * @Route(
     *     "/{id}/mixed-virtual-itinerary/activities",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_mixed_virtual_activities"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProjectPhase $projectPhase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function itineraryActivitiesAction(SpecialistProjectPhase $projectPhase, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [
            SpecialistProjectPhase::VIRTUAL_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        $em = $this->getDoctrine()->getManager();
        $audience = $em->getRepository(Audience::class)->findAll();

        // This action is shared by Virtual and Mixed proposals
        $prevAction = SpecialistProjectPhase::MIXED_TYPE === $projectPhase->getType()
            ? 'specialist_project_phase_itinerary_mixed_virtual_dates'
            : 'specialist_project_phase_itinerary_virtual_dates';

        // @todo: this form is being used as a placeholder to not break the template footer; it MUST be replaced.
        $form = $this->createForm(ItineraryActivitiesType::class, $projectPhase);
        $form->handleRequest($request);

        // Form to add Activities
        $addActivityForm = $this->createForm(
            VirtualActivityType::class,
            null,
            [
                'action' => $this->generateUrl('specialist_project_phase_itinerary_virtual_activity_add', [
                	'id' => $projectPhase->getId()
                ]),
                'project_phase' => $projectPhase,
                'audience' => $audience,
            ]
        );

        $deleteActivityWeekform = $this->createDeleteForm('ItineraryVirtualActivity', [
	        $projectPhase->getId(),
	        0,
        ]);

        if ($form->isValid()) {
            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('back')->isClicked()) {
                $nextAction = $prevAction;
            }

            // Assuming changes in activities, the summary totals must be recalculated
            if ($projectPhase->getBudget() != null) {
                $phaseBudget = $projectPhase->getBudget();

                // TODO: refactor updateVirtualGlobals for mixed, we just assume that fixed virtual globals don't apply for mixed proposals given the requirements
                if ($projectPhase->getType() == SpecialistProjectPhase::VIRTUAL_TYPE) {
                    $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updateVirtualGlobals($phaseBudget);
                }
                $phaseBudget->setSummaryTotals($em, true);
            }

            return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
        }

        $weeks = $projectPhase->getVirtualItineraryWeeks()->filter(
            function ($week) {
                return !$week instanceof WeekAfter;
            }
        );

        $weeksAfter = $projectPhase->getVirtualItineraryWeeksAfter();

        // To build the summary table, we need the activities
        $activities = $em->getRepository('AppBundle:Specialist\Virtual\Itinerary\Activity')->findInPhase($projectPhase);

        return $this->render(
            ':project/specialist/wizard/virtual/itinerary/:activities.html.twig',
            [
                'specialistProjectPhase' => $projectPhase,
                'activities' => $activities,
                'form' => $form->createView(),
                'add_activity_form' => $addActivityForm->createView(),
                'delete_activity_form' => $deleteActivityWeekform->createView(),
                'weeks' => $weeks,
                'weeks_after' => $weeksAfter,
                'goBack' => $prevAction,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/virtual-itinerary/activities/add",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_virtual_activity_add"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $projectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryActivityAddAction(Request $request, SpecialistProjectPhase $projectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [
            SpecialistProjectPhase::VIRTUAL_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        $em = $this->getDoctrine()->getManager();
        $audience = $em->getRepository(Audience::class)->findAll();
        $nextAction = SpecialistProjectPhase::MIXED_TYPE === $projectPhase->getType()
            ? 'specialist_project_phase_itinerary_mixed_virtual_activities'
            : 'specialist_project_phase_itinerary_virtual_activities';

        $form = $this->createForm(
            VirtualActivityType::class,
            null,
            ['project_phase' => $projectPhase, 'audience' => $audience]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $translator = $this->get('translator');

            /** @var ArrayCollection $topics */
            $topics = $form->get('topics')->getData();

            if ($topics->isEmpty() || ($topics->count() > 3)) {
                $this->addFlash(
                    'danger',
                    $translator->trans('You must check at least one Topic and no more than three')
                );
            } else {
                /** @var ArrayCollection $anticipatedAudienceFormData */
                $anticipatedAudienceFormData = $form->get('anticipatedAudience')->getData();
                /** @var ArrayCollection $weeksFormData */
                $activityWeeksFormData = $form->get('weeks')->getData();

                $virtualActivity = new VirtualActivity();
                $virtualActivity->setType(
                    $form->get('type')->getData()
                );
                $virtualActivity->setTypeOther(
                    $form->get('typeOther')->getData()
                );
                $virtualActivity->setTypeDesc(
                    $form->get('typeDesc')->getData()
                );
                $virtualActivity->setOnlineAvailabilityDesc(
                    $form->get('onlineAvailabilityDesc')->getData()
                );

                /** @var AreaOfExpertise $topic */
                foreach ($topics as $topic) {
                    $virtualActivity->addTopic($topic);

                    // Set the "topicOther" field only if the Other option
                    // from the Topics checklist is checked.
                    if (('Other' === $topic->getName())
                        && (null !== $form->get('topicOther')->getData())
                    ) {
                        $virtualActivity->setTopicOther(
                            $form->get('topicOther')->getData()
                        );
                    }
                }

                $em->persist($virtualActivity);

                foreach ($anticipatedAudienceFormData as $anticipatedAudienceFormDataItem) {
                    /** @var ArrayCollection $audienceFormData */
                    $audienceFormData = $anticipatedAudienceFormDataItem['audience'];

                    if (!$audienceFormData->isEmpty()) {
                        /** @var Audience $audience */
                        $audience = $audienceFormData->first();

                        $anticipatedAudience = new VirtualAnticipatedAudience(
                            $virtualActivity,
                            $audience
                        );
                        $anticipatedAudience->setEstimatedParticipants(
                            $anticipatedAudienceFormDataItem['estimatedParticipants']
                        );

                        if ('Other' === $audience->getName()) {
                            $anticipatedAudience->setAudienceOther(
                                $form->get('audienceOther')->getData()
                            );
                        }

                        $em->persist($anticipatedAudience);

                        $virtualActivity->addAnticipatedAudience(
                            $anticipatedAudience
                        );
                    }
                }

                $totalDuration = 0;
                foreach ($activityWeeksFormData as $activityWeekFormData) {
                    /** @var ArrayCollection $weekFormData */
                    $weekFormData = $activityWeekFormData['week'];

                    if (!$weekFormData->isEmpty()) {
                        /** @var Week $week */
                        $week = $weekFormData->first();

                        $activityWeek = new ActivityWeek(
                            $virtualActivity,
                            $week
                        );
                        $activityWeek->setDuration(
                            $activityWeekFormData['hours']
                        );
                        $totalDuration += $activityWeekFormData['hours'];

                        $em->persist($activityWeek);
                    }
                }
                $virtualActivity->setTotalDuration($totalDuration);
            }

            $em->flush();

            return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
        }

        return $this->render(
            ':project/specialist/wizard/virtual/itinerary/:add_activity.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/{id}/virtual-itinerary/activities/{aid}/edit",
     *     requirements={"id": "\d+", "aid": "\d+"},
     *     name="specialist_project_phase_itinerary_virtual_activity_edit"
     * )
     * @ParamConverter(
     *     "specialistProjectPhase",
     *     class="AppBundle:SpecialistProjectPhase",
     *     options={"mapping": {"id": "id"}}
     * )
     * @ParamConverter(
     *     "activity",
     *     class="AppBundle:Specialist\Virtual\Itinerary\Activity",
     *     options={"mapping": {"aid": "id"}}
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProjectPhase $projectPhase
     * @param VirtualActivity   $activity
     * @param Request           $request
     *
     * @return RedirectResponse|Response
     */
    public function itineraryActivityEditAction(Request $request, SpecialistProjectPhase $projectPhase, VirtualActivity $activity)
    {
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [
            SpecialistProjectPhase::VIRTUAL_TYPE,
            SpecialistProjectPhase::MIXED_TYPE,
        ]);

        $em = $this->getDoctrine()->getManager();
        $audience = $em->getRepository(Audience::class)->findAll();

        // This action is shared by Virtual and Mixed proposals
        $nextAction = SpecialistProjectPhase::MIXED_TYPE === $projectPhase->getType()
            ? 'specialist_project_phase_itinerary_mixed_virtual_activities'
            : 'specialist_project_phase_itinerary_virtual_activities';

        $form = $this->createForm(
            VirtualActivityType::class,
            $activity,
            [
                'action' => $this->generateUrl('specialist_project_phase_itinerary_virtual_activity_edit', [
                        'id' => $projectPhase->getId(),
                        'aid' => $activity->getId(),
	            ]),
                'project_phase' => $projectPhase,
                'audience' => $audience,
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            /** @var ArrayCollection $anticipatedAudienceFormData */
            $anticipatedAudienceFormData = $form->get('anticipatedAudience')->getData();
            /** @var ArrayCollection $weeksFormData */
            $activityWeeksFormData = $form->get('weeks')->getData();

            /** @var VirtualAnticipatedAudience $activity */
            foreach ($activity->getAnticipatedAudience() as $anticipatedAudience) {
                $activity->removeAnticipatedAudience($anticipatedAudience);
                $em->remove($anticipatedAudience);
            }

            /** @var ActivityWeek $activityWeek */
            foreach ($activity->getWeeks() as $activityWeek) {
                $activity->removeWeek($activityWeek);
                $em->remove($activityWeek);
            }

            foreach ($anticipatedAudienceFormData as $anticipatedAudienceFormDataItem) {
                /** @var ArrayCollection $audienceFormData */
                $audienceFormData = $anticipatedAudienceFormDataItem['audience'];

                if (!$audienceFormData->isEmpty()) {
                    /** @var Audience $audience */
                    $audience = $audienceFormData->first();

                    $anticipatedAudience = new VirtualAnticipatedAudience(
                        $activity,
                        $audience
                    );
                    $anticipatedAudience->setEstimatedParticipants(
                        $anticipatedAudienceFormDataItem['estimatedParticipants']
                    );

                    if ('Other' === $audience->getName()) {
                        $anticipatedAudience->setAudienceOther(
                            $form->get('audienceOther')->getData()
                        );
                    }

                    $em->persist($anticipatedAudience);

                    $activity->addAnticipatedAudience(
                        $anticipatedAudience
                    );
                }
            }

            $totalDuration = 0;
            foreach ($activityWeeksFormData as $activityWeekFormData) {
                /** @var ArrayCollection $weekFormData */
                $weekFormData = $activityWeekFormData['week'];

                if (!$weekFormData->isEmpty()) {
                    /** @var Week $week */
                    $week = $weekFormData->first();

                    $activityWeek = new ActivityWeek($activity, $week);
                    $activityWeek->setDuration($activityWeekFormData['hours']);
                    $totalDuration += $activityWeekFormData['hours'];

                    $em->persist($activityWeek);
                }
            }
            $activity->setTotalDuration($totalDuration);

//            $em->persist($activity);
            $em->flush();

            return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
        }

        return $this->render(
            ':project/specialist/wizard/virtual/itinerary/:add_activity.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Itinerary Activity Delete
     *
     * @Route(
     *     "/{id}/virtual-itinerary/activities/{awid}/delete",
     *     requirements={"id": "\d+", "awid": "\d+"},
     *     name="specialist_project_phase_itinerary_virtual_activity_delete"
     * )
     * @ParamConverter(
     *     "specialistProjectPhase",
     *     class="AppBundle:SpecialistProjectPhase",
     *     options={"mapping": {"id": "id"}}
     * )
     * @ParamConverter(
     *     "activityWeek",
     *     class="AppBundle:Specialist\Virtual\Itinerary\Activity\ActivityWeek",
     *     options={"mapping": {"awid": "id"}}
     * )
     * @Method({"DELETE"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $projectPhase
     * @param ActivityWeek      $activityWeek
     *
     * @return RedirectResponse
     */
    public function itineraryActivityDeleteAction(Request $request, SpecialistProjectPhase $projectPhase, ActivityWeek $activityWeek)
    {
        // This action is shared by Virtual and Mixed proposals
        $nextAction = SpecialistProjectPhase::MIXED_TYPE === $projectPhase->getType()
            ? 'specialist_project_phase_itinerary_mixed_virtual_activities'
            : 'specialist_project_phase_itinerary_virtual_activities';

        $form = $this->createDeleteForm('ItineraryVirtualActivity', [
	        $projectPhase->getId(),
	        $activityWeek->getId(),
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $translator = $this->get('translator');

            $em = $this->getDoctrine()->getManager();
            // gets the activity and recalculates the totalDuration
            /** @var VirtualActivity $activity */
            $activity = $activityWeek->getActivity();
            $activity->removeWeek($activityWeek);

            $activity->calculateTotalDuration();

            // If after removing this ActivityWeek from the ones associated
            // to the Activity, there aren't more weeks associated to the Activity,
            // the Activity should be removed to avoid it to get orphan.
            if ($activity->getWeeks()->isEmpty()) {
                $em->remove($activity);
            }
//            else {
//                $em->persist($activity);
//            }

            $em->remove($activityWeek);

            // checks if the budget summary exists, then recalculates
	        if ($projectPhase->getBudget()->getBudgetTotals()->count()) {
		        // checks fixed costs for virtual and mixed
		        $phaseBudget = $projectPhase->getBudget();
		        if ($projectPhase->getType() == SpecialistProjectPhase::VIRTUAL_TYPE) {
			        $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updateVirtualGlobals($phaseBudget);
		        }
		        $phaseBudget->setSummaryTotals($em);
	        }

            $em->flush();

            $this->addFlash(
                'success',
                $translator->trans('Activity deleted from Week successfully.')
            );
        }

        return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
    }

}
