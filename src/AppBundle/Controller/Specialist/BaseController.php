<?php

namespace AppBundle\Controller\Specialist;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\ItineraryActivity;
use AppBundle\Entity\Specialist\Itinerary\Rest;
use AppBundle\Entity\Specialist\Itinerary\Travel;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use AppBundle\Entity\Specialist\Virtual\Itinerary\WeekAfter;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity as VirtualActivity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Specialist phase base controller.
 *
 * @package AppBundle\Controller\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class BaseController extends Controller implements RoleSwitcherInterface
{
    /**
     * General purpose function to create a form to delete an item.
     * TODO: Add more cases according to needs
     *
     * @param string
     * @param mixed
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($type, $parameters)
    {
        switch ($type) {
            // To delete Virtual Assignment Activities
            case 'ItineraryVirtualActivity':
                $action = 'specialist_project_phase_itinerary_virtual_activity_delete';
                $urlParameters = [
                    'id' => $parameters[0],
                    'awid' => $parameters[1],
                ];
                break;

            // To delete Itinerary Activities
            case 'ItineraryActivity':
                $action = 'specialist_project_phase_itinerary_activity_delete';
                $urlParameters = [
                    'id' => $parameters[0],
                    'iaid' => $parameters[1],
                ];
                break;

            // To delete InCountry (city-cost or city) for projects
            case 'city_costs':
                $action = 'specialist_project_phase_city_costs_delete';
                $urlParameters = [
                    'id' => $parameters[0],
                    'icaid' => $parameters[1],
                ];
                break;

            // To delete a project/project phase
            case 'project':
                $action = 'specialist_project_phase_delete';
                $urlParameters = ['id' => $parameters[0]];
                break;

        }

        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl($action, $urlParameters)
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * TODO: Remove the phase parameter from function and all calls
     * Checks some data dependencies in some wizard pages, offering links to go to the right page to fulfill the
     * dependency
     *
     * @param SpecialistProjectPhase $projectPhase
     * @param $phase
     * @param string $dependency
     * @param string $type
     *
     * @return boolean
     */
    protected function validateAccess(SpecialistProjectPhase $projectPhase, $phase=0, $dependency, $link=null, $type='mandatory') {
        $translator = $this->get('translator');
        /** @var Session $session */
        $session = $this->get('session');
        $message = '';
        $messageLink = [];

        if ($link) {
            $messageLink = ['%link%' => '<a class="alert-link" href="'
                . $this->generateUrl($link, ['id' => $projectPhase->getId()])
	            . '">here</a>'];
        }

        switch ($dependency) {
            case 'city-costs':
                if (!$projectPhase->getInCountryAssignments()->count()) {
                    $message = $translator->trans('Please make sure you have added at least one city %link%', $messageLink);
                }
                break;

            case 'countries':
                if (!$projectPhase->getProjectGeneralInfo()->getCountries()->count()) {
                    $message = $translator->trans('Please make sure you have selected at least one country %link%', $messageLink);
                }
                break;

            case 'region':
                if (!$projectPhase->getProjectGeneralInfo()->getRegion()) {
                    $message = $translator->trans('Please make sure you have selected a region for the project %link%', $messageLink);
                }
                break;

            case 'partnering-organizations':
                if (!$projectPhase->getPartneringOrganizations()->count()) {
                    $message = $translator->trans('Please make sure you have selected at least one Partnering Organization %link%', $messageLink);
                }
                break;

            case '':
                break;


        }

        if ($message) {
            $this->addFlash('danger', $message);
            $session->getFlashBag()->add('block-access', 'true');
            return false;
        }

        return true;
    }

    /**
     * Validates the type of project against an array of valid types
     * @param SpecialistProjectPhase $phase
     * @param array $types
     *
     * @return boolean or generates Exception
     */
    protected function denyAccessUnlessType(SpecialistProjectPhase $phase, $types=[]) {
        if (!in_array($phase->getType(), $types)) {

            //loads the translation service
            $translator = $this->get('translator');
            $message = $translator->trans('This section is not available for this type of proposal');

            throw $this->createAccessDeniedException($message);
        }

        return true;
    }

    /**
     * Helper function to get all activities, travels and rest-days and setup variables to show them easily
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @return array
     */
    protected function itineraryActivitiesFormat($specialistProjectPhase) {
        $em = $this->getDoctrine()->getManager();

        $itineraryActivities = $emptyRows = $conflicts = [];

        if ($specialistProjectPhase->getStartDate() && $specialistProjectPhase->getEndDate()) {
            // Gets the dates range for phase and gets the activities
            $helper = $this->get('proposal.helper.controller_helper');
            $dates = $helper->dateRange($specialistProjectPhase->getStartDate(), clone $specialistProjectPhase->getEndDate());

            $activities = $em->getRepository('AppBundle:ItineraryActivity')->findAllInItinerary($specialistProjectPhase);
            $travels = $em->getRepository(Travel::class)->findAllInItinerary($specialistProjectPhase);
            $rests = $em->getRepository(Rest::class)->findAllInItinerary($specialistProjectPhase);
            $previousCity = '';

            /** @var \DateTime $date */
            foreach($dates as $date) {
                $idx = $date->getTimestamp();
                $itineraryActivities[$idx] = [
                    'activities' => [],
                    'travels' => [],
                    'rest' => [],
                    'empty' => true,
                    'activities_conflict' => false,
                ];

                $switch = 0;
                /** @var Travel $travel */
                while (!$switch && $travel = current($travels)) {
                    if ($travel->getDate() == $date) {
                        $itineraryActivities[$idx]['travels'][] = $travel;
                        $itineraryActivities[$idx]['empty'] = false;
                        array_shift($travels);
                    }
                    else {
                        $switch = 1;
                    }
                }

                $switch = 0;
                /** @var ItineraryActivity $activity */
                while (!$switch && $activity = current($activities)) {
                    if ($activity->getActivityDate() == $date) {

                        // checks if there are several activities in the date, then check the cities,
                        // if different, checks the travels array to define if there's any conflict there
                        if (count($itineraryActivities[$idx]['activities'])) {

                            $currentCity = $activity->getInCountryAssignment()->getCity();
                            if ($previousCity != $currentCity) {
                                // checks travels array, most of the time only one travel will be found
                                $activitiesConflict =  true;
                                if (count($itineraryActivities[$idx]['travels'])) {
                                    $cities = [$previousCity, $currentCity];
                                    foreach ($itineraryActivities[$idx]['travels'] as $travel) {
                                        if (in_array($travel->getDestination()->getCity(), $cities)
                                            && in_array($travel->getOrigin()->getCity(), $cities)) {
                                            $activitiesConflict = false;
                                        }
                                    }
                                }

                                $itineraryActivities[$idx]['activities_conflict'] = $activitiesConflict;
                            }
                        }

                        $itineraryActivities[$idx]['activities'][] = $activity;
                        $itineraryActivities[$idx]['empty'] = false;
                        array_shift($activities);
                        $previousCity = $activity->getInCountryAssignment()->getCity();
                    } else {
                        $switch = 1;
                    }
                }

                $switch = 0;
                /** @var Rest $rest */
                while (!$switch && $rest = current($rests)) {
                    if ($rest->getDate() == $date) {
                        $itineraryActivities[$idx]['rest'][] = $rest;
                        $itineraryActivities[$idx]['empty'] = false;
                        array_shift($rests);
                    }
                    else {
                        $switch = 1;
                    }
                }

                // Adds the date to the emptyRows array to show the tooltip easily in front-end
                if ($itineraryActivities[$idx]['empty']) {
                    $emptyRows[] = $date;
                }

                // Adds the date to the conflicts array to show the tooltip easily in front-end
                if ($itineraryActivities[$idx]['activities_conflict']) {
                    $conflicts[] = $date;
                }

            }

        }

        return [$itineraryActivities, $emptyRows, $conflicts];
    }

	/**
	 * @param SpecialistProjectPhase $projectPhase
	 * @param \DateTime              $startDate
	 * @param \DateTime              $endDate
	 * @param bool                   $after
	 */
	protected function getItineraryDateRanges(SpecialistProjectPhase $projectPhase, \DateTime $startDate, \DateTime $endDate, $after = false) {
		$em = $this->getDoctrine()->getManager();

		/** @var ArrayCollection $itineraryWeeks */
		$itineraryWeeks = $after
			? $projectPhase->getVirtualItineraryWeeksAfter()
			: $projectPhase->getVirtualItineraryWeeks()->filter(
				function ($week) {
					return !$week instanceof WeekAfter;
				}
			);

		/** @var Week $itineraryFirstWeek */
		$itineraryFirstWeek = $itineraryWeeks->first();
		/** @var Week $itineraryLastWeek */
		$itineraryLastWeek = $itineraryWeeks->last();

		$itineraryRanges = [];
		$recalculate = false;

		// This block will be executed the first time only (first user enters a Itinerary date range.)
		if ($itineraryWeeks->isEmpty()) {
			$itineraryRanges[] = ['start' => $startDate, 'end' => $endDate];
		} elseif ($startDate < $itineraryFirstWeek->getStartDate()) {
			if ($endDate < $itineraryFirstWeek->getStartDate()) {
				/** @var Week $itineraryWeek */
				foreach ($itineraryWeeks as $itineraryWeek) {
					/** @var ActivityWeek $activityWeek */
					foreach ($itineraryWeek->getActivities() as $activityWeek) {
						/** @var VirtualActivity $activity */
						$activity = $activityWeek->getActivity();
						$activity->removeWeek($activityWeek);

						// calculates new value for total duration
						$activity->calculateTotalDuration();

						// If after removing this ActivityWeek form the ones associated
						// to the Activity, there aren't more weeks associated to the Activity,
						// the Activity should be removed to avoid it to get orphan.
						if ($activity->getWeeks()->isEmpty()) {
							$em->remove($activity);
						} else {
							$em->persist($activity);
						}
					}

					$em->remove($itineraryWeek);
				}

				$em->flush();

				$itineraryRanges[] = ['start' => $startDate, 'end' => $endDate];
				$recalculate = true;
			} else {
				$itineraryRanges[] = [
					'start' => $startDate,
					'end' => $itineraryFirstWeek->getStartDate(),
				];
			}

			if ($endDate > $itineraryLastWeek->getEndDate()) {
				$itineraryRanges[] = [
					'start' => $itineraryLastWeek->getEndDate()->add(new \DateInterval('P1D')),
					'end' => $endDate,
				];
			}
		} elseif ($startDate >= $itineraryFirstWeek->getStartDate()) {
			if ($startDate > $itineraryLastWeek->getEndDate()) {
				/** @var Week $itineraryWeek */
				foreach ($itineraryWeeks as $itineraryWeek) {
					/** @var ActivityWeek $activityWeek */
					foreach ($itineraryWeek->getActivities() as $activityWeek) {
						/** @var VirtualActivity $activity */
						$activity = $activityWeek->getActivity();
						$activity->removeWeek($activityWeek);

						// calculates new value for total duration
						$activity->calculateTotalDuration();

						// If after removing this ActivityWeek form the ones associated
						// to the Activity, there aren't more weeks associated to the Activity,
						// the Activity should be removed to avoid it to get orphan.
						if ($activity->getWeeks()->isEmpty()) {
							$em->remove($activity);
						} else {
							$em->persist($activity);
						}
					}

					$em->remove($itineraryWeek);
				}

				$em->flush();

				$itineraryRanges[] = ['start' => $startDate, 'end' => $endDate];
				$recalculate = true;
			} elseif ($endDate < $itineraryLastWeek->getEndDate()) {
				/** @var Week $itineraryWeek */
				foreach ($itineraryWeeks as $itineraryWeek) {
					// Check if the week is within the date range.
					$isWeekInDateRange = ($itineraryWeek->getStartDate() >= $startDate)
						&& ($itineraryWeek->getEndDate() <= $endDate);
					// Check if the Itinerary start date is within the week.
					$isStartDateInWeek = ($startDate >= $itineraryWeek->getStartDate())
						&& ($startDate <= $itineraryWeek->getEndDate());
					// Check if the Itinerary end date is within the week.
					$isEndDateInWeek = ($endDate >= $itineraryWeek->getStartDate())
						&& ($endDate <= $itineraryWeek->getEndDate());

					if (!$isWeekInDateRange && !$isStartDateInWeek && !$isEndDateInWeek) {
						/** @var ActivityWeek $activityWeek */
						foreach ($itineraryWeek->getActivities() as $activityWeek) {
							/** @var VirtualActivity $activity */
							$activity = $activityWeek->getActivity();
							$activity->removeWeek($activityWeek);

							// calculates new value for total duration
							$activity->calculateTotalDuration();

							// If after removing this ActivityWeek form the ones associated
							// to the Activity, there aren't more weeks associated to the Activity,
							// the Activity should be removed to avoid it to get orphan.
							if ($activity->getWeeks()->isEmpty()) {
								$em->remove($activity);
							} else {
								$em->persist($activity);
							}
						}

						$em->remove($itineraryWeek);
					}

					$em->flush();
				}

				$recalculate = true;
			} elseif ($endDate > $itineraryLastWeek->getEndDate()) {
				$itineraryRanges[] = [
					'start' => $itineraryLastWeek->getEndDate()->add(new \DateInterval('P1D')),
					'end' => $endDate,
				];
			}
		}

		// This block should be executed if there are weeks to be added only
		if (!empty($itineraryRanges)) {
			$newItineraryWeeks = [];
			$weekConverter = $this->get('proposal.utils.week_converter');

			// Calculate weeks start and end dates based on provided ranges.
			foreach ($itineraryRanges as $itineraryRange) {
				$itineraryStartDate = $itineraryRange['start'];
				$itineraryEndDate = $itineraryRange['end'];

				if ($itineraryStartDate instanceof \DateTime && $itineraryEndDate instanceof \DateTime) {
					$weeks = $weekConverter->getWeeksRanges($itineraryStartDate, $itineraryEndDate);

					$newItineraryWeeks = array_merge($newItineraryWeeks, $weeks);
				}
			}

			if (!empty($newItineraryWeeks)) {
				$itineraryWeekBase = $after
					? new WeekAfter($projectPhase)
					: new Week($projectPhase);

				foreach ($newItineraryWeeks as $week) {
					$itineraryWeek = clone $itineraryWeekBase;
					$itineraryWeek->setStartDate($week['start']);
					$itineraryWeek->setEndDate($week['end']);

					$em->persist($itineraryWeek);
				}
			}

			$em->persist($projectPhase);
			$em->flush();
		}

		// checks if the budget summary exists, then recalculates
		// Assuming changes in activities, the summary totals must be recalculated
		if ($projectPhase->getBudget() != null
			&& $projectPhase->getBudget()->getBudgetTotals()->count()
			&& $recalculate) {

			$phaseBudget = $projectPhase->getBudget();
			if ($projectPhase->getType() == SpecialistProjectPhase::VIRTUAL_TYPE) {
				// checks fixed costs for virtual
				$em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updateVirtualGlobals($phaseBudget);
			}
			$phaseBudget->setSummaryTotals($em);
			$em->flush();
		}
	}

}
