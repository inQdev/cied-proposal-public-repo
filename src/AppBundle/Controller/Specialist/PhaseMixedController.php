<?php

namespace AppBundle\Controller\Specialist;

use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Form\Specialist\Mixed\Itinerary\VirtualComponentType;
use AppBundle\Form\Specialist\Mixed\Itinerary\VirtualDatesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Specialist Mixed phase controller.
 *
 * @package AppBundle\Controller\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class PhaseMixedController extends BaseController
{
    /**
     * Virtual Itinerary component for Mixed proposals.
     *
     * @Route(
     *     "/{id}/mixed-virtual-itinerary",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_mixed_virtual_component"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $projectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryVirtualComponentAction(Request $request, SpecialistProjectPhase $projectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [SpecialistProjectPhase::MIXED_TYPE]);

        $storedMixedVirtualComponent = $projectPhase->getMixedVirtualComponent();

        $form = $this->createForm(VirtualComponentType::class, $projectPhase);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $submittedMixedVirtualComponent = $projectPhase->getMixedVirtualComponent();
            $nextAction = 'specialist_project_phase_itinerary_mixed_virtual_dates';

            if (null === $submittedMixedVirtualComponent) {
                if ($form->get('save')->isClicked()) {
                    $nextAction = 'specialist_project_phase_admin_panel';
                } else {
                    $nextAction = 'specialist_project_phase_itinerary_mixed_virtual_component';

                    $this->addFlash(
                        'danger',
                        'You must select when the Virtual component will occur.'
                    );
                }
            } else {
                if ($submittedMixedVirtualComponent !== $storedMixedVirtualComponent) {
                    $projectPhase
                        ->setVirtualStartDateAfter(null)
                        ->setVirtualEndDateAfter(null)
                        ->setVirtualStartDateBefore(null)
                        ->setVirtualEndDateBefore(null);

                    // All weeks will be deleted if user changes the Virtual Component for the
                    // Mixed proposal.
                    /** @var Week $itineraryWeek */
                    foreach ($projectPhase->getVirtualItineraryWeeks() as $itineraryWeek) {
                        $projectPhase->removeVirtualItineraryWeek($itineraryWeek);
                        $em->remove($itineraryWeek);
                    }
                }

                if ($form->get('save')->isClicked()) {
                    $nextAction = 'specialist_project_phase_admin_panel';
                }
            }

            $em->persist($projectPhase);
            $em->flush();

            return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
        }

        // Prepares alert message for dates change
        if ($storedMixedVirtualComponent !== null) {
            $translator = $this->get('translator');
            $this->addFlash('warning', $translator->trans('Changing your selection may erase some current weeks and their assigned activities'));
        }

        return $this->render(
            ':project/specialist/wizard/mixed/itinerary:virtual_component.html.twig',
            [
//                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $projectPhase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * Mixed Virtual Itinerary dates.
     *
     * @Route(
     *     "/{id}/mixed-virtual-itinerary/dates",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_itinerary_mixed_virtual_dates"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $projectPhase
     *
     * @return RedirectResponse|Response
     */
    public function itineraryVirtualDatesAction(Request $request, SpecialistProjectPhase $projectPhase)
    {
        $this->denyAccessUnlessGranted('edit', $projectPhase->getProjectGeneralInfo());
        $this->denyAccessUnlessType($projectPhase, [SpecialistProjectPhase::MIXED_TYPE]);

        $prevAction = 'specialist_project_phase_itinerary_mixed_virtual_component';

        $form = $this->createForm(VirtualDatesType::class, $projectPhase);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $translator = $this->get('translator');
            $submittedMixedVirtualComponent = $projectPhase->getMixedVirtualComponent();
            $nextAction = 'specialist_project_phase_itinerary_mixed_virtual_dates';
            $error = false;

            if (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_BEFORE === $submittedMixedVirtualComponent) {
                if (!$projectPhase->getVirtualStartDateBefore()) {
                    $this->addFlash(
                        'field-error-start-date',
                        $translator->trans(
                            'Please enter the Estimated Start Date for the Itinerary'
                        )
                    );
                    $error = true;
                }

                if (!$projectPhase->getVirtualEndDateBefore()) {
                    $this->addFlash(
                        'field-error-end-date',
                        $translator->trans(
                            'Please enter the Estimated End Date for the Itinerary'
                        )
                    );
                    $error = true;
                }
            } elseif (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_AFTER === $submittedMixedVirtualComponent) {
                if (!$projectPhase->getVirtualStartDateAfter()) {
                    $this->addFlash(
                        'field-error-start-date',
                        $translator->trans(
                            'Please enter the Estimated Start Date for the Itinerary'
                        )
                    );
                    $error = true;
                }

                if (!$projectPhase->getVirtualEndDateAfter()) {
                    $this->addFlash(
                        'field-error-end-date',
                        $translator->trans(
                            'Please enter the Estimated End Date for the Itinerary'
                        )
                    );
                    $error = true;
                }
            } elseif (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_BEFORE_AND_AFTER === $submittedMixedVirtualComponent) {
                if (!$projectPhase->getVirtualStartDateBefore()) {
                    $this->addFlash(
                        'field-error-start-date',
                        $translator->trans(
                            'Please enter the Estimated Start Date for the Itinerary'
                        )
                    );
                    $error = true;
                }

                if (!$projectPhase->getVirtualEndDateBefore()) {
                    $this->addFlash(
                        'field-error-end-date',
                        $translator->trans(
                            'Please enter the Estimated End Date for the Itinerary'
                        )
                    );
                    $error = true;
                }

                if (!$projectPhase->getVirtualStartDateAfter()) {
                    $this->addFlash(
                        'field-error-start-date-after',
                        $translator->trans(
                            'Please enter the Estimated Start Date for the Itinerary'
                        )
                    );
                    $error = true;
                }

                if (!$projectPhase->getVirtualEndDateAfter()) {
                    $this->addFlash(
                        'field-error-end-date-after',
                        $translator->trans(
                            'Please enter the Estimated End Date for the Itinerary'
                        )
                    );
                    $error = true;
                }
            }

            if ($error) {
                // If there are error messages, show them only when user clicks
                // "Save and Continue"
                if ($form->get('next')->isClicked()) {
                    $nextAction = 'specialist_project_phase_itinerary_mixed_virtual_dates';

                    $this->addFlash(
                        'danger',
                        $translator->trans(
                            'You must select Start and End dates to continue'
                        )
                    );
                } else {
                    $nextAction = $form->get('save')->isClicked()
                        ? 'specialist_project_phase_admin_panel'
                        : 'specialist_project_phase_itinerary_mixed_virtual_component';
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($projectPhase);
                $em->flush();

                return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
            } else {
                if (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_BEFORE === $submittedMixedVirtualComponent) {
                    $this->getItineraryDateRanges(
                        $projectPhase,
                        $projectPhase->getVirtualStartDateBefore(),
                        $projectPhase->getVirtualEndDateBefore()
                    );
                } elseif (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_AFTER === $submittedMixedVirtualComponent) {
                    $this->getItineraryDateRanges(
                        $projectPhase,
                        $projectPhase->getVirtualStartDateAfter(),
                        $projectPhase->getVirtualEndDateAfter()
                    );
                } elseif (SpecialistProjectPhase::MIXED_VIRTUAL_COMPONENT_BEFORE_AND_AFTER === $submittedMixedVirtualComponent) {
                    $this->getItineraryDateRanges(
                        $projectPhase,
                        $projectPhase->getVirtualStartDateBefore(),
                        $projectPhase->getVirtualEndDateBefore()
                    );
                    $this->getItineraryDateRanges(
                        $projectPhase,
                        $projectPhase->getVirtualStartDateAfter(),
                        $projectPhase->getVirtualEndDateAfter(),
                        true
                    );
                }
            }

            if ($form->get('save')->isClicked()) {
                $nextAction = 'specialist_project_phase_admin_panel';
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = 'specialist_project_phase_itinerary_mixed_virtual_component';
            } elseif ($form->get('next')->isClicked()) {
                $nextAction = 'specialist_project_phase_itinerary_mixed_virtual_activities';
            }

            return $this->redirectToRoute($nextAction, ['id' => $projectPhase->getId()]);
        }

        // Prepares alert message for dates change
        if ($projectPhase->checkVirtualComponentDates()) {
            $translator = $this->get('translator');
            $this->addFlash('warning', $translator->trans('Changing the dates may erase some current weeks and their assigned activities'));
        }

        return $this->render(
            ':project/specialist/wizard/mixed/itinerary:virtual_dates.html.twig',
            [
//                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $projectPhase,
                'form' => $form->createView(),
                'goBack' => $prevAction,
            ]
        );
    }
}
