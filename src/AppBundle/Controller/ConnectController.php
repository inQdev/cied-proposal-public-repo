<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fellow\CycleFellow;
use HWI\Bundle\OAuthBundle\Controller\ConnectController as BaseConnectController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ConnectController controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ConnectController extends BaseConnectController
{
    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function connectAction(Request $request)
    {
        return $this->redirectToRoute(
          'hwi_oauth_service_redirect',
          ['service' => 'cied']
        );
    }

	/**
	 * Helper action to create the Role switcher dropdown and avoid the Twig cache on the
	 * header.html.twig template
	 * It works with the authenticated user data and the cookie set in the application system
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function roleSwitcherFormAction() {
		$authenticatedUser = $this->getUser();
		$parsedRoles = $authenticatedUser->parseRolesArray($authenticatedUser->getRoles());

		$currentRole = $this->get('security.token_storage')->getToken()->getRoles()[0]->getRole();

		$selectedRole = 0;
		foreach($parsedRoles as $key=>$role) {
			if ($key == $currentRole) {
				$selectedRole = $role['roleId'];
				break;
			}
		}

		return $this->render(
			'role_switch.html.twig', [
			'parsedRoles' => $parsedRoles,
			'selectedRole' => $selectedRole,
		]);
	}

	/**
	 * Helper action to create the user menu based on the current role selected
	 * in the Role Switcher.
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function roleMenuAction() {
		$em = $this->getDoctrine()->getManager();

		try {
			/** @var CycleFellow $currentFellowCycle */
			$currentFellowCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();
		} catch (NoResultException $e) {
			$currentFellowCycle = $em->getRepository(CycleFellow::class)->findLast();
		}

		$session = $this->get('session');

		$currentRole = $this->get('security.token_storage')->getToken()->getRoles()[0]->getRole();
		$formattedCurrentRole = strtolower(str_replace(array('ROLE_', '_'), array('', '-'), $currentRole));

		$apiRequest = $this->get('proposal.drupal_data_consumer');

		if (!$session->has('menu_' . $formattedCurrentRole)) {
			$response = $apiRequest->sendRequest(
				'GET',
				'/rolemenu/' . $formattedCurrentRole
			);

			$session->set('menu_' . $formattedCurrentRole, $response);
		}
		else {
			$response = $session->get('menu_' . $formattedCurrentRole);
		}

		return $this->render(
			'menu.html.twig',
			[
				'roleMenu'    => $response,
				'fellowCycle' => $currentFellowCycle,
			]
		);
	}

}
