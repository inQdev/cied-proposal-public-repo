<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\GlobalPAA;
use AppBundle\Form\GlobalValueType;
use AppBundle\Form\GlobalPAAType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * GlobalValue controller.
 *
 * @Route("/admin/global-value")
 */
class GlobalValueController extends Controller implements RoleSwitcherInterface
{
	/**
	 * Lists all GlobalValueFellow entities.
	 *
	 * @Route("/", name="proposal_global_value_index")
	 * @Method("GET")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

		$fellowGlobalValues = $em->getRepository('AppBundle:GlobalValueFellow')->findAll();
		$specialistGlobalValues = $em->getRepository('AppBundle:GlobalValueSpecialist')->findBy([], ['sortOrder' => 'ASC', 'id' => 'ASC']);
		$globalPAA = $em->getRepository('AppBundle:GlobalPAA')->findAll();

		return $this->render(
			'global_value/index.html.twig',
			[
				'fellowGlobalValues' => $fellowGlobalValues,
				'specialistGlobalValues' => $specialistGlobalValues,
				'globalPAA' => $globalPAA,
			]
		);
	}

	/**
	 * Displays a form to edit an existing GlobalValueFellow entities.
	 *
	 * @Route("/fellow/edit", name="proposal_global_value_fellow_edit")
	 * @Method({"GET", "POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function editFellowAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$formData = new \stdClass();
		$formData->globalValues = $em->getRepository('AppBundle:GlobalValueFellow')->findAll();

		$form = $this->createFormBuilder($formData)
			->add('globalValues', CollectionType::class, [
				'entry_type' => GlobalValueType::class,
			])
			->add('submit', SubmitType::class, ['label' => 'Save'])
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			foreach ($formData->globalValues as $globalValue) {
				$em->persist($globalValue);
			}
			$em->flush();

			return $this->redirectToRoute('proposal_global_value_index');
		}

		return $this->render(
			'global_value/edit.html.twig',
			[
				'form' => $form->createView(),
				'type' => 'Fellow'
			]
		);
	}

	/**
	 * Displays a form to edit an existing GlobalValueSpecialist entities.
	 *
	 * @Route("/specialist/{type}/edit/", name="proposal_global_value_specialist_edit")
	 * @Method({"GET", "POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param string $type
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function editSpecialistAction(Request $request, $type='in-country')
	{
		$em = $this->getDoctrine()->getManager();

		$formData = new \stdClass();
		$formData->globalValues = $em->getRepository('AppBundle:GlobalValueSpecialist')->findBy(
			['specialistType' => $type],
			['sortOrder' => 'ASC']
		);

		$formBuilder = $this->createFormBuilder($formData)
			->add('globalValues', CollectionType::class, [
				'entry_type' => GlobalValueType::class,
			])
			->add('deleteId', HiddenType::class, [
				'mapped' => false,
				'required' => false,
			])
			->add('submit', SubmitType::class, ['label' => 'Save']);

		if ($type == 'in-country') {
			$formData->globalPAA = $em->getRepository('AppBundle:GlobalPAA')->findAll();
			$formBuilder
				->add('globalPAA', CollectionType::class, [
					'entry_type' => GlobalPAAType::class,
				])
				->add('addPAA', SubmitType::class, ['label' => 'Add PAA range'])
				->add('here', SubmitType::class);

		}

		$form = $formBuilder->getForm();
		$form->handleRequest($request);

		if ($form->isValid()) {
			$nextAction = 'proposal_global_value_index';
			$parameters = [];

			foreach ($formData->globalValues as $globalValue) {
				$em->persist($globalValue);
			}

			if ($form->has('addPAA')) {
				foreach ($formData->globalPAA as $paaValue) {
					$em->persist($paaValue);
				}

				if ($form->get('addPAA')->isClicked()) {
					$paaValue = new GlobalPAA();
					$em->persist($paaValue);

					$nextAction = 'proposal_global_value_specialist_edit';
					$parameters['type'] = 'in-country';
				}

				if ($form->get('deleteId')->getViewData()) {
					$paaToDelete = $em->getRepository('AppBundle:GlobalPAA')->find($form->get('deleteId')->getViewData());
					$em->remove($paaToDelete);

					$nextAction = 'proposal_global_value_specialist_edit';
					$parameters['type'] = 'in-country';
				}
			}
			$em->flush();

			return $this->redirectToRoute($nextAction, $parameters);
		}

		return $this->render(
			'global_value/edit.html.twig',
			[
				'form' => $form->createView(),
				'type' => 'Specialist'
			]
		);
	}
}
