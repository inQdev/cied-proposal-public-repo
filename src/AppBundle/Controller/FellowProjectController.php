<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContactPoint;
use AppBundle\Entity\ContactPointAdditional;
use AppBundle\Entity\ContactPointSecondary;
use AppBundle\Entity\CostItemCustom;
use AppBundle\Entity\CostItemCustomInKind;
use AppBundle\Entity\CostItemMonthly;
use AppBundle\Entity\CostItemOneTime;
use AppBundle\Entity\Country;
use AppBundle\Entity\Fellow\Budget\Revision;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\Fellow\DutyActivityPrimary;
use AppBundle\Entity\Fellow\DutyActivitySecondary;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudget;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem;
//use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetTotals;
use AppBundle\Entity\Host;
use AppBundle\Entity\LocationHostInfo;
use AppBundle\Entity\Post;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReview;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use AppBundle\Entity\WizardStepStatus;
use AppBundle\Form\Common\ContactPointsType;
use AppBundle\Form\CountryType;
use AppBundle\Form\Fellow\CreateProjectType as CreateFellowProjectType;
use AppBundle\Form\FellowProjectFilterType;
use AppBundle\Form\PostReloType;
use AppBundle\Form\RegionType;
use AppBundle\Form\SubmitFellowProposalType;
use AppBundle\Presenter\FellowProjectPresenter;
use Doctrine\ORM\NoResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Solarium\QueryType\Select\Result\FacetSet;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * FellowProject controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @Route("/project/fellow")
 */
class FellowProjectController extends Controller implements RoleSwitcherInterface
{
    /**
     * @Route("/{id}/admin-panel", name="project_fellow_admin_menu")
     * @Method({"GET"})
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminPanelAction(FellowProject $fellowProject) {

        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        $em = $this->getDoctrine()->getManager();

        $submitProjectForm = $this->createForm(
            'AppBundle\Form\SubmitFellowProposalType',
            null,
            [
                'action' => $this->generateUrl(
                    'fellow_project_submit',
                    ['id' => $fellowProject->getId()]
                ),
            ]
        );

        $shareProjectForm = $this->createFormBuilder()
            ->setMethod('POST')
            ->setAction(
                $this->generateUrl(
                    'project_fellow_share',
                    ['id' => $fellowProject->getId()]
                )
            )
            ->add('shareProject', SubmitType::class, ['label' => 'Save'])
            ->getForm();

        $authors = $fellowProject->getProjectGeneralInfo()->getAuthors()->toArray();

        // Include creator user in the authors list
        $authors[] = $fellowProject->getProjectGeneralInfo()->getCreatedBy();

        // Get all users with RELO and EMBASSY roles, excluding those who are
        // authors of this project already.
        $embassyRELOUsers = $em->getRepository('AppBundle:User')
            ->findAllByRoles(['ROLE_EMBASSY', 'ROLE_RELO'], $authors);

        // The data should be mapped in a way the JS auto-complete plugin
        // understands it.
        $embassyRELOUsers = array_map(
            function ($user) {
                return [
                    'id' => $user['id'],
                    'name' => trim($user['name'] . ' <' . $user['email'] . '>'),
                    'fullname' => $user['name'],
                    'email' => $user['email'],
                ];
            },
            $embassyRELOUsers
        );

        return $this->render(
            'fellowproject/admin_panel.html.twig',
            [
                'fellowProject' => $fellowProject,
                'fellowProjectPresenter' => new FellowProjectPresenter(
                    $fellowProject
                ),
                'form' => $submitProjectForm->createView(),
                'embassyRELOUsers' => json_encode($embassyRELOUsers),
                'shareProjectForm' => $shareProjectForm->createView(),
            ]
        );
    }

    /**
     * @Route("/", name="proposal_fellow_index")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        // Retrieve all Fellow Projects created/own by the authenticated user.
        // TODO: This info should be retrieved from FellowProject, not ProjectGeneralInfo one.
        $fellowProposalsGeneralInfo = $em
            ->getRepository('AppBundle:ProjectGeneralInfo')
            ->findAllFellowProposalsByUser($authenticatedUser);

        $sharedProposals = $authenticatedUser->getProjects()->toArray();

        // Projects associated to user include both Fellow and Specialist ones.
        // For this action, only Fellow ones are required.
        $sharedProposals = array_filter(
            $sharedProposals,
            function ($sharedProposal) {
                return null !== $sharedProposal->getFellowProject();
            }
        );

        return $this->render(
            'fellowproject/index.html.twig',
            [
                'fellowProposalsGeneralInfo' => $fellowProposalsGeneralInfo,
                'sharedProposals' => $sharedProposals,
            ]
        );
    }

    /**
     * Browse Fellow projects.  The projects will be retrieved based on the
     * authenticated user active role.
     *
     * @Route("/browse", name="fellow_project_browse")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function browseAction(Request $request)
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        try {
            /** @var CycleFellow $currentFellowCycle */
            $currentCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();
        } catch (NoResultException $e) {
            $currentCycle = $em->getRepository(CycleFellow::class)->findLast();
        }

        $cycleForm = $this->get('form.factory')
            ->createNamedBuilder('cycle', EntityType::class, $currentCycle, [
                'method' => 'GET',
                'class' => CycleFellow::class,
                'choice_label' => 'name',
            ])
            ->getForm();

        $cycleForm->handleRequest($request);

        $fellowProjectRepo = $this->get('proposal.solr_repository.fellow_project');

        $allowedSorts = [
            'reference_number_s',
            'proposal_status_s',
            'proposal_outcome_s',
            'region_s',
            'rpo_ranking_i',
            'relo_location_s',
            'relo_ranking_i',
            'country_s',
            'host_institution_s',
            'cycle_season_s',
        ];

        $formData = $request->query->get('fellow_project_filter', []);
        $criteria = [
            'cycle_id_i' => [$cycleForm->getData()->getId()],
        ];
        $facetFields = ['country_s', 'cycle_season_s', 'proposal_status_s', 'proposal_outcome_s'];
        $results = null;
        $totalECAContribution = null;

        // If user's active role is RELO, projects should be filtered by his/her
        // RELO location associated.
        if ($this->isGranted('ROLE_RELO')) {
            $terms = $authenticatedUser->getReloLocations()->map(function ($reloLocation) {
                return (string) $reloLocation;
            })->toArray();

            if (count($terms)) {
                $criteria['relo_location_s'] = $terms;
            } else {
                $results = [
                    'facet_set' => new FacetSet([]),
                    'fellow_projects' => [],
                ];
            }
        } elseif ($this->isGranted('ROLE_RPO')) {
            $terms = $authenticatedUser->getRegions()->map(function ($region) {
                return $region->getAcronym();
            })->toArray();

            if (count($terms)) {
                $criteria['region_s'] = $terms;
            } else {
                $results = [
                    'facet_set' => new FacetSet([]),
                    'fellow_projects' => [],
                ];
            }

            $facetFields[] = 'relo_location_s';

//            $totalECAContribution = $em
//              ->getRepository(FellowProjectBudget::class)
//              ->getTotalECAContributionInRegionsInCycle($authenticatedUser->getRegions(), $cycleForm->getData());
        } elseif ($this->isGranted('ROLE_STATE_DEPARTMENT') || $this->isGranted('ROLE_ADMIN')) {
            $facetFields[] = 'relo_location_s';
            $facetFields[] = 'region_s';

//            $totalECAContribution = $em
//              ->getRepository(FellowProjectBudget::class)
//              ->getTotalECAContributionInCycle($cycleForm->getData());
        } else {
            $results = [
                'facet_set' => new FacetSet([]),
                'fellow_projects' => [],
            ];
        }

        foreach ($facetFields as $filter) {
            if (isset($formData[$filter])) {
                $criteria[$filter] = $formData[$filter];
            }
        }

        if (!$results) {
            $results = $fellowProjectRepo->getFacetedResultsForSearch($criteria, $facetFields, $allowedSorts, 1, $request->query->get('sort'), $request->query->get('sortDirection'));
        }

        $form = $this->createForm(FellowProjectFilterType::class, [], [
            'csrf_protection' => false,
            'facet_set' => $results['facet_set'],
        ]);

        $form->handleRequest($request);

        return $this->render(
          'fellowproject/browse.html.twig',
          [
            'totalECAContribution' => $totalECAContribution,
            'fellowProjects'       => $results['fellow_projects'],
            'cycleForm'            => $cycleForm->createView(),
            'form'                 => $form->createView(),
          ]
        );
    }

	/**
	 * @Route("/shopping-cart/share", name="fellow_project_shopping_cart_share")
	 * @Method({"GET"})
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function shoppingCartShareAction(Request $request)
	{
		/** @var User $authenticatedUser */
		$authenticatedUser = $this->getUser();
		$em = $this->getDoctrine()->getManager();

		if ($this->isGranted('ROLE_RPO')) {
			/** @var Region $region */
			$region = $authenticatedUser->getRegions()->first();
//			$region->getAcronym();

			$notification = $this->get('proposal.helper.notification_helper');
			$notification->shareCartNotification($authenticatedUser, $region);

			$translator = $this->get('translator');
			$this->addFlash('success', $translator->trans('Your message was sent successfully'));
		}

		return $this->redirectToRoute('fellow_project_shopping_cart');

	}

	/**
     * @Route("/shopping-cart", name="fellow_project_shopping_cart")
     * @Route("/shopping-cart/{acronym}", name="fellow_project_region_shopping_cart")
     * @Method({"GET"})
     *
     * @ParamConverter("region", options={"mapping": {"acronym": "acronym"}})
     *
     * @param Request $request The request.
     * @param Region  $region  The region.
     *
     * @return Response
     */
    public function shoppingCartAction(Request $request, Region $region = null)
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $regions = $em->getRepository(Region::class)->findAll();

        $fellowProjectSolrRepo = $this->get('proposal.solr_repository.fellow_project');

        $facetFields = ['relo_location_s', 'country_s', 'cycle_season_s', 'proposal_status_s'];

        $allowedSorts = [
            'reference_number_s',
            'proposal_status_s',
            'proposal_outcome_s',
            'region_s',
            'rpo_ranking_i',
            'relo_location_s',
            'relo_ranking_i',
            'country_s',
            'host_institution_s',
            'cycle_season_s',
            'eca_total_contribution_f',
            'total_cost_f'
        ];

        $terms = [];
        $results = null;
        $fellowProjectsUnderConsideration = [];

        try {
            /** @var CycleFellow $currentFellowCycle */
            $currentCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();
        } catch (NoResultException $e) {
        }

        $formData = $request->query->get('fellow_project_filter', []);

        $criteria = ['cycle_id_i' => [$currentCycle->getId()]];

        // For ECA users (a/k/a State Department users), the Region acronym should
        // be displayed always, to ensure it won't matter if the user is
        // associated to a Region or not.
        if ($this->isGranted('ROLE_STATE_DEPARTMENT') || $this->isGranted('ROLE_ADMIN')) {
            // If the region acronym is passed in the URL, that region is going to be
            // used instead of any region associated to the user.  In this way, we
            // can handle both cases: when is the RPO users who access the Shopping
            // Cart (no acronym passed), or when the Shopping Cart is shared with
            // an ECA user (who shouldn't be assigned to a Region).
            if (null === $region) {
                return $this->redirectToRoute(
                    'fellow_project_region_shopping_cart',
                    ['acronym' => $regions[0]->getAcronym()]
                );
            } else {
                $terms = [$region->getAcronym()];
            }
        } elseif ($this->isGranted('ROLE_RPO')) {
            /** @var Region $region */
            $region = $authenticatedUser->getRegions()->first()?$authenticatedUser->getRegions()->first():null;

            if($region) {
	            $terms = [$region->getAcronym()];
            }
        }


        if (null !== $region) {
            $fellowProjectsUnderConsideration = $em->getRepository(FellowProject::class)->findAllUnderConsiderationByRegion(
                $region
            );
        }

        // Get Budget in Shopping Cart based on Proposals found in region's Shopping Cart.
        $budgetInShoppingCart = array_reduce(
            $fellowProjectsUnderConsideration,
            function ($budget, $fellowProject) {
                if (null === $fellowProject->getFellowProjectBudget()) {
                    return $budget;
                }

                return $budget += $fellowProject->getFellowProjectBudget()->getECATotalContribution();
            },
            0
        );

	    // generates the export for the first table, if it was requested
	    if ($request->query->get('export', '') == 'selected') {
			$this->createCSV('selected', $fellowProjectsUnderConsideration);
		    exit;
	    }

        // if terms set is empty, the result set should be empty as well
        if (!empty($terms)) {
            $criteria['region_s'] = $terms;
        } else {
            $results = [
                'facet_set'       => new FacetSet([]),
                'fellow_projects' => [],
            ];
        }

        foreach ($facetFields as $filter) {
            if (isset($formData[$filter])) {
                $criteria[$filter] = $formData[$filter];
            }
        }

        // adds the outcome to the criteria to avoid withdrawn proposals (notice the ! in the array key)
	    $criteria['!proposal_outcome_s'] = ['Withdrawn'];

        if (!$results) {
            $results = $fellowProjectSolrRepo->getFacetedResultsForSearch(
                $criteria,
                $facetFields,
                $allowedSorts,
                1,
                $request->query->get('sort'),
                $request->query->get('sortDirection')
            );
        }

	    // generates the export for the proposals table, if it was requested
	    if ($request->query->get('export', '') == 'proposals') {
		    $this->createCSV('proposals', $results['fellow_projects']);
		    exit;
	    }

	    $filtersForm = $this->createForm(
            FellowProjectFilterType::class,
            [],
            ['facet_set' => $results['facet_set']]
        );
        $filtersForm->handleRequest($request);

        return $this->render(
            ':project/fellow:shopping-cart.html.twig',
            [
                'shopping_cart_region'                => $region,
                'regions'                             => $regions,
                'regional_budget'                     => null === $region ? null : $region->getBudget(),
                'budget_in_shopping_cart'             => $budgetInShoppingCart,
                'fellow_projects_under_consideration' => $fellowProjectsUnderConsideration,
                'fellow_projects'                     => $results['fellow_projects'],
                'filters_form'                        => $filtersForm->createView(),
            ]
        );
    }

    /**
     * @Route("/shopping-cart/item", name="fellow_project_shopping_cart_item_handler")
     * @Method({"POST"})
     *
     * @param Request $request The request.
     *
     * @return Response
     */
    public function shoppingCartItemHandlerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request;
        $success = false;

        $fellowProject = $em->getRepository(FellowProject::class)->find(
            (int) $data->get('fellowProjectId')
        );

        if (null !== $fellowProject) {
            $fellowProject->getProjectGeneralInfo()->setUnderConsideration(
                (bool) $data->get('underConsideration')
            );

            $em->persist($fellowProject);
            $em->flush();

            $success = true;
        }

        return new JsonResponse(['success' => $success]);
    }

    /**
     * @Route("/test", name="project_fellow_test")
     */
    public function testAction()
    {
        $api_request = $this->get('proposal.drupal_data_consumer');
        $response = $api_request->sendRequest('GET', '/role-test/get');
//            'http://dev.elprograms.org/oauth2/userInfo'
//            'http://dev.elprograms.org/api/v1/user-data?email=luis.cuellar@inqbation.com'

//        dump($api_request);
//        dump($response);
//        exit;

	    $notification = $this->get('proposal.helper.notification_helper');
	    $notification->setSendEmails(true);
	    $emailBody = "This is a test from Felipe's local using the integration with Mandrill, confirm receipt please";
	    $recipients = ['luis@agileana.com', 'virginia@agileana.com'];
	    $subject = "Testing Mandrill";
	    $notification->sendNotification($emailBody, $recipients, $subject);

//	    $em = $this->getDoctrine()->getManager();
//	    $response = $em->getRepository(FellowProject::class)->findAllUnderECAReview();

        return $this->render(
            'fellowproject/test.html.twig',
            [
                'output' => $response,
            ]
        );
    }

    /**
     * @Route("/testpdf", name="project_fellow_testpdf")
     */
    public function testPdfAction(Request $request)
    {
        $pageUrl = $this->generateUrl('proposal_fellow_index', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        //$knp_snappy_pdf = $this->get('knp_snappy.pdf');
        //$request = Request::createFromGlobals();
        //$session = $request->getSession();
        //$session->save();
        //$cookie = 'PHPSESSID='.$request->cookies->get('PHPSESSID');
        //$PHPSESSID = $session->getName().'='.$session->getId();

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $PHPSESSID = $session->getId();

        $output = $this->get('knp_snappy.pdf')->getOutput($pageUrl, array(
            'cookie' => array(
                'PHPSESSID' => $PHPSESSID,
            ),
            'zoom' => 0.8,
        )
        );

        return new Response(
            $output,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="file.pdf"'),
            )
        );

    }

    /**
     * Export the fellow form in pdf format.
     *
     * @Route(
     *     "/{id}/export-pdf",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_export_pdf"
     * )
     * @Method("GET")
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportPdfAction(Request $request, FellowProject $fellowProject)
    {

        $id = $fellowProject->getId();

        $pageUrl = $this->generateUrl('project_fellow_show', array('id' => $id), UrlGeneratorInterface::ABSOLUTE_URL);
        $pageUrl .= '?print=1';

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $PHPSESSID = $session->getId();

        $output = $this->get('knp_snappy.pdf')->getOutput($pageUrl, array(
            'cookie' => array(
                'PHPSESSID' => $PHPSESSID,
            ),
            'zoom' => 0.8,
            'run-script' => "javascript:(\$(function(){ \$('#header, #review-navigation').hide()   ;}))",
        )
        );

        return new Response(
            $output,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="fellow-proposal.pdf"'),
            )
        );

    }

    /**
     * @Route("/new", name="project_fellow_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request The request.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository(Region::class)->findAll();
        $regionsData = [];

        // This chunck of code will store the data to build dynamically the
        // dependent drop-downs.
        /** @var Region $region */
        foreach ($regions as $region) {
            $regionsData[] = [
                'id'        => $region->getId(),
                'name'      => $region->getName(),
                'countries' => $region->getCountries()
                    ->filter(
                        function (Country $country) {
                            return !$country->getReloLocations()->isEmpty();
                        }
                    )
                    ->map(
                        function (Country $country) {
                            return [
                                'id'    => $country->getId(),
                                'name'  => str_replace(['\\', "'"], ['\\\\', ''], $country->getName()),
                                'posts' => $country->getPosts()
                                    ->map(
                                        function (Post $post) {
                                            return [
                                                'id'   => $post->getId(),
                                                'name' => str_replace(['\\', "'"], ['\\\\', ''], $post->getName()),
                                            ];
                                        }
                                    )
                                    ->toArray(),
                            ];
                        }
                    )
                    ->toArray(),
            ];
        }

        try {
            /** @var CycleFellow $currentFellowCycle */
            $currentFellowCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();
        } catch (NoResultException $e) {
            $this->addFlash(
              'danger',
              'The Fellow proposal couldn\'t be created because there isn\'t a Cycle for the current period.'
            );

            return $this->redirectToRoute('proposal_fellow_index');
        }

        $projectGeneralInfo = new ProjectGeneralInfo($currentFellowCycle);
        $fellowProject = new FellowProject($projectGeneralInfo);

        // Statement needed to render a the form without a persisted entity.
        $projectGeneralInfo->setFellowProject($fellowProject);

        $form = $this->createForm(
            CreateFellowProjectType::class,
            $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Country $country */
            $country = $form->get('country')->getData();

            $projectGeneralInfo->addCountry($country);
            $projectGeneralInfo->addReloLocation($country->getReloLocations()->first());
            $projectGeneralInfo->addPost($form->get('post')->getData());

            $fellowProject->getLocationHostInfo()->setCountry($country);

            if ($this->isGranted('ROLE_RPO')) {
                $createdRole = 'RPO';
            } elseif ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
                $createdRole = 'ECA';
            } elseif ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {
                $createdRole = 'Admin';
            } elseif ($this->isGranted('ROLE_RELO')) {
                $createdRole = 'RELO';
            }else {
                $createdRole = 'Embassy';
            }

            $projectGeneralInfo->setCreatedByRole($createdRole);

            $em->persist($fellowProject);
            $em->flush();

            // Validate `General Information` section
            // TODO: should be this placed in a EventListener? Service?
            $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

            $this->get('logger')->info(
                "Fellow Proposal {$fellowProject->getId()} ({$projectGeneralInfo->getReferenceNumber()}) created."
            );

            return $this->redirectToRoute(
                'project_fellow_admin_menu',
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            ':fellowproject/wizard:new.html.twig',
            [
                'form'               => $form->createView(),
                'currentFellowCycle' => $currentFellowCycle,
                'regionsData'        => json_encode($regionsData),
                'goBack'             => null,
            ]
        );
    }

    /**
     * Deletes an additional contact point
     *
     * @Route("/{pid}/contactpoint/{cid}/delete", name="fellow_project_contact_point_delete")
     * @Method("DELETE")
     *
     * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
     * @ParamConverter("contactPointAdditional", class="AppBundle:ContactPointAdditional", options={"mapping": {"cid":
     *                                           "id"}})
     *
     * @param Request                $request
     * @param FellowProject          $fellowProject
     * @param ContactPointAdditional $contactPointAdditional
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteContactPointAction(
        Request $request,
        FellowProject $fellowProject,
        ContactPointAdditional $contactPointAdditional
    ) {
        $form = $this->createDeleteContactPointForm($contactPointAdditional);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contactPointAdditional);
            $em->flush();
        }

        return $this->redirectToRoute(
            'project_fellow_contact_points',
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * Creates a form to delete an additional contact point
     *
     * @param ContactPointAdditional $contactPointAdditional
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteContactPointForm($contactPointAdditional)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'fellow_project_contact_point_delete',
                    [
                        'pid' => $contactPointAdditional->getProjectGeneralInfo()
                            ->getFellowProject()
                            ->getId(),
                        'cid' => $contactPointAdditional->getId(),
                    ]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Deletes a Fellowship Duty
     *
     * @Route("/{pid}/fellowship_duty/{cid}/delete", name="fellow_project_fellowship_duty_delete")
     * @Method("DELETE")
     *
     * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
     * @ParamConverter("fellowshipDutyActivity", class="AppBundle:FellowshipDutyActivity", options={"mapping": {"cid":
     *                                           "id"}})
     *
     * @param Request                $request
     * @param FellowProject          $fellowProject
     * @param FellowshipDutyActivity $fellowshipDutyActivity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteFellowshipDutyActivityAction(
        Request $request,
        FellowProject $fellowProject,
        FellowshipDutyActivity $fellowshipDutyActivity
    ) {
        $form = $this->createDeleteFellowshipDutyActivityForm(
            $fellowshipDutyActivity
        );
        $form->handleRequest($request);

        if (!$fellowshipDutyActivity->getFellowshipDuty()->getType() == 1) {
            $routeToRedirect = 'project_fellow_fellowship_duty_primary';
        } else {
            $routeToRedirect = 'project_fellow_fellowship_duty_secondary';
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fellowshipDutyActivity);
            $em->flush();
        }

        return $this->redirectToRoute(
            $routeToRedirect,
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * Creates a form to delete a fellowship duty
     *
     * @param FellowshipDutyActivity $fellowshipDutyActivity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteFellowshipDutyActivityForm(
        $fellowshipDutyActivity
    ) {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'fellow_project_fellowship_duty_delete',
                    [
                        'pid' => $fellowshipDutyActivity->getFellowshipDuty()
                            ->getFellowProject()
                            ->getId(),
                        'cid' => $fellowshipDutyActivity->getId(),
                    ]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Route("/{id}/region", name="project_fellow_region")
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function regionAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(RegionType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            // Validate `General Information` section
            // TODO: should be this placed in a EventListener? Service?
            $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                // A Region must be selected to continue with the wizard
                if ('' === $form->get('region')->getViewData()) {
                    $this->addFlash('no_region', 'Please select a Region');

                    $nextAction = 'project_fellow_region';
                } else {
                    $nextAction = 'project_fellow_country';
                }
            }

            return $this->redirectToRoute($nextAction, ['id' => $fellowProject->getId()]);
        }

        return $this->render(
            ':fellowproject/wizard:region.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form'          => $form->createView(),
                'goBack'        => null,
            ]
        );
    }

    /**
     * @Route("/{id}/country", name="project_fellow_country")
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function countryAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(CountryType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        // If no region has been selected for the project, set flash message
        // notifying user that one must be selected
        if (null === $projectGeneralInfo->getRegion()) {
            $errorMsg = 'Please, select a Region from the <a class="alert-link" href="'
            . $this->generateUrl(
                'project_fellow_region',
                ['id' => $fellowProject->getId()]
            )
                . '">the previous step</a>';

            $this->addFlash('danger', $errorMsg);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $nextAction = 'project_fellow_admin_menu';

            // Country submitted?
            $newCountryId = $request->request->get('country')['countries'];

            // Check if country has been selected from the Countries drop down
            // list.
            if ($newCountryId) {
                $em = $this->getDoctrine()->getManager();

                // Fellow Proposal should be associated to one country only.
                // Clearing countries array will ensure no more than one country
                // is associated to it.
                $projectGeneralInfo->getCountries()->clear();

                $country = $em->getRepository('AppBundle:Country')->find($newCountryId);

                $projectGeneralInfo->addCountry($country);

                $em->persist($projectGeneralInfo);
                $em->flush();

                // Validate `General Information` section
                // TODO: should be this placed in a EventListener? Service?
                $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

                // Update the Solr index
                // TODO: should be this placed in a EventListener? Service?
                $this->get('solr.client')->updateDocument($fellowProject);

                if ($form->get('next')->isClicked()) {
                    $nextAction = 'project_fellow_post_relo';
                } elseif ($form->get('back')->isClicked()) {
                    $nextAction = 'project_fellow_region';
                }
            }
            // The first time ever users enter to this section, they'll be
            // allowed to go back or return to Admin Panel without selecting a
            // Country from the drop down list.
            elseif ($projectGeneralInfo->getCountries()->isEmpty()) {
                if ($form->get('next')->isClicked()) {
                    $this->addFlash('no_country', 'Please select a Country');

                    $nextAction = 'project_fellow_country';
                } elseif ($form->get('back')->isClicked()) {
                    $nextAction = 'project_fellow_region';
                }
            }
            // After the first time, users will always be requested to select a
            // country from the drop down list, no matter the button they
            // clicked.
            else {
                $this->addFlash('no_country', 'Please select a Country');

                $nextAction = 'project_fellow_country';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/country.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => 'project_fellow_region',
            ]
        );
    }

    /**
     * @Route("/{id}/post-relo", name="project_fellow_post_relo")
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postReloAction(Request $request, FellowProject $fellowProject)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(PostReloType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Post submitted?
            $newPostId = $request->request->get('post_relo')['post'];

            // Check if post has been selected from the Countries drop down
            // list.
            if ($newPostId) {
                // Fellow Proposal should be associated to one Post only.
                // Clearing countries array will ensure no more than one country
                // is associated to it.
                $projectGeneralInfo->getPosts()->clear();

                $post = $em->getRepository('AppBundle:Post')->find($newPostId);

                if ($post) {
                    $projectGeneralInfo->addPost($post);
                }
            }

            $em->persist($fellowProject);
            $em->flush();

            $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                if ('' === $form->get('fellowProject')->get('proposedStartDate')->getViewData()) {
                    $this->addFlash(
                        'no_proposal_start_date',
                        'Please enter the start date for the project'
                    );

                    $nextAction = 'project_fellow_post_relo';
                } else {
                    $nextAction = 'project_fellow_date_comments';
                }
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = 'project_fellow_country';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
          ':fellowproject/wizard:post_relo.html.twig',
          [
            'fellowProject'      => $fellowProject,
            'projectGeneralInfo' => $projectGeneralInfo,
            'form'               => $form->createView(),
            'goBack'             => 'project_fellow_country',
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/date-comments",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_date_comments"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function dateCommentsAction(
        Request $request,
        FellowProject $fellowProject
    ) {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        $form = $this->createForm('AppBundle\Form\DateCommentsType', $fellowProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fellowProject);
            $em->flush();

            // Validate `General Information` section
            // TODO: should be this placed in a EventListener? Service?
            $fellowProject->getProjectGeneralInfo()->setProjectGeneralInfoStepStatus($em);

            $nextAction = $form->get('back')->isClicked()
              ? 'project_fellow_post_relo'
              : 'project_fellow_admin_menu';

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/date_comments.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => 'project_fellow_post_relo',
            ]
        );
    }

    /**
     * @Route("/{id}/contact-points", name="project_fellow_contact_points")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contactPointsAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(ContactPointsType::class, $projectGeneralInfo);
        $form->handleRequest($request);

        $deleteForm = false; // to have something to pass to the template

        if ($form->isSubmitted() && $form->isValid()) {
            $nextAction = 'project_fellow_admin_menu';
            $em = $this->getDoctrine()->getManager();

            if ($form->get('additional')->isClicked()) {
                $nextAction = 'project_fellow_contact_points';

                $contactPointAdditional = new ContactPointAdditional($projectGeneralInfo);
                $projectGeneralInfo->addContactPoint($contactPointAdditional);
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            // Validate `Points of Contact` section
            // TODO: should be this placed in a EventListener? Service?
            $projectGeneralInfo->setContactPointsSectionStatus($em);

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        $contacPointsQty = count(
            $fellowProject->getProjectGeneralInfo()->getContactPoints()
        );

        if (is_a(
            $fellowProject->getProjectGeneralInfo()->getContactPoints(
            )[$contacPointsQty - 1],
            'AppBundle\Entity\ContactPointAdditional'
        )) {
            $deleteForm = $this->createDeleteContactPointForm(
                $fellowProject->getProjectGeneralInfo()->getContactPoints(
                )[$contacPointsQty - 1]
            );
        }

        return $this->render(
          ':fellowproject/wizard:contact_points.html.twig',
          [
            'fellowProject'  => $fellowProject,
            'form'           => $form->createView(),
            'goBack'         => null,
            'delete_form'    => ($deleteForm) ? $deleteForm->createView() : false,
            'fellow_project' => $fellowProject,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/requirements-restrictions",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_requirements"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function requirementsAction(Request $request, FellowProject $fellowProject)
    {
        /** @var $projectGeneralInfo ProjectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm('AppBundle\Form\Fellow\RequirementsType', $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
              'project_fellow_admin_menu',
              ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
          'fellowproject/wizard/requirements.html.twig',
          [
            'fellowProject' => $fellowProject,
            'form'          => $form->createView(),
            'goBack'        => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/dependents",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_project_house_info"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dependentsAction(Request $request, FellowProject $fellowProject)
    {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        return $this->wizardStepHandler(
            [
                $request,
                $fellowProject,
                $fellowProject,
                'Fellow\DependentsType',
                'dependents',
                null,
                'project_fellow_admin_menu',
                [],
            ]
        );
    }

    /**
     * @Route("/{id}/certifications", name="project_fellow_certifications")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function certificationsAction(
        Request $request,
        FellowProject $fellowProject
    ) {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        return $this->wizardStepHandler(
            [
                $request,
                $fellowProject,
                $fellowProject->getProjectGeneralInfo(),
                'CertificationsType',
                'certifications',
                null,
                'project_fellow_admin_menu',
                [],
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/location-host",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_location_host_info"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function locationHostInfoAction(Request $request, FellowProject $fellowProject)
    {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        /** @var LocationHostInfo $locationHostInfo */
        $locationHostInfo = $fellowProject->getLocationHostInfo();

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('AppBundle\Form\LocationHostInfoType', $locationHostInfo);
        $form->handleRequest($request);

        // If no country hasn't been selected for the project, set flash message
        // notifying user that one must be selected
        if (null === $locationHostInfo->getCountry()) {
            $errorMsg = 'Please, select a Country from <a class="alert-link" href="'
            . $this->generateUrl(
                'project_fellow_region',
                ['id' => $fellowProject->getId()]
            )
                . '">General Information</a> in the Administration Panel.';

            $this->addFlash('danger', $errorMsg);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            // Check if user entered a custom Host instead of select one from
            // the Hosts dropdown list.
            if ('' !== $form->get('hostCustom')->getViewData()) {
                $host = new Host();
                $host->setName($form->get('hostCustom')->getViewData());
                $host->setCountry($locationHostInfo->getCountry());

                $em->persist($host);
                $em->flush();

                $locationHostInfo->setHost($host);
            }

            $em->persist($locationHostInfo);
            $em->flush();

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

            return $this->redirectToRoute(
                'project_fellow_admin_menu',
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
          'fellowproject/wizard/location_host_info.html.twig',
          [
            'fellowProject'      => $fellowProject,
            'form'               => $form->createView(),
            'goBack'             => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/fellowship-duties",
     *     requirements={"id": "\d+"},
     *     name="fellow_project_fellowship_duties"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function fellowshipDutiesActivity(
        Request $request,
        FellowProject $fellowProject
    ) {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        $em = $this->getDoctrine()->getManager();

        if ($fellowProject->getDutyPrimaryActivities()->isEmpty()) {
            $dutyActivityPrimary = new DutyActivityPrimary($fellowProject);
            $fellowProject->addDutyPrimaryActivity($dutyActivityPrimary);
        }

        if ($fellowProject->getDutySecondaryActivities()->isEmpty()) {
            $secondaryActivityPrimary = new DutyActivitySecondary(
                $fellowProject
            );
            $fellowProject->addDutySecondaryActivity($secondaryActivityPrimary);

            $em->persist($fellowProject);
            $em->flush();
        }

        $form = $this->createForm('AppBundle\Form\Fellow\DutiesType', $fellowProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nextAction = 'fellow_project_fellowship_duties_mission_goals';

            if ($form->get('addPrimary')->isClicked()) {
                $dutyActivityPrimary = new DutyActivityPrimary($fellowProject);
                $fellowProject->addDutyPrimaryActivity($dutyActivityPrimary);

                $nextAction = 'fellow_project_fellowship_duties';
            } elseif ($form->get('addSecondary')->isClicked()) {
                $secondaryActivityPrimary = new DutyActivitySecondary($fellowProject);
                $fellowProject->addDutySecondaryActivity($secondaryActivityPrimary);

                $nextAction = 'fellow_project_fellowship_duties';
            } elseif ($form->get('save')->isClicked()) {
                $nextAction = 'project_fellow_admin_menu';
            }

            // Get the IDs of the item to remove.
            $activityPrimaryIdToRemove = $form->get('activityPrimaryIdToRemove')->getData();
            $activitySecondaryIdToRemove = $form->get('activitySecondaryIdToRemove')->getData();

            // Check if there is a Primary Duty to remove.
            if ($activityPrimaryIdToRemove) {
                $dutyActivityPrimary = $em->getRepository('AppBundle:Fellow\DutyActivityPrimary')->find($activityPrimaryIdToRemove);

                if ($dutyActivityPrimary) {
                    $fellowProject->removeDutyPrimaryActivity($dutyActivityPrimary);
                    $em->remove($dutyActivityPrimary);

                    $nextAction = 'fellow_project_fellowship_duties';
                }
            }

            // Check if there is a Secondary Duty to remove.
            if ($activitySecondaryIdToRemove) {
                $dutyActivitySecondary = $em->getRepository('AppBundle:Fellow\DutyActivitySecondary')->find($activitySecondaryIdToRemove);

                if ($dutyActivitySecondary) {
                    $fellowProject->removeDutySecondaryActivity($dutyActivitySecondary);
                    $em->remove($dutyActivitySecondary);

                    $nextAction = 'fellow_project_fellowship_duties';
                }
            }

            $em->persist($fellowProject);
            $em->flush();

            // Validate `Fellowship Duties and Mission Goals` section
            // TODO: should be this placed in a EventListener? Service?
            $fellowProject->setFellowshipDutiesAndMissiongGoalsSectionStatus($em);

            /** @var DutyActivityPrimary $dutyActivity */
            foreach ($form->get('dutyPrimaryActivities')->getViewData() as $dutyActivity) {
                if ($dutyActivity->getActivitiesFocus()->count() > 3) {
                    $this->addFlash(
                        'danger',
                        '<strong>Primary Activities:</strong> You must check at least one Activty Focus and no more than three.'
                    );

                    $nextAction = 'fellow_project_fellowship_duties';

                    break 1;
                }
            }

            /** @var DutyActivitySecondary $dutyActivity */
            foreach ($form->get('dutySecondaryActivities')->getViewData() as $dutyActivity) {
                if ($dutyActivity->getActivitiesFocus()->count() > 3) {
                    $this->addFlash(
                        'danger',
                        '<strong>Secondary Activities:</strong> You must check at least one Activty Focus and no more than three.'
                    );

                    $nextAction = 'fellow_project_fellowship_duties';

                    break 1;
                }
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/fellowship_duties.html.twig',
            [
                'form' => $form->createView(),
                'goBack' => false,
                'fellowProject' => $fellowProject,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/fellowship-duties/mission-goals",
     *     requirements={"id": "\d+"},
     *     name="fellow_project_fellowship_duties_mission_goals"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function missionGoalsAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm('AppBundle\Form\Fellow\MissionGoalsType', $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Doctrine\Common\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            $fellowProject->setFellowshipDutiesAndMissiongGoalsSectionStatus($em);

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                $nextAction = 'project_fellow_admin_menu';
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = 'fellow_project_fellowship_duties';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/mission_goals.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => 'fellow_project_fellowship_duties',
            ]
        );
    }

    /**
     * Finds and displays a FellowProject entity.
     *
     * @Route(
     *     "/{id}",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_show"
     * )
     * @Method("GET")
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FellowProject $fellowProject)
    {
        $deleteForm = $this->createDeleteForm($fellowProject);
        $fellowAtHost = '';

        $form = $this->createForm(SubmitFellowProposalType::class, null, [
            'action' => $this->generateUrl(
                'fellow_project_submit',
                ['id' => $fellowProject->getId()]
            ),
        ]);

        $monthlyCostItems = $oneTimeCostItems = [];
        $totals = 0;
        $errors = $fellowProjectBudgetTotals = null;
        if ($fellowProject->getFellowProjectBudget()) {
	        $budgetRevision = $fellowProject->getFellowProjectBudget()->getCurrentRevision();
	        $errors = $this->getErrorsInRows($budgetRevision);

	        // Gets monthly and onetime cost items
            $monthlyCostItems = $this->getDoctrine()->getRepository(CostItemMonthly::class)->findAll();
            $oneTimeCostItems = $this->getDoctrine()->getRepository(CostItemOneTime::class)->findAll();

            // calculations for budget section
            $fellowProjectBudgetTotals = $budgetRevision->getFellowProjectBudgetTotals();
            $totals = $budgetRevision->getSummaryTotals();

        }

        if (null !== $fellowProject->getLocationHostInfo()->getFellowAtHost()) {
            $fellowAtHost = array_search(
                $fellowProject->getLocationHostInfo()->getFellowAtHost(),
                LocationHostInfo::getFellowAtHostChoices()
            );
        }

        return $this->render(
            ':fellowproject:show.html.twig',
            [
                'fellowProject'             => $fellowProject,
                'fellowProjectPresenter'    => new FellowProjectPresenter($fellowProject),
                'monthlyCostItems'          => $monthlyCostItems,
                'oneTimeCostItems'          => $oneTimeCostItems,
                'totals'                    => $totals,
                'fellowProjectBudgetTotals' => $fellowProjectBudgetTotals,
                'delete_form'               => $deleteForm->createView(),
                'form'                      => $form->createView(),
                'errors'                    => $errors,
                'fellowAtHost'              => $fellowAtHost
            ]
        );
    }

    /**
     * Generates page to confirm removal of Fellow Project
     *
     * @Route("/{id}/delete", name="project_fellow_confirm_delete")
     * @Method("GET")
     *
     * @param \AppBundle\Entity\FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function confirmDeleteAction(FellowProject $fellowProject)
    {
        $this->denyAccessUnlessGranted('delete', $fellowProject->getProjectGeneralInfo());

        $deleteForm = $this->createDeleteForm($fellowProject);

        return $this->render('fellowproject/confirm_delete.html.twig', array(
            'fellowProject' => $fellowProject,
            'deleteForm' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a FellowProject entity.
     *
     * @Route("/{id}", name="project_fellow_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
        $this->denyAccessUnlessGranted('delete', $projectGeneralInfo);

        $form = $this->createDeleteForm($fellowProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Gets the project review (if exists) to remove it later
            $reviews = $fellowProject->getFellowProjectReviews();

            $projectReview = null;
            if ($reviews->count()) {
                /** @var ProjectReview $projectReview */
                $projectReview = $reviews->first();
            }

            // Gets costs items to remove them later
            $costItemsToRemove = array();
            if ($fellowProject->getFellowProjectBudget()) {
                $costItems = $fellowProject->getFellowProjectBudget()->getBudgetCostItems();
                $costItemsToRemove = array();
                /** @var FellowProjectBudgetCostItem $item */
                foreach ($costItems as $item) {
                    if ($item->getCostItem() instanceof CostItemCustom || $item->getCostItem() instanceof CostItemCustomInKind) {
                        $costItemsToRemove[] = $item->getCostItem();
                    }
                }
            }

            $em->remove($fellowProject);
//            $em->flush();

            // Removes those entities that can not be removed by DB cascade:
            // Project Review and comments, custom cost items
            if ($projectReview) {
                $em->remove($projectReview);
            }
            foreach($costItemsToRemove as $item) {
                $em->remove($item);
            }
            $em->flush();

        }

        return $this->redirectToRoute('proposal_fellow_index');
    }

    /**
     * @Route(
     *     "/{id}/share",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_share"
     * )
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function shareAction(Request $request, FellowProject $fellowProject)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('AppBundle:User');

        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $postData = $request->request->get('form');

        $shareWith = array_key_exists('shareWith', $postData)
        ? $postData['shareWith']
        : [];

        $unShareFrom = array_key_exists('unShareFrom', $postData)
        ? $postData['unShareFrom']
        : [];

        // Check if there are users to share project with
        if (!empty($shareWith)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($shareWith as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be persisted
                $projectGeneralInfo->addAuthor($user);
                $user->addProject($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
                'success',
                '<p>Project has been shared successfully with the following users:</p>'
                . $sharedWithList
            );
        }

        // Check if there are users to un-share project from
        if (!empty($unShareFrom)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($unShareFrom as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be removed
                $projectGeneralInfo->getAuthors()->removeElement($user);
                $user->getProjects()->removeElement($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
                'success',
                '<p>Project has been un-shared successfully from the following users:</p>'
                . $sharedWithList
            );
        }

        return $this->redirectToRoute(
            'project_fellow_admin_menu',
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * @Route("/ranking/relo", name="fellow_project_relo_ranking")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reloRankingAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $cycleId = $request->get('cycle');

        /** @var CycleFellow $cycleFellow */
        $cycleFellow = (null === $cycleId)
            ? $em->getRepository(CycleFellow::class)->getCurrentCycle()
            : $em->getRepository(CycleFellow::class)->find($cycleId);

        $cycleForm = $this->get('form.factory')
            ->createNamedBuilder(
                'cycle',
                EntityType::class,
                $cycleFellow,
                [
                    'method'       => Request::METHOD_GET,
                    'class'        => CycleFellow::class,
                    'choice_label' => 'name',
                ]
            )
            ->getForm();
        $cycleForm->handleRequest($request);

        $rankingForm = $this
            ->createFormBuilder(null, ['attr' => ['class' => 'ranking-form']])
            ->add('ranking', HiddenType::class)
	        ->add('submitRanking', SubmitType::class, ['label' => 'Save ranking and come back later'])
	        ->add('submitRankingNotify', SubmitType::class, ['label' => 'Save ranking and notify RPO'])
            ->getForm();
        $rankingForm->handleRequest($request);

        $fellowProjects = $em->getRepository(FellowProject::class)->findAllByRELOLocationsInCycle(
            $user->getReloLocations(),
            $cycleForm->getData()
        );

        if ($rankingForm->isSubmitted() && $rankingForm->isValid()) {
            $fellowProjectsRanking = $rankingForm->getData()['ranking'];

            $fellowProjectsIds = explode(',', $fellowProjectsRanking);

            /** @var FellowProject $fellowProject */
            foreach ($fellowProjects as $fellowProject) {
                $ranking = array_search($fellowProject->getId(), $fellowProjectsIds);

                if (false !== $ranking) {
                    /** @var ProjectGeneralInfo $projectGeneralInfo */
                    $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

                    // Indexes are zero-based. Ranking starts from 1.
                    $ranking += 1;

                    // Modify the RELO ranking if different from the current one.
                    if ($projectGeneralInfo->getReloRanking() !== $ranking) {
                        $projectGeneralInfo->setReloRanking($ranking);

                        $em->persist($projectGeneralInfo);

                        // Update the Solr index
                        // TODO: should be this placed in a EventListener? Service?
                        $this->get('solr.client')->updateDocument($fellowProject);
                    }
                }
            }

            $em->flush();

	        if ($rankingForm->get('submitRankingNotify')->isClicked()) {
		        $notification = $this->get('proposal.helper.notification_helper');
		        $recipients = $notification->sendFellowRankingNotifications($user, 'notify-rpo', $cycleForm->getData());

		        $translator = $this->get('translator');
		        $message = $translator->trans('Your message was sent successfully');
//		        $message .= '<ul><li>'. implode('</li>', $recipients) .'</li></ul>';
		        $this->addFlash('success', $message);
	        }

	        return $this->redirectToRoute(
                'fellow_project_relo_ranking',
                ['cycle' => $cycleForm->getData()->getId()]
            );
        }

        return $this->render(
            ':fellowproject:ranking.html.twig',
            [
                'cycleForm'      => $cycleForm->createView(),
                'rankingForm'    => $rankingForm->createView(),
                'fellowProjects' => $fellowProjects,
            ]
        );
    }

    /**
     * @Route("/ranking/rpo", name="fellow_project_rpo_ranking")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function rpoRankingAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $cycleId = $request->get('cycle');

        /** @var CycleFellow $cycleFellow */
        $cycleFellow = (null === $cycleId)
          ? $em->getRepository(CycleFellow::class)->getCurrentCycle()
          : $em->getRepository(CycleFellow::class)->find($cycleId);

        $cycleForm = $this->get('form.factory')
          ->createNamedBuilder(
            'cycle',
            EntityType::class,
            $cycleFellow,
            [
              'method'       => 'GET',
              'class'        => CycleFellow::class,
              'choice_label' => 'name',
            ]
          )
          ->getForm();
        $cycleForm->handleRequest($request);

        $rankingForm = $this
          ->createFormBuilder(null, ['attr' => ['class' => 'ranking-form']])
          ->add('ranking', HiddenType::class)
//          ->add('region', HiddenType::class)
          ->add('submitRanking', SubmitType::class, ['label' => 'Save ranking and come back later'])
          ->add('submitRankingNotify', SubmitType::class, ['label' => 'Save ranking and notify ECA'])
          ->getForm();
        $rankingForm->handleRequest($request);

        $fellowProjects = $em->getRepository('AppBundle:FellowProject')->findAllByRegionsInCycle($user->getRegions(), $cycleForm->getData());

        if ($rankingForm->isSubmitted() && $rankingForm->isValid()) {
            $fellowProjectsRanking = $rankingForm->getData()['ranking'];

            $fellowProjectsIds = explode(',', $fellowProjectsRanking);

            /** @var FellowProject $fellowProject */
            foreach ($fellowProjects as $fellowProject) {
                $ranking = array_search($fellowProject->getId(), $fellowProjectsIds);

                if (FALSE !== $ranking) {
                    /** @var ProjectGeneralInfo $projectGeneralInfo */
                    $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

                    // Indexes are zero-based. Ranking starts from 1.
                    $ranking += 1;

                    // Modify the RELO ranking if different from the current one.
                    if ($projectGeneralInfo->getRpoRanking() !== $ranking) {
                        $projectGeneralInfo->setRpoRanking($ranking);

                        $em->persist($projectGeneralInfo);

                        // Update the Solr index
                        // TODO: should be this placed in a EventListener? Service?
                        $this->get('solr.client')->updateDocument($fellowProject);
                    }
                }
            }

            $em->flush();

	        if ($rankingForm->get('submitRankingNotify')->isClicked()) {
		        $notification = $this->get('proposal.helper.notification_helper');
		        $notification->sendFellowRankingNotifications($user, 'notify-eca');

		        $translator = $this->get('translator');
		        $this->addFlash('success', $translator->trans('Your message was sent successfully'));
	        }

            return $this->redirectToRoute(
              'fellow_project_rpo_ranking',
              ['cycle' => $cycleForm->getData()->getId()]
            );
        }

        return $this->render(
          'fellowproject/ranking.html.twig',
          [
            'cycleForm'      => $cycleForm->createView(),
            'rankingForm'    => $rankingForm->createView(),
            'fellowProjects' => $fellowProjects,
          ]
        );
    }

    /**
     * Creates a form to delete a FellowProject entity.
     *
     * @param FellowProject $fellowProject The FellowProject entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FellowProject $fellowProject)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'project_fellow_delete',
                    ['id' => $fellowProject->getId()]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * This function handles those steps share the same logic
     *
     * @param array $wizardStepParams
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    private function wizardStepHandler($wizardStepParams)
    {
        /** @var FellowProject $fellowProject */
        list(
            $request,
            $fellowProject,
            $formEntity,
            $formType,
            $twigTemplate,
            $prevStepRoute,
            $nextStepRoute,
            $routeExtraParams
        ) = $wizardStepParams;

        $form = $this->createForm("AppBundle\\Form\\$formType", $formEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fellowProject);
            $em->flush();

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                $nextAction = $nextStepRoute;
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = $prevStepRoute;
            }

            $routeParams = array_merge(
                ['id' => $fellowProject->getId()],
                $routeExtraParams
            );

            return $this->redirectToRoute($nextAction, $routeParams);
        }

        return $this->render(
            "fellowproject/wizard/$twigTemplate.html.twig",
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => $prevStepRoute,
            ]
        );
    }

    /**
     * TODO: This method is duplicated in "FellowProjectBudgetController".  Remember to relocate in a Util class/service.
     *
     * @param Revision $budgetRevision
     * @return mixed
     */
    private function getErrorsInRows($budgetRevision) {

        $errors = [
            'rows' => [],
            'costs' => [
                'monthly' => 0,
                'onetime' => 0,
            ],
            'totals' => [
                'monthly' => 0,
                'onetime' => 0,
            ],
        ];

        if (null !== $budgetRevision) {
            /** @var FellowProjectBudgetCostItem $item */
            foreach ($budgetRevision->getBudgetCostItems() as $item) {
                $result = $item->checkTotals();
                if ($result === FellowProjectBudgetCostItem::ROW_ERROR_NO_COST) {
                    $errors['rows'][] = $item->getId();
                    if ($item->getCostItem() instanceof CostItemMonthly) {
                        $errors['costs']['monthly']++;
                    } else {
                        $errors['costs']['onetime']++;
                    }
                } else if ($result === FellowProjectBudgetCostItem::ROW_ERROR_NEGATIVE_TOTAL) {
                    $errors['rows'][] = $item->getId();
                    if ($item->getCostItem() instanceof CostItemMonthly) {
                        $errors['totals']['monthly']++;
                    } else {
                        $errors['totals']['onetime']++;
                    }
                }
            }
        }

        return $errors;
    }


	/**
	 * This function generates all the CSV files provided by the controller
	 *
	 * @param string $type
	 * @param mixed $rawData
	 *
	 */
    private function createCSV($type='selected', $rawData) {

    	$data = [];
    	switch ($type) {
		    case 'selected':
		    	$headers = ['Reference', 'Total cost of fellowship', 'ECA contribution', 'Proposal status',
				    'Proposal outcome', 'Region', 'RPO ranking', 'RELO location', 'RELO ranking', 'Country',
				    'Host institution', 'Cycle'];

			    foreach ($rawData as $rawRow) {
			    	/** @var ProjectGeneralInfo $projectGeneralInfo */
			    	$projectGeneralInfo = $rawRow->getProjectGeneralInfo();

				    /** @var FellowProjectBudget $budget */
				    $budget = $rawRow->getFellowProjectBudget();
				    if ($budget && !$budget->getFellowProjectBudgetTotals()->isEmpty()) {
				    	$totalCost = $budget->getTotalCost();
					    $ecaContribution = $budget->getECATotalContribution();
				    } else {
				    	$totalCost = $ecaContribution = 0;
				    }

				    /** @var LocationHostInfo $locationHost */
				    $locationHost = $rawRow->getLocationHostInfo();

				    $data[] = [
				    	$projectGeneralInfo->getReferenceNumber(),
					    $totalCost,
					    $ecaContribution,
					    $projectGeneralInfo->getProposalStatusText(),
					    ($projectGeneralInfo->getProjectOutcome() != null?$projectGeneralInfo->getProjectOutcome()->getName():''),
					    ($projectGeneralInfo->getRegion() != null?$projectGeneralInfo->getRegion()->getAcronym():''),
					    $projectGeneralInfo->getRpoRanking(),
					    ($projectGeneralInfo->getFirstRELOLocation() != null?$projectGeneralInfo->getFirstRELOLocation()->getName():''),
					    $projectGeneralInfo->getReloRanking(),
				        (!$projectGeneralInfo->getCountries()->isEmpty()?$projectGeneralInfo->getCountries()->last()->getName():''),
					    ($locationHost != null?$locationHost->getHost():''),
					    $rawRow->getCycleSeason(),
				    ];
			    }

		    	break;

		    case 'proposals':
			    $headers = ['Reference', 'Total cost of fellowship', 'ECA contribution', 'Proposal status',
				    'Proposal outcome', 'Region', 'RPO ranking', 'RELO location', 'RELO ranking', 'Country',
				    'Host institution', 'Cycle', 'Added to Cart'];

			    foreach ($rawData as $rawRow) {
				    /** @var ProjectGeneralInfo $projectGeneralInfo */
				    $projectGeneralInfo = $rawRow->getProjectGeneralInfo();

				    /** @var FellowProjectBudget $budget */
				    $budget = $rawRow->getFellowProjectBudget();
				    if ($budget && !$budget->getFellowProjectBudgetTotals()->isEmpty()) {
					    $totalCost = $budget->getTotalCost();
					    $ecaContribution = $budget->getECATotalContribution();
				    } else {
					    $totalCost = $ecaContribution = 0;
				    }

				    /** @var LocationHostInfo $locationHost */
				    $locationHost = $rawRow->getLocationHostInfo();

				    $data[] = [
					    $projectGeneralInfo->getReferenceNumber(),
					    $totalCost,
					    $ecaContribution,
					    $projectGeneralInfo->getProposalStatusText(),
					    ($projectGeneralInfo->getProjectOutcome() != null ? $projectGeneralInfo->getProjectOutcome()->getName() : ''),
					    ($projectGeneralInfo->getRegion() != null ? $projectGeneralInfo->getRegion()->getAcronym() : ''),
					    $projectGeneralInfo->getRpoRanking(),
					    ($projectGeneralInfo->getFirstRELOLocation() != null ? $projectGeneralInfo->getFirstRELOLocation()->getName() : ''),
					    $projectGeneralInfo->getReloRanking(),
					    (!$projectGeneralInfo->getCountries()->isEmpty() ? $projectGeneralInfo->getCountries()->last()->getName() : ''),
					    ($locationHost != null ? $locationHost->getHost() : ''),
					    $rawRow->getCycleSeason(),
					    ($projectGeneralInfo->isUnderConsideration() ? 'Yes' : 'No'),
				    ];
			    }
				break;
	    }

	    $filename = $type.'-'.date('Ymdhis').'.csv';
	    // output headers so that the file is downloaded rather than displayed
	    header('Content-Type: text/csv; charset=utf-8');
	    header("Content-Disposition: attachment; filename={$filename}");

		// create a file pointer connected to the output stream
	    $output = fopen('php://output', 'w');

	    // output the column headings
	    fputcsv($output, $headers);

		// loop over the rows, outputting them
	    foreach($data as $row) {

		    fputcsv($output, $row);
	    }

	    fclose($output);
    }
}
