<?php

namespace AppBundle\Controller;

//use AppBundle\Entity\Common\BaseContributionItem;
use AppBundle\Entity\GlobalPAA;
//use AppBundle\Entity\ItineraryActivity;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\Specialist\Budget\BudgetTotal;
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense;
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpenseCostItem;
use AppBundle\Entity\ContributionOneTime;
use AppBundle\Entity\ContributionOneTimeCostItem;
use AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem;
use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowance;
use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowanceCostItem;
use AppBundle\Entity\Specialist\Budget\Contribution\Transportation;
use AppBundle\Entity\Specialist\Budget\Contribution\TransportationCostItem;
//use AppBundle\Entity\ProjectGeneralInfo;
//use AppBundle\Entity\Specialist\Itinerary\Travel;
//use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use AppBundle\Form\Specialist\Budget\ContributionLivingExpensesType;
use AppBundle\Form\Specialist\Budget\ProgramActivitiesAllowanceType;
use AppBundle\Form\Specialist\ContributionTransportationType;
use AppBundle\Form\Specialist\FundedByType;
use AppBundle\Helper\ControllerHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Specialist\Budget\GlobalValue;

/**
 * SpecialistProjectBudgetController controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class SpecialistProjectBudgetController extends Controller implements RoleSwitcherInterface
{
    /**
     * @Route(
     *     "/project/specialist/{id}/budget/funded-by",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_funded_by",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fundedByAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {
        $em = $this->getDoctrine()->getManager();

        // Since budget is now created when the phase is created, $phaseBudget cannot be null
        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [
		    SpecialistProjectPhase::IN_COUNTRY_TYPE,
		    SpecialistProjectPhase::MIXED_TYPE,
	    ]);
	    
        // for old projects, checks if global values have NOT been set and proceed accordingly
        if ($phaseBudget->getGlobalValues() == null || !$phaseBudget->getGlobalValues()->count()) {
            $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')
		        ->cloneGlobalValuesToBudget($phaseBudget);
	        $phaseBudget->setSummaryTotals();
	        $em->flush();

	        // Updating Pre and Post work applies only for In Country projects
	        if ($specialistProjectPhase->getType() == SpecialistProjectPhase::IN_COUNTRY_TYPE) {
		        if ($phaseBudget->getBudgetTotals() && $phaseBudget->getBudgetTotals()->count()) {
			        /** @var BudgetTotal $total */
			        $total = $phaseBudget->getBudgetTotals()->first();
			        if ($total->getPrePostWork() == 0) {
				        $em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updatePrePostWork($phaseBudget);
				        $phaseBudget->setSummaryTotals($em, true);
			        }
		        }
	        }
        }

	    // Caches this to compare when form is submitted and the Funded By is changed.
	    $currentFundedBy = $phaseBudget->getFundedBy();

	    $form = $this->createForm(FundedByType::class, $phaseBudget, [
		    'role' => $this->get('security.token_storage')->getToken()->getRoles(),
	    ]);
        $form->handleRequest($request);

        if ($form->isValid()) {

	        // If project budget `Funded By` changes, the totals for the records must be re-calculated.
	        // It must apply also when if $currentFundedBy is null
            if ($currentFundedBy !== $phaseBudget->getFundedBy()) {
                /** @var ContributionLivingExpense $contributionLivingExpense */
                foreach ($phaseBudget->getContributionLivingExpenses() as $contributionLivingExpense) {
                    /** @var ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem */
                    foreach ($contributionLivingExpense->getContributionLivingExpenseCostItems() as $contributionLivingExpenseCostItem) {
                        $contributionLivingExpenseCostItem->calculateTotals();
                    }
                }

                /** @var GroundTransportCostItem $contributionGroundTransportCostItem */
                $phaseBudget->getContributionGroundTransport()->calculateTotals();

                /** @var TransportationCostItem $contributionTransportationCostItem */
                foreach ($phaseBudget->getContributionTransportation()->getTransportationCostItems() as $contributionTransportationCostItem) {
                    $contributionTransportationCostItem->calculateTotals();
                }

                /** @var ContributionOneTimeCostItem $contributionOneTimeCostItems */
                foreach ($phaseBudget->getContributionOneTime()->getContributionOneTimeCostItems() as $contributionOneTimeCostItem) {
                    $contributionOneTimeCostItem->calculateTotals();
                }

                /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
                foreach ($phaseBudget->getProgramActivitiesAllowance()->getProgramActivitiesAllowanceCostItems() as $paaCostItem) {
                    $paaCostItem->calculateTotals();
                }

                $em->flush();

	            // recalculates totals, flushing to DB
	            $phaseBudget->setSummaryTotals($em, true);
            }

            $em->persist($phaseBudget);
            $em->flush();

            $nextAction = 'specialist_project_phase_budget';
	        if ($form->get('save')->isClicked()) {
		        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

		        if ($projectGeneralInfo->getProjectReviewStatus()
		        	&& $projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
			        && $this->isGranted('ROLE_EMBASSY')) {
				        $nextAction = 'specialist_project_phase_show';
		        } else {
		        	$nextAction = 'specialist_project_phase_admin_panel';
		        }
	        }

            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

        return $this->render(
            ':project/specialist/wizard/budget:funded_by.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form'                   => $form->createView(),
                'goBack'                 => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/budget",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget",
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(SpecialistProjectPhase $specialistProjectPhase, Request $request) {
    	$em = $this->getDoctrine()->getEntityManager();

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [
		    SpecialistProjectPhase::IN_COUNTRY_TYPE,
		    SpecialistProjectPhase::MIXED_TYPE,
	    ]);

	    // We used the basic FundedByType form to have the navigation buttons in the page
	    $form = $this->createForm(FundedByType::class, $phaseBudget, [
		    'role' => $this->get('security.token_storage')->getToken()->getRoles(),
	    ]);
	    $form->handleRequest($request);

	    // While the form is processed, it's not needed to save anything to the DB
	    $prevAction = 'specialist_project_phase_budget_funded_by';
	    if ($form->isValid()) {
	    	$nextAction = 'specialist_project_phase_admin_panel';

		    if ($form->get('back')->isClicked()) {
			    $nextAction = $prevAction;
		    } else {
			    $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();
			    if ($projectGeneralInfo->getProjectReviewStatus()) {
				    if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING && $this->isGranted('ROLE_EMBASSY')) {
					    $nextAction = 'specialist_project_phase_show';
				    }
			    }
		    }

		    return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
	    }

	    // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errors = [];
	    $contributionLE = $phaseBudget->getContributionLivingExpenses();

	    if ($contributionLE) {
		    $errors['livingExpenses'] = $helper->getErrorsInRows($contributionLE->first()->getContributionLivingExpenseCostItems());
	    }
	    if ($phaseBudget->getContributionGroundTransport()) {
		    $errors['groundTransport'] = $helper->getErrorsInRows(array($phaseBudget->getContributionGroundTransport()));
	    }

	    $contributionTransportation = $phaseBudget->getContributionTransportation();
	    if ($contributionTransportation) {
		    $errors['transportation'] = $helper->getErrorsInRows($contributionTransportation->getTransportationCostItems());
	    }

	    $contributionOneTime = $phaseBudget->getContributionOneTime();
	    if ($contributionOneTime) {
		    $errors['oneTime'] = $helper->getErrorsInRows($contributionOneTime->getContributionOneTimeCostItems());
	    }

	    $paa = $phaseBudget->getProgramActivitiesAllowance();
	    if ($paa) { // $paa was already defined before the form handling
		    $errors['paa'] = $helper->getErrorsInRows($paa->getProgramActivitiesAllowanceCostItems());
	    }

	    $contributionLivingCostItems = $em->getRepository('AppBundle:CostItemLivingExpense')->findAll();
	    $livingExpenses = $em->getRepository('AppBundle:ItineraryIndex')->getCostsStats($specialistProjectPhase);
	    $totals = [
		    'Meals' => 0,
		    'Lodging' => 0,
	    ];

	    foreach ( $livingExpenses['rows'] as $livingExpenseItem) {
		    if ($livingExpenseItem != null) {
			    $totals['Meals'] += $livingExpenseItem['Meals'];
			    $totals['Lodging'] += $livingExpenseItem['Lodging'];
		    }
	    }

		$dailyCosts = $helper->formatDailyCosts($specialistProjectPhase);

	    // For mixed and virtual projects
	    $virtualActivities = null;
	    $stipendCosts = [];
	    if ($specialistProjectPhase->getType() == SpecialistProjectPhase::MIXED_TYPE) {
			// To build the summary table, we need the activities
			$virtualActivities = $em->getRepository('AppBundle:Specialist\Virtual\Itinerary\Activity')->findInPhase($specialistProjectPhase);

		    if ($phaseBudget) {
				$stipendCosts = $helper->formatVirtualFlexibleCosts($specialistProjectPhase);
		    }
	    }

	    return $this->render(
		    'project/specialist/wizard/budget/' . strtolower($phaseBudget->getFundedBy()) . '/index.html.twig',
		    [
			    'specialistProjectPhase' => $specialistProjectPhase,
			    'phaseBudget' => $phaseBudget,
			    'livingExpenses' => $livingExpenses,
			    'livingExpensesCostItems' => $contributionLivingCostItems,
			    'totals' => $totals,
			    'form' => $form->createView(),
			    'goBack' => $prevAction,
			    'errors' => $errors,
			    'dailyCosts' => $dailyCosts,
			    'virtualActivities' => $virtualActivities,
			    'stipendCosts' => $stipendCosts,
		    ]
	    );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/budget/contribution/living-expenses",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_living_expenses",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contributionLivingExpensesAction(SpecialistProjectPhase $specialistProjectPhase, Request $request)
    {
        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [
		    SpecialistProjectPhase::IN_COUNTRY_TYPE,
		    SpecialistProjectPhase::MIXED_TYPE,
	    ]);

	    $form = $this->createForm(ContributionLivingExpensesType::class, $phaseBudget);
        $form->handleRequest($request);

	    $prevAction = 'specialist_project_phase_budget';
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
	        // ContributionsLivingExpensesCostItems totals are now calculated automatically (LifecycleCallbacks)
	        // TODO-Felipe: Consider totals per sections, so that it's easier to maintain budget totals\
	        $em->flush();

	        // then totals must be recalculated
	        $phaseBudget->setSummaryTotals($em, true);

	        $nextAction = $prevAction;

	        if ($form->get('calculate')->isClicked()) {
		        $nextAction = 'specialist_project_phase_budget_living_expenses';
	        } elseif ($form->get('save')->isClicked()) {
		        $nextAction = 'specialist_project_phase_admin_panel';
	        }

            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

        // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errorsGT = $errorsLE = [];
	    $contributionLE = $phaseBudget->getContributionLivingExpenses();
	    if ($contributionLE) {
		    $errorsLE = $helper->getErrorsInRows($contributionLE->first()->getContributionLivingExpenseCostItems());
	    }
	    if ($phaseBudget->getContributionGroundTransport()) {
	        $errorsGT = $helper->getErrorsInRows(array($phaseBudget->getContributionGroundTransport()));
	    }

	    return $this->render(
          'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/contribution_living_expenses.html.twig',
          [
            'specialistProjectPhase'    => $specialistProjectPhase,
            'phaseBudget'               => $phaseBudget,
            'form'                      => $form->createView(),
            'goBack'                    => $prevAction,
            'errorsLE' => $errorsLE,
            'errorsGT' => $errorsGT,
          ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/budget/contribution/transportation",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_contribution_transportation",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contributionTransportationAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {
	    $em = $this->getDoctrine()->getManager();

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [
		    SpecialistProjectPhase::IN_COUNTRY_TYPE,
		    SpecialistProjectPhase::MIXED_TYPE,
	    ]);

	    /** @var Transportation $contributionTransportation */
        $contributionTransportation = $phaseBudget->getContributionTransportation();

        $form = $this->createForm(
          ContributionTransportationType::class,
          $contributionTransportation
        );
        $form->handleRequest($request);

	    $prevAction = 'specialist_project_phase_budget';
        if ($form->isValid()) {
//            $em->persist($contributionTransportation);
	        // totals must be recalculated
	        // TODO-Felipe: Consider totals per sections, so that it's easier to maintain budget totals\
            $em->flush(); // first, flushes the changes so that the DB is updated

	        $phaseBudget->setSummaryTotals($em, true); // then updates the totals

	        $nextAction = $prevAction;

	        if ($form->get('calculate')->isClicked()) {
	        	$nextAction = 'specialist_project_phase_budget_contribution_transportation';
	        } elseif($form->get('save')->isClicked()) {
		        $nextAction = 'specialist_project_phase_admin_panel';
	        }


            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

	    // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errors = [];
	    $contributionTransportation = $phaseBudget->getContributionTransportation();
	    if ($contributionTransportation) {
		    $errors = $helper->getErrorsInRows($contributionTransportation->getTransportationCostItems());
	    }

        return $this->render(
          ':project/specialist/wizard/budget/' . strtolower($phaseBudget->getFundedBy()) . ':contribution_transportation.html.twig',
          [
            'specialistProjectPhase'     => $specialistProjectPhase,
            'phaseBudget'                => $phaseBudget,
            'contributionTransportation' => $contributionTransportation,
            'form'                       => $form->createView(),
            'goBack'                     => $prevAction,
            'errors'                     => $errors,
          ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/budget/living-expenses",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_living_expense_estimates",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function livingExpensesAction(Request $request, SpecialistProjectPhase $specialistProjectPhase)
	{
		$em = $this->getDoctrine()->getManager();

		/** @var SpecialistProjectPhaseBudget $phaseBudget */
		$phaseBudget = $specialistProjectPhase->getBudget();

		// Checks if the user can edit the budget and if type of project is right
		$this->denyAccessUnlessGranted('edit', $phaseBudget);
		$this->denyAccessUnlessType($specialistProjectPhase, [
			SpecialistProjectPhase::IN_COUNTRY_TYPE,
			SpecialistProjectPhase::MIXED_TYPE,
		]);

		// If for some reason GroundTransport costs are not set, creates what's needed to avoid error messages
		if (!$phaseBudget->getContributionGroundTransport()) {
			$groundTransport = new GroundTransportCostItem($phaseBudget);
			$groundTransport->setCostItem(current($em->getRepository('AppBundle:CostItemGroundTransport')->findAll()));
			$em->persist($groundTransport);
			$phaseBudget->setContributionGroundTransport($groundTransport);
			$em->flush();
		}

		$form = $this->createForm(
			'AppBundle\Form\Specialist\LivingExpensesType',
			$specialistProjectPhase
		);

		$form->handleRequest($request);

		$prevAction = 'specialist_project_phase_budget';
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->flush();

			// calculates the number of days in cities per day and updates records
			$iiRepo = $em->getRepository('AppBundle:ItineraryIndex');

			$stats = $iiRepo->getCitiesStats($specialistProjectPhase);
			$cityIds = [];
			foreach($stats as $item) {
				$livingExpense = $em->getRepository('AppBundle:LivingExpense')->findOneByInCountryAssignment($item['id']);
				$cityIds[] = $item['id'];
				/** @var LivingExpenseCostItem $costItem */
				foreach($livingExpense->getLivingExpenseCostItems() as $costItem) {
					$costItem->setNumberOfDays($item['quantity']);
					$costItem->calculateTotals(); // Note - Felipe: had to explicitly call it because preUpdate was not triggered
				}
			}

			// We have to reset the living expenses not used (setNumOfDays = 0)
			$livingExpenses = $em->getRepository('AppBundle:LivingExpense')
				->findNotUsedCities($specialistProjectPhase->getBudget(), $cityIds);

			if (count($livingExpenses)) {
				/** @var LivingExpenseCostItem $costItem */
				foreach ($livingExpenses as $livingExpense) {
					foreach ($livingExpense->getLivingExpenseCostItems() as $costItem) {
						$costItem->setNumberOfDays(0);
						$costItem->calculateTotals();
					}
				}
			}

			$em->flush();
			// updates summary totals but after other updates have taken place, this does not impact the global values
			$phaseBudget->setSummaryTotals($em, true);

			$nextAction = $prevAction;

			if ($form->get('save')->isClicked()) {
				$nextAction = 'specialist_project_phase_admin_panel';
			} elseif ($form->get('calculate')->isClicked()) {
				$nextAction = 'specialist_project_phase_budget_living_expense_estimates';
			}

			return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
		}

		$contributionLivingCostItems = $em->getRepository('AppBundle:CostItemLivingExpense')->findAll();
		$livingExpenses = $em->getRepository('AppBundle:ItineraryIndex')->getCostsStats($specialistProjectPhase);

        $totals = [
        	'Meals' => 0,
	        'Lodging' => 0,
        ];

        foreach ( $livingExpenses['rows'] as $livingExpenseItem) {
            if ($livingExpenseItem != null) {
                $totals['Meals'] += $livingExpenseItem['Meals'];
                $totals['Lodging'] += $livingExpenseItem['Lodging'];
            }
        }

		return $this->render(
			'project/specialist/wizard/budget/living_expenses.html.twig',
			[
				'specialistProjectPhase' => $specialistProjectPhase,
				'phaseBudget' => $phaseBudget,
				'livingExpenses' => $livingExpenses,
				'livingExpensesCostItems' => $contributionLivingCostItems,
				'totals' => $totals,
				'form' => $form->createView(),
				'goBack' => $prevAction,
			]
		);
	}

    /**
     * @Route(
     *     "/project/specialist/{id}/budget/contribution/one-time",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_contribution_one_time",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contributionOneTimeAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {
        $em = $this->getDoctrine()->getManager();

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [
		    SpecialistProjectPhase::IN_COUNTRY_TYPE,
		    SpecialistProjectPhase::MIXED_TYPE,
	    ]);

	    /** @var ContributionOneTime $contributionOneTime */
        $contributionOneTime = $phaseBudget->getContributionOneTime();

	    // If no contributionOneTime is found, create it
	    if (!$contributionOneTime) {
	    	$contributionOneTime = new ContributionOneTime($phaseBudget);
		    $em->persist($contributionOneTime);
		    $em->flush();
	    }

        if ($contributionOneTime->getContributionOneTimeCostItems()->isEmpty()) {
            $contributionOneTimeCostItem = new ContributionOneTimeCostItem($contributionOneTime);
            $contributionOneTime->addContributionOneTimeCostItem($contributionOneTimeCostItem);

            $em->persist($contributionOneTime);
            $em->flush();
        }

        $form = $this->createForm(
          'AppBundle\Form\Specialist\ContributionOneTimeType',
          $contributionOneTime
        );
        $form->handleRequest($request);

	    $prevAction = 'specialist_project_phase_budget';
        if ($form->isValid()) {
            // Get the ID of the item to delete.
            $costItemToDeleteId = $form->get('costItemToDeleteId')->getData();

            if ($costItemToDeleteId) {
                /** @var ContributionOneTimeCostItem $costItem */
                $costItem = $em->getRepository('AppBundle:ContributionOneTimeCostItem')->find($costItemToDeleteId);

                if ($costItem) {
                    $contributionOneTime->removeContributionOneTimeCostItem($costItem);
                    $em->remove($costItem);
                }
            }

            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ContributionOneTimeCostItem $contributionOneTimeCostItem */
            foreach ($contributionOneTime->getContributionOneTimeCostItems() as $contributionOneTimeCostItem) {
                // if no description is entered by the user to the added cost
                // item, `Untitled` will be use as default description.
                if (null === $contributionOneTimeCostItem->getCostItem()->getDescription()) {
                    $contributionOneTimeCostItem->getCostItem()->setDescription('Untitled');
                }
            }

            // flushes changes to DB
            $em->flush();

	        // totals must be recalculated
	        // TODO-Felipe: Consider totals per sections, so that it's easier to maintain budget totals\
	        $phaseBudget->setSummaryTotals($em);

            $nextAction = 'specialist_project_phase_admin_panel'; // Admin Panel link

            if ($form->get('additional')->isClicked()) {
                $contributionOneTimeCostItem = new ContributionOneTimeCostItem($contributionOneTime);
                $contributionOneTime->addContributionOneTimeCostItem($contributionOneTimeCostItem);
                $nextAction = 'specialist_project_phase_budget_contribution_one_time';
            } elseif ($form->get('calculate')->isClicked()) {
                $nextAction = 'specialist_project_phase_budget_contribution_one_time';
            } elseif ($form->get('back')->isClicked() || $form->get('next')->isClicked()) {
            	$nextAction = $prevAction;
            }

            $em->persist($contributionOneTime);
            $em->flush();

            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

	    // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errors = [];
	    $contributionOneTime = $phaseBudget->getContributionOneTime();
	    if ($contributionOneTime) {
		    $errors = $helper->getErrorsInRows($contributionOneTime->getContributionOneTimeCostItems());
	    }

	    return $this->render(
            'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/contribution_one_time.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'phaseBudget'            => $phaseBudget,
                'contributionOneTime'    => $contributionOneTime,
                'form'                   => $form->createView(),
                'goBack'                 => $prevAction,
                'errors'                 => $errors,
            ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/budget/paa",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_paa",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function programActivitiesAllowanceAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [
		    SpecialistProjectPhase::IN_COUNTRY_TYPE,
		    SpecialistProjectPhase::MIXED_TYPE,
	    ]);

	    /** @var ProgramActivitiesAllowance $paa */
        $paa = $phaseBudget->getProgramActivitiesAllowance();
        $em = $this->getDoctrine()->getManager();

        // Get the length of assignments
	    $days = $specialistProjectPhase->getInCountryDuration();

        $globalPAA = $em->getRepository(GlobalPAA::class)->findByLengthOfAssignment($days);

        if ($paa->getProgramActivitiesAllowanceCostItems()->isEmpty()) {
            $paaCostItem = new ProgramActivitiesAllowanceCostItem($paa);

            $paa->addProgramActivitiesAllowanceCostItem($paaCostItem);
        }

        $form = $this->createForm(ProgramActivitiesAllowanceType::class,$paa);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
            foreach ($paa->getProgramActivitiesAllowanceCostItems() as $paaCostItem) {
                // if no description is entered by the user to the added cost
                // item, `Untitled` will be use as default description.
                if (null === $paaCostItem->getCostItem()->getDescription()) {
                    $paaCostItem->getCostItem()->setDescription('Untitled');
                }

                $paaCostItem->calculateTotals();
            }

	        // totals must be recalculated
	        // TODO-Felipe: Consider totals per sections, so that it's easier to maintain budget totals\
	        $phaseBudget->setSummaryTotals($em);

            $em->persist($paaCostItem);
            $em->flush();

            $nextAction = $form->get('calculate')->isClicked()
                ? 'specialist_project_phase_budget_paa'
                : 'specialist_project_phase_budget';

            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

	    // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errors = [];
	    if ($paa) { // $paa was already defined before the form handling
		    $errors = $helper->getErrorsInRows($paa->getProgramActivitiesAllowanceCostItems());
	    }

	    return $this->render(
            ':project/specialist/wizard/budget/'.strtolower($phaseBudget->getFundedBy()).':paa.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'phaseBudget'            => $phaseBudget,
                'globalPAA'              => $globalPAA,
                'ppa'                    => $paa,
                'form'                   => $form->createView(),
                'goBack'                 => 'specialist_project_phase_budget',
	            'errors'                  => $errors,
            ]
        );
    }

	/**
	 * TODO: Add this to a common source
	 * Validates the type of project against an array of valid types
	 * @param SpecialistProjectPhase $phase
	 * @param array $types
	 */
	private function denyAccessUnlessType(SpecialistProjectPhase $phase, $types=[]) {
		if (!in_array($phase->getType(), $types)) {

			//loads the translation service
			$translator = $this->get('translator');
			$message = $translator->trans('This section is not available for this type of proposal');

			throw $this->createAccessDeniedException($message);
		}

	}


}
