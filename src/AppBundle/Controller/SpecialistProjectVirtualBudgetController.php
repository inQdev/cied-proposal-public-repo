<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowance;
use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowanceCostItem;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use AppBundle\Form\Specialist\Budget\ProgramActivitiesAllowanceType;
//use AppBundle\Form\Specialist\ContributionLivingExpenseType;
use AppBundle\Form\Specialist\FundedByType;
use AppBundle\Helper\ControllerHelper;
//use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Specialist\Budget\GlobalValue;

/**
 * SpecialistProjectVirtualBudgetController controller.
 * Specific for Specialist Virtual proposals
 *
 * @package AppBundle\Controller
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class SpecialistProjectVirtualBudgetController extends Controller
{
    /**
     * @Route(
     *     "/project/specialist/{id}/virtual-budget/funded-by",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_virtual_funded_by",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fundedByAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {
        $em = $this->getDoctrine()->getManager();

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        if (null === $phaseBudget) {
            $currentFundedBy = false;

            // Creates a budget for the phase.
            $phaseBudget = new SpecialistProjectPhaseBudget($specialistProjectPhase);
            $em->persist($phaseBudget); // An event listener takes care of creating associated budget objects/records
            $em->flush();

	        // Note - Felipe: didn't find any other way to update the totals
	        $phaseBudget->setSummaryTotals($em);
	        $em->flush();
        }
        else {
            // Cache this to compare when form is submitted and the Funded By is changed.
            $currentFundedBy = $phaseBudget->getFundedBy();

	        // for old projects, checks if global values have NOT been set and proceed accordingly
	        if ($phaseBudget->getGlobalValues() == null || !$phaseBudget->getGlobalValues()->count()) {
	        	$em->getRepository(GlobalValue::class)
			        ->cloneGlobalValuesToBudget($phaseBudget);
		        $phaseBudget->setSummaryTotals($em);
		        $em->flush();
	        }
	        // pre and post work not considered
        }

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [SpecialistProjectPhase::VIRTUAL_TYPE]);

	    $form = $this->createForm(FundedByType::class, $phaseBudget, [
		    'role' => $this->get('security.token_storage')->getToken()->getRoles(),
	    ]);
        $form->handleRequest($request);

        if ($form->isValid()) {

	        // If project budget `Funded By` changes, the totals for the records must be re-calculated.
            if ($currentFundedBy
                && ($currentFundedBy !== $phaseBudget->getFundedBy())
            ) {

                /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
                foreach ($phaseBudget->getProgramActivitiesAllowance()->getProgramActivitiesAllowanceCostItems() as $paaCostItem) {
                    $paaCostItem->calculateTotals();
                }

                $em->flush();

	            // recalculates totals, flushing to DB
	            $phaseBudget->setSummaryTotals($em, true);
            }

            $em->persist($phaseBudget);
            $em->flush();

	        $nextAction = 'specialist_project_phase_budget_virtual';
	        if ($form->get('save')->isClicked()) {
		        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

		        if ($projectGeneralInfo->getProjectReviewStatus()
			        && $projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
			        && $this->isGranted('ROLE_EMBASSY')) {
				        $nextAction = 'specialist_project_phase_show';
		        } else {
			        $nextAction = 'specialist_project_phase_admin_panel';
		        }
	        }

            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

        return $this->render(
            ':project/specialist/wizard/budget:funded_by.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'form'                   => $form->createView(),
                'goBack'                 => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/virtual-budget",
     *     requirements={"id": "\d+"},
     *     name="specialist_project_phase_budget_virtual",
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(SpecialistProjectPhase $specialistProjectPhase, Request $request) {

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [SpecialistProjectPhase::VIRTUAL_TYPE]);

	    // We used the basic FundedByType form to have the navigation buttons in the page
	    $form = $this->createForm(FundedByType::class, $phaseBudget, [
		    'role' => $this->get('security.token_storage')->getToken()->getRoles(),
	    ]);
	    $form->handleRequest($request);

	    // While the form is processed, it's not needed to save anything to the DB
	    $prevAction = 'specialist_project_phase_budget_virtual_funded_by';
	    if ($form->isValid()) {
	    	$nextAction = 'specialist_project_phase_admin_panel';

		    if ($form->get('back')->isClicked()) {
			    $nextAction = $prevAction;
		    } else {
			    $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();
			    if ($projectGeneralInfo->getProjectReviewStatus()) {
				    if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING && $this->isGranted('ROLE_EMBASSY')) {
					    $nextAction = 'specialist_project_phase_show';
				    }
			    }
		    }

		    return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
	    }

	    // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errors = [];

	    $paa = $phaseBudget->getProgramActivitiesAllowance();
	    if ($paa) { // $paa was already defined before the form handling
		    $errors['paa'] = $helper->getErrorsInRows($paa->getProgramActivitiesAllowanceCostItems());
	    }

	    $stipendCosts = $helper->formatVirtualFlexibleCosts($specialistProjectPhase);

	    return $this->render(
		    'project/specialist/wizard/virtual/budget/' . strtolower($phaseBudget->getFundedBy()) . '/index.html.twig',
		    [
			    'specialistProjectPhase' => $specialistProjectPhase,
			    'phaseBudget' => $phaseBudget,
			    'form' => $form->createView(),
			    'goBack' => $prevAction,
			    'errors' => $errors,
			    'stipendCosts' => $stipendCosts,
		    ]
	    );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/virtual-budget/paa",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_budget_virtual_paa",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function programActivitiesAllowanceAction(Request $request, SpecialistProjectPhase $specialistProjectPhase) {

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

	    // Checks if the user can edit the budget and if type of project is right
	    $this->denyAccessUnlessGranted('edit', $phaseBudget);
	    $this->denyAccessUnlessType($specialistProjectPhase, [SpecialistProjectPhase::VIRTUAL_TYPE]);

	    /** @var ProgramActivitiesAllowance $paa */
        $paa = $phaseBudget->getProgramActivitiesAllowance();
        $em = $this->getDoctrine()->getManager();

        if ($paa->getProgramActivitiesAllowanceCostItems()->isEmpty()) {
            $paaCostItem = new ProgramActivitiesAllowanceCostItem($paa);
            $paa->addProgramActivitiesAllowanceCostItem($paaCostItem);
        }

        $form = $this->createForm(ProgramActivitiesAllowanceType::class,$paa);
        $form->handleRequest($request);

        if ($form->isValid()) {
            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
            foreach ($paa->getProgramActivitiesAllowanceCostItems() as $paaCostItem) {
                // if no description is entered by the user to the added cost
                // item, `Untitled` will be use as default description.
                if (null === $paaCostItem->getCostItem()->getDescription()) {
                    $paaCostItem->getCostItem()->setDescription('Untitled');
                }

                $paaCostItem->calculateTotals();
            }

	        // totals must be recalculated
	        // TODO-Felipe: Consider totals per sections, so that it's easier to maintain budget totals\
	        $phaseBudget->setSummaryTotals($em);

            $em->persist($paaCostItem);
            $em->flush();

            $nextAction = $form->get('calculate')->isClicked()
                ? 'specialist_project_phase_budget_virtual_paa'
                : 'specialist_project_phase_budget_virtual';

            return $this->redirectToRoute($nextAction, ['id' => $specialistProjectPhase->getId()]);
        }

	    // if form not submitted, checks discrepancies in data
	    /** @var ControllerHelper $helper */
	    $helper = $this->get('proposal.helper.controller_helper');
	    $errors = [];
	    if ($paa) { // $paa was already defined before the form handling
		    $errors = $helper->getErrorsInRows($paa->getProgramActivitiesAllowanceCostItems());
	    }

	    return $this->render(
            ':project/specialist/wizard/budget/'.strtolower($phaseBudget->getFundedBy()).':paa.html.twig',
            [
                'specialistProjectPhase' => $specialistProjectPhase,
                'phaseBudget'            => $phaseBudget,
                'globalPAA'              => 0,
                'ppa'                    => $paa,
                'form'                   => $form->createView(),
                'goBack'                 => 'specialist_project_phase_budget_virtual',
	            'errors'                  => $errors,
            ]
        );
    }

	/**
	 * Validates the type of project against an array of valid types
	 * @param SpecialistProjectPhase $phase
	 * @param array $types
	 */
	private function denyAccessUnlessType(SpecialistProjectPhase $phase, $types=[]) {
		if (!in_array($phase->getType(), $types)) {

			//loads the translation service
			$translator = $this->get('translator');
			$message = $translator->trans('This section is not available for this type of proposal');

			throw $this->createAccessDeniedException($message);
		}

	}

}
