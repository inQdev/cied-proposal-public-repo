<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\CostItemMonthly;
use AppBundle\Entity\CostItemOneTime;
use AppBundle\Entity\Fellow\Budget\Revision;
use AppBundle\Entity\GlobalValueFellow;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\CostItemCustom;
use AppBundle\Entity\CostItemCustomInKind;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudget;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem;
use AppBundle\Entity\Fellow\Budget\ProjectGlobalValue;
use AppBundle\Entity\Region;
use AppBundle\Form\FellowProjectBudgetFundedType;
use AppBundle\Form\FellowProjectBudgetType;
use AppBundle\Form\RegionBaseType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

//  * @Route("/project/fellow/budget")

/**
 * FellowProject controller.
 *
 */
class FellowProjectBudgetController extends Controller implements RoleSwitcherInterface
{

	/**
	 * Given a FellowProject, allows to select the organization funding the project
	 *
	 * @Route("/project/fellow/{id}/budget/funding", name="fellow_project_budget_funded")
	 * @Method({"GET", "POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param FellowProject $fellowProject
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function fundingAction(FellowProject $fellowProject, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		// Tries to load the Budget from the Project, if none found, create a new one with the initial revision
		/** @var FellowProjectBudget $projectBudget */
		$projectBudget = $fellowProject->getFellowProjectBudget();
		if (!$projectBudget) {
			$projectBudget = new FellowProjectBudget($fellowProject);
			$budgetRevision = new Revision($projectBudget);
			$budgetRevision->setChangeDesc('Initial Budget');
			$budgetRevision->setUser($this->getUser());
			$projectBudget->addRevision($budgetRevision);
		}

		$this->denyAccessUnlessGranted('edit', $projectBudget);

		$currentFunding = $projectBudget->getFundedBy();
		$form = $this->createForm(FellowProjectBudgetFundedType::class, $projectBudget, [
			'role' => $this->get('security.token_storage')->getToken()->getRoles(),
		]);

		$form->handleRequest($request);

		if ($form->isValid()) {

			// If the funding entity changes, values must be recalculated
			if ($currentFunding != $projectBudget->getFundedBy()) {

				$budgetRevision = $projectBudget->getCurrentRevision();
				if (!$budgetRevision->getBudgetCostItems()->isEmpty()) {
					/** @var FellowProjectBudgetCostItem $item */
					foreach ($budgetRevision->getBudgetCostItems() as $item) {
						$item->updateRow($projectBudget->getFundedBy());
					}

					// TODO: Check Revision Totals, Global and One-time totals
					$budgetRevision->setSummaryTotals($em);

				}

			}

			$em->persist($projectBudget); // if projectBudget was just created
			$em->flush();

			// Saves the status, if in this step, consider started but not completed
			if ($projectBudget->getBudgetStepStatus($fellowProject, $em) == 0) {
				$projectBudget->setBudgetStepStatus($fellowProject, $em, 1);
			}

			$nextStep = 'fellow_project_budget_costs';

			if ($form->get('save')->isClicked()) {
				$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
				$nextStep =  'project_fellow_admin_menu';

				if ($projectGeneralInfo->getProjectReviewStatus()) {
					if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
						&& $this->isGranted('ROLE_EMBASSY')) {
						$nextStep = 'project_fellow_show';
					}
				}
			}

			return $this->redirectToRoute($nextStep, [
				'id' => $fellowProject->getId()
			]);
		}

		return $this->render('fellowprojectbudget/funded_by.html.twig', [
			'fellowProject' => $fellowProject,
			'form' => $form->createView(),
			'projectBudget' => $projectBudget,
		]);
	}

	/**
	 * Given a FellowProject, shows the table to enter or modify values
	 *
	 * @Route("/project/fellow/{id}/budget/costs", name="fellow_project_budget_costs")
	 * @Method({"GET", "POST"})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function costsAction(FellowProject $fellowProject, Request $request)
	{
		$fellowProjectBudget = $fellowProject->getFellowProjectBudget();

		$this->denyAccessUnlessGranted('edit', $fellowProjectBudget);
		$errors = ['rows' => []];

		// prepares projectBudget and subitems
		$monthlyCostItems = $this->getDoctrine()->getRepository(CostItemMonthly::class)->findAll();
		$oneTimeCostItems = $this->getDoctrine()->getRepository(CostItemOneTime::class)->findAll();

		$budgetRevision = $fellowProjectBudget->getCurrentRevision();

		if ($budgetRevision->getBudgetCostItems()->isEmpty()) {
			$budgetCostItems = array();
			$i = 0;
			foreach ($monthlyCostItems as $item) {
				$budgetCostItems[$i] = new FellowProjectBudgetCostItem($budgetRevision, $item);
				$budgetRevision->addBudgetCostItem($budgetCostItems[$i]);
				$i++;
			}

			foreach ($oneTimeCostItems as $item) {
				$budgetCostItems[$i] = new FellowProjectBudgetCostItem($budgetRevision, $item);
				$budgetRevision->addBudgetCostItem($budgetCostItems[$i]);
				$i++;
			}
		} else { // so that it does not check when cost items are just created
			$errors = $this->getErrorsInRows($budgetRevision);
		}

		$form = $this->createForm(FellowProjectBudgetType::class, $fellowProjectBudget, [
			'role' => $this->get('security.token_storage')->getToken()->getRoles(),
		]);

		$form->handleRequest($request);

		// Processes the request when the form is submitted
		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();

			// processes any deletion requested
			if ($form->get('delete_id')->getData()) {
				$projectCostItem = $this->getDoctrine()->getRepository(FellowProjectBudgetCostItem::class)->find($form->get('delete_id')->getData());
				$em->remove($projectCostItem);

				// there's not cascade delete of cost item, so delete it manually
				$em->remove($projectCostItem->getCostItem());
				$em->flush();

			}

			// creates project global values if needed, if none found, create new records by copying the template from global values
			if (!count($budgetRevision->getProjectGlobalValues())) {
				$globalValueFellow = $this->getDoctrine()->getRepository(GlobalValueFellow::class)->findAll();

				/** @var GlobalValueFellow $globalValue */
				foreach ($globalValueFellow as $globalValue) {
					$projectGlobalValue = new ProjectGlobalValue($budgetRevision, $globalValue, $globalValue->getDefaultValue());
					$budgetRevision->addProjectGlobalValue($projectGlobalValue);
				}
			}

			// Now, processes the Budget items submitted
			/** @var FellowProjectBudgetCostItem $item */
			foreach ($budgetRevision->getBudgetCostItems() as $item) {

				$item->updateRow($fellowProjectBudget->getFundedBy());

				// Adds default description when needed
				if ($item->getCostItem()->getDescription() == '') {
					$item->getCostItem()->setDescription('Untitled');
				}
			}

			// In order to calculate the summaryTotals, changes must be flushed to the DB
			$em->flush();
			// Then we can update the Summary Totals
			$budgetRevision->setSummaryTotals($em);

			// controls where to redirect after processing
            if ($form->get('save')->isClicked()) {
	            $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
	            $nextStep =  'project_fellow_admin_menu';

	            if ($projectGeneralInfo->getProjectReviewStatus()) {
		            if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
			            && $this->isGranted('ROLE_EMBASSY')) {
			            $nextStep = 'project_fellow_show';
		            }
	            }

	            $this->checkBudgetStatus($fellowProjectBudget, $em);

            } elseif ($form->get('calculate1')->isClicked() || $form->get('calculate2')->isClicked()) {
				$nextStep = 'fellow_project_budget_costs';

			} else if ($form->get('additional')->isClicked()) { // Adds a cost item
	            $costItem = new CostItemCustom();
				$costItemCustom = new FellowProjectBudgetCostItem($budgetRevision, $costItem);
	            $budgetRevision->addBudgetCostItem($costItemCustom);
	            $em->persist($costItem);
	            $nextStep = 'fellow_project_budget_costs';

			} else if ($form->get('additional2')->isClicked()) { // Adds a cost item
	            $costItem = new CostItemCustomInKind();
				$costItemCustom = new FellowProjectBudgetCostItem($budgetRevision, $costItem);
	            $budgetRevision->addBudgetCostItem($costItemCustom);
	            $em->persist($costItem);
	            $nextStep = 'fellow_project_budget_costs';

			} elseif ($form->get('back')->isClicked()) {
	            $this->checkBudgetStatus($fellowProjectBudget, $em);
	            $nextStep = 'fellow_project_budget_funded';

			} else { // Goes to review
				$this->checkBudgetStatus($fellowProjectBudget, $em);
				$nextStep = 'fellow_project_budget_review';
			}

//			$em->persist($fellowProjectBudget);
			$em->flush();

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

			return $this->redirectToRoute($nextStep, [
				'id' => $fellowProject->getId(),
			]);

		}

		if ($fellowProjectBudget->getFundedBy() != 'Post') {
			$viewToRender = 'cost_items_eca.html.twig';
		} else {
			$viewToRender = 'cost_items_post.html.twig';
		}

		return $this->render('fellowprojectbudget/' . $viewToRender, [
			'fellowProject' => $fellowProject,
			'form' => $form->createView(),
			'projectBudget' => $fellowProjectBudget,
			'monthlyCostItems' => $monthlyCostItems,
			'oneTimeCostItems' => $oneTimeCostItems,
			'errors' => $errors,
		]);
	}

	/**
	 * @Route("/project/fellow/{id}/budget/review", name="fellow_project_budget_review")
	 *
	 * @Method({"GET", "POST"})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function reviewCostsAction(FellowProject $fellowProject, Request $request)
	{

		$fellowProjectBudget = $fellowProject->getFellowProjectBudget();

		$this->denyAccessUnlessGranted('edit', $fellowProjectBudget);

		// Gets monthly and onetime cost items
		$monthlyCostItems = $this->getDoctrine()->getRepository('AppBundle:CostItemMonthly')->findAll();
		$oneTimeCostItems = $this->getDoctrine()->getRepository('AppBundle:CostItemOneTime')->findAll();
		$budgetRevision = $fellowProjectBudget->getCurrentRevision();

        $errors = $this->getErrorsInRows($budgetRevision);

		$totals = $budgetRevision->getSummaryTotals();

		// addition to define on the save button label
		/** @var ProjectGeneralInfo $projectGeneralInfo */
		$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

		// Defines the label depending on the status of the proposal
		$label = 'Save and return to Panel';
		if ($projectGeneralInfo->getProjectReviewStatus()) {
			if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
				&& $this->isGranted('ROLE_EMBASSY')) {
				$label = 'Save and return to Review';
			}
		}

		$form = $this->createFormBuilder()
			->add('save', SubmitType::class, ['label' => $label])
			->add('back', submitType::class, array('label' => 'Save and go back'))
			->add('next', submitType::class, array('label' => 'Save and continue'))
			->getForm();

		$form->handleRequest($request);

		// Processes the request when the form is submitted
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			$nextStep = 'fellow_project_budget_costs';
			if ($form->get('next')->isClicked() || $form->get('save')->isClicked()) {
				// Checks if all fullCost values were set (not empty)
				$this->checkBudgetStatus($fellowProjectBudget, $em);

				$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
				$nextStep =  'project_fellow_admin_menu';

				if ($projectGeneralInfo->getProjectReviewStatus()) {
					if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
						&& $this->isGranted('ROLE_EMBASSY')) {
						$nextStep = 'project_fellow_show';
					}
				}

			}

			return $this->redirectToRoute($nextStep, [
				'id' => $fellowProject->getId(),
			]);

		}

		// When accessing the form
		if ($fellowProjectBudget->getFundedBy() == 'ECA') {
			$viewToRender = 'review_costs_eca.html.twig';
		} else {
			$viewToRender = 'review_costs_post.html.twig';
		}

		return $this->render('fellowprojectbudget/' . $viewToRender, array(
			'fellowProject' => $fellowProject,
			'projectBudget' => $fellowProjectBudget,
			'monthlyCostItems' => $monthlyCostItems,
			'oneTimeCostItems' => $oneTimeCostItems,
			'totals' => $totals,
			'fellowProjectBudgetTotals' => $budgetRevision->getFellowProjectBudgetTotals(),
			'form' => $form->createView(),
            'errors' => $errors
		));

	}

	/**
	 * Generates page to confirm removal of project custom cost item
	 *
	 * @Route("/project/fellow/{pid}/costitems/{cid}/delete", name="fellow_project_cost_item_confirm_delete")
	 * @Method("GET")
	 *
	 * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
	 * @ParamConverter("projectCostItem", class="FellowProjectBudgetCostItem", options={"mapping": {"cid": "id"}})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param FellowProjectBudgetCostItem $projectCostItem
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function confirmDeleteCustomCostAction($fellowProject, $projectCostItem)
	{

		$this->denyAccessUnlessGranted('edit', $fellowProject->getFellowProjectBudget());

		// TODO: Validate that cost item belongs to it Budget
		// TODO: Review the parameters and refactor if possible

		if (!(is_a($projectCostItem->getCostItem(), '\AppBundle\Entity\CostItemCustom'))) {
			$this->addFlash('error', 'This item can not be deleted!');
			return $this->redirectToRoute('fellow_project_budget_costs', [
				'pid' => $fellowProject->getId(),
				'bid' => $projectCostItem->getFellowProjectBudget()->getId(),
			]);
		}

		$deleteForm = $this->createDeleteForm($projectCostItem);

		return $this->render('fellowprojectbudget/confirm_delete_cost_item.html.twig', array(
			'projectCostItem' => $projectCostItem,
			'delete_form' => $deleteForm->createView(),
		));

	}

	/**
	 * Deletes a Custom cost item.
	 *
	 * @Route("/project/fellow/{pid}/costitems/{cid}/delete", name="fellow_project_cost_item_delete")
	 * @Method("DELETE")
	 *
	 * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
	 * @ParamConverter("projectCostItem", class="FellowProjectBudgetCostItem", options={"mapping": {"cid": "id"}})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param FellowProjectBudgetCostItem $projectCostItem
	 *
	 * @return null
	 *
	 */
	public function deleteAction($fellowProject, $projectCostItem, Request $request)
	{
		$form = $this->createDeleteForm($projectCostItem);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->remove($projectCostItem);

			// there's not cascade delete of cost item, so delete it manually
			$em->remove($projectCostItem->getCostItem());

			$em->flush();
		}
//		$this->addFlash('error', 'This item can not be deleted!');

		return $this->redirectToRoute('fellow_project_budget_costs', [
			'pid' => $fellowProject->getId(),
			'bid' => $projectCostItem->getFellowProjectBudget()->getId(),
		]);
	}


	/**
	 * Creates a form to delete a project custom cost item.
	 *
	 * @param FellowProjectBudgetCostItem $projectCostItem
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($projectCostItem)
	{
		return $this->createFormBuilder()
			->setAction(
				$this->generateUrl('fellow_project_cost_item_delete', [
					'pid' => $projectCostItem->getFellowProjectBudget()->getFellowProject()->getId(),
					'cid' => $projectCostItem->getId()
				])
			)
			->setMethod('DELETE')
			->getForm();
	}

	/**
	 * TODO: This method is duplicated in "FellowProjectController". set function in one place.
	 * @param Revision $budgetRevision
	 * @return mixed
	 */
	private function getErrorsInRows($budgetRevision) {

		$errors = [
			'rows' => [],
			'costs' => [
				'monthly' => 0,
				'onetime' => 0,
			],
			'totals' => [
				'monthly' => 0,
				'onetime' => 0,
			],
		];

        if (null !== $budgetRevision) {
            /** @var FellowProjectBudgetCostItem $item */
            foreach ($budgetRevision->getBudgetCostItems() as $item) {
                $result = $item->checkTotals();
                if ($result === FellowProjectBudgetCostItem::ROW_ERROR_NO_COST) {
                    $errors['rows'][] = $item->getId();
                    if ($item->getCostItem() instanceof CostItemMonthly) {
                        $errors['costs']['monthly']++;
                    } else {
                        $errors['costs']['onetime']++;
                    }
                } else if ($result === FellowProjectBudgetCostItem::ROW_ERROR_NEGATIVE_TOTAL) {
                    $errors['rows'][] = $item->getId();
                    if ($item->getCostItem() instanceof CostItemMonthly) {
                        $errors['totals']['monthly']++;
                    } else {
                        $errors['totals']['onetime']++;
                    }
                }
            }
        }

		return $errors;
	}

	/**
	 * @param FellowProjectBudget $fellowProjectBudget
	 * @param $em
	 */
	private function checkBudgetStatus($fellowProjectBudget, $em) {

		$status = 2;
		foreach($fellowProjectBudget->getCurrentRevision()->getBudgetCostItems() as $item) {
			if ($item->getFullCost() === null && !($item->getCostItem() instanceof CostItemCustomInKind)) {
				$status = 1;
				break;
			}
		}
		// if all costs have been defined, then check errors in calculations
		if ($status == 2) {
			$errors = $this->getErrorsInRows($fellowProjectBudget->getCurrentRevision());
			if (count($errors['rows'])) {
				$status = 1;
			}
		}

		// Sets the budget section completed if going forward in this step
		$fellowProjectBudget->setBudgetStepStatus($fellowProjectBudget->getFellowProject(), $em, $status);

	}

  /**
   * Page to set the Regional Budget (per region)
   *
   * @Route("/admin/regional-budget", name="regional_budget")
   * @Method({"GET", "POST"})
   *
   * @return null
   *
   */
	public function setRegionalBudget(Request $request)
	{

		$em = $this->getDoctrine()->getManager();

		$formData = new \stdClass();
		$formData->budgetPerRegion = $em->getRepository(Region::class)->findAll();

		$form = $this->createFormBuilder($formData)
			->add('budgetPerRegion', CollectionType::class, [
				'entry_type' => RegionBaseType::class,
			])
			->add('submit', SubmitType::class, ['label' => 'Save'])
			->getForm();

		$form->handleRequest($request);

		if ($form->isValid()) {
			$em->flush();

			return $this->redirectToRoute('regional_budget');
		}

		return $this->render(
			'admin/regional_budget/edit.html.twig',
			[
				'form' => $form->createView()
			]
		);

	}

}
