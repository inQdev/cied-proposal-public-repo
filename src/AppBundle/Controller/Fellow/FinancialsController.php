<?php

namespace AppBundle\Controller\Fellow;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\CostItemMonthly;
use AppBundle\Entity\CostItemOneTime;
use AppBundle\Entity\Fellow\Budget\BudgetOneTimeTotals;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudget;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem;
use AppBundle\Entity\Fellow\Budget\FellowProjectBudgetTotals;
use AppBundle\Entity\Fellow\Budget\ProjectGlobalValue;
use AppBundle\Entity\Fellow\Budget\Revision;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\GlobalValueFellow;
use AppBundle\Form\Fellow\Financials\RevisionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Fellow Financials controller.
 *
 * @package AppBundle\Controller\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FinancialsController extends Controller implements RoleSwitcherInterface
{
	/** TODO: improve the security, considering the status of the proposal (it must be submitted), so check voters */
	/**
	 * @Route(
	 *     "/{id}/financials",
	 *     requirements={"id": "\d+"},
	 *     name="fellow_project_financials"
	 * )
	 * @Method("GET")
	 * @Security("has_role('ROLE_ADMIN')")
	 *
	 * @param FellowProject $fellowProject The Fellow proposal.
	 *
	 * @return Response
	 */
	public function summaryAction(FellowProject $fellowProject)
	{
		/** @var FellowProjectBudget $budget */
		$budget = $fellowProject->getFellowProjectBudget();

		/** @var Revision $currentRevision */
		$currentRevision = $budget->getCurrentRevision();

		$livingCosts = $currentRevision->getBudgetCostItems();
		$globalCosts = $currentRevision->getProjectGlobalValues();

		$orderedCosts = [];
		$budgetPrefix = 'Budgeted total ';

		// checks if one-time totals have been created for currentRevision (compatibility with old proposals)
		if ($currentRevision->getBudgetOneTimeTotals() == null) {
			$em = $this->getDoctrine()->getManager();
			$currentRevision->setSummaryOneTimeTotals($em);
			$em->flush();
		}

		do {
			/** @var ProjectGlobalValue $item */
			$item = $globalCosts->current();
//			dump($item->getGlobalValue()->getName()); exit;
			$orderedCosts[$budgetPrefix . $item->getGlobalValue()->getName()] = ['global', $item->getFullCost(), $item];
			$globalCosts->next();
//		} while ($i++ != 1);
		} while ($item->getGlobalValue()->getName() != 'Dependent allowance');

		$livingAllowance = $budgetPrefix . 'Living allowance';
		$orderedCosts[$livingAllowance] = ['group', 0, []];
		do  {
			/** @var FellowProjectBudgetCostItem $item */
			$item = $livingCosts->current();
			$orderedCosts[$livingAllowance][1] += $item->getFullCost();
			$orderedCosts[$livingAllowance][2][] = $item;
			$livingCosts->next();
		} while ($item->getCostItem()->getDescription() !=  'Local Transportation');

		$orderedCosts[$budgetPrefix .'Settling in/one time costs'] = [
			'one-time',
			$currentRevision->getBudgetOneTimeTotals()->getFullCost(),
		];

		do {
			/** @var ProjectGlobalValue $item */
			$item = $globalCosts->current();
//			dump($item->getGlobalValue());
			$orderedCosts[$budgetPrefix . $item->getGlobalValue()->getName()] = ['global', $item->getFullCost(), $item];
		} while ($globalCosts->next());

		return $this->render(':project/fellow/financials:summary.html.twig', [
			'fellowProject' => $fellowProject,
			'currentRevision' => $currentRevision,
			'orderedCosts' => $orderedCosts,
			'budgetPrefix' => $budgetPrefix,
		]);
	}


	/**
	 * @Route(
	 *     "/{id}/financials/budget/edit",
	 *     requirements={"id": "\d+"},
	 *     name="fellow_project_financials_edit_budget"
	 * )
	 * @Method({"GET", "POST"})
	 * @Security("has_role('ROLE_ADMIN')")
	 *
	 * @param FellowProject $fellowProject The Fellow proposal.
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return Response
	 */
	public function editBudgetAction(FellowProject $fellowProject, Request $request)
	{

		$em = $this->getDoctrine()->getManager();
		// prepares projectBudget and subitems
		$monthlyCostItems = $this->getDoctrine()->getRepository(CostItemMonthly::class)->findAll();
		$oneTimeCostItems = $this->getDoctrine()->getRepository(CostItemOneTime::class)->findAll();

		/** @var FellowProjectBudget $budget */
		$budget = $fellowProject->getFellowProjectBudget();
		/** @var Revision $currentRevision */
		$currentRevision = $budget->getCurrentRevision();

		// checks for onetime totals (backwards compatibility)
		if (!$currentRevision->getBudgetOneTimeTotals()) {
			$currentRevision->setSummaryOneTimeTotals($em); // funding depends on proposal funding type in this call
			$em->flush();
		}

		// TODO: Create entity to save globalvalues totals, updates setSummaryTotals accordingly
//	    $fixedT = $em->getRepository(Revision::class)->getGlobalsSum($currentRevision);
//	    dump($fixedT);
//
		$newRevision = $this->cloneBudgetRevision($currentRevision);

		$form = $this->createForm(RevisionType::class, $newRevision);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$budget->addRevision($newRevision);

			$newRevision->setUser($this->getUser());
			$newRevision->updateCostRows('admin');
			$newRevision->updateGlobalRows('admin');
			//		    dump($newRevision->getBudgetCostItems()); exit;

			// persists data in the DB, so that summary can be calculated
			$em->persist($newRevision);
			$em->flush();

			// Once values are in the DB, calculates summary totals and persists
			$newRevision->setSummaryTotals($em, 'admin', true); // not matter funding type, ECA can contribute

			$nextStep = 'fellow_project_financials';
			return $this->redirectToRoute($nextStep, ['id' => $fellowProject->getId()]);
		}

		return $this->render(':project/fellow/financials:edit_budget.html.twig', [
//			    'generalInfo' => $generalInfo,
				'fellowProject' => $fellowProject,
				'form' => $form->createView(),
				'currentRevision' => $currentRevision,
				'monthlyCostItems' => $monthlyCostItems,
				'oneTimeCostItems' => $oneTimeCostItems,
			]
		);
	}

	/**
	 * @Route(
	 *     "/{id}/financials/payment/add",
	 *     requirements={"id": "\d+"},
	 *     name="fellow_project_financials_add_payment"
	 * )
	 * @Method({"GET", "POST"})
	 * @Security("has_role('ROLE_ADMIN')")
	 *
	 * @param FellowProject $fellowProject The Fellow proposal.
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return Response
	 */
    public function addPaymentAction(FellowProject $fellowProject, Request $request) {

    	$em = $this->getDoctrine()->getManager();
	    // prepares projectBudget and subitems
	    $monthlyCostItems = $this->getDoctrine()->getRepository(CostItemMonthly::class)->findAll();
	    $oneTimeCostItems = $this->getDoctrine()->getRepository(CostItemOneTime::class)->findAll();

	    /** @var FellowProjectBudget $budget */
	    $budget = $fellowProject->getFellowProjectBudget();
	    /** @var Revision $currentRevision */
	    $currentRevision = $budget->getCurrentRevision();

	    $form = $this->createForm(RevisionType::class, $currentRevision);
	    $form->handleRequest($request);

	    if ($form->isValid()) {

	    }

	    return $this->render(':project/fellow/financials:add_payment.html.twig', [
			    'fellowProject' => $fellowProject,
			    'form' => $form->createView(),
			    'projectBudget' => $budget,
			    'monthlyCostItems' => $monthlyCostItems,
			    'oneTimeCostItems' => $oneTimeCostItems,
		    ]
	    );
	}


	/**
	 * Creates a new revision with setting up values for dependent objects (costs, totals, etc.)
	 * @param Revision $source
	 * @return Revision
	 */
	protected function cloneBudgetRevision(Revision $source)
	{
		$newRevision = new Revision($source->getBudget(), $source->getRevision() + 1);

		/** @var FellowProjectBudgetCostItem $item */
		foreach ($source->getBudgetCostItems() as $item) {
			$newItem = clone $item;
			$newItem->setBudgetRevision($newRevision);
//    		$newItem->setCostItem($item->getCostItem());
			$newRevision->addBudgetCostItem($newItem);
		}

		/** @var ProjectGlobalValue $item */
		foreach ($source->getProjectGlobalValues() as $item) {
			$newItem = clone $item;
			$newItem->setBudgetRevision($newRevision);
			$newRevision->addProjectGlobalValue($newItem);
		}

		/** @var BudgetOneTimeTotals $newItem */
		$newItem = clone $source->getBudgetOneTimeTotals();
		$newItem->setBudgetRevision($newRevision);
		$newRevision->setBudgetOneTimeTotals($newItem);

		/** @var FellowProjectBudgetTotals $newItem */
		$newItem = clone $source->getFellowProjectBudgetTotals();
		$newItem->setBudgetRevision($newRevision);
		$newRevision->setFellowProjectBudgetTotals($newItem);

		return $newRevision;
	}

	/**
	 * @Route(
	 *     "/{id}/financials/budget",
	 *     requirements={"id": "\d+"},
	 *     name="fellow_project_financials_budget_history"
	 * )
	 * @Method({"GET", "POST"})
	 * @Security("has_role('ROLE_ADMIN')")
	 *
	 * @param FellowProject $fellowProject The Fellow proposal.
	 *
	 * @return Response
	 */
	public function budgetHistoryAction(FellowProject $fellowProject)
	{
		/** @var FellowProjectBudget $budget */
		$budget = $fellowProject->getFellowProjectBudget();

		return $this->render(':project/fellow/financials:budget_history.html.twig', [
				'fellowProject' => $fellowProject,
				'projectBudget' => $budget,
			]
		);
	}
	/**
	 * @Route(
	 *     "/{id}/financials/budget/{revision}",
	 *     requirements={"id": "\d+", "revision": "\d+"},
	 *     name="fellow_project_financials_view_budget"
	 * )
	 * @Method({"GET", "POST"})
	 * @Security("has_role('ROLE_ADMIN')")
	 *
	 * @param FellowProject $fellowProject The Fellow proposal.
	 * @param int
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return Response
	 */
	public function viewBudgetAction(FellowProject $fellowProject, $revision, Request $request)
	{
		// prepares projectBudget and subitems
		$monthlyCostItems = $this->getDoctrine()->getRepository(CostItemMonthly::class)->findAll();
		$oneTimeCostItems = $this->getDoctrine()->getRepository(CostItemOneTime::class)->findAll();

		/** @var FellowProjectBudget $budget */
		$budget = $fellowProject->getFellowProjectBudget();
		/** @var Revision $budgetRevision */
		$budgetRevision = $budget->findRevision($revision);
//		dump($budgetRevision); exit;

		return $this->render(':project/fellow/financials:view_budget.html.twig', [
				'fellowProject' => $fellowProject,
//				'form' => $form->createView(),
//				'projectBudget' => $budget,
				'budgetRevision' => $budgetRevision,
				'monthlyCostItems' => $monthlyCostItems,
				'oneTimeCostItems' => $oneTimeCostItems,
			]
		);
	}


}
