<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * User controller.
 * @package AppBundle\Controller
 */
class UserController extends Controller implements RoleSwitcherInterface
{
    /**
     * Lists all Users with EMBASSY role
     *
     * @Route("/admin/embassy", name="embassy_admin_index")
     * @Method("GET")
     */
    public function embassyAdminIndexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAllEmbassies();

        return $this->render('admin/embassy/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Lists all Users with RELO role
     *
     * @Route("/admin/relo", name="relo_admin_index")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reloAdminIndexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reloUsers = $em->getRepository('AppBundle:User')->findAllRELOs();

        return $this->render(
          'admin/relo/index.html.twig',
          ['users' => $reloUsers]
        );
    }

    /**
     * Lists all Users with RPO role
     *
     * @Route("/admin/rpo", name="rpo_admin_index")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rpoAdminIndexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rpoUsers = $em->getRepository('AppBundle:User')->findAllRPOs();

        return $this->render(
          'admin/rpo/index.html.twig',
          ['users' => $rpoUsers]
        );
    }

    /**
     * Displays a form to edit an EMBASSY.
     *
     * @Route("/admin/embassy/{id}/edit", name="embassy_admin_edit")
     * @Method({"GET", "POST"})
     */
    public function embassyAdminEditAction(Request $request, User $user)
    {
        $editForm = $this->createForm('AppBundle\Form\UserEmbassyType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('embassy_admin_index');
        }

        return $this->render('admin/embassy/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Displays a form to edit a RELO user.
     *
     * @Route("/admin/relo/{id}/edit", name="relo_admin_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function reloAdminEditAction(Request $request, User $user)
    {
        $editForm = $this->createForm('AppBundle\Form\UserReloType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('relo_admin_index');
        }

        return $this->render('admin/relo/edit.html.twig', array(
          'user' => $user,
          'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Displays a form to edit a RPO user.
     *
     * @Route("/admin/rpo/{id}/edit", name="rpo_admin_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param User    $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function rpoAdminEditAction(Request $request, User $user)
    {
        $editForm = $this->createForm('AppBundle\Form\UserRpoType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('rpo_admin_index');
        }

        return $this->render('admin/rpo/edit.html.twig', array(
          'user' => $user,
          'edit_form' => $editForm->createView(),
        ));
    }


    /**
     * Helper action to create the Role switcher dropdown and avoid the Twig cache on the
     * header.html.twig template
     * It works with the authenticated user data and the cookie set in the application system
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function roleSwitcherFormAction() {
        $authenticatedUser = $this->getUser();
        $parsedRoles = $authenticatedUser->parseRolesArray($authenticatedUser->getRoles());

	    $currentRole = $this->get('security.token_storage')->getToken()->getRoles()[0]->getRole();

	    $selectedRole = 0;
	    foreach($parsedRoles as $key=>$role) {
			if ($key == $currentRole) {
				$selectedRole = $role['roleId'];
				break;
			}
	    }

        return $this->render(
            'role_switch.html.twig', [
                'parsedRoles' => $parsedRoles,
		        'selectedRole' => $selectedRole,
        ]);
    }

    /**
     * Helper action to create the user menu based on the current role selected
     * in the Role Switcher.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function roleMenuAction() {
        $em = $this->getDoctrine()->getManager();

        try {
            /** @var CycleFellow $currentFellowCycle */
            $currentFellowCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();
        } catch (NoResultException $e) {
            $currentFellowCycle = $em->getRepository(CycleFellow::class)->findLast();
        }

        $session = $this->get('session');

	    $currentRole = $this->get('security.token_storage')->getToken()->getRoles()[0]->getRole();
        $formattedCurrentRole = strtolower(str_replace(array('ROLE_', '_'), array('', '-'), $currentRole));

        $apiRequest = $this->get('proposal.drupal_data_consumer');

        if (!$session->has('menu_' . $formattedCurrentRole)) {
            $response = $apiRequest->sendRequest(
              'GET',
              '/rolemenu/' . $formattedCurrentRole
            );

            $session->set('menu_' . $formattedCurrentRole, $response);
        }
        else {
            $response = $session->get('menu_' . $formattedCurrentRole);
        }

        return $this->render(
          'menu.html.twig',
          [
            'roleMenu'    => $response,
            'fellowCycle' => $currentFellowCycle,
          ]
        );
    }
}
