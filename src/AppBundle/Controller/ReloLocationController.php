<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\ReloLocation;
use AppBundle\Form\ReloLocationType;

/**
 * ReloLocation controller.
 *
 * @Route("/admin/relo-location")
 */
class ReloLocationController extends Controller implements RoleSwitcherInterface
{
    /**
     * Lists all ReloLocation entities.
     *
     * @Route("/", name="relo_location_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reloLocations = $em->getRepository('AppBundle:ReloLocation')->findAll();

        return $this->render(
          'admin/relo_location/index.html.twig',
          ['reloLocations' => $reloLocations]
        );
    }

    /**
     * Creates a new ReloLocation entity.
     *
     * @Route("/new", name="relo_location_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $reloLocation = new ReloLocation();
        $form = $this->createForm(
          'AppBundle\Form\ReloLocationType',
          $reloLocation
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reloLocation);
            $em->flush();

            return $this->redirectToRoute(
              'relo_location_index',
              ['id' => $reloLocation->getId()]
            );
        }

        return $this->render(
          'admin/relo_location/new.html.twig',
          [
            'reloLocation' => $reloLocation,
            'form'         => $form->createView(),
          ]
        );
    }

    /**
     * Displays a form to edit an existing ReloLocation entity.
     *
     * @Route("/{id}/edit", name="relo_location_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ReloLocation $reloLocation)
    {
        $deleteForm = $this->createDeleteForm($reloLocation);
        $editForm = $this->createForm(
          'AppBundle\Form\ReloLocationType',
          $reloLocation
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reloLocation);
            $em->flush();

            return $this->redirectToRoute(
              'relo_location_index',
              ['id' => $reloLocation->getId()]
            );
        }

        return $this->render(
          'admin/relo_location/edit.html.twig',
          [
            'reloLocation' => $reloLocation,
            'edit_form'    => $editForm->createView(),
            'delete_form'  => $deleteForm->createView(),
          ]
        );
    }

    /**
     * Deletes a ReloLocation entity.
     *
     * @Route("/{id}", name="relo_location_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ReloLocation $reloLocation)
    {
        $form = $this->createDeleteForm($reloLocation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reloLocation);
            $em->flush();
        }

        return $this->redirectToRoute('relo_location_index');
    }

    /**
     * Creates a form to delete a ReloLocation entity.
     *
     * @param ReloLocation $reloLocation The ReloLocation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ReloLocation $reloLocation)
    {
        return $this->createFormBuilder()
          ->setAction(
            $this->generateUrl(
              'relo_location_delete',
              ['id' => $reloLocation->getId()]
            )
          )
          ->setMethod('DELETE')
          ->getForm();
    }
}
