<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Assignment Entity Class
 * 
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InCountryAssignmentRepository")
 * @ORM\Table(name="in_country_assignment")
 */
class InCountryAssignment extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	protected $city;

	/**
	 * @var Country $country
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="Country",
	 *     inversedBy="inCountryAssignments"
	 * )
	 */
	protected $country;

	/**
	 * @var SpecialistProjectPhase $specialistProjectPhase
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="SpecialistProjectPhase",
	 *     inversedBy="inCountryAssignments"
	 * )
	 * @ORM\JoinColumn(
	 *     name="specialist_project_phase_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $specialistProjectPhase;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="ItineraryActivity",
	 *     mappedBy="inCountryAssignment",
	 *     cascade={"persist", "remove"}
	 * )
	 */
	protected $assignmentActivities;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="LivingExpense",
	 *     mappedBy="inCountryAssignment",
	 * )
	 */
	protected $livingExpenseEstimates;

	/****************************  Methods **********************************/

	/**
	 * InCountryAssignment constructor.
	 * @param Country $country
	 */
	public function __construct(Country $country) {
		$this->assignmentActivities = new ArrayCollection();
		$this->country = $country;
	}

	/**
     * Set city
     *
     * @param string $city
     *
     * @return InCountryAssignment
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param integer $country
     *
     * @return InCountryAssignment
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set specialistProjectPhase
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return InCountryAssignment
     */
    public function setSpecialistProjectPhase($specialistProjectPhase)
    {
        $this->specialistProjectPhase = $specialistProjectPhase;

        return $this;
    }

    /**
     * Get specialistProjectPhase
     *
     * @return SpecialistProjectPhase
     */
    public function getSpecialistProjectPhase()
    {
        return $this->specialistProjectPhase;
    }

    /**
     * Add assignmentActivity
     *
     * @param \AppBundle\Entity\ItineraryActivity $assignmentActivity
     *
     * @return InCountryAssignment
     */
    public function addAssignmentActivity(\AppBundle\Entity\ItineraryActivity $assignmentActivity)
    {
        $this->assignmentActivities[] = $assignmentActivity;

        return $this;
    }

    /**
     * Remove assignmentActivity
     *
     * @param \AppBundle\Entity\ItineraryActivity $assignmentActivity
     */
    public function removeAssignmentActivity(\AppBundle\Entity\ItineraryActivity $assignmentActivity)
    {
        $this->assignmentActivities->removeElement($assignmentActivity);
    }

    /**
     * Get assignmentActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignmentActivities()
    {
        return $this->assignmentActivities;
    }


	public function getName() {
		return sprinf("%s (%s)", $this->city, $this->getCountry()->getName());
	}


    /**
     * Add livingExpenseEstimate
     *
     * @param \AppBundle\Entity\LivingExpense $livingExpenseEstimate
     *
     * @return InCountryAssignment
     */
    public function addLivingExpenseEstimate(\AppBundle\Entity\LivingExpense $livingExpenseEstimate)
    {
        $this->livingExpenseEstimates[] = $livingExpenseEstimate;

        return $this;
    }

    /**
     * Remove livingExpenseEstimate
     *
     * @param \AppBundle\Entity\LivingExpense $livingExpenseEstimate
     */
    public function removeLivingExpenseEstimate(\AppBundle\Entity\LivingExpense $livingExpenseEstimate)
    {
        $this->livingExpenseEstimates->removeElement($livingExpenseEstimate);
    }

    /**
     * Get livingExpenseEstimates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivingExpenseEstimates()
    {
        return $this->livingExpenseEstimates;
    }
}
