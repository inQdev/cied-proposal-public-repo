<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectGeneralInfo entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\ProjectGeneralInfoRepository"
 * )
 * @ORM\EntityListeners({"AppBundle\Entity\Listener\GeneralInfoListener"})
 * @ORM\Table(name="project_general_info")
 */
class ProjectGeneralInfo extends BaseEntity
{
    /**
     * Constants.
     */
    const PROJECT_NOT_SUBMITTED = 'not_submitted';
    const PROJECT_SUBMITTED = 'submitted';

    /**
     * @var bool $visaPriorArrival
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $visaPriorArrival;

    /**
     * @var string $visaRequirementsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $visaRequirementsDesc;

    /**
     * @var bool $requiredMedicalConditions
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $requiredMedicalConditions;

    /**
     * @var bool $medicalAccessIssues
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $medicalAccessIssues;

    /**
     * @var string $medicalRestrictionsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $medicalRestrictionsDesc;

    /**
     * @var bool $requiredVaccinations
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $requiredVaccinations;

    /**
     * @var string $requiredVaccinationsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $requiredVaccinationsDesc;

    /**
     * @var bool $ageRestriction
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $ageRestriction;

    /**
     * @var bool $ageRestrictionDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $ageRestrictionDesc;

    /**
     * @var bool $degreeRequirements
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $degreeRequirements;

    /**
     * @var string $degreeRequirementsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $degreeRequirementsDesc;

    /**
     * @var string $securityInfoDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $securityInfoDesc;

    /**
     * @var string $additionalRequirementsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $additionalRequirementsDesc;

    /**
     * @var bool $rsoCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $rsoCertification;

    /**
     * @var bool $housingCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $housingCertification;

    /**
     * @var bool $visaCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $visaCertification;

    /**
     * @var bool $medicalCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $medicalCertification;

    /**
     * @var bool $logisticsCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $logisticsCertification;

    /**
     * @var bool $logisticsCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $monitoringCertification;

    /**
     * @var bool $postArrivalCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $postArrivalCertification;

    /**
     * @var bool $inCountryOrientationCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $inCountryOrientationCertification;

    /**
     * @var bool $costSharingCertification
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $costSharingCertification;

    /**
     * @var bool $underConsideration
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $underConsideration = false;

    /**
     * @var string $postMissionGoalsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $postMissionGoalsDesc;

    /**
     * @var string $submissionStatus
     *
     * @ORM\Column(type="string", length=64, options={"default"="not_submitted"})
     * @Assert\Choice(choices = {"not_submitted", "submitted"})
     */
    protected $submissionStatus = self::PROJECT_NOT_SUBMITTED;

    /**
     * @var int $reloRanking
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $reloRanking;

    /**
     * @var int $rpoRanking
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $rpoRanking;

    /**
     * @var ProjectReviewStatus $projectReviewStatus
     *
     * @ORM\ManyToOne(targetEntity="ProjectReviewStatus")
     */
    protected $projectReviewStatus;

    /**
     * @var ProjectOutcome $projectOutcome
     *
     * @ORM\ManyToOne(targetEntity="ProjectOutcome")
     */
    protected $projectOutcome;

    /**
     * @var VisaAvgLength $visaAvgLength
     *
     * @ORM\ManyToOne(targetEntity="VisaAvgLength")
     */
    protected $visaAvgLength;

    /**
     * @var Region $region
     *
     * @ORM\ManyToOne(targetEntity="Region")
     */
    protected $region;

    /**
     * @var ArrayCollection $countries
     *
     * @ORM\ManyToMany(
     *     targetEntity="Country",
     *     inversedBy="projects",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="projects_countries")
     * @ORM\OrderBy({"name"="ASC"})
     */
    protected $countries;

    /**
     * @var ArrayCollection $reloLocation
     *
     * @ORM\ManyToMany(targetEntity="ReloLocation", cascade={"persist"})
     * @ORM\JoinTable(name="projects_relo_locations")
     * @ORM\OrderBy({"name"="ASC"})
     */
    protected $reloLocations;

    /**
     * @var ArrayCollection $posts
     *
     * @ORM\ManyToMany(
     *     targetEntity="Post",
     *     inversedBy="projects",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="projects_posts")
     * @ORM\OrderBy({"name"="ASC"})
     */
    protected $posts;

    /**
     * @var AbstractCycle $cycle
     *
     * @ORM\ManyToOne(targetEntity="AbstractCycle", inversedBy="projectsGeneralInfo")
     */
    protected $cycle;

    /**
     * Cascade deletion defined in FellowProject
     * @var FellowProject $fellowProject
     *
     * @ORM\OneToOne(
     *     targetEntity="FellowProject",
     *     mappedBy="projectGeneralInfo",
     * )
     */
    protected $fellowProject;

    /**
     * TODO: Define cascade deletion on SpecialistProjectPhase
     * @var SpecialistProjectPhase $specialistProjectPhase
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhase",
     *     mappedBy="projectGeneralInfo"
     * )
     */
    protected $specialistProjectPhase;

    /**
     * Cascade deletion defined in ContactPoint
     * @var ArrayCollection $contactPoints
     *
     * @ORM\OneToMany(
     *     targetEntity="ContactPoint",
     *     mappedBy="projectGeneralInfo",
     *     cascade={"persist"}
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $contactPoints;

    /**
     * @var ArrayCollection $authors
     *
     * @ORM\ManyToMany(
     *     targetEntity="User",
     *     mappedBy="projects",
     *     cascade={"persist"}
     * )
     * @ORM\OrderBy({"name"="ASC"})
     */
    private $authors;

    /**
     * @var int $createdBy
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @Gedmo\Blameable(on="create")
     */
    protected $createdBy;

    /**
     * @var int $updatedBy
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * @Gedmo\Blameable(on="update")
     */
    protected $updatedBy;

    /**
     * @var int $submittedBy
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="submitted_by", referencedColumnName="id")
     */
    protected $submittedBy;

    /**
     * @var string $submittedByRole
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $submittedByRole;

    /**
     * @var string $createdByRole
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $createdByRole;

	/**
	 * @var string $referenceNumber
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $referenceNumber;

    /******************************* Methods Definition ***************************************/

    /**
     * ProjectGeneralInfo constructor.
     *
     * @param AbstractCycle $cycle
     */
    public function __construct(AbstractCycle $cycle)
    {
        $this->cycle = $cycle;

        $this->contactPoints = new ArrayCollection();
        $this->authors = new ArrayCollection();
        $this->countries = new ArrayCollection();
        $this->reloLocations = new ArrayCollection();
        $this->posts = new ArrayCollection();

        $this->addContactPoint(new ContactPoint($this));
        $this->addContactPoint(new ContactPointSecondary($this));
    }

    /**
     * Check whether the project is editable or not.
     * TODO: felipe - 20160620 - Remove this method since it's not needed anymore, check dependencies before it
     *
     * @return bool TRUE if project is editable, FALSE otherwise.
     */
    public function isEditable()
    {
        return ($this->submissionStatus === self::PROJECT_NOT_SUBMITTED) || ($this->projectReviewStatus === ProjectReviewStatus::UNDER_POST_RE_REVIEW);
    }

    /**
     * Checks if the Proposal is submitted and not completed, in such case it's withdrawable
     * @return bool
     */
    public function isWithdrawable() {
        $output = false;

        if ($this->submissionStatus === self::PROJECT_SUBMITTED) {
            if ( !in_array($this->getProjectReviewStatus()->getId(), [
                    ProjectReviewStatus::REVIEW_COMPLETE,
                    ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
                ]) ) {
                $output = true;
            }
        }

        return $output;
    }

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    public function setProjectGeneralInfoStepStatus($em)
    {
        /** @var FellowProject $fellowProject */
        $fellowProject = $this->fellowProject;

        $newStepStatus = WizardStepStatus::NOT_STARTED;

        if ((null !== $this->region)
            && !$this->countries->isEmpty()
            && !$this->posts->isEmpty()
            && !$this->reloLocations->isEmpty()
            && (null !== $fellowProject->getProposedStartDate())
            && (null !== $fellowProject->getProposedEndDate())
            && (null !== $fellowProject->getCycleSeason())
            && (null !== $fellowProject->isProposedStartDateFlexible())
        ) {
            $newStepStatus = WizardStepStatus::COMPLETED;
        } elseif ((null !== $this->region)
            || !$this->countries->isEmpty()
            || !$this->posts->isEmpty()
            || !$this->reloLocations->isEmpty()
            || (null !== $fellowProject->getProposedStartDate())
            || (null !== $fellowProject->getProposedEndDate())
            || (null !== $fellowProject->getCycleSeason())
            || (null !== $fellowProject->isProposedStartDateFlexible())
            || (null !== $fellowProject->getCycleSeasonDesc())
        ) {
            $newStepStatus = WizardStepStatus::IN_PROGRESS;
        }

        /** @var WizardStepStatus $generalInfoStep */
        $generalInfoStep = $em->getRepository('AppBundle:WizardStepStatus')
          ->findStepStatusByProject(
            $fellowProject,
            WizardStepStatus::GENERAL_INFO_STEP
          );

        if ($newStepStatus !== $generalInfoStep->getStatus()) {
            $generalInfoStep->setStatus($newStepStatus);
            $em->persist($generalInfoStep);
            $em->flush();
        }
    }

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    public function setContactPointsSectionStatus($em)
    {
        $isContactPointsSectionComplete = true;

        /** @var ContactPoint $contactPoint */
        foreach ($this->contactPoints as $contactPoint) {
            if (!$contactPoint instanceof ContactPointAdditional) {
                if ((null !== $contactPoint->getFirstName())
                  && (null !== $contactPoint->getLastName())
                  && (null !== $contactPoint->getTitle())
                  && (null !== $contactPoint->getEmail())
                ) {
                    $isContactPointsSectionComplete = $isContactPointsSectionComplete && true;
                } elseif ((null !== $contactPoint->getFirstName())
                  || (null !== $contactPoint->getLastName())
                  || (null !== $contactPoint->getTitle())
                  || (null !== $contactPoint->getEmail())
                  || (null !== $contactPoint->getAlternateEmail())
                  || (null !== $contactPoint->getAnticipatedDepartureDate())
                ) {
                    $isContactPointsSectionComplete = $isContactPointsSectionComplete && false;
                } else {
                    $isContactPointsSectionComplete = false;
                }
            }

            $contactPointsStepStatus = $isContactPointsSectionComplete
              ? WizardStepStatus::COMPLETED
              : WizardStepStatus::IN_PROGRESS;

            /** @var WizardStepStatus $contactPointStep */
            $contactPointStep = $em->getRepository('AppBundle:WizardStepStatus')
              ->findStepStatusByProject(
                $this->fellowProject,
                WizardStepStatus::POINT_OF_CONTACT_STEP
              );

            if ($contactPointsStepStatus !== $contactPointStep->getStatus()) {
                $contactPointStep->setStatus($contactPointsStepStatus);
                $em->persist($contactPointStep);
                $em->flush();
            }
        }
    }

    /**
     * Return the list of Countries associated to project formatted in a
     * that the Solr server can index them.
     *
     * @return array Countries associated to project.
     */
    public function getCountriesToIndex()
    {
        return $this->countries->toArray();
    }

    /**
     * Return the list of RELO Locations associated to project formatted in a
     * way that the Solr server can index them.
     *
     * @return array Countries associated to project.
     */
    public function getReloLocationsToIndex()
    {
        return $this->reloLocations->toArray();
    }

    /**
     * Set visaPriorArrival
     *
     * @param boolean $visaPriorArrival
     *
     * @return ProjectGeneralInfo
     */
    public function setVisaPriorArrival($visaPriorArrival)
    {
        $this->visaPriorArrival = $visaPriorArrival;

        return $this;
    }

    /**
     * Get visaPriorArrival
     *
     * @return boolean
     */
    public function getVisaPriorArrival()
    {
        return $this->visaPriorArrival;
    }

    /**
     * Set visaRequirementsDesc
     *
     * @param string $visaRequirementsDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setVisaRequirementsDesc($visaRequirementsDesc)
    {
        $this->visaRequirementsDesc = $visaRequirementsDesc;

        return $this;
    }

    /**
     * Get visaRequirementsDesc
     *
     * @return string
     */
    public function getVisaRequirementsDesc()
    {
        return $this->visaRequirementsDesc;
    }

    /**
     * Set requiredMedicalConditions
     *
     * @param boolean $requiredMedicalConditions
     *
     * @return ProjectGeneralInfo
     */
    public function setRequiredMedicalConditions($requiredMedicalConditions)
    {
        $this->requiredMedicalConditions = $requiredMedicalConditions;

        return $this;
    }

    /**
     * Get requiredMedicalConditions
     *
     * @return boolean
     */
    public function getRequiredMedicalConditions()
    {
        return $this->requiredMedicalConditions;
    }

    /**
     * Set medicalAccessIssues
     *
     * @param boolean $medicalAccessIssues
     *
     * @return ProjectGeneralInfo
     */
    public function setMedicalAccessIssues($medicalAccessIssues)
    {
        $this->medicalAccessIssues = $medicalAccessIssues;

        return $this;
    }

    /**
     * Get medicalAccessIssues
     *
     * @return boolean
     */
    public function getMedicalAccessIssues()
    {
        return $this->medicalAccessIssues;
    }

    /**
     * Set medicalRestrictionsDesc
     *
     * @param string $medicalRestrictionsDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setMedicalRestrictionsDesc($medicalRestrictionsDesc)
    {
        $this->medicalRestrictionsDesc = $medicalRestrictionsDesc;

        return $this;
    }

    /**
     * Get medicalRestrictionsDesc
     *
     * @return string
     */
    public function getMedicalRestrictionsDesc()
    {
        return $this->medicalRestrictionsDesc;
    }

    /**
     * Set requiredVaccinations
     *
     * @param boolean $requiredVaccinations
     *
     * @return ProjectGeneralInfo
     */
    public function setRequiredVaccinations($requiredVaccinations)
    {
        $this->requiredVaccinations = $requiredVaccinations;

        return $this;
    }

    /**
     * Get requiredVaccinations
     *
     * @return boolean
     */
    public function getRequiredVaccinations()
    {
        return $this->requiredVaccinations;
    }

    /**
     * Set requiredVaccinationsDesc
     *
     * @param string $requiredVaccinationsDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setRequiredVaccinationsDesc($requiredVaccinationsDesc)
    {
        $this->requiredVaccinationsDesc = $requiredVaccinationsDesc;

        return $this;
    }

    /**
     * Get requiredVaccinationsDesc
     *
     * @return string
     */
    public function getRequiredVaccinationsDesc()
    {
        return $this->requiredVaccinationsDesc;
    }

    /**
     * Set ageRestriction
     *
     * @param boolean $ageRestriction
     *
     * @return ProjectGeneralInfo
     */
    public function setAgeRestriction($ageRestriction)
    {
        $this->ageRestriction = $ageRestriction;

        return $this;
    }

    /**
     * Get ageRestriction
     *
     * @return boolean
     */
    public function getAgeRestriction()
    {
        return $this->ageRestriction;
    }

    /**
     * Set ageRestrictionDesc
     *
     * @param string $ageRestrictionDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setAgeRestrictionDesc($ageRestrictionDesc)
    {
        $this->ageRestrictionDesc = $ageRestrictionDesc;

        return $this;
    }

    /**
     * Get ageRestrictionDesc
     *
     * @return string
     */
    public function getAgeRestrictionDesc()
    {
        return $this->ageRestrictionDesc;
    }

    /**
     * Set degreeRequirements
     *
     * @param boolean $degreeRequirements
     *
     * @return ProjectGeneralInfo
     */
    public function setDegreeRequirements($degreeRequirements)
    {
        $this->degreeRequirements = $degreeRequirements;

        return $this;
    }

    /**
     * Get degreeRequirements
     *
     * @return boolean
     */
    public function getDegreeRequirements()
    {
        return $this->degreeRequirements;
    }

    /**
     * Set degreeRequirementsDesc
     *
     * @param string $degreeRequirementsDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setDegreeRequirementsDesc($degreeRequirementsDesc)
    {
        $this->degreeRequirementsDesc = $degreeRequirementsDesc;

        return $this;
    }

    /**
     * Get degreeRequirementsDesc
     *
     * @return string
     */
    public function getDegreeRequirementsDesc()
    {
        return $this->degreeRequirementsDesc;
    }

    /**
     * Set securityInfoDesc
     *
     * @param string $securityInfoDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setSecurityInfoDesc($securityInfoDesc)
    {
        $this->securityInfoDesc = $securityInfoDesc;

        return $this;
    }

    /**
     * Get securityInfoDesc
     *
     * @return string
     */
    public function getSecurityInfoDesc()
    {
        return $this->securityInfoDesc;
    }

    /**
     * Set additionalRequirementsDesc
     *
     * @param string $additionalRequirementsDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setAdditionalRequirementsDesc($additionalRequirementsDesc)
    {
        $this->additionalRequirementsDesc = $additionalRequirementsDesc;

        return $this;
    }

    /**
     * Get additionalRequirementsDesc
     *
     * @return string
     */
    public function getAdditionalRequirementsDesc()
    {
        return $this->additionalRequirementsDesc;
    }

    /**
     * Set rsoCertification
     *
     * @param boolean $rsoCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setRsoCertification($rsoCertification)
    {
        $this->rsoCertification = $rsoCertification;

        return $this;
    }

    /**
     * Get rsoCertification
     *
     * @return boolean
     */
    public function getRsoCertification()
    {
        return $this->rsoCertification;
    }

    /**
     * Set housingCertification
     *
     * @param boolean $housingCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setHousingCertification($housingCertification)
    {
        $this->housingCertification = $housingCertification;

        return $this;
    }

    /**
     * Get housingCertification
     *
     * @return boolean
     */
    public function getHousingCertification()
    {
        return $this->housingCertification;
    }

    /**
     * Set visaCertification
     *
     * @param boolean $visaCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setVisaCertification($visaCertification)
    {
        $this->visaCertification = $visaCertification;

        return $this;
    }

    /**
     * Get visaCertification
     *
     * @return boolean
     */
    public function getVisaCertification()
    {
        return $this->visaCertification;
    }

    /**
     * Set medicalCertification
     *
     * @param boolean $medicalCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setMedicalCertification($medicalCertification)
    {
        $this->medicalCertification = $medicalCertification;

        return $this;
    }

    /**
     * Get medicalCertification
     *
     * @return boolean
     */
    public function getMedicalCertification()
    {
        return $this->medicalCertification;
    }

    /**
     * Set logisticsCertification
     *
     * @param boolean $logisticsCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setLogisticsCertification($logisticsCertification)
    {
        $this->logisticsCertification = $logisticsCertification;

        return $this;
    }

    /**
     * Get logisticsCertification
     *
     * @return boolean
     */
    public function getLogisticsCertification()
    {
        return $this->logisticsCertification;
    }

    /**
     * Set monitoringCertification
     *
     * @param boolean $monitoringCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setMonitoringCertification($monitoringCertification)
    {
        $this->monitoringCertification = $monitoringCertification;

        return $this;
    }

    /**
     * Get monitoringCertification
     *
     * @return boolean
     */
    public function getMonitoringCertification()
    {
        return $this->monitoringCertification;
    }

    /**
     * Set postArrivalCertification
     *
     * @param boolean $postArrivalCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setPostArrivalCertification($postArrivalCertification)
    {
        $this->postArrivalCertification = $postArrivalCertification;

        return $this;
    }

    /**
     * Get postArrivalCertification
     *
     * @return boolean
     */
    public function getPostArrivalCertification()
    {
        return $this->postArrivalCertification;
    }

    /**
     * Set costSharingCertification
     *
     * @param boolean $costSharingCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setCostSharingCertification($costSharingCertification)
    {
        $this->costSharingCertification = $costSharingCertification;

        return $this;
    }

    /**
     * Get costSharingCertification
     *
     * @return boolean
     */
    public function getCostSharingCertification()
    {
        return $this->costSharingCertification;
    }

    /**
     * Set underConsideration
     *
     * @param boolean $underConsideration
     *
     * @return ProjectGeneralInfo
     */
    public function setUnderConsideration($underConsideration)
    {
        $this->underConsideration = $underConsideration;

        return $this;
    }

    /**
     * Get underConsideration
     *
     * @return boolean
     */
    public function isUnderConsideration()
    {
        return $this->underConsideration;
    }

    /**
     * Set postMissionGoalsDesc
     *
     * @param string $postMissionGoalsDesc
     *
     * @return ProjectGeneralInfo
     */
    public function setPostMissionGoalsDesc($postMissionGoalsDesc)
    {
        $this->postMissionGoalsDesc = $postMissionGoalsDesc;

        return $this;
    }

    /**
     * Get postMissionGoalsDesc
     *
     * @return string
     */
    public function getPostMissionGoalsDesc()
    {
        return $this->postMissionGoalsDesc;
    }

    /**
     * Set submissionStatus
     *
     * @param string $submissionStatus
     *
     * @return ProjectGeneralInfo
     */
    public function setSubmissionStatus($submissionStatus)
    {
        $this->submissionStatus = $submissionStatus;

        return $this;
    }

    /**
     * Get submissionStatus
     *
     * @return string
     */
    public function getSubmissionStatus()
    {
        return $this->submissionStatus;
    }

    /**
     * Set reloRanking
     *
     * @param integer $reloRanking
     *
     * @return ProjectGeneralInfo
     */
    public function setReloRanking($reloRanking)
    {
        $this->reloRanking = $reloRanking;

        return $this;
    }

    /**
     * Get reloRanking
     *
     * @return integer
     */
    public function getReloRanking()
    {
        return $this->reloRanking;
    }

    /**
     * Set rpoRanking
     *
     * @param integer $rpoRanking
     *
     * @return ProjectGeneralInfo
     */
    public function setRpoRanking($rpoRanking)
    {
        $this->rpoRanking = $rpoRanking;

        return $this;
    }

    /**
     * Get rpoRanking
     *
     * @return integer
     */
    public function getRpoRanking()
    {
        return $this->rpoRanking;
    }

    /**
     * Set projectReviewStatus
     *
     * @param ProjectReviewStatus $projectReviewStatus
     *
     * @return ProjectGeneralInfo
     */
    public function setProjectReviewStatus(
        ProjectReviewStatus $projectReviewStatus = null
    ) {
        $this->projectReviewStatus = $projectReviewStatus;

        return $this;
    }

    /**
     * Get projectReviewStatus
     *
     * @return ProjectReviewStatus
     */
    public function getProjectReviewStatus()
    {
        return $this->projectReviewStatus;
    }

    /**
     * Set projectOutcome
     *
     * @param ProjectOutcome $projectOutcome
     *
     * @return ProjectGeneralInfo
     */
    public function setProjectOutcome(ProjectOutcome $projectOutcome = null)
    {
        $this->projectOutcome = $projectOutcome;

        return $this;
    }

    /**
     * Get projectOutcome
     *
     * @return ProjectOutcome
     */
    public function getProjectOutcome()
    {
        return $this->projectOutcome;
    }

    /**
     * Set visaAvgLength
     *
     * @param VisaAvgLength $visaAvgLength
     *
     * @return ProjectGeneralInfo
     */
    public function setVisaAvgLength(VisaAvgLength $visaAvgLength = null)
    {
        $this->visaAvgLength = $visaAvgLength;

        return $this;
    }

    /**
     * Get visaAvgLength
     *
     * @return VisaAvgLength
     */
    public function getVisaAvgLength()
    {
        return $this->visaAvgLength;
    }

    /**
     * Set region
     *
     * @param Region $region
     *
     * @return ProjectGeneralInfo
     */
    public function setRegion(Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get region acronym
     *
     * @return string
     */
    public function getRegionAcronym()
    {
        if ($this->getRegion()) {
            return $this->getRegion()->getAcronym();
        }

        return null;
    }

    /**
     * Add country
     *
     * @param Country $country
     *
     * @return ProjectGeneralInfo
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    public function getLastCountry()
    {
        if (count($this->countries)) {
            return $this->countries->last();
        }

        return null;
    }

    /**
     * Add reloLocation
     *
     * @param ReloLocation $reloLocation
     *
     * @return ProjectGeneralInfo
     */
    public function addReloLocation(ReloLocation $reloLocation)
    {
        $this->reloLocations[] = $reloLocation;

        return $this;
    }

    /**
     * Remove reloLocation
     *
     * @param ReloLocation $reloLocation
     */
    public function removeReloLocation(ReloLocation $reloLocation)
    {
        $this->reloLocations->removeElement($reloLocation);
    }

    /**
     * Get reloLocations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReloLocations()
    {
        return $this->reloLocations;
    }

    /**
     * Return the first RELO Location associated to the project.
     *
     * @return ReloLocation|null The first RELO Location in the collection.  NULL if collection is empty.
     */
    public function getFirstRELOLocation()
    {
        if (!$this->reloLocations->isEmpty()) {
            return $this->reloLocations->first();
        }

        return null;
    }

    /**
     * Add post
     *
     * @param Post $post
     *
     * @return ProjectGeneralInfo
     */
    public function addPost(Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set cycle
     *
     * @param AbstractCycle $cycle
     *
     * @return ProjectGeneralInfo
     */
    public function setCycle(AbstractCycle $cycle = null)
    {
        $this->cycle = $cycle;

        return $this;
    }

    /**
     * Get cycle
     *
     * @return AbstractCycle
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * Get cycle
     *
     * @return integer
     */
    public function getCycleId()
    {
        return $this->getCycle()->getId();
    }

    /**
     * Set fellowProject
     *
     * @param FellowProject $fellowProject
     *
     * @return ProjectGeneralInfo
     */
    public function setFellowProject(FellowProject $fellowProject = null)
    {
        $this->fellowProject = $fellowProject;

        return $this;
    }

    /**
     * Get fellowProject
     *
     * @return FellowProject
     */
    public function getFellowProject()
    {
        return $this->fellowProject;
    }

    /**
     * Set specialistProjectPhase
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return ProjectGeneralInfo
     */
    public function setSpecialistProjectPhase(
        SpecialistProjectPhase $specialistProjectPhase = null
    ) {
        $this->specialistProjectPhase = $specialistProjectPhase;

        return $this;
    }

    /**
     * Get specialistProjectPhase
     *
     * @return SpecialistProjectPhase
     */
    public function getSpecialistProjectPhase()
    {
        return $this->specialistProjectPhase;
    }

    /**
     * Add contactPoint
     *
     * @param ContactPoint $contactPoint
     *
     * @return ProjectGeneralInfo
     */
    public function addContactPoint(ContactPoint $contactPoint)
    {
        $this->contactPoints[] = $contactPoint;

        return $this;
    }

    /**
     * Remove contactPoint
     *
     * @param ContactPoint $contactPoint
     */
    public function removeContactPoint(ContactPoint $contactPoint)
    {
        $this->contactPoints->removeElement($contactPoint);
    }

    /**
     * Get contactPoints
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactPoints()
    {
        return $this->contactPoints;
    }

    /**
     * Add author
     *
     * @param User $author
     *
     * @return ProjectGeneralInfo
     */
    public function addAuthor(User $author)
    {
        $this->authors[] = $author;

        return $this;
    }

    /**
     * Remove author
     *
     * @param User $author
     */
    public function removeAuthor(User $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Get authors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return ProjectGeneralInfo
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return ProjectGeneralInfo
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set submittedBy
     *
     * @param User $submittedBy
     *
     * @return ProjectGeneralInfo
     */
    public function setSubmittedBy(User $submittedBy = null)
    {
        $this->submittedBy = $submittedBy;

        return $this;
    }

    /**
     * Get submittedBy
     *
     * @return User
     */
    public function getSubmittedBy()
    {
        return $this->submittedBy;
    }

    /**
     * Set referenceNumber
     *
     * @param string $referenceNumber
     *
     * @return FellowProject
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get referenceNumber
     *
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * Get proposal status text
     *
     * @return string
     */
    public function getProposalStatusText()
    {
        if ($this->submissionStatus === 'submitted') {
            if ($this->projectReviewStatus) {
                return $this->projectReviewStatus->getName();
            }
        } else {
            return ucwords(str_replace('_', ' ', $this->submissionStatus));
        }

        return '';
    }


    /**
     * Set submittedByRole
     *
     * @param string $submittedByRole
     *
     * @return ProjectGeneralInfo
     */
    public function setSubmittedByRole($submittedByRole)
    {
        $this->submittedByRole = $submittedByRole;

        return $this;
    }

    /**
     * Get submittedByRole
     *
     * @return string
     */
    public function getSubmittedByRole()
    {
        return $this->submittedByRole;
    }

    /**
     * Set inCountryOrientationCertification
     *
     * @param boolean $inCountryOrientationCertification
     *
     * @return ProjectGeneralInfo
     */
    public function setInCountryOrientationCertification($inCountryOrientationCertification)
    {
        $this->inCountryOrientationCertification = $inCountryOrientationCertification;

        return $this;
    }

    /**
     * Get inCountryOrientationCertification
     *
     * @return boolean
     */
    public function getInCountryOrientationCertification()
    {
        return $this->inCountryOrientationCertification;
    }

    /**
     * Set createdByRole
     *
     * @param string $createdByRole
     *
     * @return ProjectGeneralInfo
     */
    public function setCreatedByRole($createdByRole)
    {
        $this->createdByRole = $createdByRole;

        return $this;
    }

    /**
     * Get createdByRole
     *
     * @return string
     */
    public function getCreatedByRole()
    {
        return $this->createdByRole;
    }

    /**
     * Get underConsideration
     *
     * @return boolean
     */
    public function getUnderConsideration()
    {
        return $this->underConsideration;
    }
}
