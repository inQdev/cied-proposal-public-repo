<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * Class ContributionOneTime
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Entity @HasLifecycleCallbacks
 * @ORM\Table(name="contribution_one_time")
 */
class ContributionOneTime extends BaseEntity
{
    /**
     * @var SpecialistProjectPhaseBudget $phaseBudget
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhaseBudget",
     *     inversedBy="contributionOneTime"
     * )
     * @ORM\JoinColumn(
     *     name="phase_budget_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $phaseBudget;

    /**
     * @var ArrayCollection $contributionOneTimeCostItems
     *
     * @ORM\OneToMany(
     *     targetEntity="ContributionOneTimeCostItem",
     *     mappedBy="contributionOneTime",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $contributionOneTimeCostItems;

    /**
     * ContributionOneTime constructor.
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     */
    public function __construct(SpecialistProjectPhaseBudget $phaseBudget)
    {
        $this->phaseBudget = $phaseBudget;
        $this->contributionOneTimeCostItems = new ArrayCollection();
    }

    /**
     *
     */
    public function calculateSummaryTotals() {
        $summaryTotals = [
          'cost'          => 0,
          'post'          => 0,
          'hostMonetary'  => 0,
          'hostInKind'    => 0,
          'postHostTotal' => 0,
          'postTotal'     => 0,
          'ecaTotal'      => 0,
        ];

        /** @var ContributionOneTimeCostItem $costItem */
        if (!$this->contributionOneTimeCostItems->isEmpty()) {
	        foreach ($this->contributionOneTimeCostItems as $costItem) {
		        $summaryTotals['cost'] += $costItem->getTotalCost();
		        $summaryTotals['post'] += $costItem->getPostContribution();
		        $summaryTotals['hostMonetary'] += $costItem->getHostContributionMonetary();
		        $summaryTotals['hostInKind'] += $costItem->getHostContributionInKind();
		        $summaryTotals['postHostTotal'] += $costItem->getPostHostTotalContribution();
		        $summaryTotals['postTotal'] += $costItem->getPostTotalContribution();
		        $summaryTotals['ecaTotal'] += $costItem->getEcaTotalContribution();
	        }
        }

        return $summaryTotals;
    }

    /**
     * Set phaseBudget
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     *
     * @return ContributionOneTime
     */
    public function setPhaseBudget(
      SpecialistProjectPhaseBudget $phaseBudget = null
    ) {
        $this->phaseBudget = $phaseBudget;

        return $this;
    }

    /**
     * Get phaseBudget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getPhaseBudget()
    {
        return $this->phaseBudget;
    }

    /**
     * Add contributionOneTimeCostItem
     *
     * @param ContributionOneTimeCostItem $contributionOneTimeCostItem
     *
     * @return ContributionOneTime
     */
    public function addContributionOneTimeCostItem(
      ContributionOneTimeCostItem $contributionOneTimeCostItem
    ) {
        $this->contributionOneTimeCostItems[] = $contributionOneTimeCostItem;

        return $this;
    }

    /**
     * Remove contributionOneTimeCostItem
     *
     * @param ContributionOneTimeCostItem $contributionOneTimeCostItem
     */
    public function removeContributionOneTimeCostItem(
      ContributionOneTimeCostItem $contributionOneTimeCostItem
    ) {
        $this->contributionOneTimeCostItems->removeElement(
          $contributionOneTimeCostItem
        );
    }

    /**
     * Get contributionOneTimeCostItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributionOneTimeCostItems()
    {
        return $this->contributionOneTimeCostItems;
    }

}
