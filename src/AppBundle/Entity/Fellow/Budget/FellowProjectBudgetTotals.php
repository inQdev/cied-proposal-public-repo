<?php

namespace AppBundle\Entity\Fellow\Budget;

use Doctrine\ORM\Mapping as ORM;

/**
 * FellowProjectBudgetTotals
 * @package AppBundle\Entity\Fellow\Budget
 *
 * @ORM\Table(name="fellow_project_budget_totals")
 * @ORM\Entity()
 */
class FellowProjectBudgetTotals extends BaseBudgetCostItem
{

	/**
	 * @var Revision
	 *
	 * @ORM\OneToOne(targetEntity="Revision", inversedBy="fellowProjectBudgetTotals")
	 * @ORM\JoinColumn(
	 *     name="budget_revision_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budgetRevision;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=14, scale=2)
	 */
	private $global;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=14, scale=2)
	 */
	private $total;

	/********************** Methods Definitions *******************************/

	/**
	 * Set global
	 *
	 * @param string $global
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setGlobal($global)
	{
		$this->global = $global;

		return $this;
	}

	/**
	 * Get global
	 *
	 * @return float
	 */
	public function getGlobal()
	{
		return $this->global;
	}

	/**
	 * Set total
	 *
	 * @param string $total
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setTotal($total)
	{
		$this->total = $total;

		return $this;
	}

	/**
	 * Get total
	 *
	 * @return string
	 */
	public function getTotal()
	{
		return $this->total;
	}
}

