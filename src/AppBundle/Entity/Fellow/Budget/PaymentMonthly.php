<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\CostItemMonthly;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentOneTime
 * @package AppBundle\Entity\Fellow\Budget
 *
 * @ORM\Table(name="fellow_payment_detail_monthly")
 * @ORM\Entity()
 */
class PaymentMonthly extends BasePaymentDetail
{

	/**
	 * @var FellowProjectBudget
	 *
	 * @ORM\ManyToOne(targetEntity="FellowProjectBudget", inversedBy="paymentsMonthly")
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budget;

	/**
	 * @var CostItemMonthly
	 *
	 * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\CostItemMonthly")
	 */
	protected $costItem;

	/*************************** Methods ********************************/

	/**
	 * constructor.
	 * @param Payment
	 * @param CostItemMonthly
	 */
	public function __construct($payment, $costItem)
	{
		parent::__construct($payment);

		$this->costItem = $costItem;
	}


}

