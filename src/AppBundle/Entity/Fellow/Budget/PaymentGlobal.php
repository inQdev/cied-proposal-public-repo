<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\GlobalValueFellow;
use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentOneTime
 * @package AppBundle\Entity\Fellow\Budget
 *
 * @ORM\Table(name="fellow_payment_detail_global_value")
 * @ORM\Entity()
 */
class PaymentGlobal extends BasePaymentDetail
{

	/**
	 * @var FellowProjectBudget
	 *
	 * @ORM\ManyToOne(targetEntity="FellowProjectBudget", inversedBy="paymentsGlobal")
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budget;

	/**
	 * @var GlobalValueFellow $globalValue
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GlobalValueFellow")
	 *
	 */
	protected $globalValue;

	/******************************** Methods ****************************/

	/**
	 * constructor.
	 * @param Payment
	 * @param GlobalValueFellow $globalValue
	 */
	public function __construct($payment, $globalValue)
	{
		parent::__construct($payment);

		$this->globalValue = $globalValue;
	}

	/**
	 * Set globalValue
	 *
	 * @param GlobalValueFellow $globalValue
	 *
	 * @return PaymentGlobal
	 */
	public function setGlobalValue(GlobalValueFellow $globalValue)
	{
		$this->globalValue = $globalValue;

		return $this;
	}

	/**
	 * Get globalValue
	 *
	 * @return GlobalValueFellow
	 */
	public function getGlobalValue()
	{
		return $this->globalValue;
	}



}

