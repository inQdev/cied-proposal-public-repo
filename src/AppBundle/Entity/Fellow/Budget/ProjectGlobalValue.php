<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\GlobalValueFellow;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ProjectGlobalValue
 *
 * @ORM\Table(name="project_global_value")
 * @ORM\Entity()
 */
class ProjectGlobalValue extends BaseBudgetCostItem
{
	/**
	 * @var GlobalValueFellow $globalValue
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GlobalValueFellow")
	 *
	 */
	protected $globalValue;

	/**
	 * @var Revision
	 *
	 * @ORM\ManyToOne(targetEntity="Revision", inversedBy="projectGlobalValues")
	 * @ORM\JoinColumn(
	 *     name="budget_revision_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budgetRevision;

	/**
	 * @var string
	 * NOTE - 20161223: Changing $actualValue to $full_cost and overriding DB field settings
	 *
	 * @ORM\Column(name="value", type="decimal", precision=14, scale=2)
	 */
	protected $full_cost;

	/************************** Methods Definitions ***************************/

	/**
	 * ProjectGlobalValue constructor.
	 * @param Revision $budgetRevision
	 * @param GlobalValueFellow $globalValue
	 * @param float $value
	 */
	public function __construct($budgetRevision, $globalValue, $value)
	{
		parent::__construct($budgetRevision);

		$this->globalValue = $globalValue;
		$this->full_cost = $value;
	}

	/**
	 * Set globalValue
	 *
	 * @param GlobalValueFellow $globalValue
	 *
	 * @return ProjectGlobalValue
	 */
	public function setGlobalValue(GlobalValueFellow $globalValue = null)
	{
		$this->globalValue = $globalValue;

		return $this;
	}

	/**
	 * Get globalValue
	 *
	 * @return GlobalValueFellow
	 */
	public function getGlobalValue()
	{
		return $this->globalValue;
	}

	/**
	 * Set value
	 * Function used to keep compatibility with old implementation, does the same as setFullCost()
	 *
	 * @param string $value
	 *
	 * @return ProjectGlobalValue
	 */
	public function setActualValue($value)
	{
		$this->full_cost = $value;

		return $this;
	}

	/**
	 * Get value
	 * Function used to keep compatibility with old implementation, does the same as getFullCost()
	 *
	 * @return float
	 */
	public function getActualValue()
	{
		return $this->full_cost;
	}
}

