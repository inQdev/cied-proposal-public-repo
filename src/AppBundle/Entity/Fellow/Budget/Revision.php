<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\CostItemMonthly;
use AppBundle\Entity\Specialist\Budget\BudgetTotal;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This entity keeps the Explanations for budget changes (on submitted proposals)
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

/**
 * Revision
 *
 * @ORM\Table(name="fellow_project_budget_revision")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Fellow\Budget\RevisionRepository")
 *
 */
class Revision extends BaseEntity
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="revision", type="integer")
	 */
	protected $revision;

	/**
	 * @var FellowProjectBudget
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="FellowProjectBudget",
	 *     inversedBy="revisions"
	 * )
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budget;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="change_desc", type="text")
	 * @Assert\NotBlank()
	 */
	private $changeDesc;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
	 * @ORM\JoinColumn(
	 *     name="user_id",
	 *     referencedColumnName="id",
	 *     onDelete="SET NULL"
	 * )
	 */
	private $user;

	/**
	 * Cascade deletion defined in FellowProjectBudgetCostItem
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *    targetEntity = "FellowProjectBudgetCostItem",
	 *    mappedBy = "budgetRevision",
	 *    cascade = {"persist"}
	 * )
	 */
	protected $budgetCostItems;

	/**
	 * @var ProjectGlobalValue
	 *
	 * @ORM\OneToMany(
	 *    targetEntity = "ProjectGlobalValue",
	 *    mappedBy = "budgetRevision",
	 *    cascade = {"persist"}
	 * )
	 */
	protected $projectGlobalValues;

	/**
	 * @var FellowProjectBudgetTotals
	 *
	 * @ORM\OneToOne (
	 *     targetEntity = "FellowProjectBudgetTotals",
	 *     mappedBy = "budgetRevision",
	 *     cascade = {"persist"}
	 * )
	 */
	protected $fellowProjectBudgetTotals;

	/**
	 * @var BudgetOneTimeTotals
	 *
	 * @ORM\OneToOne(
	 *    targetEntity = "BudgetOneTimeTotals",
	 *    mappedBy = "budgetRevision",
	 *    cascade = {"persist"}
	 * )
	 */
	protected $budgetOneTimeTotals;

	/************************************* Methods **************************************/

	/**
	 * Constructor
	 */
	public function __construct($budget, $revision=1)
	{
		$this->budget = $budget;
		$this->revision = $revision;
		$this->budgetCostItems = new ArrayCollection();
		$this->projectGlobalValues = new ArrayCollection();
	}

//	function __clone()
//	{
//		// TODO: Implement __clone() method.
//		$this->revision = null;
//		$this->id = null;
//		$this->changeDesc = '';
//		foreach($this->budgetCostItems as )
//
//	}

	/**
	 * Set revisionId
	 *
	 * @param integer $revision
	 *
	 * @return Revision
	 */
	public function setRevision($revision)
	{
		$this->revision = $revision;

		return $this;
	}

	/**
	 * Get revision
	 *
	 * @return int
	 */
	public function getRevision()
	{
		return $this->revision;
	}

	/**
	 * Set budget
	 *
	 * @param FellowProjectBudget $budget
	 *
	 * @return Revision
	 */
	public function setBudget(FellowProjectBudget $budget)
	{
		$this->budget = $budget;

		return $this;
	}

	/**
	 * Get budget
	 *
	 * @return FellowProjectBudget
	 */
	public function getBudget()
	{
		return $this->budget;
	}

	/**
	 * Set changeDesc
	 *
	 * @param string $changeDesc
	 *
	 * @return Revision
	 */
	public function setChangeDesc($changeDesc)
	{
		$this->changeDesc = $changeDesc;

		return $this;
	}

	/**
	 * Get changeDesc
	 *
	 * @return string
	 */
	public function getChangeDesc()
	{
		return $this->changeDesc;
	}

	/**
	 * Set user
	 *
	 * @param User $user
	 *
	 * @return Revision
	 */
	public function setUser(User $user = null)
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user
	 *
	 * @return \AppBundle\Entity\User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * Add budgetCostItem
	 *
	 * @param \AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem $budgetCostItem
	 *
	 * @return Revision
	 */
	public function addBudgetCostItem(\AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem $budgetCostItem)
	{
		$this->budgetCostItems[] = $budgetCostItem;

		return $this;
	}

	/**
	 * Remove budgetCostItem
	 *
	 * @param \AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem $budgetCostItem
	 */
	public function removeBudgetCostItem(\AppBundle\Entity\Fellow\Budget\FellowProjectBudgetCostItem $budgetCostItem)
	{
		$this->budgetCostItems->removeElement($budgetCostItem);
	}

	/**
	 * Get budgetCostItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getBudgetCostItems()
	{
		return $this->budgetCostItems;
	}

	/**
	 * Add projectGlobalValue
	 *
	 * @param \AppBundle\Entity\Fellow\Budget\ProjectGlobalValue $projectGlobalValue
	 *
	 * @return Revision
	 */
	public function addProjectGlobalValue(\AppBundle\Entity\Fellow\Budget\ProjectGlobalValue $projectGlobalValue)
	{
		$this->projectGlobalValues[] = $projectGlobalValue;

		return $this;
	}

	/**
	 * Remove projectGlobalValue
	 *
	 * @param \AppBundle\Entity\Fellow\Budget\ProjectGlobalValue $projectGlobalValue
	 */
	public function removeProjectGlobalValue(\AppBundle\Entity\Fellow\Budget\ProjectGlobalValue $projectGlobalValue)
	{
		$this->projectGlobalValues->removeElement($projectGlobalValue);
	}

	/**
	 * Get projectGlobalValues
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProjectGlobalValues()
	{
		return $this->projectGlobalValues;
	}

	/**
	 * Set fellowProjectBudgetTotals
	 *
	 * @param \AppBundle\Entity\Fellow\Budget\FellowProjectBudgetTotals $fellowProjectBudgetTotals
	 *
	 * @return Revision
	 */
	public function setFellowProjectBudgetTotals(\AppBundle\Entity\Fellow\Budget\FellowProjectBudgetTotals $fellowProjectBudgetTotals = null)
	{
		$this->fellowProjectBudgetTotals = $fellowProjectBudgetTotals;

		return $this;
	}

	/**
	 * Get fellowProjectBudgetTotals
	 *
	 * @return \AppBundle\Entity\Fellow\Budget\FellowProjectBudgetTotals
	 */
	public function getFellowProjectBudgetTotals()
	{
		return $this->fellowProjectBudgetTotals;
	}

	/**
	 * Set budgetOneTimeTotals
	 *
	 * @param \AppBundle\Entity\Fellow\Budget\BudgetOneTimeTotals $budgetOneTimeTotals
	 *
	 * @return Revision
	 */
	public function setBudgetOneTimeTotals(\AppBundle\Entity\Fellow\Budget\BudgetOneTimeTotals $budgetOneTimeTotals = null)
	{
		$this->budgetOneTimeTotals = $budgetOneTimeTotals;

		return $this;
	}

	/**
	 * Get budgetOneTimeTotals
	 *
	 * @return \AppBundle\Entity\Fellow\Budget\BudgetOneTimeTotals
	 */
	public function getBudgetOneTimeTotals()
	{
		return $this->budgetOneTimeTotals;
	}

	/**
	 * Return budget ECA total contribution.
	 *
	 * @return float ECA total contribution.
	 */
	public function getECATotalContribution()
	{
		/** @var FellowProjectBudgetTotals $budgetTotals */
		$budgetTotals = $this->fellowProjectBudgetTotals;

		return $budgetTotals->getEcaTotalContribution();
	}

	/**
	 * Return the total cost of the Fellow Project.
	 *
	 * @return float Fellow Project total cost.
	 */
	public function getTotalCost()
	{
		/** @var FellowProjectBudgetTotals $budgetTotals */
		$budgetTotals = $this->fellowProjectBudgetTotals;

		return $budgetTotals->getTotal();
	}


	/**
	 * Calculates the totals that will be shown in the summary tables
	 *
	 * @return array $totals
	 */
	public function getSummaryTotals() {
		// Calculates totals (values that are not in the DB table)
		$totals = array(
			'monthly' => array(
				'fellowship' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'onetime' => array(
				'fellowship' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'shared' => 0,
		);

		foreach ($this->getBudgetCostItems() as $item) {
			if ($item->getCostItem() instanceof CostItemMonthly) { //monthly

				$totals['monthly']['fellowship'] += $item->getFullCost();
				$totals['monthly']['post'] += $item->getPostContribution();
				$totals['monthly']['host_monetary'] += $item->getHostContributionMonetary();
				$totals['monthly']['host_kind'] += $item->getHostContributionInKind();
				$totals['monthly']['post_total'] += $item->getPostTotalContribution();
				$totals['monthly']['eca'] += $item->getECATotalContribution();

			} else { // one time cost

				$totals['onetime']['fellowship'] += $item->getFullCost();
				$totals['onetime']['post'] += $item->getPostContribution();
				$totals['onetime']['host_monetary'] += $item->getHostContributionMonetary();
				$totals['onetime']['host_kind'] += $item->getHostContributionInKind();
				$totals['onetime']['post_total'] += $item->getPostTotalContribution();
				$totals['onetime']['eca'] += $item->getECATotalContribution();

			}
		}

		/** @var BudgetTotal $fellowProjectBudgetTotals */
		$fellowProjectBudgetTotals = $this->getFellowProjectBudgetTotals();

		if ($this->budget->getFundedBy() == 'ECA') {
			$totals['shared'] = (float)$fellowProjectBudgetTotals->getHostContributionMonetary() + $fellowProjectBudgetTotals->getPostContribution() + $fellowProjectBudgetTotals->getHostContributionInKind();
		} else {
			$totals['shared'] = $fellowProjectBudgetTotals->getTotal();
		}

		return $totals;
	}


	/**
	 * Calculates and saves totals (Summary) to avoid some recalculations and to expedite searches
	 * @param EntityManager $em
	 * @param string $funding: ECA|Post|Admin|null
	 * @param boolean $save - to control if the settings must be saved
	 */
	public function setSummaryTotals($em, $funding=null, $save=false) {

		$budgetTotals = $this->getFellowProjectBudgetTotals();

		if (!$budgetTotals) {
			$budgetTotals = new FellowProjectBudgetTotals($this);
			$this->setFellowProjectBudgetTotals($budgetTotals);
			$em->persist($budgetTotals); // if a new budget was created
		}

		$totals = [
			'mo' => $em->getRepository(Revision::class)->getMonthlySum($this),
			'global' => 0,
		];

		if (!$funding) {
			$funding = $this->budget->getFundedBy();
		}

		// Updates OneTimeTotals
		/** @var BudgetOneTimeTotals $oneTimeTotals */
		$oneTimeTotals = $this->setSummaryOneTimeTotals($em, $funding);

		// calculates the global values total
		// TODO: I wonder if summary requires changes due to changes in project globals
		/** @var ProjectGlobalValue $projectGlobalValue */
		foreach ($this->getProjectGlobalValues() as $projectGlobalValue) {
			$totals['global'] += $projectGlobalValue->getActualValue();
		}
		$budgetTotals->setGlobal($totals['global']);

		$budgetTotals->setPostContribution($totals['mo']['s_pc'] + $oneTimeTotals->getPostContribution());
		$budgetTotals->setPostContributionGU($totals['mo']['s_pcgu'] + $oneTimeTotals->getPostContributionGU());
		$budgetTotals->setPostContributionNonGU($totals['mo']['s_pcngu'] + $oneTimeTotals->getPostContributionNonGU());
		$budgetTotals->setPostTotalContribution($totals['mo']['s_ptc'] + $oneTimeTotals->getPostTotalContribution());
		$budgetTotals->setHostContributionMonetary($totals['mo']['s_hc'] + $oneTimeTotals->getHostContributionMonetary());
		$budgetTotals->setHostContributionGU($totals['mo']['s_hcgu'] + $oneTimeTotals->getHostContributionGU());
		$budgetTotals->setHostContributionNonGU($totals['mo']['s_hcngu'] + $oneTimeTotals->getHostContributionNonGU());
		$budgetTotals->setHostContributionInKind($totals['mo']['s_hcik'] + $oneTimeTotals->getHostContributionInKind());

		// Full cost includes the costs (monetary) and in-kind contributions by host
		$budgetTotals->setFullCost($totals['mo']['s_fc'] + $oneTimeTotals->getFullCost() + $budgetTotals->getHostContributionInKind());
		$budgetTotals->setTotal($budgetTotals->getFullCost() + $budgetTotals->getGlobal());

		if ($funding != 'Post') {
			$budgetTotals->setPostHostTotalContribution($budgetTotals->getPostContribution() + $budgetTotals->getHostContributionMonetary());

			// TODO: refactor total ECA since globals can be split among ECA, Posts, Host
			$budgetTotals->setEcaTotalContribution($totals['mo']['s_eca'] + $oneTimeTotals->getEcaTotalContribution() + $totals['global']);
		} else {
			$budgetTotals->setPostHostTotalContribution($budgetTotals->getHostContributionMonetary());
			$budgetTotals->setEcaTotalContribution(0);

			// TODO: refactor total ECA since globals can be split among ECA, Posts, Host
			$budgetTotals->setPostTotalContribution($budgetTotals->getPostTotalContribution() + $totals['global']);
		}

		if ($save) {
			$em->flush();
		}

		return;
//		return $totals;
	}

	/**
	 * @param EntityManager $em
	 * @param null $funding
	 * @return object
	 */
	public function setSummaryOneTimeTotals($em, $funding=null) {

		$oneTimeTotals = $this->getBudgetOneTimeTotals();

		if (!$oneTimeTotals) {
			$oneTimeTotals = new BudgetOneTimeTotals($this);
			$this->setBudgetOneTimeTotals($oneTimeTotals);
			$em->persist($oneTimeTotals);
		}

		$totals = $em->getRepository(Revision::class)->getOneTimeSum($this);

		if (!$funding) {
			$funding = $this->budget->getFundedBy();
		}

		// Updates OneTimeTotals
		$oneTimeTotals->setFullCost($totals['s_fc']);
		$oneTimeTotals->setPostContribution($totals['s_pc']);
		$oneTimeTotals->setHostContributionMonetary($totals['s_hc']);
		$oneTimeTotals->setHostContributionInKind($totals['s_hcik']); // or should be 0
		// Updates oneTimeTotals Row (dependent calculations)
		$oneTimeTotals->updateRow($funding);

		return $oneTimeTotals;
	}

	/**
	 * triggers updateRow in BudgetCostItems associated to revision
	 * @param null $funding
	 */
	public function updateCostRows($funding=null) {

		if (!$funding) {
			$funding = $this->budget->getFundedBy();
		}

		/** @var FellowProjectBudgetCostItem $item */
		foreach ($this->budgetCostItems as $item) {
			$item->updateRow($funding);
			// Adds default description when needed
			if ($item->getCostItem()->getDescription() == '') {
				$item->getCostItem()->setDescription('Untitled');
			}
		}

	}

	/**
	 * Triggers update of Global Values associated to revision
	 * @param null $funding
	 */
	public function updateGlobalRows($funding=null) {

		if (!$funding) {
			$funding = $this->budget->getFundedBy();
		}

		/** @var ProjectGlobalValue $item */
		foreach ($this->projectGlobalValues as $item) {
			$item->updateRow($funding);
		}

	}


	/**
	 * This function retrieves the budgetCostItems applying a filter
	 * NOTE - Felipe - 20170112: Actually not in use but kept in case we could take advantage of it
	 * Example of usage in function getMonthlyCosts
	 *
	 * @param $costItems
	 * @return mixed
	 */
    protected function getBudgetCostsFiltered($costItems) {

    	$criteria = Criteria::create();
    	$criteria->where(Criteria::expr()->in('costItem', $costItems));

	    return $this->budgetCostItems->matching($criteria);
    }

	/**
	 * NOTE - Felipe - 20170112: Actually not in use but kept in case we could take advantage of it
	 * @param EntityManager $em
	 * @return mixed
	 */
    public function getMonthlyCosts(EntityManager $em) {

    	$monthlyItems = $em->getRepository(CostItemMonthly::class)->findAll();
    	return $this->getBudgetCostsFiltered($monthlyItems);
    }

}
