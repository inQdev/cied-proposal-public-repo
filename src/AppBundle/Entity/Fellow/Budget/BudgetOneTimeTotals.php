<?php

namespace AppBundle\Entity\Fellow\Budget;

use Doctrine\ORM\Mapping as ORM;

/**
 * BudgetOneTimeTotals
 * @package AppBundle\Entity\Fellow\Budget
 *
 * @ORM\Table(name="fellow_project_budget_onetime_totals")
 * @ORM\Entity()
 */
class BudgetOneTimeTotals extends BaseBudgetCostItem
{

	/**
	 * @var Revision
	 *
	 * @ORM\OneToOne(targetEntity="Revision", inversedBy="budgetOneTimeTotals")
	 * @ORM\JoinColumn(
	 *     name="budget_revision_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budgetRevision;

	/********************************** Methods *************************************/

}

