<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class BaseBudgetCostItem mapped super class
 *
 * @package AppBundle\Entity\Fellow\Budget
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\MappedSuperclass()
 */


class BasePaymentDetail extends BaseEntity
{
	// This is overridden in children but is set here to define the getter and setter
	protected $payment;

	/**
	 * @var float
	 * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
	 */
	protected $post;

	/**
	 * @var float
	 * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
	 */
	protected $host;

	/**
	 * @var float
	 * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
	 */
	protected $eca;

	/**
	 * @var float
	 * @ORM\Column(type="decimal", precision=14, scale=2, nullable=true)
	 */
	protected $total;

	/************************ Methods *****************************/

	/**
	 * constructor.
	 * @param Payment
	 */
	public function __construct($payment)
	{
		$this->payment = $payment;
	}

	/**
	 * Set payment
	 * @param $payment
	 * @return BasePaymentDetail
	 */
	public function setPayment($payment) {
		$this->payment = $payment;

		return $this;
	}

	/**
	 * Get payment
	 * @return object
	 */
	public function getPayment() {
		return $this->payment;
	}

	/**
	 * Set post
	 * @param $post
	 * @return BasePaymentDetail
	 */
	public function setPost($post) {
		$this->post = $post;

		return $this;
	}

	/**
	 * Get post
	 * @return float
	 */
	public function getPost() {
		return $this->post;
	}

	/**
	 * Set host
	 * @param $host
	 * @return BasePaymentDetail
	 */
	public function setHost($host) {
		$this->host = $host;

		return $this;
	}

	/**
	 * Get host
	 * @return float
	 */
	public function getHost() {
		return $this->host;
	}

	/**
	 * Set eca
	 * @param $eca
	 * @return BasePaymentDetail
	 */
	public function setEca($eca) {
		$this->eca = $eca;

		return $this;
	}

	/**
	 * Get eca
	 * @return float
	 */
	public function getEca() {
		return $this->eca;
	}

	/**
	 * Set total
	 * @param $total
	 * @return BasePaymentDetail
	 */
	public function setTotal($total) {
		$this->total = $total;

		return $this;
	}

	/**
	 * Get total
	 * @return float
	 */
	public function getTotal() {
		return $this->total;
	}


	/**
	 * Calculates total from the individual contributions
	 */
	public function sumRow() {
		$this->total = $this->post + $this->host + $this->eca;
	}

}