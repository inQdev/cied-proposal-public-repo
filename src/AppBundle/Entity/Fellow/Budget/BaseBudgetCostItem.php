<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class BaseBudgetCostItem mapped super class
 *
 * @package AppBundle\Entity\Fellow\Budget
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\MappedSuperclass()
 */
class BaseBudgetCostItem extends BaseEntity
{
	/**
	 *
	 */
	const ROW_ERROR_NO_COST = 0;

	/**
	 *
	 */
	const ROW_ERROR_NEGATIVE_TOTAL = -1;

	/**
	 *
	 */
	const ROW_COSTS_OK = 1;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $full_cost;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_contribution;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $host_contribution_monetary;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $host_contribution_in_kind;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_host_total_contribution;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_total_contribution;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $eca_total_contribution;

	/**
	 * @var float
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_contribution_gu;

	/**
	 * @var float
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_contribution_non_gu;

	/**
	 * @var float
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $host_contribution_gu;

	/**
	 * @var float
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $host_contribution_non_gu;

	/**
	 * @var Revision
	 * declaration for field changes on subclasses
	 */
	protected $budgetRevision;

	/***************************** Methods definitions *******************************/

	/**
	 * Constructor. - may be overridden  in subclasses
	 * @param Revision $budgetRevision
	 */
	public function __construct($budgetRevision)
	{
		$this->budgetRevision = $budgetRevision;
	}

	/**
	 * Executed everytime the item is cloned, resets some values that must be assigned either manually or
	 * automatically by the framework
	 */
	public function __clone()
	{
		$this->id = null;
		$this->budgetRevision = null;
		$this->createdAt = null;
		$this->updatedAt = null;
	}

	/**
	 * Set budgetRevision
	 *
	 * @param Revision
	 *
	 * @return object
	 */
	public function setBudgetRevision(Revision $budgetRevision = null)
	{
		$this->budgetRevision = $budgetRevision;

		return $this;
	}

	/**
	 * Get budgetRevision
	 *
	 * @return Revision
	 */
	public function getBudgetRevision()
	{
		return $this->budgetRevision;
	}

	/**
	 * Set full_cost
	 *
	 * @param string $full_cost
	 *
	 * @return object
	 */
	public function setFullCost($full_cost)
	{
		$this->full_cost = $full_cost;

		return $this;
	}

	/**
	 * Get full_cost
	 *
	 * @return float
	 */
	public function getFullCost()
	{
		return $this->full_cost;
	}

	/**
	 * Set postContribution
	 *
	 * @param string $postContribution
	 *
	 * @return object
	 */
	public function setPostContribution($postContribution)
	{
		$this->post_contribution = $postContribution;

		return $this;
	}

	/**
	 * Get postContribution
	 *
	 * @return float
	 */
	public function getPostContribution()
	{
		return $this->post_contribution;
	}

	/**
	 * Set hostContributionMonetary
	 *
	 * @param string $hostContributionMonetary
	 *
	 * @return object
	 */
	public function setHostContributionMonetary($hostContributionMonetary)
	{
		$this->host_contribution_monetary = $hostContributionMonetary;

		return $this;
	}

	/**
	 * Get hostContributionMonetary
	 *
	 * @return float
	 */
	public function getHostContributionMonetary()
	{
		return $this->host_contribution_monetary;
	}

	/**
	 * Set hostContributionInKind
	 *
	 * @param string $hostContributionInKind
	 *
	 * @return object
	 */
	public function setHostContributionInKind($hostContributionInKind)
	{
		$this->host_contribution_in_kind = $hostContributionInKind;

		return $this;
	}

	/**
	 * Get hostContributionInKind
	 *
	 * @return float
	 */
	public function getHostContributionInKind()
	{
		return $this->host_contribution_in_kind;
	}

	/**
	 * Set postHostTotalContribution
	 *
	 * @param string $postHostTotalContribution
	 *
	 * @return object
	 */
	public function setPostHostTotalContribution($postHostTotalContribution)
	{
		$this->post_host_total_contribution = $postHostTotalContribution;

		return $this;
	}

	/**
	 * Get postHostTotalContribution
	 *
	 * @return float
	 */
	public function getPostHostTotalContribution()
	{
		return $this->post_host_total_contribution;
	}

	/**
	 * Set postTotalContribution
	 *
	 * @param string $postTotalContribution
	 *
	 * @return object
	 */
	public function setPostTotalContribution($postTotalContribution)
	{
		$this->post_total_contribution = $postTotalContribution;

		return $this;
	}

	/**
	 * Get postTotalContribution
	 *
	 * @return float
	 */
	public function getPostTotalContribution()
	{
		return $this->post_total_contribution;
	}

	/**
	 * Set ecaTotalContribution
	 *
	 * @param string $ecaTotalContribution
	 *
	 * @return object
	 */
	public function setEcaTotalContribution($ecaTotalContribution)
	{
		$this->eca_total_contribution = $ecaTotalContribution;

		return $this;
	}

	/**
	 * Get ecaTotalContribution
	 *
	 * @return float
	 */
	public function getEcaTotalContribution()
	{
		return $this->eca_total_contribution;
	}

	/**
	 * Set PostContributionGU
	 *
	 * @param string
	 *
	 * @return object
	 */
	public function setPostContributionGU($value)
	{
		$this->post_contribution_gu = $value;

		return $this;
	}

	/**
	 * Get PostContributionGU
	 *
	 * @return float
	 */
	public function getPostContributionGU()
	{
		return $this->post_contribution_gu;
	}

	/**
	 * Set PostContributionNonGU
	 *
	 * @param string
	 *
	 * @return object
	 */
	public function setPostContributionNonGU($value)
	{
		$this->post_contribution_non_gu = $value;

		return $this;
	}

	/**
	 * Get PostContributionNonGU
	 *
	 * @return float
	 */
	public function getPostContributionNonGU()
	{
		return $this->post_contribution_non_gu;
	}

	/**
	 * Set HostContributionGU
	 *
	 * @param string
	 *
	 * @return object
	 */
	public function setHostContributionGU($value)
	{
		$this->host_contribution_gu = $value;

		return $this;
	}

	/**
	 * Get HostContributionGU
	 *
	 * @return float
	 */
	public function getHostContributionGU()
	{
		return $this->host_contribution_gu;
	}

	/**
	 * Set HostContributionNonGU
	 *
	 * @param string
	 *
	 * @return object
	 */
	public function setHostContributionNonGU($value)
	{
		$this->host_contribution_non_gu = $value;

		return $this;
	}

	/**
	 * Get HostContributionNonGU
	 *
	 * @return float
	 */
	public function getHostContributionNonGU()
	{
		return $this->host_contribution_non_gu;
	}

	/**
	 * TODO: refactor usage and remove
	 * Alias of updateRow
	 */
	public function calculateTotals($funding=null) {
		$this->updateRow($funding);
	}

	/**
	 * NOTE: 20170112: Changed for better readability
	 * Updates the values in a row
	 * @param string - optional
	 */
	public function updateRow($funding=null)
	{
		if (!$funding) {
			$funding = $this->budgetRevision->getBudget()->getFundedBy();
		}

		if (!$this->post_contribution) {
			$this->post_contribution = 0;
		}

		if (!$this->post_contribution_gu) {
			$this->post_contribution_gu = 0;
		}

		if (!$this->host_contribution_monetary) {
			$this->host_contribution_monetary = 0;
		}

		if (!$this->host_contribution_gu) {
			$this->host_contribution_gu = 0;
		}

		if (!$this->host_contribution_in_kind) {
			$this->host_contribution_in_kind = 0;
		}

		$this->post_contribution_non_gu = $this->post_contribution - $this->post_contribution_gu;
		$this->host_contribution_non_gu = $this->host_contribution_monetary - $this->host_contribution_gu;

		if ($funding != 'Post') { // ECA funded, not defined or from Admin

			$this->post_host_total_contribution = $this->post_contribution + $this->host_contribution_monetary;
			$this->post_total_contribution = $this->post_contribution;

			if ($this->full_cost) {
				$this->eca_total_contribution = $this->full_cost - $this->post_host_total_contribution;
			} else {
				$this->eca_total_contribution = 0;
			}
		} else { // Post funded

			$this->post_host_total_contribution = $this->host_contribution_monetary;

			if ($this->full_cost) {
				$this->post_total_contribution = $this->full_cost - $this->post_host_total_contribution;
			} else {
				$this->post_total_contribution = 0;
			}
			$this->post_contribution = $this->post_total_contribution;
			$this->eca_total_contribution = 0;

		}

//		return $output;
	} // End calculateTotals

	/**
	 * @return int
	 */
	public function checkTotals() {
		$output = self::ROW_COSTS_OK;

		// checks error condition (independent of the funding type)
		if ($this->full_cost == 0 || $this->full_cost == '') {
			if ($this->post_host_total_contribution > 0) {
				$output = self::ROW_ERROR_NO_COST;
			}
		} else {
			if ($this->budgetRevision->getBudget()->getFundedBy() == 'ECA') { // ECA funded
				if ($this->eca_total_contribution < 0) {
					$output = self::ROW_ERROR_NEGATIVE_TOTAL;
				}
			} else {
				if ($this->post_total_contribution < 0) {
					$output = self::ROW_ERROR_NEGATIVE_TOTAL;
				}
			}

		}

		return $output;
	}

}

