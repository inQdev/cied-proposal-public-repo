<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\WizardStepStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class FellowProjectBudget
 *
 * @package AppBundle\Entity\Fellow\Budget
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FellowProjectBudgetRepository")
 * @ORM\Table(name="fellow_project_budget")
 */
class FellowProjectBudget extends BaseEntity {

	/**
	 * @var string $funded_by
	 *
	 * @ORM\Column(type="string")
	 */
	protected $funded_by;

	/**
	 * @var string $postFundingSource
	 *
	 * @ORM\Column(
	 *     type="string",
	 *     length=4294967295,
	 *     nullable=true
	 * )
	 */
	protected $postFundingSource;

	/**
	 * @var \AppBundle\Entity\FellowProject $fellowProject
	 *
	 * @ORM\OneToOne(targetEntity="\AppBundle\Entity\FellowProject",
	 *     inversedBy="fellowProjectBudget"
	 * )
	 * @ORM\JoinColumn(
	 *     name="fellow_project_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $fellowProject;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *	targetEntity = "Revision",
	 *	mappedBy = "budget",
	 *	cascade = {"persist"}
	 * )
	 * @ORM\OrderBy({"revision" = "DESC", "id" = "ASC"})
	 */
	protected $revisions;

	
//	protected $budgetCostItemsCustom;

	/************************ Methods ********************************/
	/**
	 * Constructor
	 * @param FellowProject
	 */
	public function __construct($fellowProject) {
		$this->fellowProject = $fellowProject;
		$this->revisions = new ArrayCollection();
	}

	/**
	 * Set fellowProject
	 *
	 * @param FellowProject $fellowProject
	 *
	 * @return FellowProjectBudget
	 */
	public function setFellowProject(FellowProject $fellowProject = null) {
		$this->fellowProject = $fellowProject;

		return $this;
	}

	/**
	 * Get fellowProject
	 *
	 * @return \AppBundle\Entity\FellowProject
	 */
	public function getFellowProject() {
		return $this->fellowProject;
	}

	/**
	 * Set fundedBy
	 *
	 * @param string $fundedBy
	 *
	 * @return FellowProjectBudget
	 */
	public function setFundedBy($fundedBy) {
		$this->funded_by = $fundedBy;

		return $this;
	}

	/**
	 * Get fundedBy
	 *
	 * @return string
	 */
	public function getFundedBy() {
		return $this->funded_by;
	}

	/**
	 * Set postFundingSource
	 *
	 * @param string $postFundingSource
	 *
	 * @return FellowProjectBudget
	 */
	public function setPostFundingSource($postFundingSource)
	{
		$this->postFundingSource = $postFundingSource;

		return $this;
	}

	/**
	 * Get postFundingSource
	 *
	 * @return string
	 */
	public function getPostFundingSource()
	{
		return $this->postFundingSource;
	}

	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Doctrine\ORM\EntityManager     $em
	 * @return int $status
	 */
	public function getBudgetStepStatus($fellowProject, $em)
	{
		$budgetStep = $em->getRepository(WizardStepStatus::class)
			->findStepStatusByProject($fellowProject, WizardStepStatus::BUDGET_STEP);

		return $budgetStep->getStatus();
	}

	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Doctrine\ORM\EntityManager     $em
	 * @param int $status
	 */
	public function setBudgetStepStatus($fellowProject, $em, $status)
	{
		$budgetStep = $em->getRepository(WizardStepStatus::class)
			->findStepStatusByProject($fellowProject, WizardStepStatus::BUDGET_STEP);

		$budgetStep->setStatus($status);
		$em->persist($budgetStep);
		$em->flush();

	}


//	/**
//	 * @param int null $revision
//	 * @return ArrayCollection
//	 */
//    public function getOneTimeTotalsByRevision($revision=null) {
//    	if (!$revision) {
//    		$revision = $this->revision_id;
//	    }
//
//    	$criteria = Criteria::create();
//    	$criteria->where(Criteria::expr()->eq('revision_id', $revision));
//
//    	$result = $this->budgetOneTimeTotals->matching($criteria);
//
//    	if (!$result->isEmpty()) {
//    		return $result->first();
//	    }
//
//    	return false;
//    }

    /**
     * Add revision
     *
     * @param \AppBundle\Entity\Fellow\Budget\Revision $revision
     *
     * @return FellowProjectBudget
     */
    public function addRevision(\AppBundle\Entity\Fellow\Budget\Revision $revision)
    {
        $this->revisions[] = $revision;

        return $this;
    }

    /**
     * Remove revision
     *
     * @param \AppBundle\Entity\Fellow\Budget\Revision $revision
     */
    public function removeRevision(\AppBundle\Entity\Fellow\Budget\Revision $revision)
    {
        $this->revisions->removeElement($revision);
    }

    /**
     * Get revisions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRevisions()
    {
        return $this->revisions;
    }

	/**
	 * Get the most recent revision
	 * Takes the first element of the revisions Collection, since they are already ordered
	 *
	 * @return Revision
	 */
    public function getCurrentRevision()
    {
    	return $this->revisions->first();
    }


	public function findRevision($revision)
	{
		$criteria = Criteria::create();
		$criteria->where(Criteria::expr()->eq('revision', $revision));

		$result = $this->revisions->matching($criteria);

		if (!$result->isEmpty()) {
			return $result->first();
		}

		return false;

	}
	/**
	 * Return budget ECA total contribution.
	 *
	 * @return float ECA total contribution.
	 */
	public function getECATotalContribution()
	{
		return $this->getCurrentRevision()->getEcaTotalContribution();
	}

	/**
	 * Return the total cost of the Fellow Project.
	 *
	 * @return float Fellow Project total cost.
	 */
	public function getTotalCost()
	{
		return $this->getCurrentRevision()->getTotalCost();
	}

}
