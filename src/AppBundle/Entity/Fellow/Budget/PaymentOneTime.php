<?php

namespace AppBundle\Entity\Fellow\Budget;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentOneTime
 * @package AppBundle\Entity\Fellow\Budget
 *
 * @ORM\Table(name="fellow_payment_detail_onetime")
 * @ORM\Entity()
 */
class PaymentOneTime extends BasePaymentDetail
{

	/**
	 * @var FellowProjectBudget
	 *
	 * @ORM\ManyToOne(targetEntity="FellowProjectBudget", inversedBy="paymentsOneTime")
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budget;


}

