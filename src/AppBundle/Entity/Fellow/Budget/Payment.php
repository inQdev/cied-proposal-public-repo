<?php

namespace AppBundle\Entity\Fellow\Budget;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This entity keeps payments for Fellow projects
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

/**
 * Payment
 *
 * @ORM\Table(name="fellow_payment")
 * @ORM\Entity()
 *
 */
class Payment extends BasePaymentDetail
{

	/**
	 * @var FellowProjectBudget
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="FellowProjectBudget",
	 *     inversedBy="payments"
	 * )
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budget;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
	 * @ORM\JoinColumn(
	 *     name="user_id",
	 *     referencedColumnName="id",
	 *     onDelete="SET NULL"
	 * )
	 */
	protected $user;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *    targetEntity = "PaymentMonthly",
	 *    mappedBy = "payment",
	 *    cascade = {"persist"}
	 * )
	 */
	protected $paymentsMonthly;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *    targetEntity = "PaymentOneTime",
	 *    mappedBy = "payment",
	 *    cascade = {"persist"}
	 * )
	 */
	protected $paymentsOneTime;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany (
	 *     targetEntity = "PaymentGlobal",
	 *     mappedBy = "payment",
	 *     cascade = {"persist"}
	 * )
	 */
	protected $paymentsGlobal;


	/************************************* Methods **************************************/

	/**
	 * Constructor
	 */
	public function __construct($budget)
	{
		$this->budget = $budget;
		$this->paymentsMonthly = new ArrayCollection();
		$this->paymentsOneTime = new ArrayCollection();
		$this->paymentsGlobal = new ArrayCollection();
	}

	/**
	 * Set budget
	 *
	 * @param FellowProjectBudget $budget
	 *
	 * @return Payment
	 */
	public function setBudget(FellowProjectBudget $budget)
	{
		$this->budget = $budget;

		return $this;
	}

	/**
	 * Get budget
	 *
	 * @return FellowProjectBudget
	 */
	public function getBudget()
	{
		return $this->budget;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Payment
	 */
	public function setDescription($description)
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set user
	 *
	 * @param User $user
	 *
	 * @return Payment
	 */
	public function setUser(User $user = null)
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user
	 *
	 * @return \AppBundle\Entity\User
	 */
	public function getUser()
	{
		return $this->user;
	}


}
