<?php

namespace AppBundle\Entity\Fellow;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\FellowProject;
use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractDutyActivity entity.
 *
 * @package AppBundle\Entity\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="fellowship_duty_activity")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *     "0" = "DutyActivityPrimary",
 *     "1" = "DutyActivitySecondary"
 * })
 */
abstract class AbstractDutyActivity extends BaseEntity
{
    /**
     * @var string $activityDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $description;

    /**
     * @var float $hoursNumber
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=4,
     *     scale=1,
     *     nullable=true
     * )
     */
    protected $hoursNumber;

    /**
     * @var string $audience
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $audience;

    /**
     * @var FellowProject $fellowProject
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FellowProject")
     * @ORM\JoinColumn(
     *     name="fellow_project_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $fellowProject;

    /********************** Methods Definition ***********************/

    /**
     * @param FellowProject $fellowProject
     */
    public function __construct(FellowProject $fellowProject)
    {
        $this->fellowProject = $fellowProject;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AbstractDutyActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hoursNumber
     *
     * @param string $hoursNumber
     *
     * @return AbstractDutyActivity
     */
    public function setHoursNumber($hoursNumber)
    {
        $this->hoursNumber = $hoursNumber;

        return $this;
    }

    /**
     * Get hoursNumber
     *
     * @return string
     */
    public function getHoursNumber()
    {
        return $this->hoursNumber;
    }

    /**
     * Set audience
     *
     * @param string $audience
     *
     * @return AbstractDutyActivity
     */
    public function setAudience($audience)
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * Get audience
     *
     * @return string
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * Set fellowProject
     *
     * @param FellowProject $fellowProject
     *
     * @return AbstractDutyActivity
     */
    public function setFellowProject(FellowProject $fellowProject = null)
    {
        $this->fellowProject = $fellowProject;

        return $this;
    }

    /**
     * Get fellowProject
     *
     * @return FellowProject
     */
    public function getFellowProject()
    {
        return $this->fellowProject;
    }
}
