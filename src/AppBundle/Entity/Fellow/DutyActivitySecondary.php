<?php

namespace AppBundle\Entity\Fellow;

use AppBundle\Entity\FellowProject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DutyActivitySecondary entity.
 *
 * @package AppBundle\Entity\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class DutyActivitySecondary extends AbstractDutyActivity
{
    /**
     * @var string $hoursPeriod
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Choice(callback="getHoursPeriodChoices")
     */
    protected $hoursPeriod;

    /**
     * @var ArrayCollection $activitiesFocus
     *
     * @ORM\ManyToMany(targetEntity="DutyActivitySecondaryFocus")
     * @ORM\JoinTable(name="fellowship_duties_secondary_activities_focus")
     */
    protected $activitiesFocus;

    /**
     * @var FellowProject $fellowProject
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\FellowProject",
     *     inversedBy="dutySecondaryActivities"
     * )
     * @ORM\JoinColumn(
     *     name="fellow_project_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $fellowProject;

    /************************* Methods Definitions ***************************/

    /**
     * DutyActivitySecondary constructor.
     *
     * @param FellowProject $fellowProject
     */
    public function __construct(FellowProject $fellowProject)
    {
        parent::__construct($fellowProject);

        $this->activitiesFocus = new ArrayCollection();
    }

    /**
     * @return array
     */
    public static function getHoursPeriodChoices()
    {
        return [
          'Week'     => 'week',
          'Month'    => 'month',
          'Semester' => 'semester',
        ];
    }

    /**
     * Set hoursPeriod
     *
     * @param string $hoursPeriod
     *
     * @return AbstractDutyActivity
     */
    public function setHoursPeriod($hoursPeriod)
    {
        $this->hoursPeriod = $hoursPeriod;

        return $this;
    }

    /**
     * Get hoursPeriod
     *
     * @return string
     */
    public function getHoursPeriod()
    {
        return $this->hoursPeriod;
    }

    /**
     * Add activitiesFocus
     *
     * @param DutyActivitySecondaryFocus $activitiesFocus
     *
     * @return DutyActivitySecondary
     */
    public function addActivitiesFocus(
      DutyActivitySecondaryFocus $activitiesFocus
    ) {
        $this->activitiesFocus[] = $activitiesFocus;

        return $this;
    }

    /**
     * Remove activitiesFocus
     *
     * @param DutyActivitySecondaryFocus $activitiesFocus
     */
    public function removeActivitiesFocus(
      DutyActivitySecondaryFocus $activitiesFocus
    ) {
        $this->activitiesFocus->removeElement($activitiesFocus);
    }

    /**
     * Get activitiesFocus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivitiesFocus()
    {
        return $this->activitiesFocus;
    }
}
