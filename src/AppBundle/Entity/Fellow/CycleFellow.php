<?php

namespace AppBundle\Entity\Fellow;

use AppBundle\Entity\AbstractCycle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CycleFellow entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CycleFellowRepository")
 */
class CycleFellow extends AbstractCycle
{
    /**
     * @var \DateTime $regularCyclePeriodStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $regularCyclePeriodStartDate;

    /**
     * @var \DateTime $regularCyclePeriodEndDate
     *
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    protected $regularCyclePeriodEndDate;

    /**
     * @var \DateTime $offCyclePeriodStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $offCyclePeriodStartDate;

    /**
     * @var \DateTime $offCyclePeriodEndDate
     *
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    protected $offCyclePeriodEndDate;

    /**
     * Set regularCyclePeriodStartDate
     *
     * @param \DateTime $regularCyclePeriodStartDate
     *
     * @return CycleFellow
     */
    public function setRegularCyclePeriodStartDate($regularCyclePeriodStartDate)
    {
        $this->regularCyclePeriodStartDate = $regularCyclePeriodStartDate;

        return $this;
    }

    /**
     * Get regularCyclePeriodStartDate
     *
     * @return \DateTime
     */
    public function getRegularCyclePeriodStartDate()
    {
        return $this->regularCyclePeriodStartDate;
    }

    /**
     * Set regularCyclePeriodEndDate
     *
     * @param \DateTime $regularCyclePeriodEndDate
     *
     * @return CycleFellow
     */
    public function setRegularCyclePeriodEndDate($regularCyclePeriodEndDate)
    {
        $this->regularCyclePeriodEndDate = $regularCyclePeriodEndDate;

        return $this;
    }

    /**
     * Get regularCyclePeriodEndDate
     *
     * @return \DateTime
     */
    public function getRegularCyclePeriodEndDate()
    {
        return $this->regularCyclePeriodEndDate;
    }

    /**
     * Set offCyclePeriodStartDate
     *
     * @param \DateTime $offCyclePeriodStartDate
     *
     * @return CycleFellow
     */
    public function setOffCyclePeriodStartDate($offCyclePeriodStartDate)
    {
        $this->offCyclePeriodStartDate = $offCyclePeriodStartDate;

        return $this;
    }

    /**
     * Get offCyclePeriodStartDate
     *
     * @return \DateTime
     */
    public function getOffCyclePeriodStartDate()
    {
        return $this->offCyclePeriodStartDate;
    }

    /**
     * Set offCyclePeriodEndDate
     *
     * @param \DateTime $offCyclePeriodEndDate
     *
     * @return CycleFellow
     */
    public function setOffCyclePeriodEndDate($offCyclePeriodEndDate)
    {
        $this->offCyclePeriodEndDate = $offCyclePeriodEndDate;

        return $this;
    }

    /**
     * Get offCyclePeriodEndDate
     *
     * @return \DateTime
     */
    public function getOffCyclePeriodEndDate()
    {
        return $this->offCyclePeriodEndDate;
    }
}
