<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Specialist\Itinerary\Activity\AnticipatedAudience;
use AppBundle\Entity\Specialist\PartneringOrganization;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ItineraryActivity entity.
 *
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 * @author Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItineraryActivityRepository")
 * @ORM\Table(name="itinerary_activity")
 */
class ItineraryActivity extends BaseEntity
{

	/**
	 * @var int
	 * @ORM\ManyToOne(
	 *     targetEntity="AppBundle\Entity\Specialist\Itinerary\ActivityType"
	 * )
	 */
	protected $activityType;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $activityTypeOther;

    /**
     * @var string $activityTypeDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $activityTypeDesc;

    /**
     * @var ArrayCollection $topic
     *
     * @ORM\ManyToMany(targetEntity="AreaOfExpertise", cascade={"persist"})
     * @ORM\JoinTable(name="itinerary_activities_topics")
     */
    protected $topics;

    /**
     * @var string $topicOther
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $topicOther;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=100, nullable=true)
	 */
	protected $duration;

    /**
     * @var ArrayCollection $anticipatedAudience
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Specialist\Itinerary\Activity\AnticipatedAudience",
     *     mappedBy="activity",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $anticipatedAudience;

    /**
     * @var ArrayCollection $partneringOrganizations
     *
     * @ORM\ManyToMany(
     *     targetEntity="AppBundle\Entity\Specialist\PartneringOrganization",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="itinerary_activities_partnering_organizations")
     */
    protected $partneringOrganizations;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date")
	 */
	protected $activityDate;

	/**
	 * @var InCountryAssignment $inCountryAssignment
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="InCountryAssignment",
	 *     inversedBy="assignmentActivities"
	 * )
	 *
	 * @ORM\JoinColumn(
	 *     name="in_country_assignment_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $inCountryAssignment;

	/*********************** Methods ***************************/

    /**
     * ItineraryActivity constructor.
     */
    public function __construct()
    {
        $this->topics = new ArrayCollection();
        $this->anticipatedAudience = new ArrayCollection();
        $this->partneringOrganizations = new ArrayCollection();
    }

    /**
     * Set activityTypeOther
     *
     * @param string $activityTypeOther
     *
     * @return ItineraryActivity
     */
    public function setActivityTypeOther($activityTypeOther)
    {
        $this->activityTypeOther = $activityTypeOther;

        return $this;
    }

    /**
     * Get activityTypeOther
     *
     * @return string
     */
    public function getActivityTypeOther()
    {
        return $this->activityTypeOther;
    }

    /**
     * Set activityTypeDesc
     *
     * @param string $activityTypeDesc
     *
     * @return ItineraryActivity
     */
    public function setActivityTypeDesc($activityTypeDesc)
    {
        $this->activityTypeDesc = $activityTypeDesc;

        return $this;
    }

    /**
     * Get activityTypeDesc
     *
     * @return string
     */
    public function getActivityTypeDesc()
    {
        return $this->activityTypeDesc;
    }

    /**
     * Add topic
     *
     * @param AreaOfExpertise $topic
     *
     * @return ItineraryActivity
     */
    public function addTopic(AreaOfExpertise $topic)
    {
        $this->topics[] = $topic;

        return $this;
    }

    /**
     * Remove topic
     *
     * @param AreaOfExpertise $topic
     */
    public function removeTopic(AreaOfExpertise $topic)
    {
        $this->topics->removeElement($topic);
    }

    /**
     * Get topics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Set topicOther
     *
     * @param string $topicOther
     *
     * @return ItineraryActivity
     */
    public function setTopicOther($topicOther)
    {
        $this->topicOther = $topicOther;

        return $this;
    }

    /**
     * Get topicOther
     *
     * @return string
     */
    public function getTopicOther()
    {
        return $this->topicOther;
    }

    /**
     * Set inCountryAssignment
     *
     * @param integer $inCountryAssignment
     *
     * @return ItineraryActivity
     */
    public function setInCountryAssignment($inCountryAssignment)
    {
        $this->inCountryAssignment = $inCountryAssignment;

        return $this;
    }

    /**
     * Get inCountryAssignment
     *
     * @return integer
     */
    public function getInCountryAssignment()
    {
        return $this->inCountryAssignment;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return ItineraryActivity
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set activityDate
     *
     * @param \DateTime $activityDate
     *
     * @return ItineraryActivity
     */
    public function setActivityDate($activityDate)
    {
        $this->activityDate = $activityDate;

        return $this;
    }

    /**
     * Get activityDate
     *
     * @return \DateTime
     */
    public function getActivityDate()
    {
        return $this->activityDate;
    }

    /**
     * Set activityType
     *
     * @param \AppBundle\Entity\Specialist\Itinerary\ActivityType $activityType
     *
     * @return ItineraryActivity
     */
    public function setActivityType(\AppBundle\Entity\Specialist\Itinerary\ActivityType $activityType = null)
    {
        $this->activityType = $activityType;

        return $this;
    }

    /**
     * Get activityType
     *
     * @return \AppBundle\Entity\Specialist\Itinerary\ActivityType
     */
    public function getActivityType()
    {
        return $this->activityType;
    }

    /**
     * Add anticipatedAudience
     *
     * @param AnticipatedAudience $anticipatedAudience
     *
     * @return ItineraryActivity
     */
    public function addAnticipatedAudience(
      AnticipatedAudience $anticipatedAudience
    ) {
        $this->anticipatedAudience[] = $anticipatedAudience;

        return $this;
    }

    /**
     * Remove anticipatedAudience
     *
     * @param AnticipatedAudience $anticipatedAudience
     */
    public function removeAnticipatedAudience(
      AnticipatedAudience $anticipatedAudience
    ) {
        $this->anticipatedAudience->removeElement($anticipatedAudience);
    }

    /**
     * Get anticipatedAudience
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnticipatedAudience()
    {
        return $this->anticipatedAudience;
    }

    /**
     * Add partneringOrganization
     *
     * @param PartneringOrganization $partneringOrganization
     *
     * @return ItineraryActivity
     */
    public function addPartneringOrganization(
      PartneringOrganization $partneringOrganization
    ) {
        $this->partneringOrganizations[] = $partneringOrganization;

        return $this;
    }

    /**
     * Remove partneringOrganization
     *
     * @param PartneringOrganization $partneringOrganization
     */
    public function removePartneringOrganization(
      PartneringOrganization $partneringOrganization
    ) {
        $this->partneringOrganizations->removeElement($partneringOrganization);
    }

    /**
     * Get partneringOrganizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartneringOrganizations()
    {
        return $this->partneringOrganizations;
    }
}
