<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class CostItem
 *
 * @package AppBundle\Entity
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="cost_item")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *     "0" = "CostItemMonthly",
 *     "1" = "CostItemOneTime",
 *     "2" = "CostItemCustom",
 *     "4" = "CostItemLivingExpense",
 *     "5" = "CostItemGroundTransport",
 *     "6" = "CostItemContributionOneTime",
 *     "7" = "CostItemProgramActivityAllowance",
 *     "8" = "CostItemCustomInKind",
 *     "9" = "CostItemContributionTransportation"
 * })
 */
abstract class CostItem extends BaseEntity
{
    /**
     * @var string $description
     *
     * @ORM\Column(type="text", length=65535)
     */
    protected $description;

    /************************ Methods Definitions ************************/
    /**
     * Set description
     *
     * @param string $description
     *
     * @return CostItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}
