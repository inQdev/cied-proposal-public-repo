<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CostItemGroundTransport entity.
 *
 * @package AppBundle\Entity
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 */
class CostItemGroundTransport extends CostItem
{
}
