<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseTerm entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="base_term")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *     "0" = "VisaAvgLength",
 *     "1" = "AppBundle\Entity\Fellow\DutyActivityFocus",
 *     "3" = "AreaOfExpertise",
 *     "4" = "AppBundle\Entity\Fellow\DutyActivitySecondaryFocus",
 *     "5" = "AppBundle\Entity\Specialist\VisaAvgLength",
 *     "6" = "AppBundle\Entity\Specialist\Itinerary\ActivityType",
 *     "7" = "AppBundle\Entity\Specialist\Itinerary\Activity\Audience"
 * })
 */
abstract class BaseTerm extends BaseSingleEntity
{
}
