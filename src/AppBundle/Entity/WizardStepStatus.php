<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class WizardStepStatus
 *
 * @package AppBundle\Entity
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="wizard_step_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WizardStepStatusRepository")
 */
class WizardStepStatus extends BaseEntity
{
    /**
     * Constants.
     */
    const GENERAL_INFO_STEP = 'general_info';
    const POINT_OF_CONTACT_STEP = 'point_of_contact';
    const PROJECT_REQUIREMENTS_STEP = 'project_requirement';
    const HOUSING_INFO_STEP = 'housing_info';
    const CERTIFICATIONS_STEP = 'certifications';
    const LOCATION_HOST_INFO_STEP = 'location_host_info';
    const FELLOWSHIP_DUTIES_STEP = 'fellowship_duties';
    const BUDGET_STEP = 'budget_step';

    const NOT_STARTED = 0;
    const IN_PROGRESS = 1;
    const COMPLETED = 2;

    /**
     * @var string $step
     *
     * @ORM\Column(
     *     type="string",
     *     length=64,
     *     nullable=true
     * )
     */
    protected $step;

    /**
     * @var integer $status
     *
     * @ORM\Column(
     *     type="integer",
     *     nullable=true
     * )
     */
    protected $status;

    /**
     * @var FellowProject $fellowProject
     *
     * @ORM\ManyToOne(
     *     targetEntity="FellowProject",
     *     inversedBy="wizardStepsStatuses"
     * )
     * @ORM\JoinColumn(
     *     name="fellow_project_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $fellowProject;

    /**
     * WizardStepStatus constructor.
     *
     * @param int $step
     * @param int $status
     */
    public function __construct($step, $status)
    {
        $this->step = $step;
        $this->status = $status;
    }

    /**
     * Set step
     *
     * @param string $step
     *
     * @return WizardStepStatus
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return string
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return WizardStepStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set fellowProject
     *
     * @param FellowProject $fellowProject
     *
     * @return WizardStepStatus
     */
    public function setFellowProject(FellowProject $fellowProject = null)
    {
        $this->fellowProject = $fellowProject;

        return $this;
    }

    /**
     * Get fellowProject
     *
     * @return FellowProject
     */
    public function getFellowProject()
    {
        return $this->fellowProject;
    }
}
