<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialistProject entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="specialist_project")
 */
class SpecialistProject extends BaseEntity
{
    /**
     * @var ArrayCollection $phases
     *
     * @ORM\OneToMany(
     *     targetEntity="SpecialistProjectPhase",
     *     mappedBy="specialistProject"
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $phases;

    /***************************** Methods Definition *****************************/

    /**
     * SpecialistProject constructor.
     */
    public function __construct()
    {
        $this->areasOfExpertise = new ArrayCollection();
        $this->phases = new ArrayCollection();
    }

    /**
     * Add phase
     *
     * @param SpecialistProjectPhase $phase
     *
     * @return SpecialistProject
     */
    public function addPhase(SpecialistProjectPhase $phase)
    {
        $this->phases[] = $phase;

        return $this;
    }

    /**
     * Remove phase
     *
     * @param SpecialistProjectPhase $phase
     */
    public function removePhase(SpecialistProjectPhase $phase)
    {
        $this->phases->removeElement($phase);
    }

    /**
     * Get phases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhases()
    {
        return $this->phases;
    }

}
