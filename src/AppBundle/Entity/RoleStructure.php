<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoleStructure (name chosen to avoid ambiguety with other RoleHierarchy classes)
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 * 
 * @ORM\Table(name="role_structure")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleStructureRepository")
 */
class RoleStructure
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, unique=true)
	 */
	protected $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="roles", type="string", length=1024)
	 */
	protected $roles;


	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return RoleHierarchy
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set roles
	 *
	 * @param string $roles
	 *
	 * @return RoleHierarchy
	 */
	public function setRoles($roles)
	{
		$this->roles = $roles;

		return $this;
	}

	/**
	 * Get roles
	 *
	 * @return string
	 */
	public function getRoles()
	{
		return $this->roles;
	}

}

