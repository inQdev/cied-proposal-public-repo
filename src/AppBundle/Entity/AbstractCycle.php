<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cycle entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="cycle")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "0" = "AppBundle\Entity\Fellow\CycleFellow",
 *     "1" = "AppBundle\Entity\Specialist\CycleSpecialist"
 * })
 */
abstract class AbstractCycle extends BaseSingleEntity
{
    /**
     * @var integer $tid
     *
     * @ORM\Column(type="integer")
     */
    protected $tid;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    protected $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    protected $endDate;

    /**
     * @var \DateTime $openPeriodStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $openPeriodStartDate;

    /**
     * @var \DateTime $openPeriodEndDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $openPeriodEndDate;

    /**
     * @var \DateTime $displayPeriodStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $displayPeriodStartDate;

    /**
     * @var \DateTime $displayPeriodEndDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $displayPeriodEndDate;

    /**
     * @var string $stateDepartmentName
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $stateDepartmentName;

    /**
     * @var ArrayCollection $projectsGeneralInfo
     *
     * @ORM\OneToMany(targetEntity="ProjectGeneralInfo", mappedBy="cycle")
     */
    protected $projectsGeneralInfo;

    /**
     * AbstractCycle constructor.
     */
    public function __construct()
    {
        $this->projectsGeneralInfo = new ArrayCollection();
    }

    /**
     * Set tid
     *
     * @param integer $tid
     *
     * @return AbstractCycle
     */
    public function setTid($tid)
    {
        $this->tid = $tid;

        return $this;
    }

    /**
     * Get tid
     *
     * @return integer
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return AbstractCycle
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return AbstractCycle
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set openPeriodStartDate
     *
     * @param \DateTime $openPeriodStartDate
     *
     * @return AbstractCycle
     */
    public function setOpenPeriodStartDate($openPeriodStartDate)
    {
        $this->openPeriodStartDate = $openPeriodStartDate;

        return $this;
    }

    /**
     * Get openPeriodStartDate
     *
     * @return \DateTime
     */
    public function getOpenPeriodStartDate()
    {
        return $this->openPeriodStartDate;
    }

    /**
     * Set openPeriodEndDate
     *
     * @param \DateTime $openPeriodEndDate
     *
     * @return AbstractCycle
     */
    public function setOpenPeriodEndDate($openPeriodEndDate)
    {
        $this->openPeriodEndDate = $openPeriodEndDate;

        return $this;
    }

    /**
     * Get openPeriodEndDate
     *
     * @return \DateTime
     */
    public function getOpenPeriodEndDate()
    {
        return $this->openPeriodEndDate;
    }

    /**
     * Set displayPeriodStartDate
     *
     * @param \DateTime $displayPeriodStartDate
     *
     * @return AbstractCycle
     */
    public function setDisplayPeriodStartDate($displayPeriodStartDate)
    {
        $this->displayPeriodStartDate = $displayPeriodStartDate;

        return $this;
    }

    /**
     * Get displayPeriodStartDate
     *
     * @return \DateTime
     */
    public function getDisplayPeriodStartDate()
    {
        return $this->displayPeriodStartDate;
    }

    /**
     * Set displayPeriodEndDate
     *
     * @param \DateTime $displayPeriodEndDate
     *
     * @return AbstractCycle
     */
    public function setDisplayPeriodEndDate($displayPeriodEndDate)
    {
        $this->displayPeriodEndDate = $displayPeriodEndDate;

        return $this;
    }

    /**
     * Get displayPeriodEndDate
     *
     * @return \DateTime
     */
    public function getDisplayPeriodEndDate()
    {
        return $this->displayPeriodEndDate;
    }

    /**
     * Set stateDepartmentName
     *
     * @param string $stateDepartmentName
     *
     * @return AbstractCycle
     */
    public function setStateDepartmentName($stateDepartmentName)
    {
        $this->stateDepartmentName = $stateDepartmentName;

        return $this;
    }

    /**
     * Get stateDepartmentName
     *
     * @return string
     */
    public function getStateDepartmentName()
    {
        return $this->stateDepartmentName;
    }

    /**
     * Add projectsGeneralInfo
     *
     * @param ProjectGeneralInfo $projectsGeneralInfo
     *
     * @return AbstractCycle
     */
    public function addProjectsGeneralInfo(
      ProjectGeneralInfo $projectsGeneralInfo
    ) {
        $this->projectsGeneralInfo[] = $projectsGeneralInfo;

        return $this;
    }

    /**
     * Remove projectsGeneralInfo
     *
     * @param ProjectGeneralInfo $projectsGeneralInfo
     */
    public function removeProjectsGeneralInfo(
      ProjectGeneralInfo $projectsGeneralInfo
    ) {
        $this->projectsGeneralInfo->removeElement($projectsGeneralInfo);
    }

    /**
     * Get projectsGeneralInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectsGeneralInfo()
    {
        return $this->projectsGeneralInfo;
    }
}
