<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PrePostWork entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbaiton.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="pre_post_work")
 */
class PrePostWork extends BaseEntity
{
    /**
     * @var bool $additionalPreWork
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $additionalPreWork;

    /**
     * @var int $additionalPreWorkDays
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Choice(choices = {1, 2, 3})
     */
    protected $additionalPreWorkDays;

    /**
     * @var \DateTime $additionalPreWorkStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $additionalPreWorkStartDate;

    /**
     * @var string $additionalPreWorkDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $additionalPreWorkDesc;

    /**
     * @var bool $additionalPostWork
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $additionalPostWork;

    /**
     * @var int $additionalPostWorkDays
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Choice(choices = {1, 2, 3, 4, 5})
     */
    protected $additionalPostWorkDays;

    /**
     * @var \DateTime $additionalPostWorkStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $additionalPostWorkStartDate;

    /**
     * @var string $additionalPostWorkDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $additionalPostWorkDesc;

    /**
     * @var SpecialistProjectPhase $phase
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhase",
     *     inversedBy="prePostWork"
     * )
     * @ORM\JoinColumn(
     *     name="phase_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $phase;

    /**
     * PrePostWork constructor.
     *
     * @param SpecialistProjectPhase|null $phase
     */
    public function __construct(SpecialistProjectPhase $phase = null)
    {
        $this->phase = $phase;
    }

    /**
     * Set additionalPreWork
     *
     * @param boolean $additionalPreWork
     *
     * @return PrePostWork
     */
    public function setAdditionalPreWork($additionalPreWork)
    {
        $this->additionalPreWork = $additionalPreWork;

        return $this;
    }

    /**
     * Get additionalPreWork
     *
     * @return boolean
     */
    public function getAdditionalPreWork()
    {
        return $this->additionalPreWork;
    }

    /**
     * Set additionalPreWorkDays
     *
     * @param integer $additionalPreWorkDays
     *
     * @return PrePostWork
     */
    public function setAdditionalPreWorkDays($additionalPreWorkDays)
    {
        $this->additionalPreWorkDays = $additionalPreWorkDays;

        return $this;
    }

    /**
     * Get additionalPreWorkDays
     *
     * @return integer
     */
    public function getAdditionalPreWorkDays()
    {
        return $this->additionalPreWorkDays;
    }

    /**
     * Set additionalPreWorkStartDate
     *
     * @param \DateTime $additionalPreWorkStartDate
     *
     * @return PrePostWork
     */
    public function setAdditionalPreWorkStartDate($additionalPreWorkStartDate)
    {
        $this->additionalPreWorkStartDate = $additionalPreWorkStartDate;

        return $this;
    }

    /**
     * Get additionalPreWorkStartDate
     *
     * @return \DateTime
     */
    public function getAdditionalPreWorkStartDate()
    {
        return $this->additionalPreWorkStartDate;
    }

    /**
     * Set additionalPreWorkDesc
     *
     * @param string $additionalPreWorkDesc
     *
     * @return PrePostWork
     */
    public function setAdditionalPreWorkDesc($additionalPreWorkDesc)
    {
        $this->additionalPreWorkDesc = $additionalPreWorkDesc;

        return $this;
    }

    /**
     * Get additionalPreWorkDesc
     *
     * @return string
     */
    public function getAdditionalPreWorkDesc()
    {
        return $this->additionalPreWorkDesc;
    }

    /**
     * Set additionalPostWork
     *
     * @param boolean $additionalPostWork
     *
     * @return PrePostWork
     */
    public function setAdditionalPostWork($additionalPostWork)
    {
        $this->additionalPostWork = $additionalPostWork;

        return $this;
    }

    /**
     * Get additionalPostWork
     *
     * @return boolean
     */
    public function getAdditionalPostWork()
    {
        return $this->additionalPostWork;
    }

    /**
     * Set additionalPostWorkDays
     *
     * @param integer $additionalPostWorkDays
     *
     * @return PrePostWork
     */
    public function setAdditionalPostWorkDays($additionalPostWorkDays)
    {
        $this->additionalPostWorkDays = $additionalPostWorkDays;

        return $this;
    }

    /**
     * Get additionalPostWorkDays
     *
     * @return integer
     */
    public function getAdditionalPostWorkDays()
    {
        return $this->additionalPostWorkDays;
    }

    /**
     * Set additionalPostWorkStartDate
     *
     * @param \DateTime $additionalPostWorkStartDate
     *
     * @return PrePostWork
     */
    public function setAdditionalPostWorkStartDate($additionalPostWorkStartDate)
    {
        $this->additionalPostWorkStartDate = $additionalPostWorkStartDate;

        return $this;
    }

    /**
     * Get additionalPostWorkStartDate
     *
     * @return \DateTime
     */
    public function getAdditionalPostWorkStartDate()
    {
        return $this->additionalPostWorkStartDate;
    }

    /**
     * Set additionalPostWorkDesc
     *
     * @param string $additionalPostWorkDesc
     *
     * @return PrePostWork
     */
    public function setAdditionalPostWorkDesc($additionalPostWorkDesc)
    {
        $this->additionalPostWorkDesc = $additionalPostWorkDesc;

        return $this;
    }

    /**
     * Get additionalPostWorkDesc
     *
     * @return string
     */
    public function getAdditionalPostWorkDesc()
    {
        return $this->additionalPostWorkDesc;
    }

    /**
     * Set phase
     *
     * @param SpecialistProjectPhase $phase
     *
     * @return PrePostWork
     */
    public function setPhase(SpecialistProjectPhase $phase = null) {
        $this->phase = $phase;

        return $this;
    }

    /**
     * Get phase
     *
     * @return SpecialistProjectPhase
     */
    public function getPhase()
    {
        return $this->phase;
    }
}
