<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CostItemProgramActivityAllowance entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbaiton.com>
 *
 * @ORM\Entity()
 */
class CostItemProgramActivityAllowance extends CostItem
{
}
