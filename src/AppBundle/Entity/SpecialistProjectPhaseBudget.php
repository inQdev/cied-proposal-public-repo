<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Specialist\Budget\BudgetTotal;
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpenseCostItem;
use AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem; // Required to avoid using that long text in type hints
use AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense;
use AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowance;
use AppBundle\Entity\Specialist\Budget\Contribution\Transportation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialistProjectPhaseBudget entity
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\Specialist\Budget\PhaseRepository"
 * )
 * @ORM\Table(name="specialist_project_phase_budget")
 */
class SpecialistProjectPhaseBudget extends BaseEntity
{
	/**
	 * @var int $revisionId
	 *
	 * @ORM\Column(type="integer")
	 */
	protected $revisionId = 1;

	/**
	 * @var string $fundedBy
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $fundedBy;

	/**
	 * @var string $postFundingSource
	 *
	 * @ORM\Column(type="string", length=4294967295, nullable=true)
	 */
	protected $postFundingSource;

	/**
	 * @var SpecialistProjectPhase $phase
	 *
	 * @ORM\OneToOne(targetEntity="SpecialistProjectPhase", inversedBy="budget")
	 * @ORM\JoinColumn(
	 *     name="phase_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $phase;

	/**
	 * @var ArrayCollection $contributionLivingExpenses
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="AppBundle\Entity\Specialist\Budget\Contribution\ContributionLivingExpense",
	 *     mappedBy="phaseBudget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $contributionLivingExpenses;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="LivingExpense",
	 *     mappedBy="phaseBudget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $livingExpenseEstimates;

	/**
	 * @var GroundTransportCostItem $contributionGroundTransport
	 *
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Entity\Specialist\Budget\Contribution\GroundTransportCostItem",
	 *     mappedBy="phaseBudget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $contributionGroundTransport;

	/**
	 * @var Transportation $contributionTransportation
	 *
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Entity\Specialist\Budget\Contribution\Transportation",
	 *     mappedBy="phaseBudget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $contributionTransportation;

	/**
	 * @var ContributionOneTime $contributionOneTime
	 *
	 * @ORM\OneToOne(
	 *     targetEntity="ContributionOneTime",
	 *     mappedBy="phaseBudget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $contributionOneTime;

    /**
     * @var ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Budget\Contribution\ProgramActivitiesAllowance",
     *     mappedBy="phaseBudget",
     *     cascade={"persist"}
     * )
     */
    protected $programActivitiesAllowance;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=4294967295, nullable=true)
	 */
	protected $livingExpenseExceptions;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(
	 *     targetEntity="AppBundle\Entity\Specialist\Budget\BudgetTotal",
	 *     mappedBy="budget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $budgetTotals;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(
	 *     targetEntity="AppBundle\Entity\Specialist\Budget\GlobalValue",
	 *     mappedBy="budget",
	 *     cascade={"persist"}
	 * )
	 */
	protected $globalValues;

	/************************** Methods ***************************/

	/**
	 * SpecialistProjectPhaseBudget constructor.
	 *
	 * @param SpecialistProjectPhase $phase
	 */
	public function __construct(SpecialistProjectPhase $phase)
	{
		$this->phase = $phase;
//		$this->contributionLivingExpenses = new ArrayCollection();
	}

	/**
	 * Set revisionId
	 *
	 * @param integer $revisionId
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setRevisionId($revisionId)
	{
		$this->revisionId = $revisionId;

		return $this;
	}

	/**
	 * Get revisionId
	 *
	 * @return integer
	 */
	public function getRevisionId()
	{
		return $this->revisionId;
	}

	/**
	 * Set fundedBy
	 *
	 * @param string $fundedBy
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setFundedBy($fundedBy)
	{
		$this->fundedBy = $fundedBy;

		return $this;
	}

	/**
	 * Get fundedBy
	 *
	 * @return string
	 */
	public function getFundedBy()
	{
		return $this->fundedBy;
	}

	/**
	 * Set postFundingSource
	 *
	 * @param string $postFundingSource
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setPostFundingSource($postFundingSource)
	{
		$this->postFundingSource = $postFundingSource;

		return $this;
	}

	/**
	 * Get postFundingSource
	 *
	 * @return string
	 */
	public function getPostFundingSource()
	{
		return $this->postFundingSource;
	}

	/**
	 * Set phase
	 *
	 * @param SpecialistProjectPhase $phase
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setPhase(SpecialistProjectPhase $phase = null)
	{
		$this->phase = $phase;

		return $this;
	}

	/**
	 * Get phase
	 *
	 * @return SpecialistProjectPhase
	 */
	public function getPhase()
	{
		return $this->phase;
	}

	/**
	 * Add contributionLivingExpense
	 *
	 * @param ContributionLivingExpense $contributionLivingExpense
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function addContributionLivingExpense(
		ContributionLivingExpense $contributionLivingExpense
	)
	{
		$this->contributionLivingExpenses[] = $contributionLivingExpense;

		return $this;
	}

	/**
	 * Remove contributionLivingExpense
	 *
	 * @param ContributionLivingExpense $contributionLivingExpense
	 */
	public function removeContributionLivingExpense(
		ContributionLivingExpense $contributionLivingExpense
	)
	{
		$this->contributionLivingExpenses->removeElement(
			$contributionLivingExpense
		);
	}

	/**
	 * Get contributionLivingExpenses
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getContributionLivingExpenses()
	{
		return $this->contributionLivingExpenses;
	}

	/**
	 * Add livingExpenseEstimate
	 *
	 * @param LivingExpense $livingExpenseEstimate
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function addLivingExpenseEstimate(
		LivingExpense $livingExpenseEstimate
	)
	{
		$this->livingExpenseEstimates[] = $livingExpenseEstimate;

		return $this;
	}

	/**
	 * Remove livingExpenseEstimate
	 *
	 * @param LivingExpense $livingExpenseEstimate
	 */
	public function removeLivingExpenseEstimate(
		LivingExpense $livingExpenseEstimate
	)
	{
		$this->livingExpenseEstimates->removeElement($livingExpenseEstimate);
	}

	/**
	 * Get livingExpenseEstimates
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getLivingExpenseEstimates()
	{
		return $this->livingExpenseEstimates;
	}

	/**
	 * Set contributionTransportation
	 *
	 * @param Transportation $contributionTransportation
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setContributionTransportation(Transportation $contributionTransportation = null)
	{
		$this->contributionTransportation = $contributionTransportation;
	}

	/**
	 * Get contributionTransportation
	 *
	 * @return Transportation
	 */
	public function getContributionTransportation()
	{
		return $this->contributionTransportation;
	}

	/**
	 * Set contributionOneTime
	 *
	 * @param ContributionOneTime $contributionOneTime
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setContributionOneTime(
		ContributionOneTime $contributionOneTime = null
	)
	{
		$this->contributionOneTime = $contributionOneTime;

		return $this;
	}

	/**
	 * Get contributionOneTime
	 *
	 * @return ContributionOneTime
	 */
	public function getContributionOneTime()
	{
		return $this->contributionOneTime;
	}

	/**
	 * Set programActivitiesAllowance
	 *
	 * @param ProgramActivitiesAllowance $programActivitiesAllowance
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function setProgramActivitiesAllowance(
		ProgramActivitiesAllowance $programActivitiesAllowance = null
	)
	{
		$this->programActivitiesAllowance = $programActivitiesAllowance;

		return $this;
	}

	/**
	 * Get programActivitiesAllowance
	 *
	 * @return ProgramActivitiesAllowance
	 */
	public function getProgramActivitiesAllowance()
	{
		return $this->programActivitiesAllowance;
	}

    /**
     * Set contributionGroundTransport
     *
     * @param GroundTransportCostItem $contributionGroundTransport
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setContributionGroundTransport(GroundTransportCostItem $contributionGroundTransport = null)
    {
        $this->contributionGroundTransport = $contributionGroundTransport;

        return $this;
    }

    /**
     * Get contributionGroundTransport
     *
     * @return GroundTransportCostItem
     */
    public function getContributionGroundTransport()
    {
        return $this->contributionGroundTransport;
    }

	/**
	 * Set livingExpenseExceptions
	 *
	 * @param string $livingExpenseExceptions
	 *
	 * @return LivingExpense
	 */
	public function setLivingExpenseExceptions($livingExpenseExceptions)
	{
		$this->livingExpenseExceptions = $livingExpenseExceptions;

		return $this;
	}

	/**
	 * Get livingExpenseExceptions
	 *
	 * @return string
	 */
	public function getLivingExpenseExceptions()
	{
		return $this->livingExpenseExceptions;
	}

    /**
     * Add budgetTotal
     *
     * @param \AppBundle\Entity\Specialist\Budget\BudgetTotal $budgetTotal
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function addBudgetTotal(\AppBundle\Entity\Specialist\Budget\BudgetTotal $budgetTotal)
    {
        $this->budgetTotals[] = $budgetTotal;

        return $this;
    }

    /**
     * Remove budgetTotal
     *
     * @param \AppBundle\Entity\Specialist\Budget\BudgetTotal $budgetTotal
     */
    public function removeBudgetTotal(\AppBundle\Entity\Specialist\Budget\BudgetTotal $budgetTotal)
    {
        $this->budgetTotals->removeElement($budgetTotal);
    }

    /**
     * Get budgetTotals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBudgetTotals()
    {
        return $this->budgetTotals;
    }

    /**
     * Add globalValue
     *
     * @param \AppBundle\Entity\Specialist\Budget\GlobalValue $globalValue
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function addGlobalValue(\AppBundle\Entity\Specialist\Budget\GlobalValue $globalValue)
    {
        $this->globalValues[] = $globalValue;

        return $this;
    }

    /**
     * Remove globalValue
     *
     * @param \AppBundle\Entity\Specialist\Budget\GlobalValue $globalValue
     */
    public function removeGlobalValue(\AppBundle\Entity\Specialist\Budget\GlobalValue $globalValue)
    {
        $this->globalValues->removeElement($globalValue);
    }

    /**
     * Get globalValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGlobalValues()
    {
        return $this->globalValues;
    }



	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Doctrine\ORM\EntityManager     $em
	 * @return int $status
	 */
	public function getBudgetStepStatus($fellowProject, $em)
	{
		$budgetStep = $em->getRepository('AppBundle:WizardStepStatus')
			->findStepStatusByProject($fellowProject, WizardStepStatus::BUDGET_STEP);

		return $budgetStep->getStatus();
	}

	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Doctrine\ORM\EntityManager     $em
	 * @param int $status
	 */
	public function setBudgetStepStatus($fellowProject, $em, $status)
	{
		$budgetStep = $em->getRepository('AppBundle:WizardStepStatus')
			->findStepStatusByProject($fellowProject, WizardStepStatus::BUDGET_STEP);

		$budgetStep->setStatus($status);
		$em->persist($budgetStep);
		$em->flush();

	}

	/**
	 * Calculates and saves totals (Summary) to avoid some recalculations and to expedite searches
	 * @param EntityManager $em
	 * @param boolean $save - to control if the settings must be saved
	 * @return array
	 */
	public function setSummaryTotals($em=null, $save=false) {

		/** @var BudgetTotal $budgetTotals */
		$budgetTotals = $this->getBudgetTotals();
		/** @var SpecialistProjectPhase $projectPhase */
		$projectPhase = $this->getPhase();

		if (!count($budgetTotals)) {
			$budgetTotals = new BudgetTotal($this);
			$budgetTotals->setRevisionId(1);
//			$budgetTotals->setBudget($this);
			$this->addBudgetTotal($budgetTotals);
			$em->persist($budgetTotals);
		} else {
			$budgetTotals = $budgetTotals->first();
		}

		// Calculates totals (values that are not in the DB table)
		$totals = array(
			'flexible' => array(
				'total_cost' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'fixed' => 0,
		);

		if ($projectPhase->getType() == SpecialistProjectPhase::IN_COUNTRY_TYPE
			|| $projectPhase->getType() == SpecialistProjectPhase::MIXED_TYPE) {

			// contributions on living expenses
			if ($this->contributionLivingExpenses->count()) {
				$costItems = $this->contributionLivingExpenses->first()->getContributionLivingExpenseCostItems();
				if ($costItems->count()) {
					$this->sumCostItems($costItems, $totals['flexible']);
				}
			}

			// contributions on ground transport
			if ($this->contributionGroundTransport) {
				$costItems = new ArrayCollection(array($this->contributionGroundTransport));
				$this->sumCostItems($costItems, $totals['flexible']);
			}

			// Transportation between cities contributions
			if ($this->contributionTransportation) {
				$costItems = $this->contributionTransportation->getTransportationCostItems();
				if ($costItems->count()) {
					$this->sumCostItems($costItems, $totals['flexible']);
				}
			}

			// One Time contributions
			if ($this->contributionOneTime) {
				$costItems = $this->contributionOneTime->getContributionOneTimeCostItems();
				$this->sumCostItems($costItems, $totals['flexible']);
			}
		}

		if ($projectPhase->getType() == SpecialistProjectPhase::VIRTUAL_TYPE
			|| $projectPhase->getType() == SpecialistProjectPhase::MIXED_TYPE) {
			$em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updateVirtualFlexibleTotals($this);
		}

		// for virtual and mixed, checks fixed costs
		// since this only happens when the activities are changed, I'm taking it out from here
//		if ($projectPhase->getType() == SpecialistProjectPhase::VIRTUAL_TYPE
//			|| $projectPhase->getType() == SpecialistProjectPhase::MIXED_TYPE) {
//			$em->getRepository('AppBundle:Specialist\Budget\GlobalValue')->updateVirtualGlobals($this);
//		}

		// PAA Contributions - calculated for all type of projects
		if ($this->programActivitiesAllowance) {
			$costItems = $this->programActivitiesAllowance->getProgramActivitiesAllowanceCostItems();
			$this->sumCostItems($costItems, $totals['flexible']);
		}

		// calculates the global values total
		// It's ok for all type of projects
		foreach ($this->getGlobalValues() as $globalValue) {
			$totals['fixed'] += $globalValue->getValue();
		}
		$budgetTotals->setGlobal($totals['fixed']);

		// Calculates Pre-Post Work and adds it to the totals
//		$budgetTotals->updatePrePostWork();

		// Sets other values in Totals
		$budgetTotals->setPostContribution($totals['flexible']['post']);
		$budgetTotals->setHostContributionMonetary($totals['flexible']['host_monetary']);
		$budgetTotals->setHostContributionInKind($totals['flexible']['host_kind']);

		// Full cost includes the costs (monetary) and in-kind contributions by host
		$budgetTotals->setFullCost($totals['flexible']['total_cost'] + $budgetTotals->getPrePostWork() + $budgetTotals->getDailyGlobal() + $budgetTotals->getVirtualFlexibleTotal());
		$budgetTotals->setTotal($budgetTotals->getFullCost() + $budgetTotals->getGlobal());

		// If initially fundedBy is null, calculates as if it were ECA
		if ($this->getFundedBy() == 'Post') {
			$budgetTotals->setEcaTotalContribution(0);
			$budgetTotals->setPostTotalContribution($totals['flexible']['post_total'] + $totals['fixed'] + $budgetTotals->getPrePostWork() + $budgetTotals->getDailyGlobal() + $budgetTotals->getVirtualFlexibleTotal());
		} else {
			$budgetTotals->setPostTotalContribution($totals['flexible']['post_total']);
			$budgetTotals->setEcaTotalContribution($totals['flexible']['eca'] + $totals['fixed'] + $budgetTotals->getPrePostWork()+ $budgetTotals->getDailyGlobal() + $budgetTotals->getVirtualFlexibleTotal());
		}

		if ($save) {
//			$em->persist($budgetTotals);
			$em->flush();
		}

		return $totals;
	}

	/**
	 * @param ArrayCollection $costItems
	 * @param array $totals
	 * @return mixed
	 */
	private function sumCostItems($costItems, &$totals) {
//		dump($costItems);
		foreach ($costItems as $item) {
			$totals['total_cost'] += $item->getTotalCost();
			$totals['post'] += $item->getPostContribution();
			$totals['host_monetary'] += $item->getHostContributionMonetary();
			$totals['host_kind'] += $item->getHostContributionInKind();
			$totals['post_total'] += $item->getPostTotalContribution();
			$totals['eca'] += $item->getEcaTotalContribution();
		}

		return $totals;
	}

}
