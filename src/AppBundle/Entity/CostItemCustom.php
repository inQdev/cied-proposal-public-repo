<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CostItemCustom
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 */
class CostItemCustom extends CostItem
{

	public function __construct()
	{
		$this->description = '';
	}
}