<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class GlobalValue
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="global_value")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="project_type", type="integer")
 * @ORM\DiscriminatorMap({
 *     "0" = "GlobalValueFellow",
 *     "1" = "GlobalValueSpecialist",
 * })
 */
abstract class GlobalValue extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="default_value", type="decimal", precision=10, scale=2)
	 */
	private $defaultValue;

	/**
	 * @var int
	 * To act as flag and value: 0 = phase, x = day/hours
	 * @ORM\Column(type="smallint", nullable=true)
	 */
	protected $periodicity = 0;

	/**
	 * @var string
	 * This could be tricky, to simplify it would just keep the limit number of days/hours of project/activities
	 * but initially we will only have the hours of activities (for calculations)
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	protected $rules;

	/**
	 * @var int
	 * To arrange values in pages
	 * @ORM\Column(type="smallint", options={"default":999})
	 */
	protected $sortOrder = 999;


	/***************************** Methods ************************************/

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return GlobalValue
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set defaultValue
	 *
	 * @param string $defaultValue
	 *
	 * @return GlobalValue
	 */
	public function setDefaultValue($defaultValue)
	{
		$this->defaultValue = $defaultValue;

		return $this;
	}

	/**
	 * Get defaultValue
	 *
	 * @return string
	 */
	public function getDefaultValue()
	{
		return $this->defaultValue;
	}

    /**
     * Set periodicity
     *
     * @param boolean $periodicity
     *
     * @return GlobalValue
     */
    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;

        return $this;
    }

    /**
     * Get periodicity
     *
     * @return boolean
     */
    public function getPeriodicity()
    {
        return $this->periodicity;
    }

    /**
     * Set rules
     *
     * @param string $rules
     *
     * @return GlobalValue
     */
    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * Get rules
     *
     * @return string
     */
    public function getRules()
    {
        return $this->rules;
    }
}
