<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Common\BaseContributionItem;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * Class ContributionOneTimeCostItem
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Entity @HasLifecycleCallbacks
 * @ORM\Table(name="contribution_one_time_cost_item")
 */
class ContributionOneTimeCostItem extends BaseContributionItem
{
    /**
     * @var CostItem $costItem
     *
     * @ORM\ManyToOne(targetEntity="CostItem", cascade={"persist"})
     */
    protected $costItem;

    /**
     * @var ContributionOneTime $contributionOneTime
     *
     * @ORM\ManyToOne(
     *     targetEntity="ContributionOneTime",
     *     inversedBy="contributionOneTimeCostItems"
     * )
     * @ORM\JoinColumn(
     *     name="contribution_one_time_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $contributionOneTime;

    /**
     * ContributionOneTimeCostItem constructor.
     *
     * @param ContributionOneTime $contributionOneTime
     */
    public function __construct(ContributionOneTime $contributionOneTime)
    {
        $this->contributionOneTime = $contributionOneTime;
    }

    /**
     * @ORM\PreUpdate
     */
    public function calculateTotals()
    {
        $fundedBy = $this->contributionOneTime->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set costItem
     *
     * @param CostItem $costItem
     *
     * @return ContributionOneTimeCostItem
     */
    public function setCostItem(CostItem $costItem = null)
    {
        $this->costItem = $costItem;

        return $this;
    }

    /**
     * Get costItem
     *
     * @return CostItem
     */
    public function getCostItem()
    {
        return $this->costItem;
    }

    /**
     * Set contributionOneTime
     *
     * @param ContributionOneTime $contributionOneTime
     *
     * @return ContributionOneTimeCostItem
     */
    public function setContributionOneTime(
      ContributionOneTime $contributionOneTime = null
    ) {
        $this->contributionOneTime = $contributionOneTime;

        return $this;
    }

    /**
     * Get contributionOneTime
     *
     * @return ContributionOneTime
     */
    public function getContributionOneTime()
    {
        return $this->contributionOneTime;
    }

	/**
	 * @param string $fundingType (not used)
	 * @return int
	 */
	public function checkTotals($fundingType="")
	{
		$fundingType = $this->getContributionOneTime()->getPhaseBudget()->getFundedBy();
		return parent::checkTotals($fundingType);
	}


}
