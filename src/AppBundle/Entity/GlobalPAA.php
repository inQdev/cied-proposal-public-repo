<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalPAA
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Table(name="global_paa")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GlobalPAARepository")
 */
class GlobalPAA extends BaseEntity
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="range_start", type="integer", nullable=true)
	 */
	private $rangeStart;

	/**
	 * @var int
	 *
	 * @ORM\Column(name="range_end", type="integer", nullable=true)
	 */
	private $rangeEnd;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="default_value", type="decimal", precision=10, scale=2, nullable=true)
	 */
	private $defaultValue;


	/******************************* Methods *************************/

	/**
	 * Set rangeStart
	 *
	 * @param integer $rangeStart
	 *
	 * @return GlobalPAA
	 */
	public function setRangeStart($rangeStart)
	{
		$this->rangeStart = $rangeStart;

		return $this;
	}

	/**
	 * Get rangeStart
	 *
	 * @return int
	 */
	public function getRangeStart()
	{
		return $this->rangeStart;
	}

	/**
	 * Set rangeEnd
	 *
	 * @param integer $rangeEnd
	 *
	 * @return GlobalPAA
	 */
	public function setRangeEnd($rangeEnd)
	{
		$this->rangeEnd = $rangeEnd;

		return $this;
	}

	/**
	 * Get rangeEnd
	 *
	 * @return int
	 */
	public function getRangeEnd()
	{
		return $this->rangeEnd;
	}

	/**
	 * Set defaultValue
	 *
	 * @param string $defaultValue
	 *
	 * @return GlobalPAA
	 */
	public function setDefaultValue($defaultValue)
	{
		$this->defaultValue = $defaultValue;

		return $this;
	}

	/**
	 * Get defaultValue
	 *
	 * @return string
	 */
	public function getDefaultValue()
	{
		return $this->defaultValue;
	}
}

