<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItineraryIndex
 *
 * @ORM\Table(name="itinerary_index")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItineraryIndexRepository")
 */
class ItineraryIndex
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
	protected $date;

    /**
     * @var SpecialistProjectPhase
     *
     * @ORM\ManyToOne(
     *     targetEntity="SpecialistProjectPhase",
     *     inversedBy="itineraryIndexes"
     * )
     * @ORM\JoinColumn(
     *     name="phase_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
	protected $phase;

    /**
     * @var InCountryAssignment
     *
     * @ORM\ManyToOne(
     *     targetEntity="inCountryAssignment"
     * )
     * @ORM\JoinColumn(
     *     name="in_country_assignment_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     */
	protected $inCountryAssignment;

    /**
     * @var string
     * possible values: auto, manual
     * @ORM\Column(type="string", length=50)
     */
    protected $setWay;

	/****************************** Methods ******************************/

	/**
	 * ItineraryIndex constructor.
	 * @param \DateTime $date
	 * @param string $setWay
	 */
	public function __construct(SpecialistProjectPhase $phase,\DateTime $date, $setWay='auto')
	{
		$this->phase = $phase;
		$this->date = $date;
		$this->setWay = $setWay;
	}

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ItineraryIndex
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set phase
     *
     * @param SpecialistProjectPhase $phase
     *
     * @return ItineraryIndex
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;

        return $this;
    }

    /**
     * Get phase
     *
     * @return SpecialistProjectPhase
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * Set inCountryAssignment
     *
     * @param InCountryAssignment $inCountryAssignment
     *
     * @return ItineraryIndex
     */
    public function setInCountryAssignment($inCountryAssignment)
    {
        $this->inCountryAssignment = $inCountryAssignment;

        return $this;
    }

    /**
     * Get inCountryAssignment
     *
     * @return InCountryAssignment
     */
    public function getInCountryAssignment()
    {
        return $this->inCountryAssignment;
    }

    /**
     * Set setWay
     *
     * @param string $setWay
     *
     * @return ItineraryIndex
     */
    public function setSetWay($setWay)
    {
        $this->setWay = $setWay;

        return $this;
    }

    /**
     * Get setWay
     *
     * @return string
     */
    public function getSetWay()
    {
        return $this->setWay;
    }
}

