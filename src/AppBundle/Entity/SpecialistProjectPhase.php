<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Specialist\PartneringOrganization;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use AppBundle\Entity\Specialist\Virtual\Itinerary\WeekAfter;
use AppBundle\Entity\Specialist\VisaAvgLength;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SpecialistProjectPhase entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity(
 *     repositoryClass="AppBundle\Repository\Specialist\PhaseRepository"
 * )
 * @ORM\EntityListeners({"AppBundle\Entity\Listener\Specialist\PhaseListener"})
 * @ORM\Table(name="specialist_project_phase")
 * @Solr\Document()
 */
class SpecialistProjectPhase extends BaseEntity
{
    /**
     * Constants
     */
    const IN_COUNTRY_TYPE = 'in-country';
    const VIRTUAL_TYPE = 'virtual';
    const MIXED_TYPE = 'mixed';

    const MIXED_VIRTUAL_COMPONENT_BEFORE = 0;
    const MIXED_VIRTUAL_COMPONENT_AFTER = 1;
    const MIXED_VIRTUAL_COMPONENT_BEFORE_AND_AFTER = 2;

    /**
     * @var ProjectGeneralInfo $projectGeneralInfo
     *
     * @ORM\OneToOne(
     *     targetEntity="ProjectGeneralInfo",
     *     inversedBy="specialistProjectPhase",
     *     cascade={"persist", "remove"}
     * )
     * @Solr\Field(name="reference_number", type="string", getter="getReferenceNumber")
     * @Solr\Field(name="proposal_status", type="string", getter="getProposalStatusText")
     * @Solr\Field(name="proposal_outcome", type="string", getter="getProjectOutcome")
     * @Solr\Field(name="region", type="string", getter="getRegionAcronym")
     * @Solr\Field(name="rpo_ranking", type="integer", getter="getRpoRanking")
     * @Solr\Field(name="relo_locations", type="strings", getter="getReloLocationsToIndex")
     * @Solr\Field(name="relo_ranking", type="integer", getter="getReloRanking")
     * @Solr\Field(name="countries", type="strings", getter="getCountriesToIndex")
     * @Solr\Field(name="cycle_id", type="integer", getter="getCycleId")
     */
    protected $projectGeneralInfo;

    /**
     * @var SpecialistProject $specialistProject
     *
     * @ORM\ManyToOne(targetEntity="SpecialistProject", inversedBy="phases")
     */
    protected $specialistProject;

    /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Assert\Choice(choices = {"in-country", "virtual", "mixed"})
     * @Solr\Field(name="type", type="string", getter="getType")
     */
    protected $type;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $endDate;

    /**
     * @var \DateTime $virtualStartDateBefore
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $virtualStartDateBefore;

    /**
     * @var \DateTime $virtualEndDateBefore
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $virtualEndDateBefore;

    /**
     * @var \DateTime $virtualStartDateAfter
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $virtualStartDateAfter;

    /**
     * @var \DateTime $virtualEndDateAfter
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $virtualEndDateAfter;

    /**
     * @var bool $datesFlexible
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $datesFlexible;

    /**
     * @var string $dateLengthDesc
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $dateLengthDesc;

    /**
     * @var bool $virtualDatesFlexible
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $virtualDatesFlexible;

    /**
     * @var string $virtualDateLengthDesc
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $virtualDateLengthDesc;

    /**
     * @var bool $restDayWeek
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $restDayWeek;

    /**
     * @var string $restDayWeekDesc
     *
     * @ORM\Column(type="text", length=429496729, nullable=true)
     */
    protected $restDayWeekDesc;

    /**
     * @var bool $hoursRequiredDay
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hoursRequiredDay;

    /**
     * @var string $hoursRequiredDayDesc
     *
     * @ORM\Column(type="text", length=429496729, nullable=true)
     */
    protected $hoursRequiredDayDesc;

    /**
     * @var string $sustainabilityDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $sustainabilityDesc;

    /**
     * @var string $deliverablesDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $deliverablesDesc;

    /**
     * @var string $requiredVisaType
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Choice(callback="getRequiredVisaTypeChoices")
     */
    protected $requiredVisaType;

    /**
     * @var string $otherVisaType
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $otherVisaType;
    /**
     * @var int $mixedItineraryOption
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Choice(choices = {0, 1, 2})
     */
    protected $mixedVirtualComponent;

    /**
     * @var VisaAvgLength $visaAvgLength
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialist\VisaAvgLength")
     */
    protected $visaAvgLength;

    /**
     * cascade deletion is defined in related entity
     * @var PrePostWork $prePostWork
     *
     * @ORM\OneToOne(targetEntity="PrePostWork", mappedBy="phase")
     */
    protected $prePostWork;

    /**
     * @var ArrayCollection $partneringOrganizations
     *
     * @ORM\ManyToMany(
     *     targetEntity="AppBundle\Entity\Specialist\PartneringOrganization",
     *     inversedBy="projectsPhases",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="specialist_project_phases_partnering_organizations")
     */
    protected $partneringOrganizations;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="InCountryAssignment",
     *     mappedBy="specialistProjectPhase"
     * )
     * @ORM\OrderBy({"country" = "ASC", "city" = "ASC"})
     */
    protected $inCountryAssignments;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="ItineraryIndex",
     *     mappedBy="phase"
     * )
     * @ORM\OrderBy({"date" = "ASC"})
     */
    protected $itineraryIndexes;

    /**
     * @var ArrayCollection $virtualItineraryWeeks
     * cascade delete defined in Week entity
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Week",
     *     mappedBy="projectPhase"
     * )
     * @ORM\OrderBy({"startDate"="ASC"})
     */
    protected $virtualItineraryWeeks;

    /**
     * @var ArrayCollection $virtualItineraryWeeksAfter
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\WeekAfter",
     *     mappedBy="projectPhase"
     * )
     * @ORM\OrderBy({"startDate"="ASC"})
     */
    protected $virtualItineraryWeeksAfter;

    /**
     * @var ArrayCollection $specialistProjectReviews
     *
     * @ORM\ManyToMany(targetEntity="ProjectReview", mappedBy="specialistProjects")
     */
    protected $specialistProjectReviews;

    /**
     * @var SpecialistProjectPhaseBudget $budget
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhaseBudget",
     *     mappedBy="phase",
     *     cascade={"persist"}
     * )
     */
    protected $budget;

	/**
	 * @var string $title
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Solr\Field(name="title", type="string", getter="getTitle")
	 */
	protected $title;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=4294967295, nullable=true)
	 */
	protected $programDesc;

	/**
	 * @var string $furtherDetailsDesc
	 *
	 * @ORM\Column(type="string", length=4294967295, nullable=true)
	 */
	protected $furtherDetailsDesc;

	/**
	 * @var string $degreeRequirementsDesc
	 *
	 * @ORM\Column(type="string", length=4294967295, nullable=true)
	 */
	protected $degreeRequirementsDesc;

	/**
	 * @var string $proposedCandidate
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $proposedCandidate;

	/**
	 * @var bool $proposedCandidateContacted
	 *
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	protected $proposedCandidateContacted;

	/**
	 * @var string $proposedCandidateEmail
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $proposedCandidateEmail;

	/**
	 * @var string $subsequentPhasesDesc
	 *
	 * @ORM\Column(type="string", length=4294967295, nullable=true)
	 */
	protected $subsequentPhasesDesc;

	/**
	 * @var ArrayCollection $areasOfExpertise
	 *
	 * @ORM\ManyToMany(targetEntity="AreaOfExpertise")
	 * @ORM\JoinTable(name="specialist_project_phases_areas_of_expertise")
	 */
	protected $areasOfExpertise;

	/**
	 * @var SpecialistProjectPhase
	 * @ORM\ManyToOne(targetEntity="SpecialistProjectPhase")
	 * @ORM\JoinColumn(name="source_phase_id", referencedColumnName="id", onDelete="SET NULL")
	 *
	 */
	protected $sourcePhase;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	protected $candidateSame;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	protected $candidateSameContacted;

	/**
	 * @var boolean
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	protected $candidateSameNewVisa;

	/******************************** Methods ****************************/

    /**
     * SpecialistProjectPhase constructor.
     *
     * @param SpecialistProject  $specialistProject  The Specialist project that
     *                                               this phase will belong to.
     * @param ProjectGeneralInfo $projectGeneralInfo The general information
     *                                               part of the project phase.
     */
    public function __construct(SpecialistProject $specialistProject, ProjectGeneralInfo $projectGeneralInfo)
    {
        $this->specialistProject = $specialistProject;
        $this->projectGeneralInfo = $projectGeneralInfo;

        $this->budget = new SpecialistProjectPhaseBudget($this);

        $this->partneringOrganizations = new ArrayCollection();
        $this->virtualItineraryWeeks = new ArrayCollection();
        $this->virtualItineraryWeeksAfter = new ArrayCollection();
    }

    /**
     * @return array Type of choices.
     */
    public static function getTypeChoices()
    {
        return [
            'Entirely In-Country' => 'in-country',
            'Entirely Virtual' => 'virtual',
            'Mixed (in-country component and virtual component)' => 'mixed',
        ];
    }

    /**
     * @return array Type of choices.
     */
    public static function getRequiredVisaTypeChoices()
    {
        return [
            'Airport' => 'airport',
            'Business' => 'business',
            'E-Visa' => 'e-visa',
            'No Visa Required' => 'no-visa',
            'Other' => 'other',
            'Tourist' => 'tourist',
        ];
    }

    /**
     * @return array Type of Mixed Virtual Itinerary choices.
     */
    public static function getMixedVirtualComponentChoices()
    {
        return [
            'Before' => 0,
            'After' => 1,
            'Before and After' => 2,
        ];
    }

    /**
     * Return the humanize version of the Specialist phase type.
     *
     * @return string The humanized phase type.
     */
    public function getHumanizedType()
    {
        return array_search($this->type, self::getTypeChoices(), true);
    }

    /**
     * Set projectGeneralInfo
     *
     * @param ProjectGeneralInfo $projectGeneralInfo
     *
     * @return SpecialistProjectPhase
     */
    public function setProjectGeneralInfo(
      ProjectGeneralInfo $projectGeneralInfo = null
    ) {
        $this->projectGeneralInfo = $projectGeneralInfo;

        return $this;
    }

    /**
     * Get projectGeneralInfo
     *
     * @return ProjectGeneralInfo
     */
    public function getProjectGeneralInfo()
    {
        return $this->projectGeneralInfo;
    }

    /**
     * Set specialistProject
     *
     * @param SpecialistProject $specialistProject
     *
     * @return SpecialistProjectPhase
     */
    public function setSpecialistProject(
      SpecialistProject $specialistProject = null
    ) {
        $this->specialistProject = $specialistProject;

        return $this;
    }

    /**
     * Get specialistProject
     *
     * @return SpecialistProject
     */
    public function getSpecialistProject()
    {
        return $this->specialistProject;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return SpecialistProjectPhase
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return SpecialistProjectPhase
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return SpecialistProjectPhase
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }


    /**
     * Set virtualStartDateBefore
     *
     * @param \DateTime $virtualStartDateBefore
     *
     * @return SpecialistProjectPhase
     */
    public function setVirtualStartDateBefore($virtualStartDateBefore)
    {
        $this->virtualStartDateBefore = $virtualStartDateBefore;

        return $this;
    }

    /**
     * Get virtualStartDateBefore
     *
     * @return \DateTime
     */
    public function getVirtualStartDateBefore()
    {
        return $this->virtualStartDateBefore;
    }

    /**
     * Set virtualEndDateBefore
     *
     * @param \DateTime $virtualEndDateBefore
     *
     * @return SpecialistProjectPhase
     */
    public function setVirtualEndDateBefore($virtualEndDateBefore)
    {
        $this->virtualEndDateBefore = $virtualEndDateBefore;

        return $this;
    }

    /**
     * Get virtualEndDateBefore
     *
     * @return \DateTime
     */
    public function getVirtualEndDateBefore()
    {
        return $this->virtualEndDateBefore;
    }

    /**
     * Set virtualStartDateAfter
     *
     * @param \DateTime $virtualStartDateAfter
     *
     * @return SpecialistProjectPhase
     */
    public function setVirtualStartDateAfter($virtualStartDateAfter)
    {
        $this->virtualStartDateAfter = $virtualStartDateAfter;

        return $this;
    }

    /**
     * Get virtualStartDateAfter
     *
     * @return \DateTime
     */
    public function getVirtualStartDateAfter()
    {
        return $this->virtualStartDateAfter;
    }

    /**
     * Set virtualEndDateAfter
     *
     * @param \DateTime $virtualEndDateAfter
     *
     * @return SpecialistProjectPhase
     */
    public function setVirtualEndDateAfter($virtualEndDateAfter)
    {
        $this->virtualEndDateAfter = $virtualEndDateAfter;

        return $this;
    }

    /**
     * Get virtualEndDateAfter
     *
     * @return \DateTime
     */
    public function getVirtualEndDateAfter()
    {
        return $this->virtualEndDateAfter;
    }

    /**
     * Set datesFlexible
     *
     * @param boolean $datesFlexible
     *
     * @return SpecialistProjectPhase
     */
    public function setDatesFlexible($datesFlexible)
    {
        $this->datesFlexible = $datesFlexible;

        return $this;
    }

    /**
     * Get datesFlexible
     *
     * @return boolean
     */
    public function getDatesFlexible()
    {
        return $this->datesFlexible;
    }

    /**
     * Set dateLengthDesc
     *
     * @param string $dateLengthDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setDateLengthDesc($dateLengthDesc)
    {
        $this->dateLengthDesc = $dateLengthDesc;

        return $this;
    }

    /**
     * Get dateLengthDesc
     *
     * @return string
     */
    public function getDateLengthDesc()
    {
        return $this->dateLengthDesc;
    }

    /**
     * Set virtualDatesFlexible
     *
     * @param boolean $virtualDatesFlexible
     *
     * @return SpecialistProjectPhase
     */
    public function setVirtualDatesFlexible($virtualDatesFlexible)
    {
        $this->virtualDatesFlexible = $virtualDatesFlexible;

        return $this;
    }

    /**
     * Get virtualDatesFlexible
     *
     * @return boolean
     */
    public function getVirtualDatesFlexible()
    {
        return $this->virtualDatesFlexible;
    }

    /**
     * Set virtualDateLengthDesc
     *
     * @param string $virtualDateLengthDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setVirtualDateLengthDesc($virtualDateLengthDesc)
    {
        $this->virtualDateLengthDesc = $virtualDateLengthDesc;

        return $this;
    }

    /**
     * Get virtualDateLengthDesc
     *
     * @return string
     */
    public function getVirtualDateLengthDesc()
    {
        return $this->virtualDateLengthDesc;
    }

    /**
     * Set sustainabilityDesc
     *
     * @param string $sustainabilityDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setSustainabilityDesc($sustainabilityDesc)
    {
        $this->sustainabilityDesc = $sustainabilityDesc;

        return $this;
    }

    /**
     * Get sustainabilityDesc
     *
     * @return string
     */
    public function getSustainabilityDesc()
    {
        return $this->sustainabilityDesc;
    }

    /**
     * Set deliverablesDesc
     *
     * @param string $deliverablesDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setDeliverablesDesc($deliverablesDesc)
    {
        $this->deliverablesDesc = $deliverablesDesc;

        return $this;
    }

    /**
     * Get deliverablesDesc
     *
     * @return string
     */
    public function getDeliverablesDesc()
    {
        return $this->deliverablesDesc;
    }

    /**
     * Set requiredVisaType
     *
     * @param string $requiredVisaType
     *
     * @return SpecialistProjectPhase
     */
    public function setRequiredVisaType($requiredVisaType)
    {
        $this->requiredVisaType = $requiredVisaType;

        return $this;
    }

    /**
     * Get requiredVisaType
     *
     * @return string
     */
    public function getRequiredVisaType()
    {
        return $this->requiredVisaType;
    }

    /**
     * Set otherVisaType
     *
     * @param string $otherVisaType
     *
     * @return SpecialistProjectPhase
     */
    public function setOtherVisaType($otherVisaType)
    {
        $this->otherVisaType = $otherVisaType;

        return $this;
    }

    /**
     * Get otherVisaType
     *
     * @return string
     */
    public function getOtherVisaType()
    {
        return $this->otherVisaType;
    }

    /**
     * Set mixedVirtualComponent
     *
     * @param integer $mixedVirtualComponent
     *
     * @return SpecialistProjectPhase
     */
    public function setMixedVirtualComponent($mixedVirtualComponent)
    {
        $this->mixedVirtualComponent = $mixedVirtualComponent;

        return $this;
    }

    /**
     * Get mixedVirtualComponent
     *
     * @return integer
     */
    public function getMixedVirtualComponent()
    {
        return $this->mixedVirtualComponent;
    }

    /**
     * Set visaAvgLength
     *
     * @param \AppBundle\Entity\Specialist\VisaAvgLength $visaAvgLength
     *
     * @return SpecialistProjectPhase
     */
    public function setVisaAvgLength(
      \AppBundle\Entity\Specialist\VisaAvgLength $visaAvgLength = null
    ) {
        $this->visaAvgLength = $visaAvgLength;

        return $this;
    }

    /**
     * Get visaAvgLength
     *
     * @return \AppBundle\Entity\Specialist\VisaAvgLength
     */
    public function getVisaAvgLength()
    {
        return $this->visaAvgLength;
    }

    /**
     * Set prePostWork
     *
     * @param PrePostWork $prePostWork
     *
     * @return SpecialistProjectPhase
     */
    public function setPrePostWork(PrePostWork $prePostWork = null)
    {
        $this->prePostWork = $prePostWork;

        return $this;
    }

    /**
     * Get prePostWork
     *
     * @return PrePostWork
     */
    public function getPrePostWork()
    {
        return $this->prePostWork;
    }

    /**
     * Add partneringOrganization
     *
     * @param PartneringOrganization $partneringOrganization
     *
     * @return SpecialistProjectPhase
     */
    public function addPartneringOrganization(
      PartneringOrganization $partneringOrganization
    ) {
        $this->partneringOrganizations[] = $partneringOrganization;

        return $this;
    }

    /**
     * Remove partneringOrganization
     *
     * @param PartneringOrganization $partneringOrganization
     */
    public function removePartneringOrganization(
      PartneringOrganization $partneringOrganization
    ) {
        $this->partneringOrganizations->removeElement($partneringOrganization);
    }

    /**
     * Get partneringOrganizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartneringOrganizations()
    {
        return $this->partneringOrganizations;
    }

    /**
     * Add inCountryAssignment
     *
     * @param InCountryAssignment $inCountryAssignment
     *
     * @return SpecialistProjectPhase
     */
    public function addInCountryAssignment(
      InCountryAssignment $inCountryAssignment
    ) {
        $this->inCountryAssignments[] = $inCountryAssignment;

        return $this;
    }

    /**
     * Remove inCountryAssignment
     *
     * @param InCountryAssignment $inCountryAssignment
     */
    public function removeInCountryAssignment(
      InCountryAssignment $inCountryAssignment
    ) {
        $this->inCountryAssignments->removeElement($inCountryAssignment);
    }

    /**
     * Get inCountryAssignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInCountryAssignments()
    {
        return $this->inCountryAssignments;
    }

    /**
     * Set budget
     *
     * @param SpecialistProjectPhaseBudget $budget
     *
     * @return SpecialistProjectPhase
     */
    public function setBudget(SpecialistProjectPhaseBudget $budget = null)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set restDayWeek
     *
     * @param boolean $restDayWeek
     *
     * @return SpecialistProjectPhase
     */
    public function setRestDayWeek($restDayWeek)
    {
        $this->restDayWeek = $restDayWeek;

        return $this;
    }

    /**
     * Get restDayWeek
     *
     * @return boolean
     */
    public function getRestDayWeek()
    {
        return $this->restDayWeek;
    }

    /**
     * Set restDayWeekDesc
     *
     * @param string $restDayWeekDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setRestDayWeekDesc($restDayWeekDesc)
    {
        $this->restDayWeekDesc = $restDayWeekDesc;

        return $this;
    }

    /**
     * Get restDayWeekDesc
     *
     * @return string
     */
    public function getRestDayWeekDesc()
    {
        return $this->restDayWeekDesc;
    }

    /**
     * Set hoursRequiredDay
     *
     * @param boolean $hoursRequiredDay
     *
     * @return SpecialistProjectPhase
     */
    public function setHoursRequiredDay($hoursRequiredDay)
    {
        $this->hoursRequiredDay = $hoursRequiredDay;

        return $this;
    }

    /**
     * Get hoursRequiredDay
     *
     * @return boolean
     */
    public function getHoursRequiredDay()
    {
        return $this->hoursRequiredDay;
    }

    /**
     * Set hoursRequiredDayDesc
     *
     * @param string $hoursRequiredDayDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setHoursRequiredDayDesc($hoursRequiredDayDesc)
    {
        $this->hoursRequiredDayDesc = $hoursRequiredDayDesc;

        return $this;
    }

    /**
     * Get hoursRequiredDayDesc
     *
     * @return string
     */
    public function getHoursRequiredDayDesc()
    {
        return $this->hoursRequiredDayDesc;
    }


    /**
     * Add itineraryIndex
     *
     * @param \AppBundle\Entity\ItineraryIndex $itineraryIndex
     *
     * @return SpecialistProjectPhase
     */
    public function addItineraryIndex(\AppBundle\Entity\ItineraryIndex $itineraryIndex)
    {
        $this->itineraryIndexes[] = $itineraryIndex;

        return $this;
    }

    /**
     * Remove itineraryIndex
     *
     * @param \AppBundle\Entity\ItineraryIndex $itineraryIndex
     */
    public function removeItineraryIndex(\AppBundle\Entity\ItineraryIndex $itineraryIndex)
    {
        $this->itineraryIndexes->removeElement($itineraryIndex);
    }

    /**
     * Get itineraryIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItineraryIndexes()
    {
        return $this->itineraryIndexes;
    }

    /**
     * Add virtualItineraryWeek
     *
     * @param Week $virtualItineraryWeek
     *
     * @return SpecialistProjectPhase
     */
    public function addVirtualItineraryWeek(Week $virtualItineraryWeek)
    {
        $this->virtualItineraryWeeks[] = $virtualItineraryWeek;

        return $this;
    }

    /**
     * Remove virtualItineraryWeek
     *
     * @param Week $virtualItineraryWeek
     */
    public function removeVirtualItineraryWeek(Week $virtualItineraryWeek)
    {
        $this->virtualItineraryWeeks->removeElement($virtualItineraryWeek);
    }

    /**
     * Get virtualItineraryWeeks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVirtualItineraryWeeks()
    {
        return $this->virtualItineraryWeeks;
    }

    /**
     * Add virtualItineraryWeeksAfter
     *
     * @param WeekAfter $virtualItineraryWeeksAfter
     *
     * @return SpecialistProjectPhase
     */
    public function addVirtualItineraryWeeksAfter(WeekAfter $virtualItineraryWeeksAfter)
    {
        $this->virtualItineraryWeeksAfter[] = $virtualItineraryWeeksAfter;

        return $this;
    }

    /**
     * Remove virtualItineraryWeeksAfter
     *
     * @param WeekAfter $virtualItineraryWeeksAfter
     */
    public function removeVirtualItineraryWeeksAfter(WeekAfter $virtualItineraryWeeksAfter)
    {
        $this->virtualItineraryWeeksAfter->removeElement($virtualItineraryWeeksAfter);
    }

    /**
     * Get virtualItineraryWeeksAfter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVirtualItineraryWeeksAfter()
    {
        return $this->virtualItineraryWeeksAfter;
    }

    /**
     * Add specialistProjectReview
     *
     * @param \AppBundle\Entity\ProjectReview $specialistProjectReview
     *
     * @return SpecialistProjectPhase
     */
    public function addSpecialistProjectReview(\AppBundle\Entity\ProjectReview $specialistProjectReview)
    {
        $this->specialistProjectReviews[] = $specialistProjectReview;

        return $this;
    }

    /**
     * Remove specialistProjectReview
     *
     * @param \AppBundle\Entity\ProjectReview $specialistProjectReview
     */
    public function removeSpecialistProjectReview(\AppBundle\Entity\ProjectReview $specialistProjectReview)
    {
        $this->specialistProjectReviews->removeElement($specialistProjectReview);
    }

    /**
     * Get specialistProjectReviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialistProjectReviews()
    {
        return $this->specialistProjectReviews;
    }

	/**
	 * Set title
	 *
	 * @param string $title
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set Program Description
	 *
	 * @param string
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setProgramDesc($description)
	{
		$this->programDesc = $description;

		return $this;
	}

	/**
	 * Get Program Description
	 *
	 * @return string
	 */
	public function getProgramDesc()
	{
		return $this->programDesc;
	}

	/**
	 * Set furtherDetailsDesc
	 *
	 * @param string $furtherDetailsDesc
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setFurtherDetailsDesc($furtherDetailsDesc)
	{
		$this->furtherDetailsDesc = $furtherDetailsDesc;

		return $this;
	}

	/**
	 * Get furtherDetailsDesc
	 *
	 * @return string
	 */
	public function getFurtherDetailsDesc()
	{
		return $this->furtherDetailsDesc;
	}

	/**
	 * Set degreeRequirementsDesc
	 *
	 * @param string $degreeRequirementsDesc
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setDegreeRequirementsDesc($degreeRequirementsDesc)
	{
		$this->degreeRequirementsDesc = $degreeRequirementsDesc;

		return $this;
	}

	/**
	 * Get degreeRequirementsDesc
	 *
	 * @return string
	 */
	public function getDegreeRequirementsDesc()
	{
		return $this->degreeRequirementsDesc;
	}

	/**
	 * Set proposedCandidate
	 *
	 * @param string $proposedCandidate
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setProposedCandidate($proposedCandidate)
	{
		$this->proposedCandidate = $proposedCandidate;

		return $this;
	}

	/**
	 * Get proposedCandidate
	 *
	 * @return string
	 */
	public function getProposedCandidate()
	{
		return $this->proposedCandidate;
	}

	/**
	 * Set proposedCandidateContacted
	 *
	 * @param boolean $proposedCandidateContacted
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setProposedCandidateContacted($proposedCandidateContacted)
	{
		$this->proposedCandidateContacted = $proposedCandidateContacted;

		return $this;
	}

	/**
	 * Get proposedCandidateContacted
	 *
	 * @return boolean
	 */
	public function getProposedCandidateContacted()
	{
		return $this->proposedCandidateContacted;
	}

	/**
	 * Set proposedCandidateEmail
	 *
	 * @param string $proposedCandidateEmail
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setProposedCandidateEmail($proposedCandidateEmail)
	{
		$this->proposedCandidateEmail = $proposedCandidateEmail;

		return $this;
	}

	/**
	 * Get proposedCandidateEmail
	 *
	 * @return string
	 */
	public function getProposedCandidateEmail()
	{
		return $this->proposedCandidateEmail;
	}

	/**
	 * Add areasOfExpertise
	 *
	 * @param AreaOfExpertise $areasOfExpertise
	 *
	 * @return SpecialistProjectPhase
	 */
	public function addAreasOfExpertise(AreaOfExpertise $areasOfExpertise)
	{
		$this->areasOfExpertise[] = $areasOfExpertise;

		return $this;
	}

	/**
	 * Remove areasOfExpertise
	 *
	 * @param AreaOfExpertise $areasOfExpertise
	 */
	public function removeAreasOfExpertise(AreaOfExpertise $areasOfExpertise)
	{
		$this->areasOfExpertise->removeElement($areasOfExpertise);
	}

	/**
	 * Get areasOfExpertise
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getAreasOfExpertise()
	{
		return $this->areasOfExpertise;
	}

	/**
	 * Set subsequentPhasesDesc
	 *
	 * @param string $subsequentPhasesDesc
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setSubsequentPhasesDesc($subsequentPhasesDesc)
	{
		$this->subsequentPhasesDesc = $subsequentPhasesDesc;

		return $this;
	}

	/**
	 * Get subsequentPhasesDesc
	 *
	 * @return string
	 */
	public function getSubsequentPhasesDesc()
	{
		return $this->subsequentPhasesDesc;
	}

	/**
	 * Set sourcePhase
	 *
	 * @param SpecialistProjectPhase $sourcePhase
	 *
	 * @return SpecialistProjectPhase
	 */
	public function setSourcePhase($sourcePhase)
	{
		$this->sourcePhase = $sourcePhase;

		return $this;
	}

	/**
	 * Get sourcePhase
	 *
	 * @return SpecialistProjectPhase
	 */
	public function getSourcePhase()
	{
		return $this->sourcePhase;
	}


	/********************* Other support functions ***********************/
	/**
	 * Gets the duration of an In-Country or mixed project,
	 * @return int
	 */
	public function getInCountryDuration() {
		$days = 0;
		if ($this->startDate && $this->endDate) {
			$lengthOfAssignment = $this->startDate->diff($this->endDate);
			$days = $lengthOfAssignment->days + 1;  // +1 adjustment due to endDate time considered as 00:00:00
		}

		return $days;
	}

	/**
	 * Checks if at least one of the virtual component dates has been defined
	 * @return bool
	 */
	public function checkVirtualComponentDates() {
		if ($this->virtualStartDateBefore || $this->virtualEndDateBefore
			|| $this->virtualStartDateAfter || $this->virtualEndDateAfter) {
			return true;
		}

		return false;
	}



    /**
     * Set candidateSame
     *
     * @param boolean $candidateSame
     *
     * @return SpecialistProjectPhase
     */
    public function setCandidateSame($candidateSame)
    {
        $this->candidateSame = $candidateSame;

        return $this;
    }

    /**
     * Get candidateSame
     *
     * @return boolean
     */
    public function getCandidateSame()
    {
        return $this->candidateSame;
    }

    /**
     * Set candidateSameContacted
     *
     * @param boolean $candidateSameContacted
     *
     * @return SpecialistProjectPhase
     */
    public function setCandidateSameContacted($candidateSameContacted)
    {
        $this->candidateSameContacted = $candidateSameContacted;

        return $this;
    }

    /**
     * Get candidateSameContacted
     *
     * @return boolean
     */
    public function getCandidateSameContacted()
    {
        return $this->candidateSameContacted;
    }

    /**
     * Set candidateSameNewVisa
     *
     * @param boolean $candidateSameNewVisa
     *
     * @return SpecialistProjectPhase
     */
    public function setCandidateSameNewVisa($candidateSameNewVisa)
    {
        $this->candidateSameNewVisa = $candidateSameNewVisa;

        return $this;
    }

    /**
     * Get candidateSameNewVisa
     *
     * @return boolean
     */
    public function getCandidateSameNewVisa()
    {
        return $this->candidateSameNewVisa;
    }
}
