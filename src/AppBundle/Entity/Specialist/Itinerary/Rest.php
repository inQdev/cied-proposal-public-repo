<?php

namespace AppBundle\Entity\Specialist\Itinerary;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\InCountryAssignment;
use Doctrine\ORM\Mapping as ORM;

/**
 * Travel entity.
 *
 * @package AppBundle\Entity\Specialist\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\Itinerary\RestRepository")
 * @ORM\Table(name="itinerary_rest")
 */
class Rest extends BaseEntity
{
    /**
     * @var \DateTime $date
     *
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * @var InCountryAssignment $inCountryAssignment
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InCountryAssignment")
     * @ORM\JoinColumn(
     *     name="in_country_assignment_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $inCountryAssignment;

    /**
     * Rest constructor.
     *
     * @param InCountryAssignment $inCountryAssignment
     */
    public function __construct(InCountryAssignment $inCountryAssignment)
    {
        $this->inCountryAssignment = $inCountryAssignment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Rest
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set inCountryAssignment
     *
     * @param InCountryAssignment $inCountryAssignment
     *
     * @return Rest
     */
    public function setInCountryAssignment(
      InCountryAssignment $inCountryAssignment = null
    ) {
        $this->inCountryAssignment = $inCountryAssignment;

        return $this;
    }

    /**
     * Get inCountryAssignment
     *
     * @return InCountryAssignment
     */
    public function getInCountryAssignment()
    {
        return $this->inCountryAssignment;
    }
}
