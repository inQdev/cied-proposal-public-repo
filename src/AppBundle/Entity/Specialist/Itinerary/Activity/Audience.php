<?php

namespace AppBundle\Entity\Specialist\Itinerary\Activity;

use AppBundle\Entity\BaseTerm;
use Doctrine\ORM\Mapping as ORM;

/**
 * Audience entity.
 *
 * @package AppBundle\Entity\Specialist\Itinerary\Activity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class Audience extends BaseTerm
{
}
