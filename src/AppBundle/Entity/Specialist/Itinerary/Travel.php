<?php

namespace AppBundle\Entity\Specialist\Itinerary;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\InCountryAssignment;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Travel entity.
 *
 * @package AppBundle\Entity\Specialist\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\Itinerary\TravelRepository")
 * @ORM\Table(name="itinerary_travel")
 */
class Travel extends BaseEntity
{
    /**
     * @var \DateTime $date
     *
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * @var string $mode
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $mode;

    /**
     * @var string $travelTime
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $travelTime;

    /**
     * @var string $bookingResponsible
     *
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Assert\Choice(
     *     choices = {"georgetown", "post", "partnering-organization, specialist"}
     * )
     */
    protected $bookingResponsible;

    /**
     * @var InCountryAssignment $origin
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InCountryAssignment")
     * @ORM\JoinColumn(
     *     name="origin_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $origin;

    /**
     * @var InCountryAssignment $destination
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InCountryAssignment")
     * @ORM\JoinColumn(
     *     name="destination_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $destination;

    /**
     * Travel constructor.
     *
     * @param InCountryAssignment $origin
     * @param InCountryAssignment $destination
     */
    public function __construct(
      InCountryAssignment $origin,
      InCountryAssignment $destination
    ) {
        $this->origin = $origin;
        $this->destination = $destination;
    }

    /**
     * @return array Type of choices.
     */
    public static function getBookingResponsibleChoices()
    {
        return [
          'Georgetown'              => 'georgetown',
          'Post'                    => 'post',
          'Partnering Organization' => 'partnering-organization',
          'Specialist'              => 'specialist',
        ];
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Travel
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return Travel
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set travelTime
     *
     * @param string $travelTime
     *
     * @return Travel
     */
    public function setTravelTime($travelTime)
    {
        $this->travelTime = $travelTime;

        return $this;
    }

    /**
     * Get travelTime
     *
     * @return string
     */
    public function getTravelTime()
    {
        return $this->travelTime;
    }

    /**
     * Set bookingResponsible
     *
     * @param string $bookingResponsible
     *
     * @return Travel
     */
    public function setBookingResponsible($bookingResponsible)
    {
        $this->bookingResponsible = $bookingResponsible;

        return $this;
    }

    /**
     * Get bookingResponsible
     *
     * @return string
     */
    public function getBookingResponsible()
    {
        return $this->bookingResponsible;
    }

    /**
     * Set origin
     *
     * @param InCountryAssignment $origin
     *
     * @return Travel
     */
    public function setOrigin(InCountryAssignment $origin = null)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return InCountryAssignment
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set destination
     *
     * @param InCountryAssignment $destination
     *
     * @return Travel
     */
    public function setDestination(InCountryAssignment $destination = null)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return InCountryAssignment
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
