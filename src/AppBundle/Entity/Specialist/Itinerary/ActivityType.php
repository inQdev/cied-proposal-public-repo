<?php

namespace AppBundle\Entity\Specialist\Itinerary;

use AppBundle\Entity\BaseTerm;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityType entity.
 *
 * @package AppBundle\Entity
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 */
class ActivityType extends BaseTerm
{
}
