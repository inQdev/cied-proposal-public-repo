<?php

namespace AppBundle\Entity\Specialist;

use AppBundle\Entity\AbstractCycle;
use Doctrine\ORM\Mapping as ORM;

/**
 * CycleSpecialist entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CycleSpecialistRepository")
 */
class CycleSpecialist extends AbstractCycle
{
}
