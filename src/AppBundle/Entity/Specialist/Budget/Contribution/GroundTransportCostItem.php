<?php

namespace AppBundle\Entity\Specialist\Budget\Contribution;

use AppBundle\Entity\Common\BaseContributionItem;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use AppBundle\Entity\CostItem;

/**
 * GroundTransportCostItem entity
 *
 * @package AppBundle\Entity\Specialist\Budget\Contribution
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_ground_transport_cost_item")
 * @ORM\HasLifecycleCallbacks
 */
class GroundTransportCostItem extends BaseContributionItem
{
	/**
	 * @var  $phaseBudget
	 *
	 * @ORM\OneToOne(
	 *     targetEntity="AppBundle\Entity\SpecialistProjectPhaseBudget",
	 *     inversedBy="contributionGroundTransport"
	 * )
	 * @ORM\JoinColumn(
	 *     name="phase_budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $phaseBudget;

	/**
	 * @var CostItem $costItem
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CostItem")
	 */
	protected $costItem;

	/********************* Methods Definitions ******************************/
	/**
	 * constructor.
	 *
	 * @param SpecialistProjectPhaseBudget $phaseBudget
	 */
	public function __construct(SpecialistProjectPhaseBudget $phaseBudget)
	{
		$this->phaseBudget = $phaseBudget;
	}

	/**
	 * Set phaseBudget
	 *
	 * @param SpecialistProjectPhaseBudget $phaseBudget
	 *
	 * @return GroundTransportCostItem
	 */
	public function setPhaseBudget(SpecialistProjectPhaseBudget $phaseBudget = null) {
		$this->phaseBudget = $phaseBudget;

		return $this;
	}

	/**
	 * Get phaseBudget
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function getPhaseBudget()
	{
		return $this->phaseBudget;
	}

	/**
	 * @ORM\PreUpdate
	 */
	public function calculateTotals()
	{
		$fundedBy = $this->phaseBudget->getFundedBy();

		if ('ECA' === $fundedBy) {
			$this->calculateECATotals();
		} elseif ('Post' === $fundedBy) {
			$this->calculatePostTotals();
		}
	}

	/**
	 * Set costItem
	 *
	 * @param CostItem $costItem
	 *
	 * @return GroundTransportCostItem
	 */
	public function setCostItem(CostItem $costItem = null)
	{
		$this->costItem = $costItem;

		return $this;
	}

	/**
	 * Get costItem
	 *
	 * @return \AppBundle\Entity\CostItem
	 */
	public function getCostItem()
	{
		return $this->costItem;
	}

	/**
	 * @param string $fundingType (not used)
	 * @return int
	 */
	public function checkTotals($fundingType="")
	{
		$fundingType = $this->getPhaseBudget()->getFundedBy();
		return parent::checkTotals($fundingType);
	}
}
