<?php

namespace AppBundle\Entity\Specialist\Budget\Contribution;

use AppBundle\Entity\Common\BaseContributionItem;
use AppBundle\Entity\Specialist\Itinerary\Travel;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * TransportationCostItem entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Entity @HasLifecycleCallbacks
 * @ORM\Table(name="contribution_transportation_cost_item")
 */
class TransportationCostItem extends BaseContributionItem
{
    /**
     * @var Travel $travel
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Itinerary\Travel",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="travel_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $travel;

    /**
     * @var Transportation $transportation
     *
     * @ORM\ManyToOne(
     *     targetEntity="Transportation",
     *     inversedBy="transportationCostItems"
     * )
     * @ORM\JoinColumn(
     *     name="contribution_transportation_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $transportation;

    /************************** Methods Definitions ********************************/

    /**
     * TransportationCostItem constructor.
     *
     * @param Transportation $transportation
     */
    public function __construct(Transportation $transportation)
    {
        $this->transportation = $transportation;
    }

    /**
     * @ORM\PreUpdate
     */
    public function calculateTotals()
    {
        $fundedBy = $this->transportation->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set totalCost
     *
     * @param float $totalCost
     *
     * @return TransportationCostItem
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return float
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set postContribution
     *
     * @param float $postContribution
     *
     * @return TransportationCostItem
     */
    public function setPostContribution($postContribution)
    {
        $this->postContribution = $postContribution;

        return $this;
    }

    /**
     * Get postContribution
     *
     * @return float
     */
    public function getPostContribution()
    {
        return $this->postContribution;
    }

    /**
     * Set hostContributionMonetary
     *
     * @param float $hostContributionMonetary
     *
     * @return TransportationCostItem
     */
    public function setHostContributionMonetary($hostContributionMonetary)
    {
        $this->hostContributionMonetary = $hostContributionMonetary;

        return $this;
    }

    /**
     * Get hostContributionMonetary
     *
     * @return float
     */
    public function getHostContributionMonetary()
    {
        return $this->hostContributionMonetary;
    }

    /**
     * Set hostContributionInKind
     *
     * @param float $hostContributionInKind
     *
     * @return TransportationCostItem
     */
    public function setHostContributionInKind($hostContributionInKind)
    {
        $this->hostContributionInKind = $hostContributionInKind;

        return $this;
    }

    /**
     * Get hostContributionInKind
     *
     * @return float
     */
    public function getHostContributionInKind()
    {
        return $this->hostContributionInKind;
    }

    /**
     * Set postHostTotalContribution
     *
     * @param float $postHostTotalContribution
     *
     * @return TransportationCostItem
     */
    public function setPostHostTotalContribution($postHostTotalContribution)
    {
        $this->postHostTotalContribution = $postHostTotalContribution;

        return $this;
    }

    /**
     * Get postHostTotalContribution
     *
     * @return float
     */
    public function getPostHostTotalContribution()
    {
        return $this->postHostTotalContribution;
    }

    /**
     * Set postTotalContribution
     *
     * @param float $postTotalContribution
     *
     * @return TransportationCostItem
     */
    public function setPostTotalContribution($postTotalContribution)
    {
        $this->postTotalContribution = $postTotalContribution;

        return $this;
    }

    /**
     * Get postTotalContribution
     *
     * @return float
     */
    public function getPostTotalContribution()
    {
        return $this->postTotalContribution;
    }

    /**
     * Set ecaTotalContribution
     *
     * @param float $ecaTotalContribution
     *
     * @return TransportationCostItem
     */
    public function setEcaTotalContribution($ecaTotalContribution)
    {
        $this->ecaTotalContribution = $ecaTotalContribution;

        return $this;
    }

    /**
     * Get ecaTotalContribution
     *
     * @return float
     */
    public function getEcaTotalContribution()
    {
        return $this->ecaTotalContribution;
    }

    /**
     * Set travel
     *
     * @param Travel $travel
     *
     * @return TransportationCostItem
     */
    public function setTravel(Travel $travel = null)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * Set transportation
     *
     * @param Transportation $transportation
     *
     * @return TransportationCostItem
     */
    public function setTransportation(Transportation $transportation = null)
    {
        $this->transportation = $transportation;

        return $this;
    }

    /**
     * Get transportation
     *
     * @return Transportation
     */
    public function getTransportation()
    {
        return $this->transportation;
    }

	/**
	 * @param string $fundingType (not used)
	 * @return int
	 */
	public function checkTotals($fundingType="")
	{
		$fundingType = $this->getTransportation()->getPhaseBudget()->getFundedBy();
		return parent::checkTotals($fundingType);
	}

}
