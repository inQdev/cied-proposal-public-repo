<?php

namespace AppBundle\Entity\Specialist\Budget\Contribution;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\SpecialistProjectPhaseBudget;

/**
 * Transportation entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_transportation")
 */
class Transportation extends BaseEntity
{
    /**
     * @var SpecialistProjectPhaseBudget $phaseBudget
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\SpecialistProjectPhaseBudget",
     *     inversedBy="contributionTransportation"
     * )
     * @ORM\JoinColumn(
     *     name="phase_budget_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $phaseBudget;

    /**
     * @var ArrayCollection $transportationCostItems
     *
     * @ORM\OneToMany(
     *     targetEntity="TransportationCostItem",
     *     mappedBy="transportation",
     *     cascade={"persist"}
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $transportationCostItems;

	/******************************* Methods Definitions *********************************/

    /**
     * Transportation constructor.
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     */
    public function __construct(SpecialistProjectPhaseBudget $phaseBudget)
    {
        $this->phaseBudget = $phaseBudget;
        $this->transportationCostItems = new ArrayCollection();
    }

    /**
     *
     */
    public function calculateSummaryTotals() {
        $summaryTotals = [
          'cost'          => 0,
          'post'          => 0,
          'hostMonetary'  => 0,
          'hostInKind'    => 0,
          'postHostTotal' => 0,
          'postTotal'     => 0,
          'ecaTotal'      => 0,
        ];

        /** @var TransportationCostItem $costItem */
        foreach ($this->transportationCostItems as $costItem) {
            $summaryTotals['cost'] += $costItem->getTotalCost();
            $summaryTotals['post'] += $costItem->getPostContribution();
            $summaryTotals['hostMonetary'] += $costItem->getHostContributionMonetary();
            $summaryTotals['hostInKind'] += $costItem->getHostContributionInKind();
            $summaryTotals['postHostTotal'] += $costItem->getPostHostTotalContribution();
            $summaryTotals['postTotal'] += $costItem->getPostTotalContribution();
            $summaryTotals['ecaTotal'] += $costItem->getEcaTotalContribution();
        }

        return $summaryTotals;
    }

    /**
     * Set phaseBudget
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     *
     * @return Transportation
     */
    public function setPhaseBudget(
      SpecialistProjectPhaseBudget $phaseBudget = null
    ) {
        $this->phaseBudget = $phaseBudget;

        return $this;
    }

    /**
     * Get phaseBudget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getPhaseBudget()
    {
        return $this->phaseBudget;
    }

    /**
     * Add contributionTransportationCostItem
     *
     * @param TransportationCostItem $transportationCostItem
     *
     * @return Transportation
     */
    public function addTransportationCostItem(
	    TransportationCostItem $transportationCostItem
    ) {
        $this->transportationCostItems[] = $transportationCostItem;

        return $this;
    }

    /**
     * Remove contributionTransportationCostItem
     *
     * @param TransportationCostItem $transportationCostItem
     */
    public function removeTransportationCostItem(
	    TransportationCostItem $transportationCostItem
    ) {
        $this->transportationCostItems->removeElement(
	        $transportationCostItem
        );
    }

    /**
     * Get contributionTransportationCostItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransportationCostItems()
    {
        return $this->transportationCostItems;
    }
}
