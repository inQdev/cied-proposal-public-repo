<?php

namespace AppBundle\Entity\Specialist\Budget\Contribution;

use AppBundle\Entity\Common\BaseContributionItem;
use AppBundle\Entity\CostItemProgramActivityAllowance;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProgramActivitiesAllowanceCostItem entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="program_activities_allowance_cost_item")
 */
class ProgramActivitiesAllowanceCostItem extends BaseContributionItem
{
    /**
     * @var CostItemProgramActivityAllowance $costItem
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\CostItemProgramActivityAllowance",
     *     cascade={"persist"}
     * )
     */
    protected $costItem;

    /**
     * @var ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @ORM\ManyToOne(
     *     targetEntity="ProgramActivitiesAllowance",
     *     inversedBy="programActivitiesAllowanceCostItems"
     * )
     * @ORM\JoinColumn(
     *     name="program_activities_allowance_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $programActivitiesAllowance;

    /**************************** Methods Definitions **************************/
    /**
     * ProgramActivitiesAllowanceCostItem constructor.
     *
     * @param ProgramActivitiesAllowance $programActivitiesAllowance
     */
    public function __construct(
      ProgramActivitiesAllowance $programActivitiesAllowance
    ) {
        $this->programActivitiesAllowance = $programActivitiesAllowance;
    }

    /**
     * TODO: Check calculation to make sure they are right for every type of budget.
     */
    public function calculateTotals()
    {
        $fundedBy = $this->programActivitiesAllowance->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set totalCost
     *
     * @param float $totalCost
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return float
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set postContribution
     *
     * @param float $postContribution
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setPostContribution($postContribution)
    {
        $this->postContribution = $postContribution;

        return $this;
    }

    /**
     * Get postContribution
     *
     * @return float
     */
    public function getPostContribution()
    {
        return $this->postContribution;
    }

    /**
     * Set hostContributionMonetary
     *
     * @param float $hostContributionMonetary
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setHostContributionMonetary($hostContributionMonetary)
    {
        $this->hostContributionMonetary = $hostContributionMonetary;

        return $this;
    }

    /**
     * Get hostContributionMonetary
     *
     * @return float
     */
    public function getHostContributionMonetary()
    {
        return $this->hostContributionMonetary;
    }

    /**
     * Set hostContributionInKind
     *
     * @param float $hostContributionInKind
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setHostContributionInKind($hostContributionInKind)
    {
        $this->hostContributionInKind = $hostContributionInKind;

        return $this;
    }

    /**
     * Get hostContributionInKind
     *
     * @return float
     */
    public function getHostContributionInKind()
    {
        return $this->hostContributionInKind;
    }

    /**
     * Set postHostTotalContribution
     *
     * @param float $postHostTotalContribution
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setPostHostTotalContribution($postHostTotalContribution)
    {
        $this->postHostTotalContribution = $postHostTotalContribution;

        return $this;
    }

    /**
     * Get postHostTotalContribution
     *
     * @return float
     */
    public function getPostHostTotalContribution()
    {
        return $this->postHostTotalContribution;
    }

    /**
     * Set postTotalContribution
     *
     * @param float $postTotalContribution
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setPostTotalContribution($postTotalContribution)
    {
        $this->postTotalContribution = $postTotalContribution;

        return $this;
    }

    /**
     * Get postTotalContribution
     *
     * @return float
     */
    public function getPostTotalContribution()
    {
        return $this->postTotalContribution;
    }

    /**
     * Set ecaTotalContribution
     *
     * @param float $ecaTotalContribution
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setEcaTotalContribution($ecaTotalContribution)
    {
        $this->ecaTotalContribution = $ecaTotalContribution;

        return $this;
    }

    /**
     * Get ecaTotalContribution
     *
     * @return float
     */
    public function getEcaTotalContribution()
    {
        return $this->ecaTotalContribution;
    }

    /**
     * Set costItem
     *
     * @param CostItemProgramActivityAllowance $costItem
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setCostItem(
      CostItemProgramActivityAllowance $costItem = null
    ) {
        $this->costItem = $costItem;

        return $this;
    }

    /**
     * Get costItem
     *
     * @return CostItemProgramActivityAllowance
     */
    public function getCostItem()
    {
        return $this->costItem;
    }

    /**
     * Set programActivitiesAllowance
     *
     * @param ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setProgramActivitiesAllowance(
      ProgramActivitiesAllowance $programActivitiesAllowance = null
    ) {
        $this->programActivitiesAllowance = $programActivitiesAllowance;

        return $this;
    }

    /**
     * Get programActivitiesAllowance
     *
     * @return ProgramActivitiesAllowance
     */
    public function getProgramActivitiesAllowance()
    {
        return $this->programActivitiesAllowance;
    }

	/**
	 * @param string $fundingType (not used)
	 * @return int
	 */
	public function checkTotals($fundingType="")
	{
		$fundingType = $this->getProgramActivitiesAllowance()->getPhaseBudget()->getFundedBy();
		return parent::checkTotals($fundingType);
	}


}
