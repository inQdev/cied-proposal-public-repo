<?php

namespace AppBundle\Entity\Specialist\Budget\Contribution;

use AppBundle\Entity\Common\BaseContributionItem;
use AppBundle\Entity\LivingExpenseCostItem;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContributionLivingExpenseCostItem entity
 *
 * @package AppBundle\Entity\Specialist\Budget\Contribution
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_living_expense_cost_item")
 * @ORM\HasLifecycleCallbacks
 */
class ContributionLivingExpenseCostItem extends BaseContributionItem
{
    /**
     * @var LivingExpenseCostItem $livingExpenseCostItem
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\LivingExpenseCostItem",
     *     inversedBy="contributionLivingExpenseCostItem"
     * )
     * @ORM\JoinColumn(
     *     name="living_expense_cost_item_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $livingExpenseCostItem;

    /**
     * @var ContributionLivingExpense $livingExpense
     *
     * @ORM\ManyToOne(
     *     targetEntity="ContributionLivingExpense",
     *     inversedBy="contributionLivingExpenseCostItems"
     * )
     * @ORM\JoinColumn(
     *     name="contribution_living_expense_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $contributionLivingExpense;

	/********************** Methods Definitions **************************/
    /**
     * LivingExpenseCostItem constructor.
     *
     * @param ContributionLivingExpense $contributionLivingExpense
     * @param LivingExpenseCostItem $livingExpenseCostItem
     */
    public function __construct(ContributionLivingExpense $contributionLivingExpense, LivingExpenseCostItem $livingExpenseCostItem) {
        $this->contributionLivingExpense = $contributionLivingExpense;
	    $this->livingExpenseCostItem = $livingExpenseCostItem;
    }

    /**
     * @ORM\PreUpdate
     */
    public function calculateTotals()
    {
        $fundedBy = $this->contributionLivingExpense->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set contributionLivingExpense
     *
     * @param ContributionLivingExpense $contributionLivingExpense
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setContributionLivingExpense(ContributionLivingExpense $contributionLivingExpense = null) {
        $this->contributionLivingExpense = $contributionLivingExpense;

        return $this;
    }

    /**
     * Get contributionLivingExpense
     *
     * @return ContributionLivingExpense
     */
    public function getContributionLivingExpense()
    {
        return $this->contributionLivingExpense;
    }

	/**
	 * Set livingExpenseCostItem
	 *
	 * @param LivingExpenseCostItem $costItem
	 *
	 * @return GroundTransportCostItem
	 */
	public function setLivingExpenseCostItem(LivingExpenseCostItem $costItem = null)
	{
		$this->livingExpenseCostItem = $costItem;

		return $this;
	}

	/**
	 * Get livingExpenseCostItem
	 *
	 * @return \AppBundle\Entity\LivingExpenseCostItem
	 */
	public function getLivingExpenseCostItem()
	{
		return $this->livingExpenseCostItem;
	}


	/**
	 * @param string $fundingType (not used)
	 * @return int
	 */
	public function checkTotals($fundingType="")
	{
		$fundingType = $this->getContributionLivingExpense()->getPhaseBudget()->getFundedBy();
		return parent::checkTotals($fundingType);
	}
}
