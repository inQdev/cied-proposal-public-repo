<?php

namespace AppBundle\Entity\Specialist\Budget\Contribution;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\SpecialistProjectPhaseBudget;

/**
 * ContributionLivingExpense entity.
 *
 * @package AppBundle\Entity\Specialist\Budget\Contribution
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_living_expense")
 */
class ContributionLivingExpense extends BaseEntity
{
	/**
	 * @var SpecialistProjectPhaseBudget $phaseBudget
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="AppBundle\Entity\SpecialistProjectPhaseBudget",
	 *     inversedBy="contributionLivingExpenses"
	 * )
	 * @ORM\JoinColumn(
	 *     name="phase_budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $phaseBudget;

	/**
	 * @var ArrayCollection $livingExpenseCostItems
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="ContributionLivingExpenseCostItem",
	 *     mappedBy="contributionLivingExpense"
	 * )
	 */
	protected $contributionLivingExpenseCostItems;

	/***************************** Methods Definitions ******************************/
	/**
	 * LivingExpense constructor.
	 *
	 * @param SpecialistProjectPhaseBudget $phaseBudget
	 */
	public function __construct(SpecialistProjectPhaseBudget $phaseBudget)
	{
		$this->phaseBudget = $phaseBudget;
		$this->contributionLivingExpenseCostItems = new ArrayCollection();
	}

	/**
	 *
	 */
	public function calculateSummaryTotals()
	{
		$summaryTotals = [
			'cost' => 0,
			'post' => 0,
			'hostMonetary' => 0,
			'hostInKind' => 0,
			'postHostTotal' => 0,
			'postTotal' => 0,
			'ecaTotal' => 0,
		];

		/** @var ContributionLivingExpenseCostItem $costItem */
		foreach ($this->contributionLivingExpenseCostItems as $costItem) {
			$summaryTotals['cost'] += $costItem->getTotalCost();
			$summaryTotals['post'] += $costItem->getPostContribution();
			$summaryTotals['hostMonetary'] += $costItem->getHostContributionMonetary();
			$summaryTotals['hostInKind'] += $costItem->getHostContributionInKind();
			$summaryTotals['postHostTotal'] += $costItem->getPostHostTotalContribution();
			$summaryTotals['postTotal'] += $costItem->getPostTotalContribution();
			$summaryTotals['ecaTotal'] += $costItem->getEcaTotalContribution();
		}

		return $summaryTotals;
	}

	/**
	 * Set phaseBudget
	 *
	 * @param SpecialistProjectPhaseBudget $phaseBudget
	 *
	 * @return ContributionLivingExpense
	 */
	public function setPhaseBudget(SpecialistProjectPhaseBudget $phaseBudget = null)
	{
		$this->phaseBudget = $phaseBudget;

		return $this;
	}

	/**
	 * Get phaseBudget
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function getPhaseBudget()
	{
		return $this->phaseBudget;
	}

	/**
	 * Add contributionLivingExpenseCostItem
	 *
	 * @param ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem
	 *
	 * @return ContributionLivingExpense
	 */
	public function addContributionLivingExpenseCostItem(ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem)
	{
		$this->contributionLivingExpenseCostItems[] = $contributionLivingExpenseCostItem;

		return $this;
	}

	/**
	 * Remove contributionLivingExpenseCostItem
	 *
	 * @param ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem
	 */
	public function removeContributionLivingExpenseCostItem(ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem)
	{
		$this->contributionLivingExpenseCostItems->removeElement(
			$contributionLivingExpenseCostItem
		);
	}

	/**
	 * Get contributionLivingExpenseCostItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getContributionLivingExpenseCostItems()
	{
		return $this->contributionLivingExpenseCostItems;
	}
}
