<?php

namespace AppBundle\Entity\Specialist\Budget;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use Doctrine\ORM\Mapping as ORM;

/**
 * Total
 *
 * @ORM\Table(name="specialist_project_phase_budget_total")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\Budget\BudgetTotalRepository")
 */
class BudgetTotal extends BaseEntity
{
	/**
	 * @var SpecialistProjectPhaseBudget
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="AppBundle\Entity\SpecialistProjectPhaseBudget",
	 *     inversedBy="budgetTotals"
	 * )
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $budget;

	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $revisionId;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $fullCost = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $postContribution = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $hostContributionMonetary = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $hostContributionInKind = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $postTotalContribution = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $ecaTotalContribution = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $global = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $prePostWork = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $dailyGlobal = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $virtualFlexibleTotal = 0;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $total = 0;

	/******************************* Methods *************************************/

	/**
	 * BudgetTotal constructor.
	 * @param SpecialistProjectPhaseBudget $budget
	 */
	public function __construct($budget)
	{
		$this->budget = $budget;

	}


	/**
	 * Set revisionId
	 *
	 * @param integer $revisionId
	 *
	 * @return BudgetTotal
	 */
	public function setRevisionId($revisionId)
	{
		$this->revisionId = $revisionId;

		return $this;
	}

	/**
	 * Get revisionId
	 *
	 * @return int
	 */
	public function getRevisionId()
	{
		return $this->revisionId;
	}

	/**
	 * Set fullCost
	 *
	 * @param string $fullCost
	 *
	 * @return BudgetTotal
	 */
	public function setFullCost($fullCost)
	{
		$this->fullCost = $fullCost;

		return $this;
	}

	/**
	 * Get fullCost
	 *
	 * @return float
	 */
	public function getFullCost()
	{
		return $this->fullCost;
	}

	/**
	 * Set postContribution
	 *
	 * @param string $postContribution
	 *
	 * @return BudgetTotal
	 */
	public function setPostContribution($postContribution)
	{
		$this->postContribution = $postContribution;

		return $this;
	}

	/**
	 * Get postContribution
	 *
	 * @return string
	 */
	public function getPostContribution()
	{
		return $this->postContribution;
	}

	/**
	 * Set hostContributionMonetary
	 *
	 * @param string $hostContributionMonetary
	 *
	 * @return BudgetTotal
	 */
	public function setHostContributionMonetary($hostContributionMonetary)
	{
		$this->hostContributionMonetary = $hostContributionMonetary;

		return $this;
	}

	/**
	 * Get hostContributionMonetary
	 *
	 * @return string
	 */
	public function getHostContributionMonetary()
	{
		return $this->hostContributionMonetary;
	}

	/**
	 * Set hostContributionInKind
	 *
	 * @param string $hostContributionInKind
	 *
	 * @return BudgetTotal
	 */
	public function setHostContributionInKind($hostContributionInKind)
	{
		$this->hostContributionInKind = $hostContributionInKind;

		return $this;
	}

	/**
	 * Get hostContributionInKind
	 *
	 * @return string
	 */
	public function getHostContributionInKind()
	{
		return $this->hostContributionInKind;
	}

	/**
	 * Set postTotalContribution
	 *
	 * @param string $postTotalContribution
	 *
	 * @return BudgetTotal
	 */
	public function setPostTotalContribution($postTotalContribution)
	{
		$this->postTotalContribution = $postTotalContribution;

		return $this;
	}

	/**
	 * Get postTotalContribution
	 *
	 * @return float
	 */
	public function getPostTotalContribution()
	{
		return $this->postTotalContribution;
	}

	/**
	 * Set ecaTotalContribution
	 *
	 * @param string $ecaTotalContribution
	 *
	 * @return BudgetTotal
	 */
	public function setEcaTotalContribution($ecaTotalContribution)
	{
		$this->ecaTotalContribution = $ecaTotalContribution;

		return $this;
	}

	/**
	 * Get ecaTotalContribution
	 *
	 * @return string
	 */
	public function getEcaTotalContribution()
	{
		return $this->ecaTotalContribution;
	}

	/**
	 * Set global
	 *
	 * @param string $global
	 *
	 * @return BudgetTotal
	 */
	public function setGlobal($global)
	{
		$this->global = $global;

		return $this;
	}

	/**
	 * Get global
	 *
	 * @return float
	 */
	public function getGlobal()
	{
		return $this->global;
	}

	/**
	 * Set prePostWork
	 *
	 * @param string $prePostWork
	 *
	 * @return BudgetTotal
	 */
	public function setPrePostWork($prePostWork)
	{
		$this->prePostWork = $prePostWork;

		return $this;
	}

	/**
	 * Get prePostWork
	 *
	 * @return string
	 */
	public function getPrePostWork()
	{
		return $this->prePostWork;
	}

	/**
	 * Set DailyGlobal
	 *
	 * @param string $dailyGlobal
	 *
	 * @return BudgetTotal
	 */
	public function setDailyGlobal($dailyGlobal)
	{
		$this->dailyGlobal = $dailyGlobal;

		return $this;
	}

	/**
	 * Get DailyGlobal
	 *
	 * @return string
	 */
	public function getDailyGlobal()
	{
		return $this->dailyGlobal;
	}

	/**
	 * Set DailyGlobal
	 *
	 * @param string $virtualFlexibleTotal
	 *
	 * @return BudgetTotal
	 */
	public function setVirtualFlexibleTotal($virtualFlexibleTotal)
	{
		$this->virtualFlexibleTotal = $virtualFlexibleTotal;

		return $this;
	}

	/**
	 * Get DailyGlobal
	 *
	 * @return string
	 */
	public function getVirtualFlexibleTotal()
	{
		return $this->virtualFlexibleTotal;
	}

	/**
	 * Set total
	 *
	 * @param string $total
	 *
	 * @return BudgetTotal
	 */
	public function setTotal($total)
	{
		$this->total = $total;

		return $this;
	}

	/**
	 * Get total
	 *
	 * @return string
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * Sets the Budget
	 *
	 * @param SpecialistProjectPhaseBudget $budget
	 * @return BudgetTotal
	 */
	public function setBudget($budget)
	{
		$this->budget = $budget;

		return $this;
	}

	/**
	 * Get Budget
	 * @return SpecialistProjectPhaseBudget
	 */
	public function getBudget()
	{
		return $this->budget;
	}

}

