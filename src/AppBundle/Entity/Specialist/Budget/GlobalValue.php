<?php

namespace AppBundle\Entity\Specialist\Budget;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalValue
 *
 * @ORM\Table(name="specialist_project_phase_global_value")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\Budget\GlobalValueRepository")
 */
class GlobalValue extends BaseEntity
{
	/**
	 * @var \AppBundle\Entity\GlobalValueSpecialist
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="AppBundle\Entity\GlobalValueSpecialist"
	 * )
	 * @ORM\JoinColumn(
	 *     name="global_value_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	private $globalValue;

	/**
	 * @var \AppBundle\Entity\GlobalPAA
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="AppBundle\Entity\GlobalPAA"
	 * )
	 * @ORM\JoinColumn(
	 *     name="global_paa_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	private $globalPAA;

	/**
	 * @var SpecialistProjectPhaseBudget
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="AppBundle\Entity\SpecialistProjectPhaseBudget",
	 *     inversedBy="globalValues"
	 * )
	 * @ORM\JoinColumn(
	 *     name="budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	private $budget;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $value;

	/******************************* Methods *******************************/

	/**
	 * GlobalValue constructor.
	 * @param SpecialistProjectPhaseBudget $budget
	 * @param float $value
	 */
	public function __construct($budget, $value)
	{
		$this->budget = $budget;
		$this->value = $value;
	}

	/**
	 * Set globalValue
	 *
	 * @param \AppBundle\Entity\GlobalValue $globalValue
	 *
	 * @return GlobalValue
	 */
	public function setGlobalValue($globalValue)
	{
		$this->globalValue = $globalValue;

		return $this;
	}

	/**
	 * Get globalValue
	 *
	 * @return \AppBundle\Entity\GlobalValue
	 */
	public function getGlobalValue()
	{
		return $this->globalValue;
	}

	/**
	 * Set globalPAA
	 *
	 * @param \AppBundle\Entity\GlobalPAA $globalPAA
	 *
	 * @return GlobalValue
	 */
	public function setGlobalPAA($globalPAA)
	{
		$this->globalPAA = $globalPAA;

		return $this;
	}

	/**
	 * Get globalValue
	 *
	 * @return \AppBundle\Entity\GlobalPAA
	 */
	public function getGlobalPAA()
	{
		return $this->globalPAA;
	}

	/**
	 * Set budget
	 *
	 * @param SpecialistProjectPhaseBudget $budget
	 *
	 * @return GlobalValue
	 */
	public function setBudget($budget)
	{
		$this->budget = $budget;

		return $this;
	}

	/**
	 * Get budget
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function getBudget()
	{
		return $this->budget;
	}

	/**
	 * Set value
	 *
	 * @param string $value
	 *
	 * @return GlobalValue
	 */
	public function setValue($value)
	{
		$this->value = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}
}

