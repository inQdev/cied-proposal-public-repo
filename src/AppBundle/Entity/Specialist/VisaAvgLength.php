<?php

namespace AppBundle\Entity\Specialist;

use AppBundle\Entity\BaseTerm;
use Doctrine\ORM\Mapping as ORM;

/**
 * VisaAvgLength entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class VisaAvgLength extends BaseTerm
{
}
