<?php

namespace AppBundle\Entity\Specialist\Virtual\Itinerary;

use Doctrine\ORM\Mapping as ORM;

/**
 * WeekAfter entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class WeekAfter extends Week
{
}
