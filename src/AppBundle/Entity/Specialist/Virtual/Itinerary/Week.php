<?php

namespace AppBundle\Entity\Specialist\Virtual\Itinerary;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Week entity.
 *
 * @package AppBundle\Entity\Specialist\Virtual\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="itinerary_virtual_week")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *     "0" = "AppBundle\Entity\Specialist\Virtual\Itinerary\Week",
 *     "1" = "AppBundle\Entity\Specialist\Virtual\Itinerary\WeekAfter"
 * })
 */
class Week extends BaseEntity
{
    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $endDate;

    /**
     * @var SpecialistProjectPhase $projectPhase
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\SpecialistProjectPhase",
     *     inversedBy="virtualItineraryWeeks"
     * )
     * @ORM\JoinColumn(
     *     name="project_phase_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $projectPhase;

    /**
     * @var ArrayCollection $activities
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek",
     *     mappedBy="week",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $activities;

    /******************************* Methods Definitions ***********************************/

    /**
     * Week constructor.
     *
     * @param SpecialistProjectPhase $projectPhase The Specialist project phase.
     */

    public function __construct(SpecialistProjectPhase $projectPhase)
    {
        $this->projectPhase = $projectPhase;
        $this->activities = new ArrayCollection();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        return "{$this->startDate->format('m/d/Y')} to {$this->endDate->format('m/d/Y')}";
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Week
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Week
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set projectPhase
     *
     * @param SpecialistProjectPhase $projectPhase
     *
     * @return Week
     */
    public function setProjectPhase(SpecialistProjectPhase $projectPhase = null)
    {
        $this->projectPhase = $projectPhase;

        return $this;
    }

    /**
     * Get projectPhase
     *
     * @return SpecialistProjectPhase
     */
    public function getProjectPhase()
    {
        return $this->projectPhase;
    }

    /**
     * Add activity
     *
     * @param ActivityWeek $activity
     *
     * @return Week
     */
    public function addActivity(ActivityWeek $activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param ActivityWeek $activity
     */
    public function removeActivity(ActivityWeek $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }
}
