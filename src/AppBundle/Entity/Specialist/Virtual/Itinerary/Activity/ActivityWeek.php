<?php

namespace AppBundle\Entity\Specialist\Virtual\Itinerary\Activity;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Week;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityWeek entity.
 *
 * @package AppBundle\Entity\Specialist\Virtual\Itinerary\Activity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="itinerary_virtual_activities_weeks")
 */
class ActivityWeek extends BaseEntity
{
    /**
     * @var int $duration
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $duration;

    /**
     * @var Activity $activity
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Activity",
     *     inversedBy="weeks"
     * )
     * @ORM\JoinColumn(
     *     name="activity_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $activity;

    /**
     * @var Week $week
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Week",
     *     inversedBy="activities"
     * )
     * @ORM\JoinColumn(
     *     name="week_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $week;

    /**
     * ActivityWeek constructor.
     *
     * @param Activity $activity The activity.
     * @param Week     $week     The week.
     */
    public function __construct(Activity $activity, Week $week)
    {
        $this->activity = $activity;
        $this->week = $week;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return ActivityWeek
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set activity
     *
     * @param Activity $activity
     *
     * @return ActivityWeek
     */
    public function setActivity(Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set week
     *
     * @param Week $week
     *
     * @return ActivityWeek
     */
    public function setWeek(Week $week = null)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return Week
     */
    public function getWeek()
    {
        return $this->week;
    }
}
