<?php

namespace AppBundle\Entity\Specialist\Virtual\Itinerary\Activity;

use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\Specialist\Itinerary\Activity\Audience;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity;
use Doctrine\ORM\Mapping as ORM;

/**
 * AnticipatedAudience entity.
 *
 * @package AppBundle\Entity\Specialist\Virtual\Itinerary\Activity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="itinerary_virtual_activities_anticipated_audience")
 */
class AnticipatedAudience extends BaseEntity
{
    /**
     * @var int $estimatedParticipants
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $estimatedParticipants;

    /**
     * @var string $audienceOther
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $audienceOther;

    /**
     * @var Activity $activity
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Activity",
     *     inversedBy="anticipatedAudience",
     * )
     * @ORM\JoinColumn(
     *     name="activity_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $activity;

    /**
     * @var Audience $audience
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Itinerary\Activity\Audience"
     * )
     */
    protected $audience;

    /**
     * AnticipatedAudience constructor.
     *
     * @param Activity $activity The activity.
     * @param Audience $audience The audience.
     */
    public function __construct(Activity $activity, Audience $audience)
    {
        $this->activity = $activity;
        $this->audience = $audience;
    }

    /**
     * Set estimatedParticipants
     *
     * @param integer $estimatedParticipants
     *
     * @return AnticipatedAudience
     */
    public function setEstimatedParticipants($estimatedParticipants)
    {
        $this->estimatedParticipants = $estimatedParticipants;

        return $this;
    }

    /**
     * Get estimatedParticipants
     *
     * @return integer
     */
    public function getEstimatedParticipants()
    {
        return $this->estimatedParticipants;
    }

    /**
     * Set audienceOther
     *
     * @param string $audienceOther
     *
     * @return AnticipatedAudience
     */
    public function setAudienceOther($audienceOther)
    {
        $this->audienceOther = $audienceOther;

        return $this;
    }

    /**
     * Get audienceOther
     *
     * @return string
     */
    public function getAudienceOther()
    {
        return $this->audienceOther;
    }

    /**
     * Set activity
     *
     * @param Activity $activity
     *
     * @return AnticipatedAudience
     */
    public function setActivity(Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set audience
     *
     * @param Audience $audience
     *
     * @return AnticipatedAudience
     */
    public function setAudience(Audience $audience = null)
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * Get audience
     *
     * @return Audience
     */
    public function getAudience()
    {
        return $this->audience;
    }
}
