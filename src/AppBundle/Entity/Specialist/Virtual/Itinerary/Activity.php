<?php

namespace AppBundle\Entity\Specialist\Virtual\Itinerary;

use AppBundle\Entity\AreaOfExpertise;
use AppBundle\Entity\BaseEntity;
use AppBundle\Entity\Specialist\Itinerary\ActivityType;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek;
use AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\AnticipatedAudience;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Activity entity.
 *
 * @package AppBundle\Entity\Specialist\Virtual\Itinerary
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\Virtual\Itinerary\ActivityRepository")
 * @ORM\Table(name="itinerary_virtual_activity")
 */
class Activity extends BaseEntity
{
    /**
     * @var string $typeOther
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $typeOther;

    /**
     * @var string $typeDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $typeDesc;

    /**
     * @var string $onlineAvailabilityDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $onlineAvailabilityDesc;

    /**
     * @var string $topicOther
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $topicOther;

    /**
     * @var int $totalDuration
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $totalDuration;

    /**
     * @var ActivityType $type
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Specialist\Itinerary\ActivityType"
     * )
     */
    protected $type;

    /**
     * @var ArrayCollection $topics
     *
     * @ORM\ManyToMany(
     *     targetEntity="AppBundle\Entity\AreaOfExpertise",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="itinerary_virtual_activities_topics")
     */
    protected $topics;

    /**
     * @var ArrayCollection $anticipatedAudience
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\AnticipatedAudience",
     *     mappedBy="activity",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $anticipatedAudience;

    /**
     * @var ArrayCollection $weeks
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Specialist\Virtual\Itinerary\Activity\ActivityWeek",
     *     mappedBy="activity",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $weeks;

    /**
     * Activity constructor.
     */
    public function __construct()
    {
        $this->topics = new ArrayCollection();
        $this->anticipatedAudience = new ArrayCollection();
        $this->weeks = new ArrayCollection();
    }

    /**
     * Set typeOther
     *
     * @param string $typeOther
     *
     * @return Activity
     */
    public function setTypeOther($typeOther)
    {
        $this->typeOther = $typeOther;

        return $this;
    }

    /**
     * Get typeOther
     *
     * @return string
     */
    public function getTypeOther()
    {
        return $this->typeOther;
    }

    /**
     * Set typeDesc
     *
     * @param string $typeDesc
     *
     * @return Activity
     */
    public function setTypeDesc($typeDesc)
    {
        $this->typeDesc = $typeDesc;

        return $this;
    }

    /**
     * Get typeDesc
     *
     * @return string
     */
    public function getTypeDesc()
    {
        return $this->typeDesc;
    }

    /**
     * Set onlineAvailabilityDesc
     *
     * @param string $onlineAvailabilityDesc
     *
     * @return Activity
     */
    public function setOnlineAvailabilityDesc($onlineAvailabilityDesc)
    {
        $this->onlineAvailabilityDesc = $onlineAvailabilityDesc;

        return $this;
    }

    /**
     * Get onlineAvailabilityDesc
     *
     * @return string
     */
    public function getOnlineAvailabilityDesc()
    {
        return $this->onlineAvailabilityDesc;
    }

    /**
     * Set topicOther
     *
     * @param string $topicOther
     *
     * @return Activity
     */
    public function setTopicOther($topicOther)
    {
        $this->topicOther = $topicOther;

        return $this;
    }

    /**
     * Get topicOther
     *
     * @return string
     */
    public function getTopicOther()
    {
        return $this->topicOther;
    }

    /**
     * Set totalDuration
     *
     * @param integer $totalDuration
     *
     * @return Activity
     */
    public function setTotalDuration($totalDuration)
    {
        $this->totalDuration = $totalDuration;

        return $this;
    }

    /**
     * Get totalDuration
     *
     * @return integer
     */
    public function getTotalDuration()
    {
        return $this->totalDuration;
    }

    /**
     * Set type
     *
     * @param ActivityType $type
     *
     * @return Activity
     */
    public function setType(ActivityType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return ActivityType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add topic
     *
     * @param AreaOfExpertise $topic
     *
     * @return Activity
     */
    public function addTopic(AreaOfExpertise $topic)
    {
        $this->topics[] = $topic;

        return $this;
    }

    /**
     * Remove topic
     *
     * @param AreaOfExpertise $topic
     */
    public function removeTopic(AreaOfExpertise $topic)
    {
        $this->topics->removeElement($topic);
    }

    /**
     * Get topics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Add anticipatedAudience
     *
     * @param AnticipatedAudience $anticipatedAudience
     *
     * @return Activity
     */
    public function addAnticipatedAudience(AnticipatedAudience $anticipatedAudience)
    {
        $this->anticipatedAudience[] = $anticipatedAudience;

        return $this;
    }

    /**
     * Remove anticipatedAudience
     *
     * @param AnticipatedAudience $anticipatedAudience
     */
    public function removeAnticipatedAudience(AnticipatedAudience $anticipatedAudience)
    {
        $this->anticipatedAudience->removeElement($anticipatedAudience);
    }

    /**
     * Get anticipatedAudience
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnticipatedAudience()
    {
        return $this->anticipatedAudience;
    }

    /**
     * Add week
     *
     * @param ActivityWeek $week
     *
     * @return Activity
     */
    public function addWeek(ActivityWeek $week)
    {
        $this->weeks[] = $week;

        return $this;
    }

    /**
     * Remove week
     *
     * @param ActivityWeek $week
     */
    public function removeWeek(ActivityWeek $week)
    {
        $this->weeks->removeElement($week);
    }

    /**
     * Get weeks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWeeks()
    {
        return $this->weeks;
    }

    public function calculateTotalDuration() {
	    if ($this->weeks->count()) {
		    $totalDuration = 0;
		    /** @var ActivityWeek $week */
		    foreach ($this->weeks as $week) {
			    $totalDuration += $week->getDuration();
		    }

		    $this->totalDuration = $totalDuration;
	    }
    }

}
