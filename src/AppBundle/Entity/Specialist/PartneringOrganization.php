<?php

namespace AppBundle\Entity\Specialist;

use AppBundle\Entity\BaseSingleEntity;
use AppBundle\Entity\Country;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PartneringOrganization
 *
 * @package AppBundle\Entity\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\PartneringOrganizationRepository")
 */
class PartneringOrganization extends BaseSingleEntity
{
    /**
     * Every country should have one "Not Partnering Organization" (a/k/a Not Applicable) option.
     * This flag helps to identify it.
     *
     * @var bool $notApplicable
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $notApplicable = false;

    /**
     * @var Country $country
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Country",
     *     inversedBy="partneringOrganizations"
     * )
     */
    protected $country;

    /**
     * @var ArrayCollection $projectsPhases
     *
     * @ORM\ManyToMany(
     *     targetEntity="AppBundle\Entity\SpecialistProjectPhase",
     *     mappedBy="partneringOrganizations"
     * )
     */
    protected $projectsPhases;

    /**
     * PartneringOrganization constructor.
     *
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        $this->country = $country;
        $this->projectsPhases = new ArrayCollection();
    }

    /**
     * Set notApplicable
     *
     * @param boolean $notApplicable
     *
     * @return PartneringOrganization
     */
    public function setNotApplicable($notApplicable)
    {
        $this->notApplicable = $notApplicable;

        return $this;
    }

    /**
     * Get notApplicable
     *
     * @return boolean
     */
    public function isNotApplicable()
    {
        return $this->notApplicable;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return PartneringOrganization
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get notApplicable
     *
     * @return boolean
     */
    public function getNotApplicable()
    {
        return $this->notApplicable;
    }

    /**
     * Add projectsPhase
     *
     * @param SpecialistProjectPhase $projectsPhase
     *
     * @return PartneringOrganization
     */
    public function addProjectsPhase(SpecialistProjectPhase $projectsPhase)
    {
        $this->projectsPhases[] = $projectsPhase;

        return $this;
    }

    /**
     * Remove projectsPhase
     *
     * @param SpecialistProjectPhase $projectsPhase
     */
    public function removeProjectsPhase(SpecialistProjectPhase $projectsPhase)
    {
        $this->projectsPhases->removeElement($projectsPhase);
    }

    /**
     * Get projectsPhases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectsPhases()
    {
        return $this->projectsPhases;
    }
}
