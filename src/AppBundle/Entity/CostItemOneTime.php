<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CostItemOneTime
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 */
class CostItemOneTime extends CostItem
{
}