<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Post entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\Table(name="post")
 */
class Post extends BaseSingleEntity
{
    /**
     * @var Country $country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="posts")
     */
    protected $country;
    /**
     * @var ArrayCollection $projects
     *
     * @ORM\ManyToMany(targetEntity="ProjectGeneralInfo", mappedBy="posts")
     */
    protected $projects;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Post
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add project
     *
     * @param ProjectGeneralInfo $project
     *
     * @return Post
     */
    public function addProject(ProjectGeneralInfo $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param ProjectGeneralInfo $project
     */
    public function removeProject(ProjectGeneralInfo $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }
}
