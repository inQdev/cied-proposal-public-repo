<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GlobalValueSpecialist
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Specialist\GlobalValueSpecialistRepository")
 */
class GlobalValueSpecialist extends GlobalValue
{
	/**
	 * @var string
	 * This should match the type of project: mixed, in-country, virtual
	 * @ORM\Column(name="specialist_type", type="string", length=20)
	 */
	protected $specialistType = 'mixed';

	/**
	 * @var int
	 * @ORM\Column(type="smallint", nullable=true)
	 */
	protected $applyOnMixed = 0;

	/******************************* Methods **************************************/

	/**
	 * @return array
	 */
	public static function getSpecialistTypeChoices()
	{
		return [
			'Mixed' => 'mixed',
			'In-Country' => 'in-country',
			'Virtual' => 'virtual',
		];
	}


	/**
	 * Set specialistType
	 *
	 * @param string $specialistType
	 *
	 * @return GlobalValue
	 */
	public function setSpecialistType($specialistType)
	{
		$this->specialistType = $specialistType;

		return $this;
	}

	/**
	 * Get specialistType
	 *
	 * @return string
	 */
	public function getSpecialistType()
	{
		return $this->specialistType;
	}


    /**
     * Set applyOnMixed
     *
     * @param integer $applyOnMixed
     *
     * @return GlobalValueSpecialist
     */
    public function setApplyOnMixed($applyOnMixed)
    {
        $this->applyOnMixed = $applyOnMixed;

        return $this;
    }

    /**
     * Get applyOnMixed
     *
     * @return integer
     */
    public function getApplyOnMixed()
    {
        return $this->applyOnMixed;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return GlobalValueSpecialist
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}
