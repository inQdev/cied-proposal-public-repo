<?php

namespace AppBundle\Entity\Listener;

use AppBundle\Entity\Country;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\FellowProject as Fellow;
use AppBundle\Entity\LocationHostInfo as LocationHost;
use AppBundle\Entity\ProjectGeneralInfo as GeneralInfo;
use AppBundle\Entity\ProjectOutcome as Outcome;
use AppBundle\Entity\ProjectReviewStatus as ReviewStatus;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;

/**
 * GeneralInfoListener class.
 *
 * @package AppBundle\Entity\Listener
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class GeneralInfoListener
{
    /**
     * Path to where JSON file with projects should be allocated.
     *
     * @var string $rosterPath
     */
    private $rosterPath;

    /**
     * GeneralInfoListener constructor.
     *
     * @param $rosterPath
     */
    public function __construct($rosterPath)
    {
        $this->rosterPath = $rosterPath;
    }

    /**
     * PreUpdate event handler.
     *
     * @param GeneralInfo        $generalInfo The entity.
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(GeneralInfo $generalInfo, LifecycleEventArgs $args)
    {
        // Get the list of changed properties for the entity.
        $generalInfoChangeSet = $args->getObjectManager()->getUnitOfWork()->getEntityChangeSet($generalInfo);

        if (array_key_exists('projectOutcome', $generalInfoChangeSet)
            && null !== $generalInfo->getProjectOutcome()
        ) {
            $outcomeId = $generalInfo->getProjectOutcome()->getId();

            // If the project's Review is complete and its outcome is
            // `Approved by ECA` or `Alternate Funding Provided`,
            // the project should appear in the list of Rosters projects list
            // (Application System); the JSON with projects should be updated.
            if (null !== $generalInfo->getFellowProject()
                && ReviewStatus::REVIEW_COMPLETE === $generalInfo->getProjectReviewStatus()->getId()
                && (Outcome::SELECTED_FOR_ECA_FUNDING === $outcomeId
                    || Outcome::ALTERNATE_FUNDING_PROVIDED === $outcomeId)
            ) {
                $this->generateRosterReadyProjectsJSONFile(
                    $generalInfo->getCycle(),
                    $args->getEntityManager()
                );
            }
        }
    }

    /**
     * Generate JSON file with approved Fellow projects in a given cycle.
     *
     * @param CycleFellow   $cycle The current Fellow cycle.
     * @param EntityManager $em    The entity manager.
     */
    private function generateRosterReadyProjectsJSONFile(CycleFellow $cycle, EntityManager $em)
    {
        // Projects for the cycle (2016 - 2017) were added manually. This
        // validation will avoid the Proposal System to overwrite the JSON
        // file with those projects.
        if (null !== $cycle->getTid() && $cycle->getTid() > 5147) {
            $fs = new Filesystem();

            if ($fs->exists($this->rosterPath)) {
                $approvedFellows = $em->getRepository(Fellow::class)->findAllApprovedInCycle($cycle);

                $countriesProjects = [];

                /** @var Fellow $approvedFellow */
                foreach ($approvedFellows as $approvedFellow) {
                    /** @var GeneralInfo $generalInfo */
                    $generalInfo = $approvedFellow->getProjectGeneralInfo();

                    /** @var Country $country */
                    $country = $generalInfo->getLastCountry();
                    $countryName = $country->getName();

                    /** @var LocationHost $locationHostInfo */
                    $locationHost = $approvedFellow->getLocationHostInfo();

                    // This validation will avoid adding the Country-wide
                    // project more than once.
                    if (!array_key_exists($countryName, $countriesProjects)) {
                        $countriesProjects[$countryName]['name'] = $countryName;
                        $countriesProjects[$countryName]['ISO_code'] = $country->getIso2Code();

                        // Every country should have a Country-wide project.
                        $countriesProjects[$countryName]['projects'][] = [
                            'id' => 100,
                            'name' => 'Country-wide',
                        ];
                    }

                    // Make sure the Host instance has set the `name` property.
                    $host = (null !== $locationHost->getHost())
                        ? $locationHost->getHost()->getName()
                        : '';

                    $referenceNumber = (null !== $generalInfo->getReferenceNumber())
                        ? $generalInfo->getReferenceNumber()
                        : '';

                    $relo = '';
                    $reloEmail = '';

                    // Check if there are RELO users for RELO Location
                    // associated to the project.
                    if (!$generalInfo->getFirstRELOLocation()->getRelos()->isEmpty()) {
                        // There could be more than one user associated to the
                        // RELO Location; the first user will be the one
                        // included in the JSON file.
                        /** @var User $reloUser */
                        $reloUser = $generalInfo->getFirstRELOLocation()->getRelos()->first();

                        $relo = trim($reloUser->getName());
                        $reloEmail = trim($reloUser->getEmail());
                    }

                    $countriesProjects[$countryName]['projects'][] = [
                        'id' => $approvedFellow->getId(),
                        'name' => trim("$host ($referenceNumber)"),
                        'region' => $generalInfo->getRegion()->getName(),
                        'relo' => $relo,
                        'relo_email' => $reloEmail,
                    ];
                }

                // The array cannot be an Associative one: Country names are
                // not expected as keys in the JSON file.
                $countriesProjects = array_values($countriesProjects);

                $jsonFileName = "cied_roster_cycle_{$cycle->getTid()}.json";
                $jsonFilePath = $this->rosterPath . DIRECTORY_SEPARATOR . $jsonFileName;

                // The JSON file is going to be completely replaced with the
                // content just generated.
                if ($fs->exists($jsonFilePath)) {
                    $fs->remove($jsonFilePath);
                }

                $fs->dumpFile(
                    $jsonFilePath,
                    json_encode(['countries' => $countriesProjects])
                );
            }
        }
    }
}
