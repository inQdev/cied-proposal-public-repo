<?php

namespace AppBundle\Entity\Listener\Specialist;

use AppBundle\Entity\ContactPoint;
use AppBundle\Entity\ContactPointAdditional;
use AppBundle\Entity\Country;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\LivingExpense;
use AppBundle\Entity\LivingExpenseCostItem;
use AppBundle\Entity\Post;
use AppBundle\Entity\PrePostWork;
use AppBundle\Entity\ProjectGeneralInfo as GeneralInfo;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\Sequence;
use AppBundle\Entity\SpecialistProjectPhase as SpecialistPhase;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

/**
 * PhaseListener class.
 *
 * @package AppBundle\Entity\Listener\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PhaseListener
{

	/**
	 * @param SpecialistPhase    $phase The entity.
	 * @param LifecycleEventArgs $args
	 */
	public function prePersist(SpecialistPhase $phase, LifecycleEventArgs $args)
	{
		$em = $args->getEntityManager();

		/** @var Sequence $sequence */
		$sequence = $em->getRepository(Sequence::class)->findOneByName('SpecialistProject');

		// sets the reference number
		$referencePrefix = $phase->getProjectGeneralInfo()->getCycle()->getStateDepartmentName();
		$phase->getProjectGeneralInfo()->setReferenceNumber($referencePrefix .'-'. $sequence->getNextValue());

		// updates the Sequence
		$sequence->setNextValue($sequence->getNextValue()+1);
//		$em->persist($sequence);
		$em->flush();
	}

    /**
     * @param SpecialistPhase    $phase The entity.
     * @param LifecycleEventArgs $args
     */
    public function postPersist(SpecialistPhase $phase, LifecycleEventArgs $args)
    {
        /** @var EntityManager $em */
        $em = $args->getEntityManager();

        if (SpecialistPhase::IN_COUNTRY_TYPE === $phase->getType()) {
            $prePostWork = new PrePostWork($phase);

            $em->persist($prePostWork);
            $em->flush($prePostWork);
        }

        // If Source Phase isn't null, it means this Phase is being cloned.
        if (null !== $phase->getSourcePhase()) {
            $this->copyGeneralInfoData($phase, $em);
            $this->copyContactPointsData($phase, $em);
            $this->copyCandidateInfo($phase, $em);
            $this->copyRequirements($phase, $em);
        }
    }

    /**
     * Copy "General Info" data from source.
     *
     * @param SpecialistPhase $phase The Specialist phase.
     * @param EntityManager   $em
     */
    private function copyGeneralInfoData(SpecialistPhase $phase, EntityManager $em)
    {
        /** @var GeneralInfo $sourcePhaseGeneralInfo */
        $generalInfo = $phase->getProjectGeneralInfo();

        $isPhaseInCountry = SpecialistPhase::IN_COUNTRY_TYPE === $phase->getType();
        $isPhaseMixed = SpecialistPhase::MIXED_TYPE === $phase->getType();

        /** @var SpecialistPhase $sourcePhase */
        $sourcePhase = $phase->getSourcePhase();
        /** @var GeneralInfo $sourcePhaseGeneralInfo */
        $sourcePhaseGeneralInfo = $sourcePhase->getProjectGeneralInfo();

        $isSourcePhaseInCountry = SpecialistPhase::IN_COUNTRY_TYPE === $sourcePhase->getType();
        $isSourcePhaseMixed = SpecialistPhase::MIXED_TYPE === $sourcePhase->getType();

        // Cloning info shared by different Phases type
        $generalInfo->setRegion($sourcePhaseGeneralInfo->getRegion());

        /** @var Country $country */
        foreach ($sourcePhaseGeneralInfo->getCountries() as $country) {
            $generalInfo->addCountry($country);
        }

        /** @var ReloLocation $reloLocation */
        foreach ($sourcePhaseGeneralInfo->getReloLocations() as $reloLocation) {
            $generalInfo->addReloLocation($reloLocation);
        }

        /** @var Post $post */
        foreach ($sourcePhaseGeneralInfo->getPosts() as $post) {
            $generalInfo->addPost($post);
        }

        $em->persist($generalInfo);

        // City Costs should be copy only for In Country and Mixed phases type
        if (($isPhaseInCountry || $isPhaseMixed)
            && ($isSourcePhaseInCountry || $isSourcePhaseMixed)
        ) {
            /** @var LivingExpense $sourcelivingExpenseEstimate */
            foreach ($sourcePhase->getBudget()->getLivingExpenseEstimates() as $sourceLivingExpenseEstimate) {
                /** @var InCountryAssignment $sourceInCountryAssignment */
                $sourceInCountryAssignment = $sourceLivingExpenseEstimate->getInCountryAssignment();

                $inCountryAssignment = new InCountryAssignment(
                    $sourceInCountryAssignment->getCountry()
                );

                $inCountryAssignment
                    ->setSpecialistProjectPhase($phase)
                    ->setCity($sourceInCountryAssignment->getCity());

                $livingExpenseEstimate = new LivingExpense(
                    $phase->getBudget(),
                    $inCountryAssignment
                );

                $em->persist($livingExpenseEstimate);

                /** @var LivingExpenseCostItem $sourceCostItem */
                foreach ($sourceLivingExpenseEstimate->getLivingExpenseCostItems() as $sourceCostItem) {
                    $costItem = new LivingExpenseCostItem(
                        $livingExpenseEstimate,
                        $sourceCostItem->getCostItem()
                    );

                    $costItem->setCostPerDay($sourceCostItem->getCostPerDay());

                    $em->persist($costItem);
                }
            }
        }

        // copies program description
	    $phase->setProgramDesc($sourcePhase->getProgramDesc());

        $em->flush();
    }

    /**
     * Copy "Contact Points" data from source.
     *
     * @param SpecialistPhase $phase The Specialist phase.
     * @param EntityManager   $em
     */
    private function copyContactPointsData(SpecialistPhase $phase, EntityManager $em)
    {
        /** @var GeneralInfo $sourcePhaseGeneralInfo */
        $generalInfo = $phase->getProjectGeneralInfo();

        /** @var SpecialistPhase $sourcePhase */
        $sourcePhase = $phase->getSourcePhase();
        /** @var GeneralInfo $sourcePhaseGeneralInfo */
        $sourcePhaseGeneralInfo = $sourcePhase->getProjectGeneralInfo();

        // When the GeneralInfo entity is created, Primary and Secondary PoC
        // are created as well as part of its constructor logic.  There's no
        // need for create new PoC, but update the ones already associated to
        // it.
        $contactPoints = $generalInfo->getContactPoints();

        /** @var ContactPoint $sourcePhaseContactPoint */
        foreach ($sourcePhaseGeneralInfo->getContactPoints() as $sourcePhaseContactPoint) {
            $contactPointType = $sourcePhaseContactPoint->getType();

            // Additional ones are the only ones created.  Primary and
            // Secondary are retrieved from the array and updated.
            if (ContactPointAdditional::ADDITIONAL_TYPE === $contactPointType) {
                $contactPoint = new ContactPointAdditional($generalInfo);
            } else {
                $contactPoint = $contactPoints
                    ->filter(
                        function ($currentContactPoint) use ($contactPointType) {
                            return $contactPointType === $currentContactPoint->getType();
                        }
                    )
                    ->first();
            }

            $contactPoint
                ->setFirstName($sourcePhaseContactPoint->getFirstName())
                ->setLastName($sourcePhaseContactPoint->getLastName())
                ->setTitle($sourcePhaseContactPoint->getTitle())
                ->setEmail($sourcePhaseContactPoint->getEmail())
                ->setAlternateEmail($sourcePhaseContactPoint->getAlternateEmail())
                ->setAnticipatedDepartureDate(
                    $sourcePhaseContactPoint->getAnticipatedDepartureDate()
                );

            $em->persist($contactPoint);
        }

        $em->flush();
    }

	/**
	 * Copy "Candidate Info" data from source.
	 *
	 * @param SpecialistPhase $phase The Specialist phase.
	 * @param EntityManager $em
	 */
	private function copyCandidateInfo(SpecialistPhase $phase, EntityManager $em)
	{
		/** @var SpecialistPhase $sourcePhase */
		$sourcePhase = $phase->getSourcePhase();

		$phase
			->setFurtherDetailsDesc($sourcePhase->getFurtherDetailsDesc())
//			->setDegreeRequirements($sourcePhase->getDegreeRequirements())
			->setDegreeRequirementsDesc($sourcePhase->getDegreeRequirementsDesc())
			->setProposedCandidate($sourcePhase->getProposedCandidate())
			->setProposedCandidateContacted($sourcePhase->getProposedCandidateContacted())
			->setProposedCandidateEmail($sourcePhase->getProposedCandidateEmail());

		// Areas of expertise
		foreach ($sourcePhase->getAreasOfExpertise() as $item) {
			$phase->addAreasOfExpertise($item);
		}

		$em->flush();
	}

	/**
	 * Copy "Requirements and Restrictions" data from source.
	 *
	 * @param SpecialistPhase $phase The Specialist phase.
	 * @param EntityManager $em
	 */
	private function copyRequirements(SpecialistPhase $phase, EntityManager $em)
	{
		/** @var SpecialistPhase $sourcePhase */
		$sourcePhase = $phase->getSourcePhase();
		/** @var GeneralInfo $sourceGeneralInfo */
		$sourceGeneralInfo = $sourcePhase->getProjectGeneralInfo();

		if ($phase->getType() != SpecialistPhase::VIRTUAL_TYPE) {
			$phase
				->setRequiredVisaType($sourcePhase->getRequiredVisaType())
				->setOtherVisaType($sourcePhase->getOtherVisaType())
				->setVisaAvgLength($sourcePhase->getVisaAvgLength());

			$phase->getProjectGeneralInfo()
				->setVisaRequirementsDesc($sourceGeneralInfo->getVisaRequirementsDesc())
//				->setRequiredMedicalConditions($sourceGeneralInfo->getRequiredMedicalConditions())
				->setMedicalRestrictionsDesc($sourceGeneralInfo->getMedicalRestrictionsDesc())
//				->setAgeRestriction($sourceGeneralInfo->getAgeRestriction())
				->setAgeRestrictionDesc($sourceGeneralInfo->getAgeRestrictionDesc())
				->setSecurityInfoDesc($sourceGeneralInfo->getSecurityInfoDesc());
		}

		$phase->getProjectGeneralInfo()
			->setAdditionalRequirementsDesc($sourceGeneralInfo->getAdditionalRequirementsDesc());

		$em->flush();
	}

}
