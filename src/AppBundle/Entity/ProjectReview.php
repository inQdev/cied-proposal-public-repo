<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectReview entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class ProjectReview extends BaseEntity
{

	/**
	 * @var ArrayCollection $fellowProjects
	 *
	 * @ORM\ManyToMany(
	 *     targetEntity="FellowProject",
	 *     inversedBy="fellowProjectReviews"
	 * )
	 * @ORM\JoinTable(name="fellow_project_review")
	 */
	protected $fellowProjects;

	/**
	 * Cascade deletion defined in ProjectReviewComment
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="ProjectReviewComment",
	 *     mappedBy="projectReview"
	 * )
	 */
	protected $projectReviewComments;

    /**
     * @var ArrayCollection $specialistProjects
     *
     * @ORM\ManyToMany(
     *     targetEntity="SpecialistProjectPhase",
     *     inversedBy="specialistProjectReviews"
     * )
     * @ORM\JoinTable(name="specialist_project_phase_review")
     */
    protected $specialistProjects;


	/********************************* Methods Definition ********************/

    /**
     * ProjectReview constructor.
     */
    public function __construct()
    {
        $this->fellowProjects = new ArrayCollection();
        $this->specialistProjects = new ArrayCollection();
    }

	/**
	 * Add fellowProject
	 *
	 * @param FellowProject $fellowProject
	 *
	 * @return ProjectReview
	 */
	public function addFellowProject(FellowProject $fellowProject)
	{
		$this->fellowProjects[] = $fellowProject;

		return $this;
	}

	/**
	 * Remove fellowProject
	 *
	 * @param FellowProject $fellowProject
	 */
	public function removeFellowProject(FellowProject $fellowProject)
	{
		$this->fellowProjects->removeElement($fellowProject);
	}

	/**
	 * Get fellowProjects
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFellowProjects()
	{
		return $this->fellowProjects;
	}
    

    /**
     * Add projectReviewComment
     *
     * @param \AppBundle\Entity\ProjectReviewComment $projectReviewComment
     *
     * @return ProjectReview
     */
    public function addProjectReviewComment(\AppBundle\Entity\ProjectReviewComment $projectReviewComment)
    {
        $this->projectReviewComments[] = $projectReviewComment;

        return $this;
    }

    /**
     * Remove projectReviewComment
     *
     * @param \AppBundle\Entity\ProjectReviewComment $projectReviewComment
     */
    public function removeProjectReviewComment(\AppBundle\Entity\ProjectReviewComment $projectReviewComment)
    {
        $this->projectReviewComments->removeElement($projectReviewComment);
    }

    /**
     * Get projectReviewComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectReviewComments()
    {
        return $this->projectReviewComments;
    }

    /**
     * Add specialistProject
     *
     * @param \AppBundle\Entity\SpecialistProjectPhase $specialistProject
     *
     * @return ProjectReview
     */
    public function addSpecialistProject(\AppBundle\Entity\SpecialistProjectPhase $specialistProject)
    {
        $this->specialistProjects[] = $specialistProject;

        return $this;
    }

    /**
     * Remove specialistProject
     *
     * @param \AppBundle\Entity\SpecialistProjectPhase $specialistProject
     */
    public function removeSpecialistProject(\AppBundle\Entity\SpecialistProjectPhase $specialistProject)
    {
        $this->specialistProjects->removeElement($specialistProject);
    }

    /**
     * Get specialistProjects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialistProjects()
    {
        return $this->specialistProjects;
    }
}
