<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Util\Canonicalizer;

/**
 * Class CiedFOSUBUserProvider
 *
 * @package AppBundle\Security
 */
class CiedFOSUBUserProvider extends BaseFOSUBProvider
{
	/**
	 * {@inheritDoc}
	 *
	 * @param \Symfony\Component\Security\Core\User\UserInterface $user
	 * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
	 */
	public function connect(UserInterface $user, UserResponseInterface $response)
	{

		// get property from provider configuration by provider name
//		dump('connect');
//		dump($response);
//		exit;
		$property = $this->getProperty($response);  // ciedId
		$setter = 'set'. ucfirst($property);
		$username = $response->getUsername(); // get the unique user identifier

		//we "disconnect" previously connected users
		$existingUser = $this->userManager->findUserBy(
			[$property => $username]
		);

		if (null !== $existingUser) {
			// set current user id and token to null for disconnect
			$existingUser->$setter(null);
//			$existingUser->setCiedAccessToken(null);
			$this->userManager->updateUser($existingUser);
		}

		//we connect current user, set current user id and token
		$user->$setter($response->getResponse()['sub']);

		$serviceAccessTokenName = $response->getResourceOwner()->getName() . 'AccessToken'; // ciedAccessToken
		$serviceAccessTokenSetter = 'set' . ucfirst($serviceAccessTokenName);

		$user->$serviceAccessTokenSetter($response->getAccessToken());

		// TODO: Get and save roles in DB row


		$this->userManager->updateUser($user);

	}

	/**
	 * {@inheritdoc}
	 *
	 * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
	 *
	 * @return \AppBundle\Entity\User|\FOS\UserBundle\Model\UserInterface
	 */
	public function loadUserByOAuthUserResponse(UserResponseInterface $response)
	{
//		$userEmail = $response->getEmail();
//		$user = $this->userManager->findUserByEmail($userEmail);

		// Felipe - 20161117: Refactored to avoid issue with email changes in Drupal not synced to Proposal
		// Knowing that ciedId is always defined for users in the Proposal system
		$property = $this->getProperty($response);  // ciedId
		$ciedId = $response->getResponse()['sub'];
		/** @var User $user */
		$user = $this->userManager->findUserBy([$property => $ciedId]);

		// if null, just create new user and set it properties
		$rawResponse = $response->getResponse();
		if (null === $user) {
			$user = new User();
			$user->setEnabled(true);
			$user->setCiedId($rawResponse['sub']);
			$user->setUsername($rawResponse['preferred_username']);
			$user->setEmail($rawResponse['email']);
			$user->setName($rawResponse['name']);
			$user->setPassword('');

			$this->userManager->updateUser($user);

			return $user;
		}

		// Checks if some fields are different and update the proposal record
		if ($user->getEmail() != $rawResponse['email']
			|| $user->getUsername() != $rawResponse['preferred_username']
			|| $user->getName() != $rawResponse['name']) {

			$canonicalizer = new Canonicalizer();

			$user->setEmail($rawResponse['email']);
			$user->setEmailCanonical($canonicalizer->canonicalize($rawResponse['email']));
			$user->setUsername($rawResponse['preferred_username']);
			$user->setUsernameCanonical($canonicalizer->canonicalize($rawResponse['preferred_username']));
			$user->setName($rawResponse['name']);

			$this->userManager->updateUser($user);
		}

		// TODO: Check if we need to update the roles here
//		if ($current_role) {
//			$current_role = 'ROLE_'. strtoupper(str_replace(' ', '_', $current_role));
//			$user->setRoles([$current_role]);
//		}

		// else update access token of existing user
		$serviceName = $response->getResourceOwner()->getName();
		$setter = 'set' . ucfirst($serviceName) . 'AccessToken';
		$user->$setter($response->getAccessToken()); //update access token

		return $user;
	}
}
