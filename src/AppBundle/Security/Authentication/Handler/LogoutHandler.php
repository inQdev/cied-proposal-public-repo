<?php

namespace AppBundle\Security\Authentication\Handler;

use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LogoutHandler
 *
 * @package AppBundle\Security\Authentication\Handler
 */
class LogoutHandler implements LogoutHandlerInterface
{

    /**
     * Invalidate the current session.
     *
     * @param Request        $request
     * @param Response       $response
     * @param TokenInterface $token
     */
    public function logout(
      Request $request,
      Response $response,
      TokenInterface $token
    )
    {
        $request->getSession()->invalidate();
    }
}