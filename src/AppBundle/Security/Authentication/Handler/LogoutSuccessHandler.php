<?php

namespace AppBundle\Security\Authentication\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

/**
 * LogoutSuccessHandler handler
 *
 * @package AppBundle\Security\Authentication\Handler
 */
class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Security\Http\HttpUtils $httpUtils
     */
    protected $httpUtils;

    /**
     * @var string $targetUrl
     */
    protected $targetUrl;

    /**
     * LogoutSuccessHandler constructor.
     *
     * @param HttpUtils $httpUtils
     * @param string    $targetUrl
     */
    public function __construct(HttpUtils $httpUtils, $targetUrl = '/')
    {
        $this->httpUtils = $httpUtils;

        $this->targetUrl = $targetUrl;
    }

    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @param Request $request
     *
     * @return Response never null
     */
    public function onLogoutSuccess(Request $request)
    {
        // This will redirect to the Application System (Drupal) logout process.
        // The `proposal` flag will let the Application System the process
        // started from the Proposal System.
        return $this->httpUtils->createRedirectResponse(
          $request,
          $this->targetUrl . '?proposal=1'
        );
    }
}
