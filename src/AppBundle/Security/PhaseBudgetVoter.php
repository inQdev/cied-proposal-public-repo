<?php
/**
 * Allows to decide when a user has access to a Specialist Project Phase Budget
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

namespace AppBundle\Security;

use AppBundle\Entity\SpecialistProjectPhaseBudget;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\User;
use Symfony\Component\ExpressionLanguage\Token;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class PhaseBudgetVoter extends BaseVoter
{
	const VIEW = 'view';
	const EDIT = 'edit';

	/**
	 * @param string $attribute
	 * @param mixed $subject
	 * @return bool
	 */
	protected function supports($attribute, $subject)
	{
		// if the attribute isn't one we support, return false
		if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
			return false;
		}

		// only vote on ProjectReview objects inside this voter
		if (!$subject instanceof SpecialistProjectPhaseBudget) {
			return false;
		}

		return true;
	}

	/**
	 * @param string $attribute
	 * @param mixed $subject
	 * @param TokenInterface $token
	 * @return bool
	 */
	protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
	{
		$user = $token->getUser();

		if (!$user instanceof User) {
			// the user must be logged in; if not, deny access
			return false;
		}

		// If the user is Super Admin or Admin
		if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
			return true;
		}

		if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
			return true;
		}

		// you know $subject is a Post object, thanks to supports
		/** @var SpecialistProjectPhaseBudget $phaseBudget */
		$phaseBudget = $subject;

		switch ($attribute) {
			case self::VIEW:
				return $this->canView($phaseBudget, $token);
			case self::EDIT:
				return $this->canEdit($phaseBudget, $token);
		}

		throw new \LogicException('This code should not be reached!');
	}


	private function canEdit(SpecialistProjectPhaseBudget $phaseBudget, TokenInterface $token) {

		$user = $token->getUser();

		// Gets Project General Info
		/** @var ProjectGeneralInfo $projectGeneralInfo */
		$projectGeneralInfo = $phaseBudget->getPhase()->getProjectGeneralInfo();
		
		// If proposal hasn't been submitted, check sharing settings
		if ($projectGeneralInfo->getSubmissionStatus() == 'not_submitted') {

			$result = $this->checkDirectAccess($projectGeneralInfo, $user);

		} else {

			$roleHierarchyVoter = $this->setRoleHierarchyVoter();
			$reviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();
			$result = false;

			switch ($reviewStatus) {
				case ProjectReviewStatus::UNDER_RELO_REVIEW:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RELO')) > 0 ? true : false );
					// TODO: Add conditions related to country, it could be implemented in the BaseVoter
					break;

				case ProjectReviewStatus::UNDER_RPO_REVIEW:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RPO')) > 0 ? true : false );
					// TODO: Add conditions related to region, it could be implemented in the BaseVoter
					break;

				case ProjectReviewStatus::REVIEW_COMPLETE:
				case ProjectReviewStatus::UNDER_ECA_REVIEW:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_STATE_DEPARTMENT')) > 0 ? true : false );
					break;

				case ProjectReviewStatus::UNDER_POST_RE_REVIEW:
				case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RELO')) > 0 ? true : false );
					$result = $result || $this->checkDirectAccess($projectGeneralInfo, $token->getUser());
					break;
			}

		}

		return $result;

	}


	private function canView(SpecialistProjectPhaseBudget $phaseBudget, TokenInterface $token) {
		return true;
	}

}