<?php

namespace AppBundle\SolrRepository;

use AppBundle\Entity\SpecialistProjectPhase;
use FS\SolrBundle\Solr;

/**
 * Class SpecialstProjectRepository
 *
 * @package AppBundle\SolrRepository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class SpecialistProjectPhaseRepository
{
    /**
     * SpecialstProjectRepository constructor.
     *
     * @param Solr $solr
     */
    public function __construct(Solr $solr)
    {
        $this->solrClient = $solr->getClient();
        $this->solrMapper = $solr->getMapper();
    }

    /**
     * @param array  $criteria
     * @param array  $facetFields
     * @param array  $allowedSorts
     * @param int    $page
     * @param string $sort
     * @param string $sortDirection
     *
     * @return array
     */
    public function getFacetedResultsForSearch(array $criteria, array $facetFields, array $allowedSorts, $page = 1, $sort = null, $sortDirection = null)
    {
        $query = $this->solrClient->createSelect(); // get a select query instance
        $facets = $query->getFacetSet(); // get access to the facetset options
        $helper = $query->getHelper();

        $query->setRows(10000);

        if ($sort && in_array($sort, $allowedSorts)) {
            $direction = $query::SORT_ASC;

            if ($sortDirection === 'desc') {
                $direction = $query::SORT_DESC;
            }

            $query->addSort($sort, $direction);
        }

        foreach ($criteria as $key => $criterion) {
            if ('cycle_id_i' === $key) {
                $query->createFilterQuery('cycle_id_i')->setQuery('cycle_id_i:' . $helper->escapeTerm($criterion[0]));
            } else {
                $query->createFilterQuery($key)->setQuery(
                    $key . ':(' . implode(
                        ' OR ',
                        array_map(
                            function ($term) use ($helper) {
                                return '"' . $helper->escapeTerm($term) . '"';
                            },
                            $criterion
                        )
                    ) . ')'
                );
            }
        }

        foreach ($facetFields as $filter) {
            $facets->createFacetField($filter)->setField($filter);
        }

        $rs = $this->solrClient->select($query);

        return [
            'facet_set' => $rs->getFacetSet(),
            'specialist_projects_phases' => $this->mapDocumentsToEntites($rs),
        ];
    }

    /**
     * @param $documents
     *
     * @return array
     */
    private function mapDocumentsToEntites($documents)
    {
        $blankEntity = (new \ReflectionClass(SpecialistProjectPhase::class))->newInstanceWithoutConstructor();
        $entities = [];

        foreach ($documents as $document) {
            $entities[] = $this->solrMapper->toEntity($document, $blankEntity);
        }

        return $entities;
    }
}
