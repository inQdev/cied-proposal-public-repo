<?php

namespace APIBundle\Util;

final class Util
{
    /**
     * Generates a unique token to append to every API Request
     *
     * @param $uid
     * @param $role
     *
     * @return string
     */
    public function generateAPIToken($uid, $role)
    {
        $secret = 'djf*//?rk5/*--+l24jk@flsdjk#sdjl*+0-.!+*?nsd$=fvsjl78GA*-jkfjhfh';

        return md5($secret . '˜' . $uid . '˜' . date('Y-m-d') . '˜' . $role);
    }
}
