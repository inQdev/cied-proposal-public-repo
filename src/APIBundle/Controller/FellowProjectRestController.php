<?php

namespace APIBundle\Controller;

use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\ProjectOutcome;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * FellowProjectRestController controller.
 *
 * @package APIBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FellowProjectRestController extends FOSRestController implements TokenAuthenticatedController
{
    /**
     * @return Response
     */
    public function postAllAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $fellowProposals = [];

        $uid = (int) $request->request->get('uid');
        $user = $em->getRepository(User::class)->findOneBy(['ciedId' => $uid]);

        if (null !== $user) {
            $role = strtolower(trim($request->request->get('role')));
            $fellowProjectRepo = $em->getRepository(FellowProject::class);

            if ('embassy' == $role) {
                $fellowProposals = $fellowProjectRepo->findAllByEmbassyUser($user);
            } elseif ('relo' == $role) {
                $fellowProposals = $fellowProjectRepo->findAllByRELOUser($user);
            } elseif ('rpo' == $role) {
                $fellowProposals = $fellowProjectRepo->findAllByRPOUser($user);
            }
        }

        $view = $this->view($fellowProposals);

        return $this->handleView($view);
    }


    public function postUnderecareviewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $fellowProposals = [];

        $uid = (int) $request->request->get('uid');
        $user = $em->getRepository(User::class)->findOneBy(['ciedId' => $uid]);

        if (null !== $user) {
            $role = strtolower(trim($request->request->get('role')));
            $fellowProjectRepo = $em->getRepository(FellowProject::class);

            if ('state department' == $role) {
                $fellowProposals = $fellowProjectRepo->findAllUnderECAReview();
            }
        }

        $view = $this->view($fellowProposals);

        return $this->handleView($view);
    }

    /**
     * @param Request $request The request.
     */
    public function postIndicatorsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $fellowProjectRepo = $this->get('proposal.solr_repository.fellow_project');

        try {
            /** @var CycleFellow $currentFellowCycle */
            $currentCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();
        } catch (NoResultException $e) {
            $currentCycle = $em->getRepository(CycleFellow::class)->findLast();
        }

        // Solr results will only retrieve information of those Projects
        // (a/k/a Proposals) that have been indexed.  Because of that, it is
        // necessary to iterate over all the existing data and compare it with the
        // Solr results.
        $regions = $em->getRepository(Region::class)->findAll();
        $reviewStatuses = $em->getRepository(ProjectReviewStatus::class)->findAll();
        $outcomes = $em->getRepository(ProjectOutcome::class)->findAll();

        $criteria = ['cycle_id_i' => [$currentCycle->getId()]];
        $facetFields = ['proposal_status_s', 'proposal_outcome_s', 'region_s'];

        $indicators = [];

        $results = $fellowProjectRepo->getFacetedResultsForSearch($criteria, $facetFields, []);

        $facets = $results['facet_set']->getFacets();

        /** @var Region $region */
        foreach ($regions as $region) {
            $regionAcronym = $region->getAcronym();

            $indicators['regions'][$regionAcronym] = array_key_exists($regionAcronym, $facets['region_s']->getValues())
                ? (int) $facets['region_s']->getValues()[$regionAcronym]
                : 0;
        }

        /** @var ProjectReviewStatus $reviewStatus */
        foreach ($reviewStatuses as $reviewStatus) {
            $reviewStatusName = $reviewStatus->getName();

            $indicators['statuses'][$reviewStatusName] = array_key_exists($reviewStatusName, $facets['proposal_status_s']->getValues())
                ? (int) $facets['proposal_status_s']->getValues()[$reviewStatusName]
                : 0;
        }

        // "Not submitted" is not a Review status, therefor, it has to be added
        // manually
        $indicators['statuses']['Unsubmitted'] = $facets['proposal_status_s']->getValues()['Not Submitted'];

        /** @var ProjectOutcome $outcome */
        foreach ($outcomes as $outcome) {
            $outcomeName = $outcome->getName();

            $indicators['outcomes'][$outcomeName] = array_key_exists($outcomeName, $facets['proposal_outcome_s']->getValues())
                ? (int) $facets['proposal_outcome_s']->getValues()[$outcomeName]
                : 0;
        }

        $view = $this->view($indicators);

        return $this->handleView($view);
    }
}
