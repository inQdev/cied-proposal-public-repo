<?php

namespace APIBundle\Controller;

use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * SpecialistProjectRestController controller
 *
 * @package APIBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class SpecialistProjectRestController extends FOSRestController implements TokenAuthenticatedController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function postAllAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $phases = [];

        $uid = (int)$request->request->get('uid');
        $user = $em->getRepository(User::class)->findOneBy(['ciedId' => $uid]);

        if (null !== $user) {
            $role = strtolower(trim($request->request->get('role')));
            $phaseRepo = $em->getRepository(SpecialistProjectPhase::class);

            if ('embassy' === $role) {
                $phases = $phaseRepo->findAllByEmbassyUser($user);
            } elseif ('relo' === $role) {
                $phases = $phaseRepo->findAllByRELOUser($user);
            } elseif ('rpo' === $role) {
                $phases = $phaseRepo->findAllByRPOUser($user);
            } elseif ('state department' === $role) {
                $phases = $phaseRepo->findAllUnderECAReview();
            }
        }

        $view = $this->view($phases);

        return $this->handleView($view);
    }
}
