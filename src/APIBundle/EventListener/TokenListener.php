<?php

namespace APIBundle\EventListener;

use APIBundle\Controller\TokenAuthenticatedController;
use APIBundle\Util\Util;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * TokenListener
 *
 * @package APIBundle\EventListener
 * @author Juan Obando <juan.obando@inqbation.com>
 */
class TokenListener
{
    /**
     * @var Util
     */
    private $util;

    /**
     * TokenListener constructor.
     * @param Util $util
     */
    public function __construct(Util $util)
    {
        $this->util = $util;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        /** @var Controller $controller */
        $controller = $event->getController();

        /*
        * $controller passed can be either a class or a Closure.
        * This is not usual in Symfony but it may happen.
        * If it is a class, it comes in array format
        */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof TokenAuthenticatedController) {
            $request = $event->getRequest();

            $token = $request->request->get('token');
            $uid = $request->request->get('uid');
            $role = $request->request->get('role');

            if ($token !== $this->util->generateAPIToken($uid, $role)) {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
        }
    }
}
