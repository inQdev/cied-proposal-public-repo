<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160627094818 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE cost_item SET description = \'In-country Visa/Residency Permit\' WHERE `description` LIKE \'In-country Visa/Residency Permit Fees\'');
        $this->addSql('DELETE FROM cost_item WHERE description LIKE \'Mobile Phone Start Up Fees\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE cost_item SET description = \'In-country Visa/Residency Permit Fees\' WHERE description LIKE \'In-country Visa/Residency Permit\'');
        $this->addSql('INSERT INTO cost_item (id, description, created_at, updated_at, type) VALUES (\'39\', \'Mobile Phone Start Up Fees\', \'2016-04-18 10:59:20\', \'2016-04-18 10:59:20\', \'1\')');
    }
}
