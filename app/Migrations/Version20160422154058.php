<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160422154058 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellow_project ADD bring_children TINYINT(1) DEFAULT NULL, ADD bring_unmarried_partner TINYINT(1) DEFAULT NULL, ADD bring_same_sex_partner TINYINT(1) DEFAULT NULL, CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellow_project DROP bring_children, DROP bring_unmarried_partner, DROP bring_same_sex_partner, CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
