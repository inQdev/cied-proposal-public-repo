<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160913155211 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE global_value ADD periodicity TINYINT(1) DEFAULT NULL, ADD rules VARCHAR(20) DEFAULT NULL, ADD specialist_type VARCHAR(20) DEFAULT NULL");
    }

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("DELETE FROM global_value WHERE project_type=1");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Stipend (per day)', 200, 1, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Pre-work days (per day)', 200, 1, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Post-work days (per day)', 200, 1, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Planning days (for virtual projects of 80 hours or less)', 200, 0, '<= 80 hours', 'virtual', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Planning days (for other virtual projects)', 200, 0, '> 80 hours', 'virtual', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Planning days (for all in-country projects)', 200, 0, null, 'in-country', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Dependent allowance', 200, 0, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('International travel related costs', 200, 0, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('International airfare', 200, 0, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Travel days', 200, 0, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, created_at, updated_at) VALUES('Post-travel rest day to assignment', 200, 0, null, 'mixed', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
	}
		/**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE global_value DROP periodicity, DROP rules, DROP specialist_type');
    }
    
    public function postDown(Schema $schema) {
	    $this->connection->executeQuery("DELETE FROM global_value WHERE project_type=1");   	
	    $this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('81','Stipend','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
	    $this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('82','Planning Days allowance','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
$this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('83','Program Activities Allowance (PAA)','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
$this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('84','Post-Arrival Orientation Allowance','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
$this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('85','Travel booked by Georgetown','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
$this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('86','Pre-departure expenses','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
$this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('87','Rest stop expenses','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");
$this->connection->executeQuery("insert into global_value (id, name, default_value, created_at, updated_at, project_type) values('88','Other Costs','200.00','2016-04-18 10:58:53','2016-09-08 16:33:49','1')");


    }}
