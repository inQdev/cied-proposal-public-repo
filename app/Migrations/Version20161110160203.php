<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161110160203 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
	    // this up() migration is auto-generated, please modify it to your needs
	    $this->addSql("INSERT INTO post (country_id, name, created_at, updated_at) VALUES ((SELECT id FROM country WHERE name LIKE 'China%'), 'Wuhan', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
	    $this->addSql("DELETE FROM post WHERE name LIKE 'Wuhan' AND country_id = (SELECT id FROM country WHERE name LIKE 'China%')");
    }
}
