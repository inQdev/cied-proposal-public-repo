<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160803093554 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE living_expense DROP FOREIGN KEY FK_D56750C01EBED7F3');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C01EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE in_country_assignment_activity DROP FOREIGN KEY FK_8B0FDBDB1EBED7F3');
        $this->addSql('ALTER TABLE in_country_assignment_activity ADD CONSTRAINT FK_8B0FDBDB1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE living_expense_cost_item DROP FOREIGN KEY FK_27A7572B9E83FA0A');
        $this->addSql('ALTER TABLE living_expense_cost_item ADD CONSTRAINT FK_27A7572B9E83FA0A FOREIGN KEY (living_expense_id) REFERENCES living_expense (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE in_country_assignment_activity DROP FOREIGN KEY FK_8B0FDBDB1EBED7F3');
        $this->addSql('ALTER TABLE in_country_assignment_activity ADD CONSTRAINT FK_8B0FDBDB1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id)');
        $this->addSql('ALTER TABLE living_expense DROP FOREIGN KEY FK_D56750C01EBED7F3');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C01EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id)');
        $this->addSql('ALTER TABLE living_expense_cost_item DROP FOREIGN KEY FK_27A7572B9E83FA0A');
        $this->addSql('ALTER TABLE living_expense_cost_item ADD CONSTRAINT FK_27A7572B9E83FA0A FOREIGN KEY (living_expense_id) REFERENCES living_expense (id)');
    }
}
