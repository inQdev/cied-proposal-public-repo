<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160830101808 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE itinerary_activity CHANGE activity_desc activity_type_other VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE itinerary_activity ADD activity_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE itinerary_activity ADD CONSTRAINT FK_8D963D9BC51EFA73 FOREIGN KEY (activity_type_id) REFERENCES base_term (id)');
        $this->addSql('CREATE INDEX IDX_8D963D9BC51EFA73 ON itinerary_activity (activity_type_id)');
    }

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Workshop', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6')");
		$this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Conference', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6')");
		$this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Demonstration', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6')");
		$this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Consultation', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6')");
		$this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Observation', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6')");
		$this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Other', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6')");
	}
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE itinerary_activity DROP FOREIGN KEY FK_8D963D9BC51EFA73');
        $this->addSql('DROP INDEX IDX_8D963D9BC51EFA73 ON itinerary_activity');
        $this->addSql('ALTER TABLE itinerary_activity DROP activity_type_id');
	    $this->addSql('ALTER TABLE itinerary_activity CHANGE activity_type_other activity_desc LONGTEXT DEFAULT NULL');
	    $this->addSql('ALTER TABLE itinerary_activity CHANGE activity_type_other activity_desc LONGTEXT DEFAULT NULL');
    }
}
