<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160808151552 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE itinerary_activity (id INT AUTO_INCREMENT NOT NULL, in_country_assignment_id INT DEFAULT NULL, activity_desc LONGTEXT DEFAULT NULL, topic VARCHAR(255) DEFAULT NULL, audience_desc VARCHAR(255) DEFAULT NULL, duration VARCHAR(100) DEFAULT NULL, partnering_organization VARCHAR(255) DEFAULT NULL, activity_date DATE NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8D963D9B1EBED7F3 (in_country_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary_activity ADD CONSTRAINT FK_8D963D9B1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE in_country_assignment_activity');
        $this->addSql('ALTER TABLE in_country_assignment DROP FOREIGN KEY FK_A9F5D4761FB8D185');
        $this->addSql('DROP INDEX IDX_A9F5D4761FB8D185 ON in_country_assignment');
        $this->addSql('ALTER TABLE in_country_assignment DROP host_id, DROP deliverables_desc, DROP host_desc');
        $this->addSql('ALTER TABLE living_expense_cost_item CHANGE cost_per_day cost_per_day NUMERIC(10, 2) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE in_country_assignment_activity (id INT AUTO_INCREMENT NOT NULL, in_country_assignment_id INT DEFAULT NULL, activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, topic VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, audience_desc VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, participants SMALLINT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8B0FDBDB1EBED7F3 (in_country_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE in_country_assignment_activity ADD CONSTRAINT FK_8B0FDBDB1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE itinerary_activity');
        $this->addSql('ALTER TABLE in_country_assignment ADD host_id INT DEFAULT NULL, ADD deliverables_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, ADD host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE in_country_assignment ADD CONSTRAINT FK_A9F5D4761FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('CREATE INDEX IDX_A9F5D4761FB8D185 ON in_country_assignment (host_id)');
        $this->addSql('ALTER TABLE living_expense_cost_item CHANGE cost_per_day cost_per_day NUMERIC(10, 2) DEFAULT \'0.00\'');
    }
}
