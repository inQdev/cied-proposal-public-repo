<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160627161240 extends AbstractMigration
{
	/**
	 * @param Schema $schema
	 */
	public function up(Schema $schema)
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE project_global_value DROP FOREIGN KEY FK_73D5C61449A59333');
		$this->addSql('ALTER TABLE project_global_value ADD CONSTRAINT FK_73D5C61449A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE fellow_project_budget_totals DROP FOREIGN KEY FK_856B8DC749A59333');
		$this->addSql('ALTER TABLE fellow_project_budget_totals ADD CONSTRAINT FK_856B8DC749A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE location_host_info DROP FOREIGN KEY FK_5B11604398358A31');
		$this->addSql('ALTER TABLE location_host_info ADD CONSTRAINT FK_5B11604398358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE contact_point DROP FOREIGN KEY FK_9A8FE32B149A197C');
		$this->addSql('ALTER TABLE contact_point ADD CONSTRAINT FK_9A8FE32B149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE fellow_project_budget DROP FOREIGN KEY FK_58EDDE8998358A31');
		$this->addSql('ALTER TABLE fellow_project_budget ADD CONSTRAINT FK_58EDDE8998358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE fellow_project_budget_cost_item DROP FOREIGN KEY FK_C0A5B2BC49A59333');
		$this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD CONSTRAINT FK_C0A5B2BC49A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE fellow_project DROP FOREIGN KEY FK_4B19EE78149A197C');
		$this->addSql('ALTER TABLE fellow_project ADD CONSTRAINT FK_4B19EE78149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE wizard_step_status DROP FOREIGN KEY FK_3B230E5B98358A31');
		$this->addSql('ALTER TABLE wizard_step_status ADD CONSTRAINT FK_3B230E5B98358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE fellowship_duty_activity DROP FOREIGN KEY FK_B2D7EF4E98358A31');
		$this->addSql('ALTER TABLE fellowship_duty_activity ADD CONSTRAINT FK_B2D7EF4E98358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE project_review_comment DROP FOREIGN KEY FK_E62DD2392F12D34C');
		$this->addSql('ALTER TABLE project_review_comment ADD CONSTRAINT FK_E62DD2392F12D34C FOREIGN KEY (project_review_id) REFERENCES project_review (id) ON DELETE CASCADE');
	}

	/**
	 * @param Schema $schema
	 */
	public function down(Schema $schema)
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE contact_point DROP FOREIGN KEY FK_9A8FE32B149A197C');
		$this->addSql('ALTER TABLE contact_point ADD CONSTRAINT FK_9A8FE32B149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id)');
		$this->addSql('ALTER TABLE fellow_project DROP FOREIGN KEY FK_4B19EE78149A197C');
		$this->addSql('ALTER TABLE fellow_project ADD CONSTRAINT FK_4B19EE78149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id)');
		$this->addSql('ALTER TABLE fellow_project_budget DROP FOREIGN KEY FK_58EDDE8998358A31');
		$this->addSql('ALTER TABLE fellow_project_budget ADD CONSTRAINT FK_58EDDE8998358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id)');
		$this->addSql('ALTER TABLE fellow_project_budget_cost_item DROP FOREIGN KEY FK_C0A5B2BC49A59333');
		$this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD CONSTRAINT FK_C0A5B2BC49A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id)');
		$this->addSql('ALTER TABLE fellow_project_budget_totals DROP FOREIGN KEY FK_856B8DC749A59333');
		$this->addSql('ALTER TABLE fellow_project_budget_totals ADD CONSTRAINT FK_856B8DC749A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id)');
		$this->addSql('ALTER TABLE location_host_info DROP FOREIGN KEY FK_5B11604398358A31');
		$this->addSql('ALTER TABLE location_host_info ADD CONSTRAINT FK_5B11604398358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id)');
		$this->addSql('ALTER TABLE project_global_value DROP FOREIGN KEY FK_73D5C61449A59333');
		$this->addSql('ALTER TABLE project_global_value ADD CONSTRAINT FK_73D5C61449A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id)');
		$this->addSql('ALTER TABLE wizard_step_status DROP FOREIGN KEY FK_3B230E5B98358A31');
		$this->addSql('ALTER TABLE wizard_step_status ADD CONSTRAINT FK_3B230E5B98358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id)');
		$this->addSql('ALTER TABLE fellowship_duty_activity DROP FOREIGN KEY FK_B2D7EF4E98358A31');
		$this->addSql('ALTER TABLE fellowship_duty_activity ADD CONSTRAINT FK_B2D7EF4E98358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id)');
		$this->addSql('ALTER TABLE project_review_comment DROP FOREIGN KEY FK_E62DD2392F12D34C');
		$this->addSql('ALTER TABLE project_review_comment ADD CONSTRAINT FK_E62DD2392F12D34C FOREIGN KEY (project_review_id) REFERENCES project_review (id)');
	}
}
