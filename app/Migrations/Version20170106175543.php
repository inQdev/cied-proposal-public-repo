<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170106175543 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_global_value DROP FOREIGN KEY FK_73D5C61449A59333');
        $this->addSql('DROP INDEX IDX_73D5C61449A59333 ON project_global_value');
        $this->addSql('ALTER TABLE project_global_value DROP fellow_project_budget_id');
        $this->addSql('ALTER TABLE fellow_project_budget_totals DROP FOREIGN KEY FK_856B8DC749A59333');
        $this->addSql('DROP INDEX IDX_856B8DC749A59333 ON fellow_project_budget_totals');
        $this->addSql('ALTER TABLE fellow_project_budget_totals DROP fellow_project_budget_id');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item DROP FOREIGN KEY FK_C0A5B2BC49A59333');
        $this->addSql('DROP INDEX IDX_C0A5B2BC49A59333 ON fellow_project_budget_cost_item');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item DROP fellow_project_budget_id');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals DROP FOREIGN KEY FK_B2D5958C49A59333');
        $this->addSql('DROP INDEX IDX_B2D5958C49A59333 ON fellow_project_budget_onetime_totals');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals DROP fellow_project_budget_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD fellow_project_budget_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD CONSTRAINT FK_C0A5B2BC49A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C0A5B2BC49A59333 ON fellow_project_budget_cost_item (fellow_project_budget_id)');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals ADD fellow_project_budget_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals ADD CONSTRAINT FK_B2D5958C49A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_B2D5958C49A59333 ON fellow_project_budget_onetime_totals (fellow_project_budget_id)');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD fellow_project_budget_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD CONSTRAINT FK_856B8DC749A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_856B8DC749A59333 ON fellow_project_budget_totals (fellow_project_budget_id)');
        $this->addSql('ALTER TABLE project_global_value ADD fellow_project_budget_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_global_value ADD CONSTRAINT FK_73D5C61449A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_73D5C61449A59333 ON project_global_value (fellow_project_budget_id)');
    }

	public function postDown(Schema $schema)
	{
		$this->connection->executeQuery("UPDATE fellow_project_budget_cost_item SET fellow_project_budget_id = budget_revision_id");
		$this->connection->executeQuery("UPDATE fellow_project_budget_onetime_totals SET fellow_project_budget_id = budget_revision_id");
		$this->connection->executeQuery("UPDATE fellow_project_budget_totals SET fellow_project_budget_id = budget_revision_id");
		$this->connection->executeQuery("UPDATE project_global_value SET fellow_project_budget_id = budget_revision_id");
	}
}
