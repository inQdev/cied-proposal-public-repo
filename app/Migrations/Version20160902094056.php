<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20160902094056 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20160902094056 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
          $this->connection->getDatabasePlatform()->getName() != 'mysql',
          'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
          'CREATE TABLE itinerary_activities_anticipated_audience (id INT AUTO_INCREMENT NOT NULL, activity_id INT DEFAULT NULL, audience_id INT DEFAULT NULL, estimated_participants INT DEFAULT NULL, audience_other VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DE5E65FB81C06096 (activity_id), INDEX IDX_DE5E65FB848CC616 (audience_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
          'ALTER TABLE itinerary_activities_anticipated_audience ADD CONSTRAINT FK_DE5E65FB81C06096 FOREIGN KEY (activity_id) REFERENCES itinerary_activity (id)'
        );
        $this->addSql(
          'ALTER TABLE itinerary_activities_anticipated_audience ADD CONSTRAINT FK_DE5E65FB848CC616 FOREIGN KEY (audience_id) REFERENCES base_term (id)'
        );
        $this->addSql('ALTER TABLE itinerary_activity DROP audience_desc');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type=7");

        $this->connection->executeQuery(
          "INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Pre-service teachers', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('In-service teachers (K-6)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('In-service teachers (7-12)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('University teachers', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('Trainer of Trainers', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('MOE Officials', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('Access Teachers', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('Plenary Audience', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7), ('Other', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 7)"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
          $this->connection->getDatabasePlatform()->getName() != 'mysql',
          'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE itinerary_activities_anticipated_audience');
        $this->addSql(
          'ALTER TABLE itinerary_activity ADD audience_desc VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci'
        );
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type=7");
    }
}
