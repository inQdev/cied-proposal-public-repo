<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161223154639 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fellow_project_budget_onetime_totals (id INT AUTO_INCREMENT NOT NULL, fellow_project_budget_id INT DEFAULT NULL, revision_id INT NOT NULL, full_cost NUMERIC(14, 2) DEFAULT \'0\', post_contribution NUMERIC(14, 2) DEFAULT \'0\', host_contribution_monetary NUMERIC(14, 2) DEFAULT \'0\', host_contribution_in_kind NUMERIC(14, 2) DEFAULT \'0\', post_host_total_contribution NUMERIC(14, 2) DEFAULT \'0\', post_total_contribution NUMERIC(14, 2) DEFAULT \'0\', eca_total_contribution NUMERIC(14, 2) DEFAULT \'0\', post_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', post_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', host_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', host_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B2D5958C49A59333 (fellow_project_budget_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals ADD CONSTRAINT FK_B2D5958C49A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD post_host_total_contribution NUMERIC(14, 2) DEFAULT \'0\', ADD post_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', ADD post_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', CHANGE full_cost full_cost NUMERIC(14, 2) DEFAULT \'0\', CHANGE post_contribution post_contribution NUMERIC(14, 2) DEFAULT \'0\', CHANGE host_contribution_monetary host_contribution_monetary NUMERIC(14, 2) DEFAULT \'0\', CHANGE host_contribution_in_kind host_contribution_in_kind NUMERIC(14, 2) DEFAULT \'0\', CHANGE post_total_contribution post_total_contribution NUMERIC(14, 2) DEFAULT \'0\', CHANGE eca_total_contribution eca_total_contribution NUMERIC(14, 2) DEFAULT \'0\', CHANGE global global NUMERIC(14, 2) NOT NULL, CHANGE total total NUMERIC(14, 2) NOT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD post_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', ADD post_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', ADD updated_at DATETIME NOT NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE fellow_project_budget_onetime_totals');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item DROP post_contribution_gu, DROP post_contribution_non_gu, DROP host_contribution_gu, DROP host_contribution_non_gu, DROP updated_at');
        $this->addSql('ALTER TABLE fellow_project_budget_totals DROP post_host_total_contribution, DROP post_contribution_gu, DROP post_contribution_non_gu, DROP host_contribution_gu, DROP host_contribution_non_gu, CHANGE global global NUMERIC(10, 2) NOT NULL, CHANGE total total NUMERIC(10, 2) NOT NULL, CHANGE full_cost full_cost NUMERIC(10, 2) NOT NULL, CHANGE post_contribution post_contribution NUMERIC(10, 2) NOT NULL, CHANGE host_contribution_monetary host_contribution_monetary NUMERIC(10, 2) NOT NULL, CHANGE host_contribution_in_kind host_contribution_in_kind NUMERIC(10, 2) NOT NULL, CHANGE post_total_contribution post_total_contribution NUMERIC(10, 2) NOT NULL, CHANGE eca_total_contribution eca_total_contribution NUMERIC(10, 2) NOT NULL');
    }
}
