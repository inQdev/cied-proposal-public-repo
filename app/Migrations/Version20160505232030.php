<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160505232030 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE day_breakdown_activity ADD specialist_project_phase_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD CONSTRAINT FK_A1A860DC4A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id)');
        $this->addSql('CREATE INDEX IDX_A1A860DC4A1B6C5F ON day_breakdown_activity (specialist_project_phase_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE day_breakdown_activity DROP FOREIGN KEY FK_A1A860DC4A1B6C5F');
        $this->addSql('DROP INDEX IDX_A1A860DC4A1B6C5F ON day_breakdown_activity');
        $this->addSql('ALTER TABLE day_breakdown_activity DROP specialist_project_phase_id');
    }
}
