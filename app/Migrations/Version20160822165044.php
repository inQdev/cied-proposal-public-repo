<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160822165044 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql('DELETE FROM contribution_living_expense_cost_item');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729C5401DA61');
        $this->addSql('DROP INDEX IDX_7525729C5401DA61 ON contribution_living_expense_cost_item');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item CHANGE cost_item_id living_expense_cost_item_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729CEF1191CE FOREIGN KEY (living_expense_cost_item_id) REFERENCES living_expense_cost_item (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7525729CEF1191CE ON contribution_living_expense_cost_item (living_expense_cost_item_id)');
        $this->addSql('ALTER TABLE contribution_living_expense DROP FOREIGN KEY FK_6B26F0D2F92F3E70');
        $this->addSql('DROP INDEX IDX_6B26F0D2F92F3E70 ON contribution_living_expense');
        $this->addSql('ALTER TABLE contribution_living_expense DROP country_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contribution_living_expense ADD country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution_living_expense ADD CONSTRAINT FK_6B26F0D2F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('CREATE INDEX IDX_6B26F0D2F92F3E70 ON contribution_living_expense (country_id)');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729CEF1191CE');
        $this->addSql('DROP INDEX UNIQ_7525729CEF1191CE ON contribution_living_expense_cost_item');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item CHANGE living_expense_cost_item_id cost_item_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729C5401DA61 FOREIGN KEY (cost_item_id) REFERENCES cost_item (id)');
        $this->addSql('CREATE INDEX IDX_7525729C5401DA61 ON contribution_living_expense_cost_item (cost_item_id)');
    }
}
