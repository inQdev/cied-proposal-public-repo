<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161213142542 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql('DROP TABLE specialist_projects_areas_of_expertise');
	    $this->addSql('ALTER TABLE specialist_project DROP title, DROP further_details_desc, DROP degree_requirements, DROP degree_requirements_desc, DROP proposed_candidate, DROP proposed_candidate_contacted, DROP proposed_candidate_email, DROP subsequent_phases_desc');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql('CREATE TABLE specialist_projects_areas_of_expertise (specialist_project_id INT NOT NULL, area_of_expertise_id INT NOT NULL, INDEX IDX_F7732EDC1BB7C343 (specialist_project_id), INDEX IDX_F7732EDC52D09986 (area_of_expertise_id), PRIMARY KEY(specialist_project_id, area_of_expertise_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
	    $this->addSql('ALTER TABLE specialist_projects_areas_of_expertise ADD CONSTRAINT FK_F7732EDC1BB7C343 FOREIGN KEY (specialist_project_id) REFERENCES specialist_project (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE specialist_projects_areas_of_expertise ADD CONSTRAINT FK_F7732EDC52D09986 FOREIGN KEY (area_of_expertise_id) REFERENCES base_term (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE specialist_project ADD title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD further_details_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, ADD degree_requirements TINYINT(1) DEFAULT NULL, ADD degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, ADD proposed_candidate VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD proposed_candidate_contacted TINYINT(1) DEFAULT NULL, ADD proposed_candidate_email VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD subsequent_phases_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }

	public function postDown(Schema $schema)
	{
		$this->connection->executeQuery("UPDATE specialist_project SP SET title = (SELECT title FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET further_details_desc = (SELECT further_details_desc FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET degree_requirements = (SELECT degree_requirements FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET degree_requirements_desc = (SELECT degree_requirements_desc FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET proposed_candidate = (SELECT proposed_candidate FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET proposed_candidate_contacted = (SELECT proposed_candidate_contacted FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET proposed_candidate_email = (SELECT proposed_candidate_email FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
		$this->connection->executeQuery("UPDATE specialist_project SP SET subsequent_phases_desc = (SELECT subsequent_phases_desc FROM specialist_project_phase SPP WHERE SPP.specialist_project_id = SP.id)");
	}
}
