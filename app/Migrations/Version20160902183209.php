<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20160902183209 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20160902183209 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
          $this->connection->getDatabasePlatform()->getName() != 'mysql',
          'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
          'ALTER TABLE itinerary_activities_anticipated_audience DROP FOREIGN KEY FK_DE5E65FB81C06096'
        );
        $this->addSql(
          'ALTER TABLE itinerary_activities_anticipated_audience ADD CONSTRAINT FK_DE5E65FB81C06096 FOREIGN KEY (activity_id) REFERENCES itinerary_activity (id) ON DELETE CASCADE'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
          $this->connection->getDatabasePlatform()->getName() != 'mysql',
          'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
          'ALTER TABLE itinerary_activities_anticipated_audience DROP FOREIGN KEY FK_DE5E65FB81C06096'
        );
        $this->addSql(
          'ALTER TABLE itinerary_activities_anticipated_audience ADD CONSTRAINT FK_DE5E65FB81C06096 FOREIGN KEY (activity_id) REFERENCES itinerary_activity (id)'
        );
    }
}
