<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160815125522 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE itinerary_travel (id INT AUTO_INCREMENT NOT NULL, origin_id INT DEFAULT NULL, destination_id INT DEFAULT NULL, date DATE NOT NULL, mode VARCHAR(64) DEFAULT NULL, travel_time VARCHAR(64) DEFAULT NULL, booking_responsible VARCHAR(45) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_5EBB7CFB56A273CC (origin_id), INDEX IDX_5EBB7CFB816C6140 (destination_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary_travel ADD CONSTRAINT FK_5EBB7CFB56A273CC FOREIGN KEY (origin_id) REFERENCES in_country_assignment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE itinerary_travel ADD CONSTRAINT FK_5EBB7CFB816C6140 FOREIGN KEY (destination_id) REFERENCES in_country_assignment (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE itinerary_travel');
    }
}
