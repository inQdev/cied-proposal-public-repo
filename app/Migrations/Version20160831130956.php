<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160831130956 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE itinerary_activities_partnering_organizations (itinerary_activity_id INT NOT NULL, partnering_organization_id INT NOT NULL, INDEX IDX_62CA5E4CE483EC63 (itinerary_activity_id), INDEX IDX_62CA5E4C41F1045F (partnering_organization_id), PRIMARY KEY(itinerary_activity_id, partnering_organization_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary_activities_partnering_organizations ADD CONSTRAINT FK_62CA5E4CE483EC63 FOREIGN KEY (itinerary_activity_id) REFERENCES itinerary_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE itinerary_activities_partnering_organizations ADD CONSTRAINT FK_62CA5E4C41F1045F FOREIGN KEY (partnering_organization_id) REFERENCES partnering_organization (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE itinerary_activity DROP partnering_organization');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE itinerary_activities_partnering_organizations');
        $this->addSql('ALTER TABLE itinerary_activity ADD partnering_organization VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
