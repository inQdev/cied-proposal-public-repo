<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160613113919 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fellow_project_budget_totals (id INT AUTO_INCREMENT NOT NULL, fellow_project_budget_id INT DEFAULT NULL, revision_id INT NOT NULL, full_cost NUMERIC(10, 2) NOT NULL, post_contribution NUMERIC(10, 2) NOT NULL, host_contribution_monetary NUMERIC(10, 2) NOT NULL, host_contribution_in_kind NUMERIC(10, 2) NOT NULL, post_total_contribution NUMERIC(10, 2) NOT NULL, eca_total_contribution NUMERIC(10, 2) NOT NULL, global NUMERIC(10, 2) NOT NULL, total NUMERIC(10, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_856B8DC749A59333 (fellow_project_budget_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD CONSTRAINT FK_856B8DC749A59333 FOREIGN KEY (fellow_project_budget_id) REFERENCES fellow_project_budget (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE fellow_project_budget_totals');
    }
}
