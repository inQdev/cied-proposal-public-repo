<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160606162707 extends AbstractMigration
{
	/**
	 * @param Schema $schema
	 */
	public function up(Schema $schema)
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE sequence (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, nextValue INT NOT NULL, UNIQUE INDEX UNIQ_5286D72B5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
		$this->addSql('ALTER TABLE specialist_project ADD reference_number VARCHAR(255) DEFAULT NULL');
		$this->addSql('ALTER TABLE fellow_project ADD reference_number VARCHAR(255) DEFAULT NULL');
	}

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("INSERT INTO sequence (name, nextValue) VALUES ('FellowProject', 1)");
		$this->connection->executeQuery("INSERT INTO sequence (name, nextValue) VALUES ('SpecialistProject', 1)");
	}

	/**
	 * @param Schema $schema
	 */
	public function down(Schema $schema)
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE sequence');
		$this->addSql('ALTER TABLE fellow_project DROP reference_number');
		$this->addSql('ALTER TABLE specialist_project DROP reference_number');
	}
}
