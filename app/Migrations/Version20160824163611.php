<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160824163611 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contribution_transportation_cost_item ADD travel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item ADD CONSTRAINT FK_B72F48D2ECAB15B3 FOREIGN KEY (travel_id) REFERENCES itinerary_travel (id)');
        $this->addSql('CREATE INDEX IDX_B72F48D2ECAB15B3 ON contribution_transportation_cost_item (travel_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contribution_transportation_cost_item DROP FOREIGN KEY FK_B72F48D2ECAB15B3');
        $this->addSql('DROP INDEX IDX_B72F48D2ECAB15B3 ON contribution_transportation_cost_item');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item DROP travel_id');
    }
}
