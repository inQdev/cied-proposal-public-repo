<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160509185628 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE living_expense_cost_item DROP FOREIGN KEY FK_27A7572B9E83FA0A');
        $this->addSql('CREATE TABLE contribution_living_expense (id INT AUTO_INCREMENT NOT NULL, phase_budget_id INT DEFAULT NULL, country_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_6B26F0D257844D22 (phase_budget_id), INDEX IDX_6B26F0D2F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contribution_living_expense_cost_item (id INT AUTO_INCREMENT NOT NULL, cost_item_id INT DEFAULT NULL, contribution_living_expense_id INT DEFAULT NULL, total_cost NUMERIC(14, 2) DEFAULT \'0\', post_contribution NUMERIC(14, 2) DEFAULT \'0\', host_contribution_monetary NUMERIC(14, 2) DEFAULT \'0\', host_contribution_in_kind NUMERIC(14, 2) DEFAULT \'0\', post_host_total_contribution NUMERIC(14, 2) DEFAULT \'0\', post_total_contribution NUMERIC(14, 2) DEFAULT \'0\', eca_total_contribution NUMERIC(14, 2) DEFAULT \'0\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_7525729C5401DA61 (cost_item_id), INDEX IDX_7525729C2EFD4045 (contribution_living_expense_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contribution_living_expense ADD CONSTRAINT FK_6B26F0D257844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE contribution_living_expense ADD CONSTRAINT FK_6B26F0D2F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729C5401DA61 FOREIGN KEY (cost_item_id) REFERENCES cost_item (id)');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729C2EFD4045 FOREIGN KEY (contribution_living_expense_id) REFERENCES contribution_living_expense (id)');
        $this->addSql('DROP TABLE living_expense');
        $this->addSql('DROP TABLE living_expense_cost_item');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL, CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE pre_post_work CHANGE additional_pre_work_desc additional_pre_work_desc LONGTEXT DEFAULT NULL, CHANGE additional_post_work_desc additional_post_work_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project_phase_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729C2EFD4045');
        $this->addSql('CREATE TABLE living_expense (id INT AUTO_INCREMENT NOT NULL, phase_budget_id INT DEFAULT NULL, country_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D56750C057844D22 (phase_budget_id), INDEX IDX_D56750C0F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE living_expense_cost_item (id INT AUTO_INCREMENT NOT NULL, cost_item_id INT DEFAULT NULL, living_expense_id INT DEFAULT NULL, total_cost NUMERIC(14, 2) DEFAULT \'0.00\', post_contribution NUMERIC(14, 2) DEFAULT \'0.00\', host_contribution_monetary NUMERIC(14, 2) DEFAULT \'0.00\', host_contribution_in_kind NUMERIC(14, 2) DEFAULT \'0.00\', post_host_total_contribution NUMERIC(14, 2) DEFAULT \'0.00\', post_total_contribution NUMERIC(14, 2) DEFAULT \'0.00\', eca_total_contribution NUMERIC(14, 2) DEFAULT \'0.00\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_27A7572B5401DA61 (cost_item_id), INDEX IDX_27A7572B9E83FA0A (living_expense_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C057844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C0F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE living_expense_cost_item ADD CONSTRAINT FK_27A7572B5401DA61 FOREIGN KEY (cost_item_id) REFERENCES cost_item (id)');
        $this->addSql('ALTER TABLE living_expense_cost_item ADD CONSTRAINT FK_27A7572B9E83FA0A FOREIGN KEY (living_expense_id) REFERENCES living_expense (id)');
        $this->addSql('DROP TABLE contribution_living_expense');
        $this->addSql('DROP TABLE contribution_living_expense_cost_item');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE pre_post_work CHANGE additional_pre_work_desc additional_pre_work_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_post_work_desc additional_post_work_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project_phase_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
