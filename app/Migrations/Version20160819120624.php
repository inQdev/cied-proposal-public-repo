<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160819120624 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contribution_ground_transport_cost_item (id INT AUTO_INCREMENT NOT NULL, phase_budget_id INT DEFAULT NULL, cost_item_id INT DEFAULT NULL, total_cost NUMERIC(14, 2) DEFAULT \'0\', post_contribution NUMERIC(14, 2) DEFAULT \'0\', host_contribution_monetary NUMERIC(14, 2) DEFAULT \'0\', host_contribution_in_kind NUMERIC(14, 2) DEFAULT \'0\', post_host_total_contribution NUMERIC(14, 2) DEFAULT \'0\', post_total_contribution NUMERIC(14, 2) DEFAULT \'0\', eca_total_contribution NUMERIC(14, 2) DEFAULT \'0\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_E76506DE57844D22 (phase_budget_id), INDEX IDX_E76506DE5401DA61 (cost_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contribution_ground_transport_cost_item ADD CONSTRAINT FK_E76506DE57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contribution_ground_transport_cost_item ADD CONSTRAINT FK_E76506DE5401DA61 FOREIGN KEY (cost_item_id) REFERENCES cost_item (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contribution_ground_transport_cost_item');
    }
}
