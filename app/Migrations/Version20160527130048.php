<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160527130048 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus DROP FOREIGN KEY FK_12B045366E21606F');
        $this->addSql('DROP INDEX IDX_12B045366E21606F ON fellowship_duties_secondary_activities_focus');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus CHANGE duty_activity_focus_id duty_activity_secondary_focus_id INT NOT NULL');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus ADD CONSTRAINT FK_12B0453648EA31B3 FOREIGN KEY (duty_activity_secondary_focus_id) REFERENCES base_term (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_12B0453648EA31B3 ON fellowship_duties_secondary_activities_focus (duty_activity_secondary_focus_id)');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus ADD PRIMARY KEY (duty_activity_secondary_id, duty_activity_secondary_focus_id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type = 1 OR type = 4");

        $this->connection->executeQuery("INSERT INTO base_term(name, created_at, updated_at, type) VALUES ('4 Skills, Grammar, and Vocabulary', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('Content-based Instruction', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('TESOL methods and Techniques', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('Materials Development', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('Syllabus Design', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('English for Academic Purposes (EAP) (For example – but not limited to – Academic Writing)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('English for Specific Purposes (ESP) (For example – but not limited to – Law, Stem, Tourism, Business)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('Instructional Technology (CALL)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('Proficiency Exam Preparation (Including: TOEFL, IELTS, GRE)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1),('American Culture, Studies, and/or Literature', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1)");

        $this->connection->executeQuery("INSERT INTO base_term(name, created_at, updated_at, type) VALUES ('4 Skills, Grammar, and Vocabulary', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('Content-based Instruction', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('TESOL methods and Techniques', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('Materials Development', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('Syllabus Design', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('English for Academic Purposes (EAP) (For example – but not limited to – Academic Writing)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('English for Specific Purposes (ESP) (For example – but not limited to – Law, Stem, Tourism, Business)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('Instructional Technology (CALL)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('Proficiency Exam Preparation (Including: TOEFL, IELTS, GRE)', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('American Culture, Studies, and/or Literature', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4),('Leading Relevant Extra-curricular Activities', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus DROP FOREIGN KEY FK_12B0453648EA31B3');
        $this->addSql('DROP INDEX IDX_12B0453648EA31B3 ON fellowship_duties_secondary_activities_focus');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus CHANGE duty_activity_secondary_focus_id duty_activity_focus_id INT NOT NULL');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus ADD CONSTRAINT FK_12B045366E21606F FOREIGN KEY (duty_activity_focus_id) REFERENCES base_term (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_12B045366E21606F ON fellowship_duties_secondary_activities_focus (duty_activity_focus_id)');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus ADD PRIMARY KEY (duty_activity_secondary_id, duty_activity_focus_id)');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE description description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type = 1 OR type = 4");
    }
}
