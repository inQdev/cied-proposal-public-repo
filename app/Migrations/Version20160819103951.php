<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160819103951 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phase ADD visa_avg_length_id INT DEFAULT NULL, ADD required_visa_type VARCHAR(32) DEFAULT NULL, ADD other_visa_type VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project_phase ADD CONSTRAINT FK_6FB61C8C60AEFA9D FOREIGN KEY (visa_avg_length_id) REFERENCES base_term (id)');
        $this->addSql('CREATE INDEX IDX_6FB61C8C60AEFA9D ON specialist_project_phase (visa_avg_length_id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type = 5");

        $this->connection->executeQuery("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Upon arrival', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5), ('Less than 1 month', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5), ('1 month', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5), ('2 months', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5), ('3 months', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5), ('4 months', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5), ('5 months or more', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phase DROP FOREIGN KEY FK_6FB61C8C60AEFA9D');
        $this->addSql('DROP INDEX IDX_6FB61C8C60AEFA9D ON specialist_project_phase');
        $this->addSql('ALTER TABLE specialist_project_phase DROP visa_avg_length_id, DROP required_visa_type, DROP other_visa_type');
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type = 5");
    }
}
