<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160510165234 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE living_expense (id INT AUTO_INCREMENT NOT NULL, phase_budget_id INT DEFAULT NULL, in_country_assignment_id INT DEFAULT NULL, revision_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D56750C057844D22 (phase_budget_id), INDEX IDX_D56750C01EBED7F3 (in_country_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE living_expense_cost_item (id INT AUTO_INCREMENT NOT NULL, living_expense_id INT DEFAULT NULL, cost_item_id INT DEFAULT NULL, cost_per_day NUMERIC(10, 2) DEFAULT \'0\', number_of_days INT DEFAULT NULL, cost_total NUMERIC(10, 2) DEFAULT \'0\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_27A7572B9E83FA0A (living_expense_id), INDEX IDX_27A7572B5401DA61 (cost_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C057844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C01EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id)');
        $this->addSql('ALTER TABLE living_expense_cost_item ADD CONSTRAINT FK_27A7572B9E83FA0A FOREIGN KEY (living_expense_id) REFERENCES living_expense (id)');
        $this->addSql('ALTER TABLE living_expense_cost_item ADD CONSTRAINT FK_27A7572B5401DA61 FOREIGN KEY (cost_item_id) REFERENCES cost_item (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE living_expense_cost_item DROP FOREIGN KEY FK_27A7572B9E83FA0A');
        $this->addSql('DROP TABLE living_expense');
        $this->addSql('DROP TABLE living_expense_cost_item');
    }
}
