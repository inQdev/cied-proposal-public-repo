<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20161123164400 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20161123164400 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks DROP FOREIGN KEY FK_4DA0BB49C86F3B2F'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks ADD CONSTRAINT FK_4DA0BB49C86F3B2F FOREIGN KEY (week_id) REFERENCES itinerary_virtual_week (id) ON DELETE CASCADE'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks DROP FOREIGN KEY FK_4DA0BB49C86F3B2F'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks ADD CONSTRAINT FK_4DA0BB49C86F3B2F FOREIGN KEY (week_id) REFERENCES itinerary_virtual_week (id)'
        );
    }
}
