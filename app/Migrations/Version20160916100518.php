<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160916100518 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE specialist_project_phase_global_value (id INT AUTO_INCREMENT NOT NULL, global_value_id INT DEFAULT NULL, global_paa_id INT DEFAULT NULL, budget_id INT DEFAULT NULL, value NUMERIC(10, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_A0DE88682E46ED8C (global_value_id), INDEX IDX_A0DE8868EBB96B8B (global_paa_id), INDEX IDX_A0DE886836ABA6B8 (budget_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialist_project_phase_budget_total (id INT AUTO_INCREMENT NOT NULL, budget_id INT DEFAULT NULL, revision_id INT DEFAULT NULL, full_cost NUMERIC(10, 2) NOT NULL, post_contribution NUMERIC(10, 2) NOT NULL, host_contribution_monetary NUMERIC(10, 2) NOT NULL, host_contribution_in_kind NUMERIC(10, 2) NOT NULL, post_total_contribution NUMERIC(10, 2) NOT NULL, eca_total_contribution NUMERIC(10, 2) NOT NULL, global NUMERIC(10, 2) NOT NULL, total NUMERIC(10, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_5775C1AD36ABA6B8 (budget_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE specialist_project_phase_global_value ADD CONSTRAINT FK_A0DE88682E46ED8C FOREIGN KEY (global_value_id) REFERENCES global_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE specialist_project_phase_global_value ADD CONSTRAINT FK_A0DE8868EBB96B8B FOREIGN KEY (global_paa_id) REFERENCES global_paa (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE specialist_project_phase_global_value ADD CONSTRAINT FK_A0DE886836ABA6B8 FOREIGN KEY (budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE specialist_project_phase_budget_total ADD CONSTRAINT FK_5775C1AD36ABA6B8 FOREIGN KEY (budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE specialist_project_phase_global_value');
        $this->addSql('DROP TABLE specialist_project_phase_budget_total');
    }
}
