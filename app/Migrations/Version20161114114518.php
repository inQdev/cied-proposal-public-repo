<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20161114114518 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20161114114518 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            "DELETE FROM post WHERE name LIKE 'Hong Kong' AND country_id=(SELECT id FROM country WHERE name LIKE 'China%')"
        );
        $this->addSql("ALTER TABLE country MODIFY iso2_code VARCHAR(5)");
        $this->addSql(
            "INSERT INTO country (region_id, iso2_code, name, created_at, updated_at) VALUES ((SELECT id FROM region WHERE acronym='NEA'), 'IL-JM', 'Jerusalem', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        );
        $this->addSql(
            "INSERT INTO country (region_id, iso2_code, name, created_at, updated_at) VALUES ((SELECT id FROM region WHERE acronym='EAP'), 'MO', 'Macau', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        );
        $this->addSql(
            "INSERT INTO post (country_id, name, created_at, updated_at) VALUES ((SELECT id FROM country WHERE iso2_code='IL-JM'), 'Jerusalem', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        );
        $this->addSql(
            "INSERT INTO post (country_id, name, created_at, updated_at) VALUES ((SELECT id FROM country WHERE iso2_code='MO'), 'Macau', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            "INSERT INTO post (country_id, name, created_at, updated_at) VALUES ((SELECT id FROM country WHERE name LIKE 'China%'), 'Hong Kong', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        );
        $this->addSql(
            "DELETE FROM post WHERE name LIKE 'Jerusalem' AND country_id=(SELECT id FROM country WHERE iso2_code='IL-JM')"
        );
        $this->addSql(
            "DELETE FROM post WHERE name LIKE 'Macau' AND country_id=(SELECT id FROM country WHERE iso2_code='MO')"
        );
        $this->addSql("DELETE FROM country WHERE iso2_code='IL-JM'");
        $this->addSql("DELETE FROM country WHERE iso2_code='MO'");
    }
}
