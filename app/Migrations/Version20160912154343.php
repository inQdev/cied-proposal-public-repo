<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20160912154343 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20160912154343 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE projects_relo_locations (project_general_info_id INT NOT NULL, relo_location_id INT NOT NULL, INDEX IDX_CAB7270E149A197C (project_general_info_id), INDEX IDX_CAB7270E694A6237 (relo_location_id), PRIMARY KEY(project_general_info_id, relo_location_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE projects_relo_locations ADD CONSTRAINT FK_CAB7270E149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id) ON DELETE CASCADE'
        );
        // Keep existing association between Project General Info and RELO Location.
        $this->addSql('INSERT INTO projects_relo_locations(project_general_info_id, relo_location_id) SELECT id, relo_location_id FROM project_general_info WHERE relo_location_id IS NOT NULL');
        $this->addSql(
            'ALTER TABLE projects_relo_locations ADD CONSTRAINT FK_CAB7270E694A6237 FOREIGN KEY (relo_location_id) REFERENCES relo_location (id) ON DELETE CASCADE'
        );
        $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515694A6237');
        $this->addSql('DROP INDEX IDX_19EDC515694A6237 ON project_general_info');
        $this->addSql('ALTER TABLE project_general_info DROP relo_location_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE project_general_info ADD relo_location_id INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515694A6237 FOREIGN KEY (relo_location_id) REFERENCES relo_location (id)'
        );
        $this->addSql('CREATE INDEX IDX_19EDC515694A6237 ON project_general_info (relo_location_id)');
        // Keep existing association between Project General Info and RELO Location.
        $this->addSql('UPDATE project_general_info pgi INNER JOIN projects_relo_locations prl ON pgi.id=prl.project_general_info_id set pgi.relo_location_id=prl.relo_location_id');
        $this->addSql('DROP TABLE projects_relo_locations');
    }
}
