<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161213164934 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phase ADD source_phase_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project_phase ADD CONSTRAINT FK_6FB61C8CDA29275A FOREIGN KEY (source_phase_id) REFERENCES specialist_project_phase (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_6FB61C8CDA29275A ON specialist_project_phase (source_phase_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phase DROP FOREIGN KEY FK_6FB61C8CDA29275A');
        $this->addSql('DROP INDEX IDX_6FB61C8CDA29275A ON specialist_project_phase');
        $this->addSql('ALTER TABLE specialist_project_phase DROP source_phase_id');
    }
}
