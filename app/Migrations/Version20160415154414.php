<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160415154414 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS projects_user_authors (user_id INT NOT NULL, project_general_info_id INT NOT NULL, INDEX IDX_BF5A2801A76ED395 (user_id), INDEX IDX_BF5A2801149A197C (project_general_info_id), PRIMARY KEY(user_id, project_general_info_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE projects_user_authors ADD CONSTRAINT FK_BF5A2801A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE projects_user_authors ADD CONSTRAINT FK_BF5A2801149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE IF EXISTS project_owner');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info ADD created_by INT DEFAULT NULL, ADD updated_by INT DEFAULT NULL, ADD submitted_by INT DEFAULT NULL, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC51516FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515641EE842 FOREIGN KEY (submitted_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_19EDC515DE12AB56 ON project_general_info (created_by)');
        $this->addSql('CREATE INDEX IDX_19EDC51516FE72E1 ON project_general_info (updated_by)');
        $this->addSql('CREATE INDEX IDX_19EDC515641EE842 ON project_general_info (submitted_by)');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS project_owner (id INT AUTO_INCREMENT NOT NULL, project_general_info_id INT DEFAULT NULL, user_id INT DEFAULT NULL, submitter TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DC35EFE2149A197C (project_general_info_id), INDEX IDX_DC35EFE2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_owner ADD CONSTRAINT FK_DC35EFE2149A197C FOREIGN KEY (project_general_info_id) REFERENCES project_general_info (id)');
        $this->addSql('ALTER TABLE project_owner ADD CONSTRAINT FK_DC35EFE2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE IF EXISTS projects_user_authors');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515DE12AB56');
        $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC51516FE72E1');
        $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515641EE842');
        $this->addSql('DROP INDEX IDX_19EDC515DE12AB56 ON project_general_info');
        $this->addSql('DROP INDEX IDX_19EDC51516FE72E1 ON project_general_info');
        $this->addSql('DROP INDEX IDX_19EDC515641EE842 ON project_general_info');
        $this->addSql('ALTER TABLE project_general_info DROP created_by, DROP updated_by, DROP submitted_by, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
