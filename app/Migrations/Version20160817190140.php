<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160817190140 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE day_breakdown_activity');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729C2EFD4045');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729C2EFD4045 FOREIGN KEY (contribution_living_expense_id) REFERENCES contribution_living_expense (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contribution_one_time_cost_item DROP FOREIGN KEY FK_3B7A17D629B545C3');
        $this->addSql('ALTER TABLE contribution_one_time_cost_item ADD CONSTRAINT FK_3B7A17D629B545C3 FOREIGN KEY (contribution_one_time_id) REFERENCES contribution_one_time (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item DROP FOREIGN KEY FK_B72F48D247C532AB');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item ADD CONSTRAINT FK_B72F48D247C532AB FOREIGN KEY (contribution_transportation_id) REFERENCES contribution_transportation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE specialist_project_phase_budget DROP FOREIGN KEY FK_4DA1487F99091188');
        $this->addSql('ALTER TABLE specialist_project_phase_budget ADD CONSTRAINT FK_4DA1487F99091188 FOREIGN KEY (phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contribution_transportation DROP FOREIGN KEY FK_B7F0D23F57844D22');
        $this->addSql('ALTER TABLE contribution_transportation ADD CONSTRAINT FK_B7F0D23F57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE in_country_assignment DROP FOREIGN KEY FK_A9F5D4764A1B6C5F');
        $this->addSql('ALTER TABLE in_country_assignment ADD CONSTRAINT FK_A9F5D4764A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contribution_one_time DROP FOREIGN KEY FK_F7E8083A57844D22');
        $this->addSql('ALTER TABLE contribution_one_time ADD CONSTRAINT FK_F7E8083A57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE living_expense DROP FOREIGN KEY FK_D56750C057844D22');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C057844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contribution_living_expense DROP FOREIGN KEY FK_6B26F0D257844D22');
        $this->addSql('ALTER TABLE contribution_living_expense ADD CONSTRAINT FK_6B26F0D257844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE virtual_assignment DROP FOREIGN KEY FK_CE42E7B24A1B6C5F');
	    $this->addSql('ALTER TABLE virtual_assignment ADD CONSTRAINT FK_CE42E7B24A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE program_activities_allowance DROP FOREIGN KEY FK_70A9FF8A57844D22');
	    $this->addSql('ALTER TABLE program_activities_allowance ADD CONSTRAINT FK_70A9FF8A57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE virtual_assignment_activity DROP FOREIGN KEY FK_954593CD2976AA17');
	    $this->addSql('ALTER TABLE virtual_assignment_activity ADD CONSTRAINT FK_954593CD2976AA17 FOREIGN KEY (virtual_assignment_id) REFERENCES virtual_assignment (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE program_activities_allowance_cost_item DROP FOREIGN KEY FK_EF9D24B3B6CDBD28');
	    $this->addSql('ALTER TABLE program_activities_allowance_cost_item ADD CONSTRAINT FK_EF9D24B3B6CDBD28 FOREIGN KEY (program_activities_allowance_id) REFERENCES program_activities_allowance (id) ON DELETE CASCADE');
	    $this->addSql('ALTER TABLE pre_post_work DROP FOREIGN KEY FK_687FD9F799091188');
	    $this->addSql('ALTER TABLE pre_post_work ADD CONSTRAINT FK_687FD9F799091188 FOREIGN KEY (phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE day_breakdown_activity (id INT AUTO_INCREMENT NOT NULL, in_country_assignment_id INT DEFAULT NULL, specialist_project_phase_id INT DEFAULT NULL, day_breakdown_activity_type_id INT DEFAULT NULL, activity_date DATE NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_A1A860DC1EBED7F3 (in_country_assignment_id), INDEX IDX_A1A860DC4A1B6C5F (specialist_project_phase_id), INDEX IDX_A1A860DCD30867FD (day_breakdown_activity_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD CONSTRAINT FK_A1A860DC1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id)');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD CONSTRAINT FK_A1A860DC4A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id)');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD CONSTRAINT FK_A1A860DCD30867FD FOREIGN KEY (day_breakdown_activity_type_id) REFERENCES base_term (id)');
        $this->addSql('ALTER TABLE contribution_living_expense DROP FOREIGN KEY FK_6B26F0D257844D22');
        $this->addSql('ALTER TABLE contribution_living_expense ADD CONSTRAINT FK_6B26F0D257844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729C2EFD4045');
        $this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729C2EFD4045 FOREIGN KEY (contribution_living_expense_id) REFERENCES contribution_living_expense (id)');
        $this->addSql('ALTER TABLE contribution_one_time DROP FOREIGN KEY FK_F7E8083A57844D22');
        $this->addSql('ALTER TABLE contribution_one_time ADD CONSTRAINT FK_F7E8083A57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE contribution_one_time_cost_item DROP FOREIGN KEY FK_3B7A17D629B545C3');
        $this->addSql('ALTER TABLE contribution_one_time_cost_item ADD CONSTRAINT FK_3B7A17D629B545C3 FOREIGN KEY (contribution_one_time_id) REFERENCES contribution_one_time (id)');
        $this->addSql('ALTER TABLE contribution_transportation DROP FOREIGN KEY FK_B7F0D23F57844D22');
        $this->addSql('ALTER TABLE contribution_transportation ADD CONSTRAINT FK_B7F0D23F57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item DROP FOREIGN KEY FK_B72F48D247C532AB');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item ADD CONSTRAINT FK_B72F48D247C532AB FOREIGN KEY (contribution_transportation_id) REFERENCES contribution_transportation (id)');
        $this->addSql('ALTER TABLE in_country_assignment DROP FOREIGN KEY FK_A9F5D4764A1B6C5F');
        $this->addSql('ALTER TABLE in_country_assignment ADD CONSTRAINT FK_A9F5D4764A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id)');
        $this->addSql('ALTER TABLE living_expense DROP FOREIGN KEY FK_D56750C057844D22');
        $this->addSql('ALTER TABLE living_expense ADD CONSTRAINT FK_D56750C057844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
        $this->addSql('ALTER TABLE specialist_project_phase_budget DROP FOREIGN KEY FK_4DA1487F99091188');
        $this->addSql('ALTER TABLE specialist_project_phase_budget ADD CONSTRAINT FK_4DA1487F99091188 FOREIGN KEY (phase_id) REFERENCES specialist_project_phase (id)');
	    $this->addSql('ALTER TABLE pre_post_work DROP FOREIGN KEY FK_687FD9F799091188');
	    $this->addSql('ALTER TABLE pre_post_work ADD CONSTRAINT FK_687FD9F799091188 FOREIGN KEY (phase_id) REFERENCES specialist_project_phase (id)');
	    $this->addSql('ALTER TABLE program_activities_allowance DROP FOREIGN KEY FK_70A9FF8A57844D22');
	    $this->addSql('ALTER TABLE program_activities_allowance ADD CONSTRAINT FK_70A9FF8A57844D22 FOREIGN KEY (phase_budget_id) REFERENCES specialist_project_phase_budget (id)');
	    $this->addSql('ALTER TABLE program_activities_allowance_cost_item DROP FOREIGN KEY FK_EF9D24B3B6CDBD28');
	    $this->addSql('ALTER TABLE program_activities_allowance_cost_item ADD CONSTRAINT FK_EF9D24B3B6CDBD28 FOREIGN KEY (program_activities_allowance_id) REFERENCES program_activities_allowance (id)');
	    $this->addSql('ALTER TABLE virtual_assignment DROP FOREIGN KEY FK_CE42E7B24A1B6C5F');
	    $this->addSql('ALTER TABLE virtual_assignment ADD CONSTRAINT FK_CE42E7B24A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id)');
	    $this->addSql('ALTER TABLE virtual_assignment_activity DROP FOREIGN KEY FK_954593CD2976AA17');
	    $this->addSql('ALTER TABLE virtual_assignment_activity ADD CONSTRAINT FK_954593CD2976AA17 FOREIGN KEY (virtual_assignment_id) REFERENCES virtual_assignment (id)');
    }
}
