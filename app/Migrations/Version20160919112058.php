<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160919112058 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515418358D5');
        $this->addSql("REPLACE INTO project_review_status (id, name, created_at, updated_at) VALUES (4, 'Review Complete', CURRENT_TIMESTAMP , CURRENT_TIMESTAMP )");
        $this->addSql("DELETE FROM project_review_status WHERE id = 2");
	    $this->addSql("UPDATE project_general_info set project_review_status_id = 4 WHERE project_review_status_id = 2");
	    $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515418358D5 FOREIGN KEY (project_review_status_id) REFERENCES project_review_status (id)');

	    $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515FE481651');
        $this->addSql("REPLACE INTO project_outcome (id, name, created_at, updated_at) VALUES (0, 'Selected for ECA funding', CURRENT_TIMESTAMP , CURRENT_TIMESTAMP )");
        $this->addSql("REPLACE INTO project_outcome (id, name, created_at, updated_at) VALUES (1, 'Not Selected for ECA funding', CURRENT_TIMESTAMP , CURRENT_TIMESTAMP )");
        $this->addSql("REPLACE INTO project_outcome (id, name, created_at, updated_at) VALUES (5, 'Alternate Funding Provided', CURRENT_TIMESTAMP , CURRENT_TIMESTAMP )");
        $this->addSql("REPLACE INTO project_outcome (id, name, created_at, updated_at) VALUES (6, 'Alternate Funding Unavailable', CURRENT_TIMESTAMP , CURRENT_TIMESTAMP )");
	    $this->addSql("DELETE FROM project_outcome WHERE id = 2");
	    $this->addSql("UPDATE project_general_info set project_outcome_id = 0 WHERE project_outcome_id = 2");
	    $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515FE481651 FOREIGN KEY (project_outcome_id) REFERENCES project_outcome (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    $this->throwIrreversibleMigrationException("This migration was permanent, it can not be reverted");
    }
}
