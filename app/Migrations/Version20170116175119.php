<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170116175119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
	    // this up() migration is auto-generated, please modify it to your needs
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    // Makes data in CostItems consistent
	    $this->addSql("UPDATE base_term SET name = 'Content-Based Instruction / Content and Language Integrated Learning' WHERE name LIKE 'Content Based Instruction / Content and Language Integrated Learning'");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
	    // this up() migration is auto-generated, please modify it to your needs
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    // Makes data in CostItems consistent
	    $this->addSql("UPDATE base_term SET name = 'Content Based Instruction / Content and Language Integrated Learning' WHERE name LIKE 'Content-Based Instruction / Content and Language Integrated Learning'");

    }
}
