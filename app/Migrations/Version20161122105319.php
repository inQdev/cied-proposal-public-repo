<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161122105319 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE virtual_assignment_activity DROP FOREIGN KEY FK_954593CD2976AA17');
        $this->addSql('DROP TABLE virtual_assignment');
        $this->addSql('DROP TABLE virtual_assignment_activity');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE virtual_assignment (id INT AUTO_INCREMENT NOT NULL, host_id INT DEFAULT NULL, specialist_project_phase_id INT DEFAULT NULL, country_id INT DEFAULT NULL, start_date DATE DEFAULT NULL, end_date DATE DEFAULT NULL, other_countries LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, platform LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, deliverables_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, additional_comments LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_CE42E7B2F92F3E70 (country_id), INDEX IDX_CE42E7B24A1B6C5F (specialist_project_phase_id), INDEX IDX_CE42E7B21FB8D185 (host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE virtual_assignment_activity (id INT AUTO_INCREMENT NOT NULL, virtual_assignment_id INT DEFAULT NULL, activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, audience LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, participants INT DEFAULT NULL, hours_per_week NUMERIC(10, 2) DEFAULT NULL, hours_total NUMERIC(10, 2) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_954593CD2976AA17 (virtual_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE virtual_assignment ADD CONSTRAINT FK_CE42E7B21FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE virtual_assignment ADD CONSTRAINT FK_CE42E7B24A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE virtual_assignment ADD CONSTRAINT FK_CE42E7B2F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE virtual_assignment_activity ADD CONSTRAINT FK_954593CD2976AA17 FOREIGN KEY (virtual_assignment_id) REFERENCES virtual_assignment (id) ON DELETE CASCADE');
    }
}
