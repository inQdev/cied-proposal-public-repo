<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20161128170608 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20161128170608 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE specialist_project_phase ADD virtual_start_date_before DATE DEFAULT NULL, ADD virtual_end_date_before DATE DEFAULT NULL, ADD virtual_start_date_after DATE DEFAULT NULL, ADD virtual_end_date_after DATE DEFAULT NULL'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE specialist_project_phase DROP virtual_start_date_before, DROP virtual_end_date_before, DROP virtual_start_date_after, DROP virtual_end_date_after'
        );
    }
}
