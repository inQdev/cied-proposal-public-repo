<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160606104901 extends AbstractMigration
{
	/**
	 * @param Schema $schema
	 */
	public function up(Schema $schema)
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE role_structure (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, roles VARCHAR(1024) NOT NULL, UNIQUE INDEX UNIQ_3EF5E4365E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
	}

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("INSERT INTO role_structure (name, roles) VALUES ('ROLE_RELO', '[\"ROLE_EMBASSY\"]')");
		$this->connection->executeQuery("INSERT INTO role_structure (name, roles) VALUES ('ROLE_RPO', '[\"ROLE_RELO\"]')");
		$this->connection->executeQuery("INSERT INTO role_structure (name, roles) VALUES ('ROLE_STATE_DEPARTMENT', '[\"ROLE_RPO\"]')");
		$this->connection->executeQuery("INSERT INTO role_structure (name, roles) VALUES ('ROLE_ADMIN', '[\"ROLE_STATE_DEPARTMENT\"]')");
		$this->connection->executeQuery("INSERT INTO role_structure (name, roles) VALUES ('ROLE_SUPER_ADMIN', '[\"ROLE_ADMIN\"]')");
	}

	/**
	 * @param Schema $schema
	 */
	public function down(Schema $schema)
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE role_structure');
	}
}
