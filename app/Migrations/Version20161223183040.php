<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161223183040 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_global_value ADD revision_id INT NOT NULL, ADD post_contribution NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_monetary NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_in_kind NUMERIC(14, 2) DEFAULT \'0\', ADD post_host_total_contribution NUMERIC(14, 2) DEFAULT \'0\', ADD post_total_contribution NUMERIC(14, 2) DEFAULT \'0\', ADD eca_total_contribution NUMERIC(14, 2) DEFAULT \'0\', ADD post_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', ADD post_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_gu NUMERIC(14, 2) DEFAULT \'0\', ADD host_contribution_non_gu NUMERIC(14, 2) DEFAULT \'0\', CHANGE value value NUMERIC(14, 2) NOT NULL');
    }

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("UPDATE project_global_value SET revision_id = 1");
	}

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_global_value DROP revision_id, DROP post_contribution, DROP host_contribution_monetary, DROP host_contribution_in_kind, DROP post_host_total_contribution, DROP post_total_contribution, DROP eca_total_contribution, DROP post_contribution_gu, DROP post_contribution_non_gu, DROP host_contribution_gu, DROP host_contribution_non_gu, CHANGE value value NUMERIC(10, 2) NOT NULL');
    }
}
