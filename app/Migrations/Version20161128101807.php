<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161128101807 extends AbstractMigration
{
	/**
	 * @param Schema $schema
	 */
	public function up(Schema $schema)
	{
		$this->addSql("UPDATE global_value SET name = 'Additional Pre-work days (per day)' WHERE name LIKE 'Pre-work days (per day)'");

	}

	/**
	 * @param Schema $schema
	 */
	public function down(Schema $schema)
	{
		$this->addSql("UPDATE global_value SET name = 'Pre-work days (per day)' WHERE name LIKE 'Additional Pre-work days (per day)'");

	}
}
