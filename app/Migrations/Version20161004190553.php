<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161004190553 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phase_budget_total ADD daily_global NUMERIC(10, 2) NOT NULL');
    }

	/**
	 * Adjust some old data in the DB, this does not need to be reverted
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("DELETE FROM specialist_project_phase_global_value WHERE global_value_id IN (SELECT id FROM global_value WHERE name LIKE 'Stipend (per day)')");
		$this->connection->executeQuery("DELETE FROM specialist_project_phase_global_value WHERE global_value_id IN (SELECT id FROM global_value WHERE NAME LIKE 'Post-work days (per day)')");
		$this->connection->executeQuery("UPDATE global_value SET `name` = replace(`name`, 'Planning days', 'Default pre-work days') WHERE project_type = 1");
	}

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phase_budget_total DROP daily_global');
    }
}
