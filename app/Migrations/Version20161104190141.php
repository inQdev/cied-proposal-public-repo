<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161104190141 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
	    $this->connection->executeQuery("INSERT INTO cost_item (description, TYPE, created_at, updated_at) SELECT 'Ground Transport', 5, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM cost_item WHERE description LIKE 'Ground Transport' AND TYPE = 5)");
	    $this->connection->executeQuery("INSERT INTO cost_item (description, TYPE, created_at, updated_at) SELECT 'Meals', 4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM cost_item WHERE description LIKE 'Meals' AND TYPE = 4)");
	    $this->connection->executeQuery("INSERT INTO cost_item (description, TYPE, created_at, updated_at) SELECT 'Lodging', 4, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM cost_item WHERE description LIKE 'Lodging' AND TYPE = 4)");
    }

	/**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
