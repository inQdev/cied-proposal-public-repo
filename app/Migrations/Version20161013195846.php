<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20161013195846 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20161013195846 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE partnering_organization ADD not_applicable TINYINT(1) DEFAULT NULL');

        $this->addSql(
            "INSERT INTO partnering_organization (country_id, name, created_at, updated_at, not_applicable) SELECT id, 'No Partnering Organizations', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1 FROM country"
        );
        $this->addSql("UPDATE partnering_organization SET not_applicable=0 WHERE not_applicable IS NULL");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("DELETE FROM partnering_organization WHERE not_applicable=1");
        $this->addSql('ALTER TABLE partnering_organization DROP not_applicable');
    }
}
