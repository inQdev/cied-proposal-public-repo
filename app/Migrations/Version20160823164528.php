<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160823164528 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_general_info ADD in_country_orientation_certification TINYINT(1) DEFAULT NULL, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_general_info DROP in_country_orientation_certification, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
