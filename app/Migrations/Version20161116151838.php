<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Version20161116151838 migration.
 *
 * @package Application\Migrations
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class Version20161116151838 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE itinerary_virtual_activities_weeks (id INT AUTO_INCREMENT NOT NULL, activity_id INT DEFAULT NULL, week_id INT DEFAULT NULL, duration INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_4DA0BB4981C06096 (activity_id), INDEX IDX_4DA0BB49C86F3B2F (week_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE itinerary_virtual_activities_anticipated_audience (id INT AUTO_INCREMENT NOT NULL, activity_id INT DEFAULT NULL, audience_id INT DEFAULT NULL, estimated_participants INT DEFAULT NULL, audience_other VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_6A66CDF181C06096 (activity_id), INDEX IDX_6A66CDF1848CC616 (audience_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE itinerary_virtual_activity (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, type_other VARCHAR(255) DEFAULT NULL, type_desc LONGTEXT DEFAULT NULL, online_availability_desc LONGTEXT DEFAULT NULL, topic_other VARCHAR(255) DEFAULT NULL, total_duration INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_EA94DB7DC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE itinerary_virtual_activities_topics (activity_id INT NOT NULL, area_of_expertise_id INT NOT NULL, INDEX IDX_F6E69D1981C06096 (activity_id), INDEX IDX_F6E69D1952D09986 (area_of_expertise_id), PRIMARY KEY(activity_id, area_of_expertise_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE itinerary_virtual_week (id INT AUTO_INCREMENT NOT NULL, project_phase_id INT DEFAULT NULL, start_date DATE DEFAULT NULL, end_date DATE DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_7F6031E7A4479A53 (project_phase_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks ADD CONSTRAINT FK_4DA0BB4981C06096 FOREIGN KEY (activity_id) REFERENCES itinerary_virtual_activity (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks ADD CONSTRAINT FK_4DA0BB49C86F3B2F FOREIGN KEY (week_id) REFERENCES itinerary_virtual_week (id)'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_anticipated_audience ADD CONSTRAINT FK_6A66CDF181C06096 FOREIGN KEY (activity_id) REFERENCES itinerary_virtual_activity (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_anticipated_audience ADD CONSTRAINT FK_6A66CDF1848CC616 FOREIGN KEY (audience_id) REFERENCES base_term (id)'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activity ADD CONSTRAINT FK_EA94DB7DC54C8C93 FOREIGN KEY (type_id) REFERENCES base_term (id)'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_topics ADD CONSTRAINT FK_F6E69D1981C06096 FOREIGN KEY (activity_id) REFERENCES itinerary_virtual_activity (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_topics ADD CONSTRAINT FK_F6E69D1952D09986 FOREIGN KEY (area_of_expertise_id) REFERENCES base_term (id) ON DELETE CASCADE'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_week ADD CONSTRAINT FK_7F6031E7A4479A53 FOREIGN KEY (project_phase_id) REFERENCES specialist_project_phase (id)'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks DROP FOREIGN KEY FK_4DA0BB4981C06096'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_anticipated_audience DROP FOREIGN KEY FK_6A66CDF181C06096'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_topics DROP FOREIGN KEY FK_F6E69D1981C06096'
        );
        $this->addSql(
            'ALTER TABLE itinerary_virtual_activities_weeks DROP FOREIGN KEY FK_4DA0BB49C86F3B2F'
        );
        $this->addSql('DROP TABLE itinerary_virtual_activities_weeks');
        $this->addSql('DROP TABLE itinerary_virtual_activities_anticipated_audience');
        $this->addSql('DROP TABLE itinerary_virtual_activity');
        $this->addSql('DROP TABLE itinerary_virtual_activities_topics');
        $this->addSql('DROP TABLE itinerary_virtual_week');
    }
}
