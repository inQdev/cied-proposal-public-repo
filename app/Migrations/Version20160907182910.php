<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160907182910 extends AbstractMigration
{
	/**
	 * @param Schema $schema
	 */
	public function up(Schema $schema)
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE global_paa (id INT AUTO_INCREMENT NOT NULL, range_start INT DEFAULT NULL, range_end INT DEFAULT NULL, default_value NUMERIC(10, 2) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
	}

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("INSERT INTO global_paa (range_start, range_end, default_value, created_at, updated_at) VALUES (0, 59, 250.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_paa (range_start, range_end, default_value, created_at, updated_at) VALUES (60, 89, 500.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_paa (range_start, range_end, default_value, created_at, updated_at) VALUES (90, 119, 750.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_paa (range_start, range_end, default_value, created_at, updated_at) VALUES (120, 149, 1000.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_paa (range_start, range_end, default_value, created_at, updated_at) VALUES (150, NULL, 1250.00, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
	}

	/**
	 * @param Schema $schema
	 */
	public function down(Schema $schema)
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE global_paa');
	}
}
