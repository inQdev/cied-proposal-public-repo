<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160909125258 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE itinerary_index (id INT AUTO_INCREMENT NOT NULL, phase_id INT DEFAULT NULL, in_country_assignment_id INT DEFAULT NULL, date DATE NOT NULL, set_way VARCHAR(50) NOT NULL, INDEX IDX_EC72D18699091188 (phase_id), INDEX IDX_EC72D1861EBED7F3 (in_country_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary_index ADD CONSTRAINT FK_EC72D18699091188 FOREIGN KEY (phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE itinerary_index ADD CONSTRAINT FK_EC72D1861EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649C05FB297 ON user (confirmation_token)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE itinerary_index');
        $this->addSql('DROP INDEX UNIQ_8D93D649C05FB297 ON user');
    }
}
