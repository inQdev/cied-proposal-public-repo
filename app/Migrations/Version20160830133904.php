<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160830133904 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE itinerary_activities_topics (itinerary_activity_id INT NOT NULL, area_of_expertise_id INT NOT NULL, INDEX IDX_781B6755E483EC63 (itinerary_activity_id), INDEX IDX_781B675552D09986 (area_of_expertise_id), PRIMARY KEY(itinerary_activity_id, area_of_expertise_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary_activities_topics ADD CONSTRAINT FK_781B6755E483EC63 FOREIGN KEY (itinerary_activity_id) REFERENCES itinerary_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE itinerary_activities_topics ADD CONSTRAINT FK_781B675552D09986 FOREIGN KEY (area_of_expertise_id) REFERENCES base_term (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE itinerary_activity CHANGE topic topic_other VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE itinerary_activities_topics');
        $this->addSql('ALTER TABLE itinerary_activity CHANGE topic_other topic VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
