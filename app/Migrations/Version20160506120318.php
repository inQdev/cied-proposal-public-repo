<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160506120318 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pre_post_work (id INT AUTO_INCREMENT NOT NULL, phase_id INT DEFAULT NULL, additional_pre_work TINYINT(1) DEFAULT NULL, additional_pre_work_days INT DEFAULT NULL, additional_pre_work_start_date DATE DEFAULT NULL, additional_pre_work_desc LONGTEXT DEFAULT NULL, additional_post_work TINYINT(1) DEFAULT NULL, additional_post_work_days INT DEFAULT NULL, additional_post_work_start_date DATE DEFAULT NULL, additional_post_work_desc LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_687FD9F799091188 (phase_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pre_post_work ADD CONSTRAINT FK_687FD9F799091188 FOREIGN KEY (phase_id) REFERENCES specialist_project_phase (id)');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL, CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE pre_post_work');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
