<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170106144006 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_global_value ADD budget_revision_id INT DEFAULT NULL, DROP revision_id');
        $this->addSql('ALTER TABLE project_global_value ADD CONSTRAINT FK_73D5C6146BF03AD0 FOREIGN KEY (budget_revision_id) REFERENCES fellow_project_budget_revision (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_73D5C6146BF03AD0 ON project_global_value (budget_revision_id)');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD budget_revision_id INT DEFAULT NULL, DROP revision_id');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD CONSTRAINT FK_856B8DC76BF03AD0 FOREIGN KEY (budget_revision_id) REFERENCES fellow_project_budget_revision (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_856B8DC76BF03AD0 ON fellow_project_budget_totals (budget_revision_id)');
        $this->addSql('ALTER TABLE fellow_project_budget_revision ADD user_id INT DEFAULT NULL, CHANGE revision_id revision INT NOT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_revision ADD CONSTRAINT FK_1868CFDBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_1868CFDBA76ED395 ON fellow_project_budget_revision (user_id)');
        $this->addSql('ALTER TABLE fellow_project_budget DROP revision_id');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD budget_revision_id INT DEFAULT NULL, DROP revision_id');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD CONSTRAINT FK_C0A5B2BC6BF03AD0 FOREIGN KEY (budget_revision_id) REFERENCES fellow_project_budget_revision (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_C0A5B2BC6BF03AD0 ON fellow_project_budget_cost_item (budget_revision_id)');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals ADD budget_revision_id INT DEFAULT NULL, DROP revision_id');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals ADD CONSTRAINT FK_B2D5958C6BF03AD0 FOREIGN KEY (budget_revision_id) REFERENCES fellow_project_budget_revision (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2D5958C6BF03AD0 ON fellow_project_budget_onetime_totals (budget_revision_id)');
    }

	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("INSERT INTO fellow_project_budget_revision (id, budget_id, revision, change_desc, created_at, updated_at) SELECT id, id AS bid, 1, 'Initial Budget',  created_at, updated_at FROM fellow_project_budget");
		$this->connection->executeQuery("UPDATE project_global_value SET budget_revision_id = fellow_project_budget_id");
		$this->connection->executeQuery("UPDATE fellow_project_budget_totals SET budget_revision_id = fellow_project_budget_id");
		$this->connection->executeQuery("UPDATE fellow_project_budget_cost_item SET budget_revision_id = fellow_project_budget_id");
		$this->connection->executeQuery("UPDATE fellow_project_budget_onetime_totals SET budget_revision_id = fellow_project_budget_id");

	}

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellow_project_budget ADD revision_id INT NOT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item DROP FOREIGN KEY FK_C0A5B2BC6BF03AD0');
        $this->addSql('DROP INDEX IDX_C0A5B2BC6BF03AD0 ON fellow_project_budget_cost_item');
        $this->addSql('ALTER TABLE fellow_project_budget_cost_item ADD revision_id INT NOT NULL, DROP budget_revision_id');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals DROP FOREIGN KEY FK_B2D5958C6BF03AD0');
        $this->addSql('DROP INDEX UNIQ_B2D5958C6BF03AD0 ON fellow_project_budget_onetime_totals');
        $this->addSql('ALTER TABLE fellow_project_budget_onetime_totals ADD revision_id INT NOT NULL, DROP budget_revision_id');
        $this->addSql('ALTER TABLE fellow_project_budget_revision DROP FOREIGN KEY FK_1868CFDBA76ED395');
        $this->addSql('DROP INDEX IDX_1868CFDBA76ED395 ON fellow_project_budget_revision');
        $this->addSql('ALTER TABLE fellow_project_budget_revision DROP user_id, CHANGE revision revision_id INT NOT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget_totals DROP FOREIGN KEY FK_856B8DC76BF03AD0');
        $this->addSql('DROP INDEX UNIQ_856B8DC76BF03AD0 ON fellow_project_budget_totals');
        $this->addSql('ALTER TABLE fellow_project_budget_totals ADD revision_id INT NOT NULL, DROP budget_revision_id');
        $this->addSql('ALTER TABLE project_global_value DROP FOREIGN KEY FK_73D5C6146BF03AD0');
        $this->addSql('DROP INDEX IDX_73D5C6146BF03AD0 ON project_global_value');
        $this->addSql('ALTER TABLE project_global_value ADD revision_id INT NOT NULL, DROP budget_revision_id');
    }

	public function postDown(Schema $schema)
	{
		$this->connection->executeQuery("DELETE FROM fellow_project_budget_revision WHERE change_desc = 'Initial Budget'");
		$this->connection->executeQuery("UPDATE fellow_project_budget SET revision_id = 1");
		$this->connection->executeQuery("UPDATE fellow_project_budget_cost_item SET revision_id = 1");
		$this->connection->executeQuery("UPDATE fellow_project_budget_onetime_totals SET revision_id = 1");
		$this->connection->executeQuery("UPDATE fellow_project_budget_totals SET revision_id = 1");
		$this->connection->executeQuery("UPDATE project_global_value SET revision_id = 1");

	}

}
