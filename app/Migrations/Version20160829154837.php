<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160829154837 extends AbstractMigration
{
	/**
	 * @param Schema $schema
	 */
	public function up(Schema $schema)
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729CEF1191CE');
		$this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729CEF1191CE FOREIGN KEY (living_expense_cost_item_id) REFERENCES living_expense_cost_item (id) ON DELETE CASCADE');
		$this->addSql('ALTER TABLE contribution_transportation_cost_item DROP FOREIGN KEY FK_B72F48D2ECAB15B3');
		$this->addSql('ALTER TABLE contribution_transportation_cost_item ADD CONSTRAINT FK_B72F48D2ECAB15B3 FOREIGN KEY (travel_id) REFERENCES itinerary_travel (id) ON DELETE CASCADE');
	}

	/**
	 * @param Schema $schema
	 */
	public function down(Schema $schema)
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE contribution_living_expense_cost_item DROP FOREIGN KEY FK_7525729CEF1191CE');
		$this->addSql('ALTER TABLE contribution_living_expense_cost_item ADD CONSTRAINT FK_7525729CEF1191CE FOREIGN KEY (living_expense_cost_item_id) REFERENCES living_expense_cost_item (id)');
		$this->addSql('ALTER TABLE contribution_transportation_cost_item DROP FOREIGN KEY FK_B72F48D2ECAB15B3');
		$this->addSql('ALTER TABLE contribution_transportation_cost_item ADD CONSTRAINT FK_B72F48D2ECAB15B3 FOREIGN KEY (travel_id) REFERENCES itinerary_travel (id)');
	}
}
