<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170116110459 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
	    // this up() migration is auto-generated, please modify it to your needs
	    $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

	    // Makes data in CostItems consistent
	    $this->addSql("UPDATE fellow_project_budget_cost_item SET post_contribution_non_gu = post_contribution, host_contribution_non_gu = host_contribution_monetary WHERE budget_revision_id IN (SELECT id from fellow_project_budget_revision WHERE revision = 1)");

	    // Makes data in Global values consistent
	    $this->addSql("UPDATE project_global_value SET post_contribution = `value`, post_contribution_non_gu = `value`, post_host_total_contribution = `value`, post_total_contribution = `value` WHERE budget_revision_id IN (SELECT R.id from fellow_project_budget_revision R join fellow_project_budget B ON R.budget_id = B.id WHERE R.revision = 1 and B.funded_by = 'Post')");
	    $this->addSql("UPDATE project_global_value SET eca_total_contribution = `value` WHERE budget_revision_id IN (SELECT R.id from fellow_project_budget_revision R join fellow_project_budget B ON R.budget_id = B.id WHERE R.revision = 1 and B.funded_by = 'ECA')");

	    // Makes data in Totals consistent
	    $this->addSql("UPDATE fellow_project_budget_totals SET post_contribution_non_gu = post_contribution, host_contribution_non_gu = host_contribution_monetary WHERE budget_revision_id IN (SELECT id from fellow_project_budget_revision WHERE revision = 1)");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
	    // No action needed

    }
}
