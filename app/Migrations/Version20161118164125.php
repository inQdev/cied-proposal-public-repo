<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161118164125 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE global_value ADD apply_on_mixed SMALLINT DEFAULT NULL');
    }

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("UPDATE global_value SET apply_on_mixed = 1 WHERE specialist_type LIKE 'in-country'");
		$this->connection->executeQuery("UPDATE global_value SET apply_on_mixed = 0 WHERE rules IN ('pre-work', 'post-work')");
		$this->connection->executeQuery("UPDATE global_value SET apply_on_mixed = 0 WHERE specialist_type LIKE 'virtual'");
		$this->connection->executeQuery("UPDATE global_value SET apply_on_mixed = 1 WHERE name LIKE 'Stipend (per 8 hrs)'");
	}

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE global_value DROP apply_on_mixed');
    }
}
