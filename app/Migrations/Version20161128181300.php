<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161128181300 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE itinerary_virtual_week DROP FOREIGN KEY FK_7F6031E7A4479A53');
        $this->addSql('ALTER TABLE itinerary_virtual_week ADD CONSTRAINT FK_7F6031E7A4479A53 FOREIGN KEY (project_phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE itinerary_virtual_week DROP FOREIGN KEY FK_7F6031E7A4479A53');
        $this->addSql('ALTER TABLE itinerary_virtual_week ADD CONSTRAINT FK_7F6031E7A4479A53 FOREIGN KEY (project_phase_id) REFERENCES specialist_project_phase (id)');
    }
}
