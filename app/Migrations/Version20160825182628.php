<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160825182628 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE partnering_organization (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_16CC6E65F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialist_project_phases_partnering_organizations (specialist_project_phase_id INT NOT NULL, partnering_organization_id INT NOT NULL, INDEX IDX_974935204A1B6C5F (specialist_project_phase_id), INDEX IDX_9749352041F1045F (partnering_organization_id), PRIMARY KEY(specialist_project_phase_id, partnering_organization_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE partnering_organization ADD CONSTRAINT FK_16CC6E65F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE specialist_project_phases_partnering_organizations ADD CONSTRAINT FK_974935204A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE specialist_project_phases_partnering_organizations ADD CONSTRAINT FK_9749352041F1045F FOREIGN KEY (partnering_organization_id) REFERENCES partnering_organization (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE specialist_project_phases_partnering_organizations DROP FOREIGN KEY FK_9749352041F1045F');
        $this->addSql('DROP TABLE partnering_organization');
        $this->addSql('DROP TABLE specialist_project_phases_partnering_organizations');
    }
}
