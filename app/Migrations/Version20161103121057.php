<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161103121057 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE global_value ADD sort_order SMALLINT DEFAULT 999 NOT NULL, CHANGE periodicity periodicity SMALLINT DEFAULT NULL');
    }

	/**
	 * @param Schema $schema
	 */
	public function postUp(Schema $schema)
	{
		$this->connection->executeQuery("UPDATE global_value SET specialist_type = 'in-country' WHERE specialist_type = 'mixed'");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, sort_order, created_at, updated_at) VALUES('Stipend (per 8 hrs)', 200, 8, null, 'virtual', 1, 100, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
		$this->connection->executeQuery("INSERT INTO global_value (name, default_value, periodicity, rules, specialist_type, project_type, sort_order, created_at, updated_at) VALUES('Program Activities Allowance (PAA)', 0, 0, null, 'virtual', 1, 999, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");
	}

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE global_value DROP sort_order, CHANGE periodicity periodicity TINYINT(1) DEFAULT NULL');
    }
}
