<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160524133020 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellowship_duty_activity DROP FOREIGN KEY FK_B2D7EF4EA2C313EB');
        $this->addSql('ALTER TABLE fellowship_duty_activity DROP FOREIGN KEY FK_B2D7EF4EC2669B92');
        $this->addSql('CREATE TABLE fellowship_duties_primary_activities_focus (duty_activity_primary_id INT NOT NULL, duty_activity_focus_id INT NOT NULL, INDEX IDX_66FE675AD489062 (duty_activity_primary_id), INDEX IDX_66FE6756E21606F (duty_activity_focus_id), PRIMARY KEY(duty_activity_primary_id, duty_activity_focus_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fellowship_duties_secondary_activities_focus (duty_activity_secondary_id INT NOT NULL, duty_activity_focus_id INT NOT NULL, INDEX IDX_12B04536A2A7667B (duty_activity_secondary_id), INDEX IDX_12B045366E21606F (duty_activity_focus_id), PRIMARY KEY(duty_activity_secondary_id, duty_activity_focus_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fellowship_duties_primary_activities_focus ADD CONSTRAINT FK_66FE675AD489062 FOREIGN KEY (duty_activity_primary_id) REFERENCES fellowship_duty_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fellowship_duties_primary_activities_focus ADD CONSTRAINT FK_66FE6756E21606F FOREIGN KEY (duty_activity_focus_id) REFERENCES base_term (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus ADD CONSTRAINT FK_12B04536A2A7667B FOREIGN KEY (duty_activity_secondary_id) REFERENCES fellowship_duty_activity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fellowship_duties_secondary_activities_focus ADD CONSTRAINT FK_12B045366E21606F FOREIGN KEY (duty_activity_focus_id) REFERENCES base_term (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_B2D7EF4EA2C313EB ON fellowship_duty_activity');
        $this->addSql('DROP INDEX IDX_B2D7EF4EC2669B92 ON fellowship_duty_activity');
        $this->addSql('ALTER TABLE fellowship_duty_activity ADD fellow_project_id INT DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD type INT NOT NULL');

        $this->addSql('UPDATE fellowship_duty_activity fda SET fda.fellow_project_id = (SELECT fd.fellow_project_id FROM fellowship_duty fd WHERE fd.id=fda.fellowship_duty_id), fda.description=fda.activity_desc');

        $this->addSql('ALTER TABLE fellowship_duty_activity DROP fellowship_duty_activity_skill_needed_id, DROP fellowship_duty_id, DROP activity_desc');
        $this->addSql('DROP TABLE fellowship_duty');
        $this->addSql('ALTER TABLE fellowship_duty_activity ADD CONSTRAINT FK_B2D7EF4E98358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id)');
        $this->addSql('CREATE INDEX IDX_B2D7EF4E98358A31 ON fellowship_duty_activity (fellow_project_id)');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_host_info CHANGE current_fellow_name current_fellow_name LONGTEXT DEFAULT NULL, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL, CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE pre_post_work CHANGE additional_pre_work_desc additional_pre_work_desc LONGTEXT DEFAULT NULL, CHANGE additional_post_work_desc additional_post_work_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info DROP sustainability_by_locals_desc, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE subsequent_phases_desc subsequent_phases_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project_phase_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("DELETE FROM base_term WHERE type = 1");

        $this->connection->executeQuery("INSERT INTO base_term(name, type) VALUES ('4 Skills, Grammar, and Vocabulary', 1),('Content-based Instruction', 1),('TESOL methods and Techniques', 1),('Materials Development', 1),('Syllabus Design', 1),('English for Academic Purposes (EAP) (For example – but not limited to – Academic Writing)', 1),('English for Specific Purposes (ESP) (For example – but not limited to – Law, Stem, Tourism, Business)', 1),('Instructional Technology (CALL)', 1),('Proficiency Exam Preparation (Including: TOEFL, IELTS, GRE)', 1),('American Culture, Studies, and/or Literature', 1),('Leading Relevant Extra-curricular Activities', 1)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fellowship_duty (id INT AUTO_INCREMENT NOT NULL, fellow_project_id INT DEFAULT NULL, additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, type INT NOT NULL, INDEX IDX_305A6AF798358A31 (fellow_project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fellowship_duty ADD CONSTRAINT FK_305A6AF798358A31 FOREIGN KEY (fellow_project_id) REFERENCES fellow_project (id)');
        $this->addSql('DROP TABLE fellowship_duties_primary_activities_focus');
        $this->addSql('DROP TABLE fellowship_duties_secondary_activities_focus');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity DROP FOREIGN KEY FK_B2D7EF4E98358A31');
        $this->addSql('DROP INDEX IDX_B2D7EF4E98358A31 ON fellowship_duty_activity');
        $this->addSql('ALTER TABLE fellowship_duty_activity ADD fellowship_duty_id INT DEFAULT NULL, ADD activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, DROP description, DROP type, CHANGE fellow_project_id fellowship_duty_activity_skill_needed_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity ADD CONSTRAINT FK_B2D7EF4EA2C313EB FOREIGN KEY (fellowship_duty_activity_skill_needed_id) REFERENCES base_term (id)');
        $this->addSql('ALTER TABLE fellowship_duty_activity ADD CONSTRAINT FK_B2D7EF4EC2669B92 FOREIGN KEY (fellowship_duty_id) REFERENCES fellowship_duty (id)');
        $this->addSql('CREATE INDEX IDX_B2D7EF4EA2C313EB ON fellowship_duty_activity (fellowship_duty_activity_skill_needed_id)');
        $this->addSql('CREATE INDEX IDX_B2D7EF4EC2669B92 ON fellowship_duty_activity (fellowship_duty_id)');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE current_fellow_name current_fellow_name LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE pre_post_work CHANGE additional_pre_work_desc additional_pre_work_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_post_work_desc additional_post_work_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info ADD sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE subsequent_phases_desc subsequent_phases_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project_phase_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
