<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160825094928 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contribution_transportation_cost_item DROP FOREIGN KEY FK_B72F48D25401DA61');
        $this->addSql('DROP INDEX IDX_B72F48D25401DA61 ON contribution_transportation_cost_item');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item DROP cost_item_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contribution_transportation_cost_item ADD cost_item_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contribution_transportation_cost_item ADD CONSTRAINT FK_B72F48D25401DA61 FOREIGN KEY (cost_item_id) REFERENCES cost_item (id)');
        $this->addSql('CREATE INDEX IDX_B72F48D25401DA61 ON contribution_transportation_cost_item (cost_item_id)');
    }
}
