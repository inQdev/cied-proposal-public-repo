<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class AppKernel
 */
class AppKernel extends Kernel
{
    /**
     * @return array
     */
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // Doctrine
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            // Doctrine Migrations Bundle
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            // Doctrine Extensions Bundle
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),

            // JMSTranslationBundle
            new JMS\TranslationBundle\JMSTranslationBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\AopBundle\JMSAopBundle(),

            // SSO / OpenID_connect
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new FOS\UserBundle\FOSUserBundle(),

            // KnpSnappyBundle
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),

            // FSSolrBundle
            new FS\SolrBundle\FSSolrBundle(),

            // Mandrill
//            new Hip\MandrillBundle\HipMandrillBundle(),
            new Accord\MandrillSwiftMailerBundle\AccordMandrillSwiftMailerBundle(),

            // JMSSerializerBundle
            new JMS\SerializerBundle\JMSSerializerBundle(),

            // FOSRestBundle
            new FOS\RestBundle\FOSRestBundle(),

            new AppBundle\AppBundle(),
            new APIBundle\APIBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    /**
     * @return string
     */
    public function getRootDir()
    {
        return __DIR__;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return dirname(__DIR__) . '/var/cache/' . $this->getEnvironment();
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        return dirname(__DIR__) . '/var/logs';
    }

    /**
     * @param \Symfony\Component\Config\Loader\LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(
            $this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml'
        );
    }

}
