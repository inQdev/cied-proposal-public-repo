imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: "@APIBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en
    grant_docs_submission_deadline: '2017-03-31'

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
        #assets_version: SomeVersionScheme
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    autoescape: 'name'
    form_themes:
        - 'form/form.html.twig'
    globals:
        application_system_url: %oauth_base_url%
        proposal:
            ga_code: 'UA-XXXXXXXX-Y'
            gtm_code: 'GTM-WWWWWW'
            mouseflow_enabled: "%mouseflow_enabled%"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Doctrine Migrations
doctrine_migrations:
    dir_name:   "%kernel.root_dir%/Migrations"
    namespace:  Application\Migrations
    table_name: proposal_migration_versions
    name:       Proposal System migrations

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }


# StofDoctrineExtensions Configuration
stof_doctrine_extensions:
    orm:
        default:
            blameable:     true
            timestampable: true


# Configuration for Translate Web UI (otherwise an error raises)
#messages:
#    default_locale: en
#    locales: [en, es]
#    strategy: prefix

# The console command to run:  bin/console translation:extract --config=app en
jms_translation:
    configs:
        app:
            dirs: [%kernel.root_dir%, %kernel.root_dir%/../src]
            output_dir: %kernel.root_dir%/Resources/translations
            ignored_domains: [routes]
            excluded_names: ["*TestCase.php", "*Test.php"]
            excluded_dirs: [cache, data, logs]
            output-format: xliff

# JMS DI extra configuration - Felipe: It makes functionality crash (2016/02/15)
#jms_di_extra:
#    automatic_controller_injections:
#        properties:
#            request: "@request"
#            router: "@router"
#
#        method_calls:
#            setRouter: ["@router"]

fos_user:
    db_driver: orm
    firewall_name: secure_area
    user_class: AppBundle\Entity\User
    registration:
        confirmation:
            enabled: false

hwi_oauth:
#    http_client:
#        timeout: 15

    # name of the firewall in which this bundle is active, this setting MUST be set
    firewall_names: [secure_area]
    connect:
        account_connector: cied.user_provider
        confirmation: true

    resource_owners:
        cied:
            type: oauth2
            client_id: %oauth_client_id%
            client_secret: %oauth_client_secret%
            access_token_url: %oauth_base_url%/oauth2/token
            authorization_url: %oauth_base_url%/oauth2/authorize
            infos_url: %oauth_base_url%/oauth2/userInfo
            scope: "read profile openid email offline_access"
            user_response_class: HWI\Bundle\OAuthBundle\OAuth\Response\PathUserResponse
            paths:
                identifier: sub
                nickname: name # this isn't used in the application, but it is required for configuration to work
                realname: name
                email: email
            options:
                csrf: true

    fosub:
        # try 30 times to check if a username is available (foo, foo1, foo2 etc)
        username_iterations: 30

        # mapping between resource owners (see below) and properties
        properties:
            cied: ciedId
# KnpSnappyBundle Configuration
knp_snappy:
    pdf:
        enabled: true
        binary: /opt/wkhtmltox/bin/wkhtmltopdf
        options: []
    image:
        enabled: false
        binary: /opt/wkhtmltox/bin/wkhtmltoimage
        options: []
    temporary_folder: %kernel.cache_dir%/snappy

fs_solr:
    endpoints:
        core0:
            host: %solr_host%
            port: %solr_port%
            path: %solr_path%
            core: %solr_core%
            timeout: %solr_timeout%

#hip_mandrill:
#    api_key: %mandrill_api_key%
#    disable_delivery: %mandrill_disable_delivery% # useful for dev/test environment. Default value is 'false'
    # debug: passed to \Mandrill causing it to output curl requests. Useful to see output
    # from CLI script. Default value is 'false'
#    debug: %mandrill_debug%
#    default:
#        sender: %mandrill_sender%
#        sender_name: %mandrill_sender_name% # Optionally define a sender name (from name)
        #subaccount: %mandrill_subaccount% # Optionally define a subaccount to use

accord_mandrill_swift_mailer:
    api_key: "%mandrill_api_key%"
#    async: true # optional