CIED - Proposal system 
======================

A Symfony project created on January 30, 2016, 9:31 am. to manage fellow and specialist projects, review, financial data, reports 

# Installation

You could run this project standalone, but it's recommended to run it as a submodule of `Docker CIED Proposal` repo (https://bitbucket.org/inQdev/docker-cied-proposal-public)

Once you get the copy of the repo in your local, you must run:

```
$ composer install
```

Refer to the README file of Docker `CIED Proposal repo` for more information about the parameters configuration

# Notes on configuration files

The folder `app/config` contains the configuration files, they are kept in the repo except the file `parameters.yml` since it it specific for every environment the proposal system is deployed, if you need to add more parameters to the project, add them to the file `parameters.yml.dist` then run

```
$ composer install
```

That will allow you to define the values for the environment to work on

# Web assets 

The folder `web/assets` keeps the CSS, JS and images for the proposal system, CSS sources as SASS files, the project requires NodeJS, Bower and Gulp in order to modify them and compile/minify JS files:

* [gulp](http://gulpjs.com/) build script that compiles both Sass and Less, checks for JavaScript errors, optimizes images, and concatenates and minifies files
* [Bower](http://bower.io/) for front-end package management
* [Bootstrap](http://getbootstrap.com/)
* [node.js](http://nodejs.org/download/)

## Install gulp and Bower

Install the latest version of NodeJS

```bash
$ npm install -g npm@latest 
``` 

Install Gulp and Bower

```bash
$ npm install -g gulp bower
``` 

Go to the `web` folder and install NodeJS dependencies

```bash
$ npm install
```

Install Bower dependencies

```bash
$ bower install
```

You now have all the necessary dependencies to run the build process.

## Available gulp commands

* `gulp` - Compiles and optimizes the files in your assets directory
* `gulp watch` - Compiles assets when file changes are made
* `gulp styles` - Compiles and optimizes only CSS
* `gulp scripts` - Check, compiles and minifies JS files

## Images and external JS files

Images are kept in the folder `web/assets/images`, any new image needed must be saved there, `gulp` will optimize and copy it to the folder `web/dist/images`

Additional JS libraries are usually added as dependencies for Bower in the file `web/bower.json`, if the library can not be set that way, add the JS file needed to the folder `web/assets/scripts`

** There is already a file to keep custom code and functions: `web/assets/scripts/main.js` **